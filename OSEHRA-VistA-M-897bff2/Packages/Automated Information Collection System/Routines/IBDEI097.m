IBDEI097 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,12198,0)
 ;;=365.10^^103^821^12
 ;;^UTILITY(U,$J,358.3,12198,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12198,1,3,0)
 ;;=3^365.10
 ;;^UTILITY(U,$J,358.3,12198,1,4,0)
 ;;=4^Open-Angle Glaucoma Nos
 ;;^UTILITY(U,$J,358.3,12198,2)
 ;;=^51206
 ;;^UTILITY(U,$J,358.3,12199,0)
 ;;=365.13^^103^821^14
 ;;^UTILITY(U,$J,358.3,12199,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12199,1,3,0)
 ;;=3^365.13
 ;;^UTILITY(U,$J,358.3,12199,1,4,0)
 ;;=4^Pigmentary Glaucoma
 ;;^UTILITY(U,$J,358.3,12199,2)
 ;;=^51211
 ;;^UTILITY(U,$J,358.3,12200,0)
 ;;=365.00^^103^821^15
 ;;^UTILITY(U,$J,358.3,12200,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12200,1,3,0)
 ;;=3^365.00
 ;;^UTILITY(U,$J,358.3,12200,1,4,0)
 ;;=4^Preglaucoma Nos
 ;;^UTILITY(U,$J,358.3,12200,2)
 ;;=^97584
 ;;^UTILITY(U,$J,358.3,12201,0)
 ;;=V80.1^^103^821^17
 ;;^UTILITY(U,$J,358.3,12201,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12201,1,3,0)
 ;;=3^V80.1
 ;;^UTILITY(U,$J,358.3,12201,1,4,0)
 ;;=4^Screening For Glaucoma
 ;;^UTILITY(U,$J,358.3,12201,2)
 ;;=^295684
 ;;^UTILITY(U,$J,358.3,12202,0)
 ;;=365.05^^103^821^13
 ;;^UTILITY(U,$J,358.3,12202,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12202,1,3,0)
 ;;=3^365.05
 ;;^UTILITY(U,$J,358.3,12202,1,4,0)
 ;;=4^Opn Angle w/ Brdrlne Hi Risk
 ;;^UTILITY(U,$J,358.3,12202,2)
 ;;=^340511
 ;;^UTILITY(U,$J,358.3,12203,0)
 ;;=365.06^^103^821^16
 ;;^UTILITY(U,$J,358.3,12203,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12203,1,3,0)
 ;;=3^365.06
 ;;^UTILITY(U,$J,358.3,12203,1,4,0)
 ;;=4^Prim Angle Clos w/o Damage
 ;;^UTILITY(U,$J,358.3,12203,2)
 ;;=^340512
 ;;^UTILITY(U,$J,358.3,12204,0)
 ;;=365.70^^103^821^5
 ;;^UTILITY(U,$J,358.3,12204,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12204,1,3,0)
 ;;=3^365.70
 ;;^UTILITY(U,$J,358.3,12204,1,4,0)
 ;;=4^Glaucoma Stage NOS
 ;;^UTILITY(U,$J,358.3,12204,2)
 ;;=^340609
 ;;^UTILITY(U,$J,358.3,12205,0)
 ;;=365.71^^103^821^9
 ;;^UTILITY(U,$J,358.3,12205,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12205,1,3,0)
 ;;=3^365.71
 ;;^UTILITY(U,$J,358.3,12205,1,4,0)
 ;;=4^Mild Stage Glaucoma
 ;;^UTILITY(U,$J,358.3,12205,2)
 ;;=^340513
 ;;^UTILITY(U,$J,358.3,12206,0)
 ;;=365.72^^103^821^10
 ;;^UTILITY(U,$J,358.3,12206,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12206,1,3,0)
 ;;=3^365.72
 ;;^UTILITY(U,$J,358.3,12206,1,4,0)
 ;;=4^Moderate Stage Glaucoma
 ;;^UTILITY(U,$J,358.3,12206,2)
 ;;=^340514
 ;;^UTILITY(U,$J,358.3,12207,0)
 ;;=365.73^^103^821^18
 ;;^UTILITY(U,$J,358.3,12207,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12207,1,3,0)
 ;;=3^365.73
 ;;^UTILITY(U,$J,358.3,12207,1,4,0)
 ;;=4^Severe Stage Glaucoma
 ;;^UTILITY(U,$J,358.3,12207,2)
 ;;=^340515
 ;;^UTILITY(U,$J,358.3,12208,0)
 ;;=365.74^^103^821^7
 ;;^UTILITY(U,$J,358.3,12208,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12208,1,3,0)
 ;;=3^365.74
 ;;^UTILITY(U,$J,358.3,12208,1,4,0)
 ;;=4^Indeterm Stage Glaucoma
 ;;^UTILITY(U,$J,358.3,12208,2)
 ;;=^340516
 ;;^UTILITY(U,$J,358.3,12209,0)
 ;;=V19.11^^103^821^4
 ;;^UTILITY(U,$J,358.3,12209,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12209,1,3,0)
 ;;=3^V19.11
 ;;^UTILITY(U,$J,358.3,12209,1,4,0)
 ;;=4^Family Hx of Glaucoma
 ;;^UTILITY(U,$J,358.3,12209,2)
 ;;=^340617
 ;;^UTILITY(U,$J,358.3,12210,0)
 ;;=V19.19^^103^821^3
 ;;^UTILITY(U,$J,358.3,12210,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12210,1,3,0)
 ;;=3^V19.19
 ;;^UTILITY(U,$J,358.3,12210,1,4,0)
 ;;=4^Family Hx of Eye Disorder NEC
 ;;^UTILITY(U,$J,358.3,12210,2)
 ;;=^340618
 ;;^UTILITY(U,$J,358.3,12211,0)
 ;;=362.34^^103^822^1
 ;;^UTILITY(U,$J,358.3,12211,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12211,1,3,0)
 ;;=3^362.34
 ;;^UTILITY(U,$J,358.3,12211,1,4,0)
 ;;=4^Amaurosis Fugax
 ;;^UTILITY(U,$J,358.3,12211,2)
 ;;=^268622
 ;;^UTILITY(U,$J,358.3,12212,0)
 ;;=368.13^^103^822^2
 ;;^UTILITY(U,$J,358.3,12212,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12212,1,3,0)
 ;;=3^368.13
 ;;^UTILITY(U,$J,358.3,12212,1,4,0)
 ;;=4^Asthenopia/Photophobia
 ;;^UTILITY(U,$J,358.3,12212,2)
 ;;=^126851
 ;;^UTILITY(U,$J,358.3,12213,0)
 ;;=368.30^^103^822^3
 ;;^UTILITY(U,$J,358.3,12213,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12213,1,3,0)
 ;;=3^368.30
 ;;^UTILITY(U,$J,358.3,12213,1,4,0)
 ;;=4^Binocular Vision Dis Nos
 ;;^UTILITY(U,$J,358.3,12213,2)
 ;;=^14407
 ;;^UTILITY(U,$J,358.3,12214,0)
 ;;=368.59^^103^822^4
 ;;^UTILITY(U,$J,358.3,12214,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12214,1,3,0)
 ;;=3^368.59
 ;;^UTILITY(U,$J,358.3,12214,1,4,0)
 ;;=4^Other Specif Color Deficiency
 ;;^UTILITY(U,$J,358.3,12214,2)
 ;;=^87408
 ;;^UTILITY(U,$J,358.3,12215,0)
 ;;=368.2^^103^822^5
 ;;^UTILITY(U,$J,358.3,12215,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12215,1,3,0)
 ;;=3^368.2
 ;;^UTILITY(U,$J,358.3,12215,1,4,0)
 ;;=4^Diplopia
 ;;^UTILITY(U,$J,358.3,12215,2)
 ;;=^35208
 ;;^UTILITY(U,$J,358.3,12216,0)
 ;;=784.0^^103^822^6
 ;;^UTILITY(U,$J,358.3,12216,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12216,1,3,0)
 ;;=3^784.0
 ;;^UTILITY(U,$J,358.3,12216,1,4,0)
 ;;=4^Headache
 ;;^UTILITY(U,$J,358.3,12216,2)
 ;;=^54133
 ;;^UTILITY(U,$J,358.3,12217,0)
 ;;=368.14^^103^822^7
 ;;^UTILITY(U,$J,358.3,12217,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12217,1,3,0)
 ;;=3^368.14
 ;;^UTILITY(U,$J,358.3,12217,1,4,0)
 ;;=4^Metamorphopsia
 ;;^UTILITY(U,$J,358.3,12217,2)
 ;;=^268836
 ;;^UTILITY(U,$J,358.3,12218,0)
 ;;=346.90^^103^822^8
 ;;^UTILITY(U,$J,358.3,12218,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12218,1,3,0)
 ;;=3^346.90
 ;;^UTILITY(U,$J,358.3,12218,1,4,0)
 ;;=4^Migraine Unspec W/O Intract.
 ;;^UTILITY(U,$J,358.3,12218,2)
 ;;=^293880
 ;;^UTILITY(U,$J,358.3,12219,0)
 ;;=346.91^^103^822^9
 ;;^UTILITY(U,$J,358.3,12219,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12219,1,3,0)
 ;;=3^346.91
 ;;^UTILITY(U,$J,358.3,12219,1,4,0)
 ;;=4^Migraine Unspec. Intractable
 ;;^UTILITY(U,$J,358.3,12219,2)
 ;;=^293881
 ;;^UTILITY(U,$J,358.3,12220,0)
 ;;=378.73^^103^822^10
 ;;^UTILITY(U,$J,358.3,12220,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12220,1,3,0)
 ;;=3^378.73
 ;;^UTILITY(U,$J,358.3,12220,1,4,0)
 ;;=4^Neuromuscle Dis Strabism
 ;;^UTILITY(U,$J,358.3,12220,2)
 ;;=^269274
 ;;^UTILITY(U,$J,358.3,12221,0)
 ;;=379.50^^103^822^11
 ;;^UTILITY(U,$J,358.3,12221,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12221,1,3,0)
 ;;=3^379.50
 ;;^UTILITY(U,$J,358.3,12221,1,4,0)
 ;;=4^Nystagmus Nos
 ;;^UTILITY(U,$J,358.3,12221,2)
 ;;=^84761
 ;;^UTILITY(U,$J,358.3,12222,0)
 ;;=379.91^^103^822^12
 ;;^UTILITY(U,$J,358.3,12222,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12222,1,3,0)
 ;;=3^379.91
 ;;^UTILITY(U,$J,358.3,12222,1,4,0)
 ;;=4^Pain In Or Around Eye
 ;;^UTILITY(U,$J,358.3,12222,2)
 ;;=^89093
 ;;^UTILITY(U,$J,358.3,12223,0)
 ;;=368.12^^103^822^13
 ;;^UTILITY(U,$J,358.3,12223,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12223,1,3,0)
 ;;=3^368.12
 ;;^UTILITY(U,$J,358.3,12223,1,4,0)
 ;;=4^Transient Visual Loss
 ;;^UTILITY(U,$J,358.3,12223,2)
 ;;=^268834
 ;;^UTILITY(U,$J,358.3,12224,0)
 ;;=377.9^^103^822^14
 ;;^UTILITY(U,$J,358.3,12224,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12224,1,3,0)
 ;;=3^377.9
 ;;^UTILITY(U,$J,358.3,12224,1,4,0)
 ;;=4^Optic Nerve Disorder Nos
 ;;^UTILITY(U,$J,358.3,12224,2)
 ;;=^269251
 ;;^UTILITY(U,$J,358.3,12225,0)
 ;;=368.40^^103^822^15
 ;;^UTILITY(U,$J,358.3,12225,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12225,1,3,0)
 ;;=3^368.40
 ;;^UTILITY(U,$J,358.3,12225,1,4,0)
 ;;=4^Visual Field Defect Nos
 ;;^UTILITY(U,$J,358.3,12225,2)
 ;;=^126859
 ;;^UTILITY(U,$J,358.3,12226,0)
 ;;=373.00^^103^823^1
 ;;^UTILITY(U,$J,358.3,12226,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12226,1,3,0)
 ;;=3^373.00
 ;;^UTILITY(U,$J,358.3,12226,1,4,0)
 ;;=4^Blepharitis Nos
 ;;^UTILITY(U,$J,358.3,12226,2)
 ;;=^15271
 ;;^UTILITY(U,$J,358.3,12227,0)
 ;;=374.34^^103^823^2
 ;;^UTILITY(U,$J,358.3,12227,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12227,1,3,0)
 ;;=3^374.34
 ;;^UTILITY(U,$J,358.3,12227,1,4,0)
 ;;=4^Blepharochalasis
 ;;^UTILITY(U,$J,358.3,12227,2)
 ;;=^15275
 ;;^UTILITY(U,$J,358.3,12228,0)
 ;;=373.2^^103^823^3
 ;;^UTILITY(U,$J,358.3,12228,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12228,1,3,0)
 ;;=3^373.2
 ;;^UTILITY(U,$J,358.3,12228,1,4,0)
 ;;=4^Chalazion
 ;;^UTILITY(U,$J,358.3,12228,2)
 ;;=^22156
 ;;^UTILITY(U,$J,358.3,12229,0)
 ;;=374.87^^103^823^4
 ;;^UTILITY(U,$J,358.3,12229,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12229,1,3,0)
 ;;=3^374.87
 ;;^UTILITY(U,$J,358.3,12229,1,4,0)
 ;;=4^Dermatochalasis
 ;;^UTILITY(U,$J,358.3,12229,2)
 ;;=^269123
 ;;^UTILITY(U,$J,358.3,12230,0)
 ;;=375.15^^103^823^5
 ;;^UTILITY(U,$J,358.3,12230,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12230,1,3,0)
 ;;=3^375.15
 ;;^UTILITY(U,$J,358.3,12230,1,4,0)
 ;;=4^Dry Eye Syndrome
 ;;^UTILITY(U,$J,358.3,12230,2)
 ;;=^37168
 ;;^UTILITY(U,$J,358.3,12231,0)
 ;;=374.10^^103^823^6
 ;;^UTILITY(U,$J,358.3,12231,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12231,1,3,0)
 ;;=3^374.10
 ;;^UTILITY(U,$J,358.3,12231,1,4,0)
 ;;=4^Ectropion Nos
 ;;^UTILITY(U,$J,358.3,12231,2)
 ;;=^38326
 ;;^UTILITY(U,$J,358.3,12232,0)
 ;;=374.00^^103^823^7
 ;;^UTILITY(U,$J,358.3,12232,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12232,1,3,0)
 ;;=3^374.00
 ;;^UTILITY(U,$J,358.3,12232,1,4,0)
 ;;=4^Entropion Nos
 ;;^UTILITY(U,$J,358.3,12232,2)
 ;;=^41016
 ;;^UTILITY(U,$J,358.3,12233,0)
 ;;=375.20^^103^823^8
 ;;^UTILITY(U,$J,358.3,12233,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12233,1,3,0)
 ;;=3^375.20
 ;;^UTILITY(U,$J,358.3,12233,1,4,0)
 ;;=4^Epiphora Nos
 ;;^UTILITY(U,$J,358.3,12233,2)
 ;;=^269136
 ;;^UTILITY(U,$J,358.3,12234,0)
 ;;=373.11^^103^823^9
 ;;^UTILITY(U,$J,358.3,12234,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12234,1,3,0)
 ;;=3^373.11
 ;;^UTILITY(U,$J,358.3,12234,1,4,0)
 ;;=4^Hordeolum Externum
 ;;^UTILITY(U,$J,358.3,12234,2)
 ;;=^58510
 ;;^UTILITY(U,$J,358.3,12235,0)
 ;;=374.30^^103^823^10
 ;;^UTILITY(U,$J,358.3,12235,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,12235,1,3,0)
 ;;=3^374.30
