IBDEI0IC ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.4)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.4,1516,0)
 ;;="M" MISC DIAGNOSIS^11^178
 ;;^UTILITY(U,$J,358.4,1517,0)
 ;;="N/O" MISC DISGNOSIS^12^178
 ;;^UTILITY(U,$J,358.4,1518,0)
 ;;="P" MISC DIAGNOSIS^13^178
 ;;^UTILITY(U,$J,358.4,1519,0)
 ;;="V/W/X/Y/Z" MISC DIAGNOSIS^17^178
 ;;^UTILITY(U,$J,358.4,1520,0)
 ;;="S" MISC DIAGNOSIS^15^178
 ;;^UTILITY(U,$J,358.4,1521,0)
 ;;=CANCER (NOT MELANOMA)^4^178
 ;;^UTILITY(U,$J,358.4,1522,0)
 ;;="G" MISC DIAGNOSIS^7^178
 ;;^UTILITY(U,$J,358.4,1523,0)
 ;;="R" DIAGNOSES^14^178
 ;;^UTILITY(U,$J,358.4,1524,0)
 ;;="T/U" MISC DIAGNOSIS^16^178
 ;;^UTILITY(U,$J,358.4,1525,0)
 ;;=CARDIOVASCULAR^2^179
 ;;^UTILITY(U,$J,358.4,1526,0)
 ;;=RESPIRATORY^7^179
 ;;^UTILITY(U,$J,358.4,1527,0)
 ;;=SIGNS, SYMPTOMS, CONDITIONS^8^179
 ;;^UTILITY(U,$J,358.4,1528,0)
 ;;=COMMON DIAGNOSES^1^179
 ;;^UTILITY(U,$J,358.4,1529,0)
 ;;=POST-OP COMPLICATIONS^6^179
 ;;^UTILITY(U,$J,358.4,1530,0)
 ;;=AFTERCARE POST SURGERY^4^179
 ;;^UTILITY(U,$J,358.4,1531,0)
 ;;=METASTATIC CANCER SITES^5^179
 ;;^UTILITY(U,$J,358.4,1532,0)
 ;;=ABDOMINAL PAIN^3^179
 ;;^UTILITY(U,$J,358.4,1533,0)
 ;;=CHEST WALL AND NECK^1^180
 ;;^UTILITY(U,$J,358.4,1534,0)
 ;;=THORACIC^2^180
 ;;^UTILITY(U,$J,358.4,1535,0)
 ;;=OTHER PROCEDURES^3^180
 ;;^UTILITY(U,$J,358.4,1536,0)
 ;;=NEW PATIENT^2^181
 ;;^UTILITY(U,$J,358.4,1537,0)
 ;;=ESTABLISHED PATIENT^1^181
 ;;^UTILITY(U,$J,358.4,1538,0)
 ;;=CONSULTATIONS^3^181
 ;;^UTILITY(U,$J,358.4,1539,0)
 ;;=POST OP VISIT (GLOBAL)^4^181
 ;;^UTILITY(U,$J,358.4,1540,0)
 ;;=BONE MARROW^1^182
 ;;^UTILITY(U,$J,358.4,1541,0)
 ;;=HEART^2^182
 ;;^UTILITY(U,$J,358.4,1542,0)
 ;;=HEART-LUNG^3^182
 ;;^UTILITY(U,$J,358.4,1543,0)
 ;;=LIVER^4^182
 ;;^UTILITY(U,$J,358.4,1544,0)
 ;;=LUNG^5^182
 ;;^UTILITY(U,$J,358.4,1545,0)
 ;;=PANCREAS^6^182
 ;;^UTILITY(U,$J,358.4,1546,0)
 ;;=RENAL^7^182
 ;;^UTILITY(U,$J,358.4,1547,0)
 ;;=REPAIR/RESECTION OF DONOR HEART^8^182
 ;;^UTILITY(U,$J,358.4,1548,0)
 ;;=CARDIOMYOPATHY^2^183
 ;;^UTILITY(U,$J,358.4,1549,0)
 ;;=LUNG^10^183
 ;;^UTILITY(U,$J,358.4,1550,0)
 ;;=CA HEART & LUNG^1^183
 ;;^UTILITY(U,$J,358.4,1551,0)
 ;;=CHRONIC PULMONARY HEART DISEASE^3^183
 ;;^UTILITY(U,$J,358.4,1552,0)
 ;;=DIS OF BLOOD/FORMING ORGANS^4^183
 ;;^UTILITY(U,$J,358.4,1553,0)
 ;;=DONORS^5^183
 ;;^UTILITY(U,$J,358.4,1554,0)
 ;;=HEART^6^183
 ;;^UTILITY(U,$J,358.4,1555,0)
 ;;=HIV^7^183
 ;;^UTILITY(U,$J,358.4,1556,0)
 ;;=KIDNEY/PANCREAS^8^183
 ;;^UTILITY(U,$J,358.4,1557,0)
 ;;=LIVER^9^183
 ;;^UTILITY(U,$J,358.4,1558,0)
 ;;=MALIGNANCY^11^183
 ;;^UTILITY(U,$J,358.4,1559,0)
 ;;=MALIGNANT LYMPH & HEMATOPOIETIC^12^183
 ;;^UTILITY(U,$J,358.4,1560,0)
 ;;=OTHER MEDTABOLIC/IMMUNITY DIS^13^183
 ;;^UTILITY(U,$J,358.4,1561,0)
 ;;=OTHER METABOLIC DISORDERS^14^183
 ;;^UTILITY(U,$J,358.4,1562,0)
 ;;=MALE URETHRAL DILATION^3^184
 ;;^UTILITY(U,$J,358.4,1563,0)
 ;;=MISCELLANEOUS^4^184
 ;;^UTILITY(U,$J,358.4,1564,0)
 ;;=URODYNAMICS^5^184
 ;;^UTILITY(U,$J,358.4,1565,0)
 ;;=ZOLADEX^6^184
 ;;^UTILITY(U,$J,358.4,1566,0)
 ;;=CYSTOURETHROSCOPY^1^184
 ;;^UTILITY(U,$J,358.4,1567,0)
 ;;=FEMALE ONLY PROCEDURES^2^184
 ;;^UTILITY(U,$J,358.4,1568,0)
 ;;=UNLISTED UROLOGY PROCEDURES^7^184
 ;;^UTILITY(U,$J,358.4,1569,0)
 ;;=NEW PATIENT^2^185
 ;;^UTILITY(U,$J,358.4,1570,0)
 ;;=ESTABLISHED PATIENT^1^185
 ;;^UTILITY(U,$J,358.4,1571,0)
 ;;=CONSULTATIONS^3^185
 ;;^UTILITY(U,$J,358.4,1572,0)
 ;;=POST OP VISIT (GLOBAL)^4^185
 ;;^UTILITY(U,$J,358.4,1573,0)
 ;;=OTHER E&M SERVICES^5^185
 ;;^UTILITY(U,$J,358.4,1574,0)
 ;;=NEURO-UROLOGY^15^186
 ;;^UTILITY(U,$J,358.4,1575,0)
 ;;=BLADDER^1^186
 ;;^UTILITY(U,$J,358.4,1576,0)
 ;;=KIDNEY/URETER^8^186
 ;;^UTILITY(U,$J,358.4,1577,0)
 ;;=PENIS/EXTERNAL GENITALIA^16^186
 ;;^UTILITY(U,$J,358.4,1578,0)
 ;;=PROSTATE^18^186
 ;;^UTILITY(U,$J,358.4,1579,0)
 ;;=POST OP^17^186
 ;;^UTILITY(U,$J,358.4,1580,0)
 ;;=MISCELLANEOUS/SYMPTOMS^10^186
 ;;^UTILITY(U,$J,358.4,1581,0)
 ;;=MISC/CONDITIONS/STATES^9^186
 ;;^UTILITY(U,$J,358.4,1582,0)
 ;;=INFECTIONS-VENEREAL^7^186
 ;;^UTILITY(U,$J,358.4,1583,0)
 ;;=INFECTIONS-RENAL/URETER^6^186
 ;;^UTILITY(U,$J,358.4,1584,0)
 ;;=INFECTIONS-PROSTATE^5^186
 ;;^UTILITY(U,$J,358.4,1585,0)
 ;;=INFECTIONS-KIDNEY^4^186
 ;;^UTILITY(U,$J,358.4,1586,0)
 ;;=INFECTION-MISCELLANEOUS^3^186
 ;;^UTILITY(U,$J,358.4,1587,0)
 ;;=NEOPLASIA-BLADDER/URETHRA^11^186
 ;;^UTILITY(U,$J,358.4,1588,0)
 ;;=NEOPLASIA-PROSTATE^14^186
 ;;^UTILITY(U,$J,358.4,1589,0)
 ;;=NEOPLASIA-KIDNEY/URETER^13^186
 ;;^UTILITY(U,$J,358.4,1590,0)
 ;;=NEOPLASIA-EXTERNAL GENITALIA^12^186
 ;;^UTILITY(U,$J,358.4,1591,0)
 ;;=COMPLICATIONS^2^186
 ;;^UTILITY(U,$J,358.4,1592,0)
 ;;=NEW PATIENT^2^187
 ;;^UTILITY(U,$J,358.4,1593,0)
 ;;=ESTABLISHED PATIENT^1^187
 ;;^UTILITY(U,$J,358.4,1594,0)
 ;;=CONSULTATIONS^3^187
 ;;^UTILITY(U,$J,358.4,1595,0)
 ;;=RESPIRATORY^8^188
 ;;^UTILITY(U,$J,358.4,1596,0)
 ;;=CARDIOVASCULAR^4^188
 ;;^UTILITY(U,$J,358.4,1597,0)
 ;;=ABDOMINAL PAIN^2^188
 ;;^UTILITY(U,$J,358.4,1598,0)
 ;;=AFTERCARE POST SURGERY^3^188
 ;;^UTILITY(U,$J,358.4,1599,0)
 ;;=COMMON VASCULAR DX^1^188
 ;;^UTILITY(U,$J,358.4,1600,0)
 ;;=POST-OP COMPLICATIONS^7^188
 ;;^UTILITY(U,$J,358.4,1601,0)
 ;;=METASTATIC CANCER SITES^6^188
 ;;^UTILITY(U,$J,358.4,1602,0)
 ;;=GENERAL/SIGNS & SYMPTOMS^5^188
 ;;^UTILITY(U,$J,358.4,1603,0)
 ;;=DEBRIDEMENT^2^189
 ;;^UTILITY(U,$J,358.4,1604,0)
 ;;=I&D/ASPIRATION^3^189
 ;;^UTILITY(U,$J,358.4,1605,0)
 ;;=UNNA BOOT^4^189
 ;;^UTILITY(U,$J,358.4,1606,0)
 ;;=PEG TUBE^5^189
 ;;^UTILITY(U,$J,358.4,1607,0)
 ;;=WOUND VAC DRESSING^6^189
 ;;^UTILITY(U,$J,358.4,1608,0)
 ;;=VASCULAR PROCEDURES^1^189
 ;;^UTILITY(U,$J,358.4,1609,0)
 ;;=GYN PROCEDURES^5^190
 ;;^UTILITY(U,$J,358.4,1610,0)
 ;;=INJECTIONS^1^190
 ;;^UTILITY(U,$J,358.4,1611,0)
 ;;=ADMINISTRATION OF INJECTION^2^190
 ;;^UTILITY(U,$J,358.4,1612,0)
 ;;=EAR WAX REMOVAL^6^190
 ;;^UTILITY(U,$J,358.4,1613,0)
 ;;=CARDIAC^7^190
 ;;^UTILITY(U,$J,358.4,1614,0)
 ;;=FOREIGN BODY^8^190
 ;;^UTILITY(U,$J,358.4,1615,0)
 ;;=UA/FINGERSTICK^9^190
 ;;^UTILITY(U,$J,358.4,1616,0)
 ;;=FLUSHES^10^190
 ;;^UTILITY(U,$J,358.4,1617,0)
 ;;=TB TEST^11^190
 ;;^UTILITY(U,$J,358.4,1618,0)
 ;;=DRAIN/INJECT JOINT^12^190
 ;;^UTILITY(U,$J,358.4,1619,0)
 ;;=DESTRUCTION OF LESIONS^13^190
 ;;^UTILITY(U,$J,358.4,1620,0)
 ;;=IMMUNIZATIONS^4^190
 ;;^UTILITY(U,$J,358.4,1621,0)
 ;;=NEW PATIENT^2^191
 ;;^UTILITY(U,$J,358.4,1622,0)
 ;;=ESTABLISHED PATIENT^1^191
 ;;^UTILITY(U,$J,358.4,1623,0)
 ;;=CONSULTATIONS^3^191
 ;;^UTILITY(U,$J,358.4,1624,0)
 ;;=POST-OP F/U VISIT^5^191
 ;;^UTILITY(U,$J,358.4,1625,0)
 ;;=HEALTHY/PREVENTIVE VISIT^4^191
 ;;^UTILITY(U,$J,358.4,1626,0)
 ;;=CARDIO/VASCULAR^2^192
 ;;^UTILITY(U,$J,358.4,1627,0)
 ;;=ENDOCRINE, METOBOLIC, NUTRITIONAL^3^192
 ;;^UTILITY(U,$J,358.4,1628,0)
 ;;=EYES, EARS, & NOSE^4^192
 ;;^UTILITY(U,$J,358.4,1629,0)
 ;;=GASTROINTESTINAL^5^192
 ;;^UTILITY(U,$J,358.4,1630,0)
 ;;=GENITOURINARY & RENAL^6^192
 ;;^UTILITY(U,$J,358.4,1631,0)
 ;;=MUSCULOSKETAL^7^192
 ;;^UTILITY(U,$J,358.4,1632,0)
 ;;=NEUROLOGY^8^192
 ;;^UTILITY(U,$J,358.4,1633,0)
 ;;=PULMONARY/RESPIRATORY^9^192
 ;;^UTILITY(U,$J,358.4,1634,0)
 ;;=PSYCHIATRIC^10^192
 ;;^UTILITY(U,$J,358.4,1635,0)
 ;;=SKIN^11^192
 ;;^UTILITY(U,$J,358.4,1636,0)
 ;;=GYNECOLOGICAL/BREAST^1^192
 ;;^UTILITY(U,$J,358.4,1637,0)
 ;;=IMMUNIZATIONS^12^192
 ;;^UTILITY(U,$J,358.4,1638,0)
 ;;=CONTACT W/ HAZARDOUS SUBSTANCES^.5^192
 ;;^UTILITY(U,$J,358.4,1639,0)
 ;;=ANEMIA^1^193
 ;;^UTILITY(U,$J,358.4,1640,0)
 ;;=COAGULATION DISORDERS^4^193
 ;;^UTILITY(U,$J,358.4,1641,0)
 ;;=MYELOID NEOPLASMS & DISORDERS^12^193
 ;;^UTILITY(U,$J,358.4,1642,0)
 ;;=LYMPHOID NEOPLASMS^9^193
 ;;^UTILITY(U,$J,358.4,1643,0)
 ;;=GI CANCER^7^193
 ;;^UTILITY(U,$J,358.4,1644,0)
 ;;=HEAD, NECK & LUNG NEOPLASMS^8^193
 ;;^UTILITY(U,$J,358.4,1645,0)
 ;;=MISC. NEOPLASMS^11^193
 ;;^UTILITY(U,$J,358.4,1646,0)
 ;;=METASTATIC SITES^10^193
 ;;^UTILITY(U,$J,358.4,1647,0)
 ;;=COUNSELING & SCREENING^5^193
 ;;^UTILITY(U,$J,358.4,1648,0)
 ;;=GENITOURINARY NEOPLASMS^6^193
 ;;^UTILITY(U,$J,358.4,1649,0)
 ;;=BREAST & GYN NEOPLASMS^3^193
 ;;^UTILITY(U,$J,358.4,1650,0)
 ;;=ARTIFICIAL OPENING STATUS^2^193
 ;;^UTILITY(U,$J,358.4,1651,0)
 ;;=PERSONAL HX OF CANCER^15^193
 ;;^UTILITY(U,$J,358.4,1652,0)
 ;;=NEOPLASMS OF UNCERTAIN BEHAVIOR^13^193
 ;;^UTILITY(U,$J,358.4,1653,0)
 ;;=NEOPLASMS OF UNSPECIFIED NATURE^14^193
 ;;^UTILITY(U,$J,358.4,1654,0)
 ;;=SPECIAL SERVICES^5^194
 ;;^UTILITY(U,$J,358.4,1655,0)
 ;;=TREATMENT^4^194
 ;;^UTILITY(U,$J,358.4,1656,0)
 ;;=MISCELLANEOUS^7^194
 ;;^UTILITY(U,$J,358.4,1657,0)
 ;;=IMMUNIZATIONS^6^194
 ;;^UTILITY(U,$J,358.4,1658,0)
 ;;=CHEMO ADMINISTRATION^1^194
 ;;^UTILITY(U,$J,358.4,1659,0)
 ;;=CHEMO DRUGS^2^194
 ;;^UTILITY(U,$J,358.4,1660,0)
 ;;=OTHER DRUGS^3^194
 ;;^UTILITY(U,$J,358.4,1661,0)
 ;;=NEW PATIENT^2^195
 ;;^UTILITY(U,$J,358.4,1662,0)
 ;;=ESTABLISHED PATIENT^1^195
 ;;^UTILITY(U,$J,358.4,1663,0)
 ;;=CONSULTATIONS^3^195
