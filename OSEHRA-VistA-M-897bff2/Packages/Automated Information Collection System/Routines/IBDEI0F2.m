IBDEI0F2 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,20201,1,4,0)
 ;;=4^717.42
 ;;^UTILITY(U,$J,358.3,20202,0)
 ;;=717.41^^151^1318^37
 ;;^UTILITY(U,$J,358.3,20202,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20202,1,3,0)
 ;;=3^Tear,Lat Men Buck Hand O
 ;;^UTILITY(U,$J,358.3,20202,1,4,0)
 ;;=4^717.41
 ;;^UTILITY(U,$J,358.3,20203,0)
 ;;=717.43^^151^1318^41
 ;;^UTILITY(U,$J,358.3,20203,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20203,1,3,0)
 ;;=3^Tear,Lat Meniscus Post Hor
 ;;^UTILITY(U,$J,358.3,20203,1,4,0)
 ;;=4^717.43
 ;;^UTILITY(U,$J,358.3,20204,0)
 ;;=717.49^^151^1318^40
 ;;^UTILITY(U,$J,358.3,20204,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20204,1,3,0)
 ;;=3^Tear,Lat Meniscus Oth Old
 ;;^UTILITY(U,$J,358.3,20204,1,4,0)
 ;;=4^717.49
 ;;^UTILITY(U,$J,358.3,20205,0)
 ;;=836.0^^151^1318^43
 ;;^UTILITY(U,$J,358.3,20205,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20205,1,3,0)
 ;;=3^Tear,Med Menisc Current
 ;;^UTILITY(U,$J,358.3,20205,1,4,0)
 ;;=4^836.0
 ;;^UTILITY(U,$J,358.3,20206,0)
 ;;=717.1^^151^1318^44
 ;;^UTILITY(U,$J,358.3,20206,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20206,1,3,0)
 ;;=3^Tear,Med Meniscus Ant Horn
 ;;^UTILITY(U,$J,358.3,20206,1,4,0)
 ;;=4^717.1
 ;;^UTILITY(U,$J,358.3,20207,0)
 ;;=717.0^^151^1318^42
 ;;^UTILITY(U,$J,358.3,20207,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20207,1,3,0)
 ;;=3^Tear,Med Men Buck Hand O
 ;;^UTILITY(U,$J,358.3,20207,1,4,0)
 ;;=4^717.0
 ;;^UTILITY(U,$J,358.3,20208,0)
 ;;=717.2^^151^1318^46
 ;;^UTILITY(U,$J,358.3,20208,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20208,1,3,0)
 ;;=3^Tear,Med Meniscus Pst Horn
 ;;^UTILITY(U,$J,358.3,20208,1,4,0)
 ;;=4^717.2
 ;;^UTILITY(U,$J,358.3,20209,0)
 ;;=717.3^^151^1318^45
 ;;^UTILITY(U,$J,358.3,20209,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20209,1,3,0)
 ;;=3^Tear,Med Meniscus Oth Old
 ;;^UTILITY(U,$J,358.3,20209,1,4,0)
 ;;=4^717.3
 ;;^UTILITY(U,$J,358.3,20210,0)
 ;;=716.16^^151^1318^47
 ;;^UTILITY(U,$J,358.3,20210,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20210,1,3,0)
 ;;=3^Traumatic Arthritis
 ;;^UTILITY(U,$J,358.3,20210,1,4,0)
 ;;=4^716.16
 ;;^UTILITY(U,$J,358.3,20211,0)
 ;;=726.79^^151^1318^18
 ;;^UTILITY(U,$J,358.3,20211,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20211,1,3,0)
 ;;=3^Illio-Tibial Band
 ;;^UTILITY(U,$J,358.3,20211,1,4,0)
 ;;=4^726.79
 ;;^UTILITY(U,$J,358.3,20211,2)
 ;;=^272555
 ;;^UTILITY(U,$J,358.3,20212,0)
 ;;=844.8^^151^1318^32.1
 ;;^UTILITY(U,$J,358.3,20212,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20212,1,3,0)
 ;;=3^Quadriceps Tendon Rupture
 ;;^UTILITY(U,$J,358.3,20212,1,4,0)
 ;;=4^844.8
 ;;^UTILITY(U,$J,358.3,20212,2)
 ;;=^274503
 ;;^UTILITY(U,$J,358.3,20213,0)
 ;;=719.46^^151^1318^27
 ;;^UTILITY(U,$J,358.3,20213,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20213,1,3,0)
 ;;=3^Pain In Knee
 ;;^UTILITY(U,$J,358.3,20213,1,4,0)
 ;;=4^719.46
 ;;^UTILITY(U,$J,358.3,20213,2)
 ;;=^272403
 ;;^UTILITY(U,$J,358.3,20214,0)
 ;;=726.64^^151^1318^30
 ;;^UTILITY(U,$J,358.3,20214,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20214,1,3,0)
 ;;=3^Patellar Tendonitis
 ;;^UTILITY(U,$J,358.3,20214,1,4,0)
 ;;=4^726.64
 ;;^UTILITY(U,$J,358.3,20214,2)
 ;;=^272545
 ;;^UTILITY(U,$J,358.3,20215,0)
 ;;=727.83^^151^1318^31
 ;;^UTILITY(U,$J,358.3,20215,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20215,1,3,0)
 ;;=3^Plica Syndrome
 ;;^UTILITY(U,$J,358.3,20215,1,4,0)
 ;;=4^727.83
 ;;^UTILITY(U,$J,358.3,20215,2)
 ;;=^322154
 ;;^UTILITY(U,$J,358.3,20216,0)
 ;;=736.81^^151^1319^17
 ;;^UTILITY(U,$J,358.3,20216,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20216,1,3,0)
 ;;=3^Unequal Leg Length
 ;;^UTILITY(U,$J,358.3,20216,1,4,0)
 ;;=4^736.81
 ;;^UTILITY(U,$J,358.3,20216,2)
 ;;=^68758
 ;;^UTILITY(U,$J,358.3,20217,0)
 ;;=958.8^^151^1319^1
 ;;^UTILITY(U,$J,358.3,20217,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20217,1,3,0)
 ;;=3^Anterior Compartment Syn.
 ;;^UTILITY(U,$J,358.3,20217,1,4,0)
 ;;=4^958.8
 ;;^UTILITY(U,$J,358.3,20218,0)
 ;;=754.43^^151^1319^2
 ;;^UTILITY(U,$J,358.3,20218,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20218,1,3,0)
 ;;=3^Cong Bowing Tibia/Fibula
 ;;^UTILITY(U,$J,358.3,20218,1,4,0)
 ;;=4^754.43
 ;;^UTILITY(U,$J,358.3,20219,0)
 ;;=924.10^^151^1319^3
 ;;^UTILITY(U,$J,358.3,20219,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20219,1,3,0)
 ;;=3^Contusion 
 ;;^UTILITY(U,$J,358.3,20219,1,4,0)
 ;;=4^924.10
 ;;^UTILITY(U,$J,358.3,20220,0)
 ;;=823.01^^151^1319^4
 ;;^UTILITY(U,$J,358.3,20220,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20220,1,3,0)
 ;;=3^Fracture, Fibula Proximal
 ;;^UTILITY(U,$J,358.3,20220,1,4,0)
 ;;=4^823.01
 ;;^UTILITY(U,$J,358.3,20221,0)
 ;;=823.21^^151^1319^5
 ;;^UTILITY(U,$J,358.3,20221,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20221,1,3,0)
 ;;=3^Fracture, Fubula Shaft
 ;;^UTILITY(U,$J,358.3,20221,1,4,0)
 ;;=4^823.21
 ;;^UTILITY(U,$J,358.3,20222,0)
 ;;=823.02^^151^1319^9
 ;;^UTILITY(U,$J,358.3,20222,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20222,1,3,0)
 ;;=3^Fracture,Tibia/Fibula Prox
 ;;^UTILITY(U,$J,358.3,20222,1,4,0)
 ;;=4^823.02
 ;;^UTILITY(U,$J,358.3,20223,0)
 ;;=823.22^^151^1319^10
 ;;^UTILITY(U,$J,358.3,20223,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20223,1,3,0)
 ;;=3^Fracture,Tibia/Fibula,Shaf
 ;;^UTILITY(U,$J,358.3,20223,1,4,0)
 ;;=4^823.22
 ;;^UTILITY(U,$J,358.3,20224,0)
 ;;=823.00^^151^1319^7
 ;;^UTILITY(U,$J,358.3,20224,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20224,1,3,0)
 ;;=3^Fracture, Tibia Proximal
 ;;^UTILITY(U,$J,358.3,20224,1,4,0)
 ;;=4^823.00
 ;;^UTILITY(U,$J,358.3,20225,0)
 ;;=823.20^^151^1319^8
 ;;^UTILITY(U,$J,358.3,20225,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20225,1,3,0)
 ;;=3^Fracture, Tibia Shaft
 ;;^UTILITY(U,$J,358.3,20225,1,4,0)
 ;;=4^823.20
 ;;^UTILITY(U,$J,358.3,20226,0)
 ;;=733.16^^151^1319^6
 ;;^UTILITY(U,$J,358.3,20226,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20226,1,3,0)
 ;;=3^Fracture, Pathological
 ;;^UTILITY(U,$J,358.3,20226,1,4,0)
 ;;=4^733.16
 ;;^UTILITY(U,$J,358.3,20227,0)
 ;;=891.0^^151^1319^11
 ;;^UTILITY(U,$J,358.3,20227,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20227,1,3,0)
 ;;=3^Open Wound
 ;;^UTILITY(U,$J,358.3,20227,1,4,0)
 ;;=4^891.0
 ;;^UTILITY(U,$J,358.3,20228,0)
 ;;=891.1^^151^1319^13
 ;;^UTILITY(U,$J,358.3,20228,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20228,1,3,0)
 ;;=3^Open Wound, Complication
 ;;^UTILITY(U,$J,358.3,20228,1,4,0)
 ;;=4^891.1
 ;;^UTILITY(U,$J,358.3,20229,0)
 ;;=891.2^^151^1319^12
 ;;^UTILITY(U,$J,358.3,20229,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20229,1,3,0)
 ;;=3^Open Wound W/ Tendon
 ;;^UTILITY(U,$J,358.3,20229,1,4,0)
 ;;=4^891.2
 ;;^UTILITY(U,$J,358.3,20230,0)
 ;;=729.5^^151^1319^14
 ;;^UTILITY(U,$J,358.3,20230,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20230,1,3,0)
 ;;=3^Pain In Leg
 ;;^UTILITY(U,$J,358.3,20230,1,4,0)
 ;;=4^729.5
 ;;^UTILITY(U,$J,358.3,20230,2)
 ;;=^89086
 ;;^UTILITY(U,$J,358.3,20231,0)
 ;;=707.12^^151^1319^16
 ;;^UTILITY(U,$J,358.3,20231,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20231,1,3,0)
 ;;=3^Ulcer,Calf
 ;;^UTILITY(U,$J,358.3,20231,1,4,0)
 ;;=4^707.12
 ;;^UTILITY(U,$J,358.3,20231,2)
 ;;=^322144
 ;;^UTILITY(U,$J,358.3,20232,0)
 ;;=958.92^^151^1319^15
 ;;^UTILITY(U,$J,358.3,20232,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20232,1,3,0)
 ;;=3^Traum Compartment Syn. Lwr Ext
 ;;^UTILITY(U,$J,358.3,20232,1,4,0)
 ;;=4^958.92
 ;;^UTILITY(U,$J,358.3,20232,2)
 ;;=^334174
 ;;^UTILITY(U,$J,358.3,20233,0)
 ;;=726.71^^151^1320^18
 ;;^UTILITY(U,$J,358.3,20233,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20233,1,3,0)
 ;;=3^Tendonitis, Achilles
 ;;^UTILITY(U,$J,358.3,20233,1,4,0)
 ;;=4^726.71
 ;;^UTILITY(U,$J,358.3,20233,2)
 ;;=^272550
 ;;^UTILITY(U,$J,358.3,20234,0)
 ;;=824.2^^151^1320^2
 ;;^UTILITY(U,$J,358.3,20234,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20234,1,3,0)
 ;;=3^Fracture, Lateral Malleolus
 ;;^UTILITY(U,$J,358.3,20234,1,4,0)
 ;;=4^824.2
 ;;^UTILITY(U,$J,358.3,20234,2)
 ;;=^274247
 ;;^UTILITY(U,$J,358.3,20235,0)
 ;;=824.0^^151^1320^3
 ;;^UTILITY(U,$J,358.3,20235,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20235,1,3,0)
 ;;=3^Fracture, Medial Malleolus
 ;;^UTILITY(U,$J,358.3,20235,1,4,0)
 ;;=4^824.0
 ;;^UTILITY(U,$J,358.3,20235,2)
 ;;=^274245
 ;;^UTILITY(U,$J,358.3,20236,0)
 ;;=824.6^^151^1320^4
 ;;^UTILITY(U,$J,358.3,20236,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20236,1,3,0)
 ;;=3^Fracture, Trimalleolar
 ;;^UTILITY(U,$J,358.3,20236,1,4,0)
 ;;=4^824.6
 ;;^UTILITY(U,$J,358.3,20236,2)
 ;;=^274251
 ;;^UTILITY(U,$J,358.3,20237,0)
 ;;=824.8^^151^1320^5.1
 ;;^UTILITY(U,$J,358.3,20237,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20237,1,3,0)
 ;;=3^Fx, Tibial Pilon (Tibia)
 ;;^UTILITY(U,$J,358.3,20237,1,4,0)
 ;;=4^824.8
 ;;^UTILITY(U,$J,358.3,20237,2)
 ;;=^274256
 ;;^UTILITY(U,$J,358.3,20238,0)
 ;;=726.79^^151^1320^19
 ;;^UTILITY(U,$J,358.3,20238,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20238,1,3,0)
 ;;=3^Tendonitis, Peroneal/Other
 ;;^UTILITY(U,$J,358.3,20238,1,4,0)
 ;;=4^726.79
 ;;^UTILITY(U,$J,358.3,20238,2)
 ;;=^272555
 ;;^UTILITY(U,$J,358.3,20239,0)
 ;;=718.87^^151^1320^6
 ;;^UTILITY(U,$J,358.3,20239,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20239,1,3,0)
 ;;=3^Instability
 ;;^UTILITY(U,$J,358.3,20239,1,4,0)
 ;;=4^718.87
 ;;^UTILITY(U,$J,358.3,20239,2)
 ;;=^272346
 ;;^UTILITY(U,$J,358.3,20240,0)
 ;;=718.17^^151^1320^9
 ;;^UTILITY(U,$J,358.3,20240,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20240,1,3,0)
 ;;=3^Loose Body
 ;;^UTILITY(U,$J,358.3,20240,1,4,0)
 ;;=4^718.17
 ;;^UTILITY(U,$J,358.3,20240,2)
 ;;=^272294
 ;;^UTILITY(U,$J,358.3,20241,0)
 ;;=891.0^^151^1320^10
 ;;^UTILITY(U,$J,358.3,20241,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20241,1,3,0)
 ;;=3^Open Open Wound
 ;;^UTILITY(U,$J,358.3,20241,1,4,0)
 ;;=4^891.0
 ;;^UTILITY(U,$J,358.3,20241,2)
 ;;=^275087
 ;;^UTILITY(U,$J,358.3,20242,0)
 ;;=891.1^^151^1320^12
 ;;^UTILITY(U,$J,358.3,20242,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20242,1,3,0)
 ;;=3^Open Wound, Complicated
