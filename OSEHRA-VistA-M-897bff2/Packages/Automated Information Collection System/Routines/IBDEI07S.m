IBDEI07S ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,10228,1,4,0)
 ;;=4^366.53
 ;;^UTILITY(U,$J,358.3,10228,2)
 ;;=After Cataract Obscurring Vision^268823
 ;;^UTILITY(U,$J,358.3,10229,0)
 ;;=366.9^^79^682^3
 ;;^UTILITY(U,$J,358.3,10229,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10229,1,3,0)
 ;;=3^Cataract, Unspecified
 ;;^UTILITY(U,$J,358.3,10229,1,4,0)
 ;;=4^366.9
 ;;^UTILITY(U,$J,358.3,10229,2)
 ;;=Cataract, Unspecified^20266
 ;;^UTILITY(U,$J,358.3,10230,0)
 ;;=362.83^^79^682^11
 ;;^UTILITY(U,$J,358.3,10230,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10230,1,3,0)
 ;;=3^Macular Edema (CSME)
 ;;^UTILITY(U,$J,358.3,10230,1,4,0)
 ;;=4^362.83
 ;;^UTILITY(U,$J,358.3,10230,2)
 ;;=Macular Edema^89576
 ;;^UTILITY(U,$J,358.3,10231,0)
 ;;=362.52^^79^682^4
 ;;^UTILITY(U,$J,358.3,10231,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10231,1,3,0)
 ;;=3^Cystoid Macular Degeneration
 ;;^UTILITY(U,$J,358.3,10231,1,4,0)
 ;;=4^362.52
 ;;^UTILITY(U,$J,358.3,10231,2)
 ;;=^268637
 ;;^UTILITY(U,$J,358.3,10232,0)
 ;;=340.^^79^683^4
 ;;^UTILITY(U,$J,358.3,10232,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10232,1,3,0)
 ;;=3^Multiple Sclerosis
 ;;^UTILITY(U,$J,358.3,10232,1,4,0)
 ;;=4^340.
 ;;^UTILITY(U,$J,358.3,10232,2)
 ;;=^79761
 ;;^UTILITY(U,$J,358.3,10233,0)
 ;;=907.2^^79^683^3
 ;;^UTILITY(U,$J,358.3,10233,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10233,1,3,0)
 ;;=3^Late Eff Spinal Cord Inj
 ;;^UTILITY(U,$J,358.3,10233,1,4,0)
 ;;=4^907.2
 ;;^UTILITY(U,$J,358.3,10233,2)
 ;;=^275240
 ;;^UTILITY(U,$J,358.3,10234,0)
 ;;=V60.0^^79^683^2
 ;;^UTILITY(U,$J,358.3,10234,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10234,1,3,0)
 ;;=3^Lack of Housing
 ;;^UTILITY(U,$J,358.3,10234,1,4,0)
 ;;=4^V60.0
 ;;^UTILITY(U,$J,358.3,10234,2)
 ;;=^295539
 ;;^UTILITY(U,$J,358.3,10235,0)
 ;;=V87.39^^79^683^1
 ;;^UTILITY(U,$J,358.3,10235,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10235,1,3,0)
 ;;=3^Cont/Exp Hazard Sub NEC
 ;;^UTILITY(U,$J,358.3,10235,1,4,0)
 ;;=4^V87.39
 ;;^UTILITY(U,$J,358.3,10235,2)
 ;;=^336815
 ;;^UTILITY(U,$J,358.3,10236,0)
 ;;=99201^^80^684^1
 ;;^UTILITY(U,$J,358.3,10236,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10236,1,1,0)
 ;;=1^Problem Focus
 ;;^UTILITY(U,$J,358.3,10236,1,2,0)
 ;;=2^99201
 ;;^UTILITY(U,$J,358.3,10237,0)
 ;;=99202^^80^684^2
 ;;^UTILITY(U,$J,358.3,10237,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10237,1,1,0)
 ;;=1^Expanded Problem Focus
 ;;^UTILITY(U,$J,358.3,10237,1,2,0)
 ;;=2^99202
 ;;^UTILITY(U,$J,358.3,10238,0)
 ;;=99203^^80^684^3
 ;;^UTILITY(U,$J,358.3,10238,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10238,1,1,0)
 ;;=1^Detailed
 ;;^UTILITY(U,$J,358.3,10238,1,2,0)
 ;;=2^99203
 ;;^UTILITY(U,$J,358.3,10239,0)
 ;;=99204^^80^684^4
 ;;^UTILITY(U,$J,358.3,10239,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10239,1,1,0)
 ;;=1^Comprehensive, Moderate
 ;;^UTILITY(U,$J,358.3,10239,1,2,0)
 ;;=2^99204
 ;;^UTILITY(U,$J,358.3,10240,0)
 ;;=99205^^80^684^5
 ;;^UTILITY(U,$J,358.3,10240,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10240,1,1,0)
 ;;=1^Comprehensive, High
 ;;^UTILITY(U,$J,358.3,10240,1,2,0)
 ;;=2^99205
 ;;^UTILITY(U,$J,358.3,10241,0)
 ;;=99211^^80^685^1
 ;;^UTILITY(U,$J,358.3,10241,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10241,1,1,0)
 ;;=1^Brief (no MD seen)
 ;;^UTILITY(U,$J,358.3,10241,1,2,0)
 ;;=2^99211
 ;;^UTILITY(U,$J,358.3,10242,0)
 ;;=99212^^80^685^2
 ;;^UTILITY(U,$J,358.3,10242,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10242,1,1,0)
 ;;=1^Problem Focused
 ;;^UTILITY(U,$J,358.3,10242,1,2,0)
 ;;=2^99212
 ;;^UTILITY(U,$J,358.3,10243,0)
 ;;=99213^^80^685^3
 ;;^UTILITY(U,$J,358.3,10243,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10243,1,1,0)
 ;;=1^Expanded Problem Focus
 ;;^UTILITY(U,$J,358.3,10243,1,2,0)
 ;;=2^99213
 ;;^UTILITY(U,$J,358.3,10244,0)
 ;;=99214^^80^685^4
 ;;^UTILITY(U,$J,358.3,10244,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10244,1,1,0)
 ;;=1^Detailed
 ;;^UTILITY(U,$J,358.3,10244,1,2,0)
 ;;=2^99214
 ;;^UTILITY(U,$J,358.3,10245,0)
 ;;=99215^^80^685^5
 ;;^UTILITY(U,$J,358.3,10245,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10245,1,1,0)
 ;;=1^Comprehensive
 ;;^UTILITY(U,$J,358.3,10245,1,2,0)
 ;;=2^99215
 ;;^UTILITY(U,$J,358.3,10246,0)
 ;;=99024^^80^685^6
 ;;^UTILITY(U,$J,358.3,10246,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10246,1,1,0)
 ;;=1^Post Op visit in Global
 ;;^UTILITY(U,$J,358.3,10246,1,2,0)
 ;;=2^99024
 ;;^UTILITY(U,$J,358.3,10247,0)
 ;;=99241^^80^686^1
 ;;^UTILITY(U,$J,358.3,10247,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10247,1,1,0)
 ;;=1^Problem Focused
 ;;^UTILITY(U,$J,358.3,10247,1,2,0)
 ;;=2^99241
 ;;^UTILITY(U,$J,358.3,10248,0)
 ;;=99242^^80^686^2
 ;;^UTILITY(U,$J,358.3,10248,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10248,1,1,0)
 ;;=1^Expanded Problem Focus
 ;;^UTILITY(U,$J,358.3,10248,1,2,0)
 ;;=2^99242
 ;;^UTILITY(U,$J,358.3,10249,0)
 ;;=99243^^80^686^3
 ;;^UTILITY(U,$J,358.3,10249,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10249,1,1,0)
 ;;=1^Detailed
 ;;^UTILITY(U,$J,358.3,10249,1,2,0)
 ;;=2^99243
 ;;^UTILITY(U,$J,358.3,10250,0)
 ;;=99244^^80^686^4
 ;;^UTILITY(U,$J,358.3,10250,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10250,1,1,0)
 ;;=1^Comprehensive, Moderate
 ;;^UTILITY(U,$J,358.3,10250,1,2,0)
 ;;=2^99244
 ;;^UTILITY(U,$J,358.3,10251,0)
 ;;=99245^^80^686^5
 ;;^UTILITY(U,$J,358.3,10251,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,10251,1,1,0)
 ;;=1^Comprehensive, High
 ;;^UTILITY(U,$J,358.3,10251,1,2,0)
 ;;=2^99245
 ;;^UTILITY(U,$J,358.3,10252,0)
 ;;=19100^^81^687^1
 ;;^UTILITY(U,$J,358.3,10252,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10252,1,2,0)
 ;;=2^Bx,Breast,needle,w/o imaging
 ;;^UTILITY(U,$J,358.3,10252,1,4,0)
 ;;=4^19100
 ;;^UTILITY(U,$J,358.3,10253,0)
 ;;=11100^^81^687^2
 ;;^UTILITY(U,$J,358.3,10253,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10253,1,2,0)
 ;;=2^Bx,Skin,first lesion
 ;;^UTILITY(U,$J,358.3,10253,1,4,0)
 ;;=4^11100
 ;;^UTILITY(U,$J,358.3,10254,0)
 ;;=11101^^81^687^3
 ;;^UTILITY(U,$J,358.3,10254,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10254,1,2,0)
 ;;=2^Bx,Skin,each addit lesion
 ;;^UTILITY(U,$J,358.3,10254,1,4,0)
 ;;=4^11101
 ;;^UTILITY(U,$J,358.3,10255,0)
 ;;=11042^^81^687^4^^^^1
 ;;^UTILITY(U,$J,358.3,10255,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10255,1,2,0)
 ;;=2^Debride skin/SQ 20sq cm or <
 ;;^UTILITY(U,$J,358.3,10255,1,4,0)
 ;;=4^11042
 ;;^UTILITY(U,$J,358.3,10256,0)
 ;;=11043^^81^687^6^^^^1
 ;;^UTILITY(U,$J,358.3,10256,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10256,1,2,0)
 ;;=2^Debride skin-SQ/Muscle 20sq cm or <
 ;;^UTILITY(U,$J,358.3,10256,1,4,0)
 ;;=4^11043
 ;;^UTILITY(U,$J,358.3,10257,0)
 ;;=11045^^81^687^5^^^^1
 ;;^UTILITY(U,$J,358.3,10257,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10257,1,2,0)
 ;;=2^Debride skin/SQ,ea addl 20sq cm
 ;;^UTILITY(U,$J,358.3,10257,1,4,0)
 ;;=4^11045
 ;;^UTILITY(U,$J,358.3,10258,0)
 ;;=11046^^81^687^7^^^^1
 ;;^UTILITY(U,$J,358.3,10258,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10258,1,2,0)
 ;;=2^Deb skin-SQ/Musc,ea adl 20sq cm
 ;;^UTILITY(U,$J,358.3,10258,1,4,0)
 ;;=4^11046
 ;;^UTILITY(U,$J,358.3,10259,0)
 ;;=10060^^81^688^9
 ;;^UTILITY(U,$J,358.3,10259,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10259,1,2,0)
 ;;=2^I&D Abscess,simple or single
 ;;^UTILITY(U,$J,358.3,10259,1,4,0)
 ;;=4^10060
 ;;^UTILITY(U,$J,358.3,10260,0)
 ;;=10061^^81^688^8
 ;;^UTILITY(U,$J,358.3,10260,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10260,1,2,0)
 ;;=2^I&D Abscess,complic or multip
 ;;^UTILITY(U,$J,358.3,10260,1,4,0)
 ;;=4^10061
 ;;^UTILITY(U,$J,358.3,10261,0)
 ;;=10160^^81^688^12
 ;;^UTILITY(U,$J,358.3,10261,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10261,1,2,0)
 ;;=2^Needle asp absc/cyst/hematoma
 ;;^UTILITY(U,$J,358.3,10261,1,4,0)
 ;;=4^10160
 ;;^UTILITY(U,$J,358.3,10262,0)
 ;;=10140^^81^688^11
 ;;^UTILITY(U,$J,358.3,10262,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10262,1,2,0)
 ;;=2^I&D hematoma/seroma,skin
 ;;^UTILITY(U,$J,358.3,10262,1,4,0)
 ;;=4^10140
 ;;^UTILITY(U,$J,358.3,10263,0)
 ;;=19000^^81^688^1
 ;;^UTILITY(U,$J,358.3,10263,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10263,1,2,0)
 ;;=2^Aspirate breast cyst, first
 ;;^UTILITY(U,$J,358.3,10263,1,4,0)
 ;;=4^19000
 ;;^UTILITY(U,$J,358.3,10264,0)
 ;;=19001^^81^688^2
 ;;^UTILITY(U,$J,358.3,10264,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10264,1,2,0)
 ;;=2^Aspirate each addit breast cyst
 ;;^UTILITY(U,$J,358.3,10264,1,4,0)
 ;;=4^19001
 ;;^UTILITY(U,$J,358.3,10265,0)
 ;;=26011^^81^688^4
 ;;^UTILITY(U,$J,358.3,10265,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10265,1,2,0)
 ;;=2^Drain abscess finger,complic
 ;;^UTILITY(U,$J,358.3,10265,1,4,0)
 ;;=4^26011
 ;;^UTILITY(U,$J,358.3,10266,0)
 ;;=26020^^81^688^6
 ;;^UTILITY(U,$J,358.3,10266,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10266,1,2,0)
 ;;=2^Drain tendon sheath,hand
 ;;^UTILITY(U,$J,358.3,10266,1,4,0)
 ;;=4^26020
 ;;^UTILITY(U,$J,358.3,10267,0)
 ;;=10120^^81^688^14
 ;;^UTILITY(U,$J,358.3,10267,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10267,1,2,0)
 ;;=2^Removal,foreign body,simple
 ;;^UTILITY(U,$J,358.3,10267,1,4,0)
 ;;=4^10120
 ;;^UTILITY(U,$J,358.3,10268,0)
 ;;=10121^^81^688^13
 ;;^UTILITY(U,$J,358.3,10268,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10268,1,2,0)
 ;;=2^Removal,foreign body,complex
 ;;^UTILITY(U,$J,358.3,10268,1,4,0)
 ;;=4^10121
 ;;^UTILITY(U,$J,358.3,10269,0)
 ;;=26010^^81^688^5^^^^1
 ;;^UTILITY(U,$J,358.3,10269,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10269,1,2,0)
 ;;=2^Drain abscess finger,simple
 ;;^UTILITY(U,$J,358.3,10269,1,4,0)
 ;;=4^26010
 ;;^UTILITY(U,$J,358.3,10270,0)
 ;;=10180^^81^688^10^^^^1
 ;;^UTILITY(U,$J,358.3,10270,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10270,1,2,0)
 ;;=2^I&D complex postop wound
 ;;^UTILITY(U,$J,358.3,10270,1,4,0)
 ;;=4^10180
 ;;^UTILITY(U,$J,358.3,10271,0)
 ;;=10080^^81^688^3^^^^1
 ;;^UTILITY(U,$J,358.3,10271,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10271,1,2,0)
 ;;=2^Drain Pilonidal Cyst
