IBDEI0C6 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,16197,0)
 ;;=754.59^^119^1018^3
 ;;^UTILITY(U,$J,358.3,16197,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16197,1,3,0)
 ;;=3^754.59
 ;;^UTILITY(U,$J,358.3,16197,1,5,0)
 ;;=5^Calcaneovarus, talipes
 ;;^UTILITY(U,$J,358.3,16197,2)
 ;;=^273008
 ;;^UTILITY(U,$J,358.3,16198,0)
 ;;=V64.2^^119^1018^6
 ;;^UTILITY(U,$J,358.3,16198,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16198,1,3,0)
 ;;=3^V64.2
 ;;^UTILITY(U,$J,358.3,16198,1,5,0)
 ;;=5^Cancelled surgical or other procedures because of patient decision 
 ;;^UTILITY(U,$J,358.3,16198,2)
 ;;=^295559
 ;;^UTILITY(U,$J,358.3,16199,0)
 ;;=V64.1^^119^1018^5
 ;;^UTILITY(U,$J,358.3,16199,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16199,1,3,0)
 ;;=3^V64.1
 ;;^UTILITY(U,$J,358.3,16199,1,5,0)
 ;;=5^Cancelled surgical or other procedure because of contraindication
 ;;^UTILITY(U,$J,358.3,16199,2)
 ;;=^295558
 ;;^UTILITY(U,$J,358.3,16200,0)
 ;;=V64.3^^119^1018^4
 ;;^UTILITY(U,$J,358.3,16200,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16200,1,3,0)
 ;;=3^V64.3
 ;;^UTILITY(U,$J,358.3,16200,1,5,0)
 ;;=5^Cancelled procedure because of other reasons
 ;;^UTILITY(U,$J,358.3,16200,2)
 ;;=^295560
 ;;^UTILITY(U,$J,358.3,16201,0)
 ;;=726.79^^119^1018^7
 ;;^UTILITY(U,$J,358.3,16201,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16201,1,3,0)
 ;;=3^726.79
 ;;^UTILITY(U,$J,358.3,16201,1,5,0)
 ;;=5^Capsulitis of ankle/toe/foot
 ;;^UTILITY(U,$J,358.3,16201,2)
 ;;=^272555
 ;;^UTILITY(U,$J,358.3,16202,0)
 ;;=736.75^^119^1018^8
 ;;^UTILITY(U,$J,358.3,16202,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16202,1,3,0)
 ;;=3^736.75
 ;;^UTILITY(U,$J,358.3,16202,1,5,0)
 ;;=5^Cavovarus deformity of foot/ankle acquired
 ;;^UTILITY(U,$J,358.3,16202,2)
 ;;=^272747
 ;;^UTILITY(U,$J,358.3,16203,0)
 ;;=681.10^^119^1018^11
 ;;^UTILITY(U,$J,358.3,16203,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16203,1,3,0)
 ;;=3^681.10
 ;;^UTILITY(U,$J,358.3,16203,1,5,0)
 ;;=5^Cellulitis and abscess, toe, acquired
 ;;^UTILITY(U,$J,358.3,16203,2)
 ;;=^271885
 ;;^UTILITY(U,$J,358.3,16204,0)
 ;;=682.7^^119^1018^9
 ;;^UTILITY(U,$J,358.3,16204,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16204,1,3,0)
 ;;=3^682.7
 ;;^UTILITY(U,$J,358.3,16204,1,5,0)
 ;;=5^Cellulitis and abscess, foot, except toes
 ;;^UTILITY(U,$J,358.3,16204,2)
 ;;=^271895
 ;;^UTILITY(U,$J,358.3,16205,0)
 ;;=682.6^^119^1018^10
 ;;^UTILITY(U,$J,358.3,16205,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16205,1,3,0)
 ;;=3^682.6
 ;;^UTILITY(U,$J,358.3,16205,1,5,0)
 ;;=5^Cellulitis and abscess, leg, except foot
 ;;^UTILITY(U,$J,358.3,16205,2)
 ;;=^271894
 ;;^UTILITY(U,$J,358.3,16206,0)
 ;;=094.0^^119^1018^12
 ;;^UTILITY(U,$J,358.3,16206,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16206,1,3,0)
 ;;=3^094.0
 ;;^UTILITY(U,$J,358.3,16206,1,5,0)
 ;;=5^Charcot's joint disease (713.5)
 ;;^UTILITY(U,$J,358.3,16206,2)
 ;;=^117008
 ;;^UTILITY(U,$J,358.3,16207,0)
 ;;=443.9^^119^1018^13
 ;;^UTILITY(U,$J,358.3,16207,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16207,1,3,0)
 ;;=3^443.9
 ;;^UTILITY(U,$J,358.3,16207,1,5,0)
 ;;=5^Claudication, intermittent
 ;;^UTILITY(U,$J,358.3,16207,2)
 ;;=^184182
 ;;^UTILITY(U,$J,358.3,16208,0)
 ;;=440.21^^119^1018^14
 ;;^UTILITY(U,$J,358.3,16208,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16208,1,3,0)
 ;;=3^440.21
 ;;^UTILITY(U,$J,358.3,16208,1,5,0)
 ;;=5^Claudication, intermittent due to arteriosclerosis
 ;;^UTILITY(U,$J,358.3,16208,2)
 ;;=^293885
 ;;^UTILITY(U,$J,358.3,16209,0)
 ;;=735.5^^119^1018^16
 ;;^UTILITY(U,$J,358.3,16209,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16209,1,3,0)
 ;;=3^735.5
 ;;^UTILITY(U,$J,358.3,16209,1,5,0)
 ;;=5^Claw toe, acquired
 ;;^UTILITY(U,$J,358.3,16209,2)
 ;;=^272713
 ;;^UTILITY(U,$J,358.3,16210,0)
 ;;=754.71^^119^1018^17
 ;;^UTILITY(U,$J,358.3,16210,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16210,1,3,0)
 ;;=3^754.71
 ;;^UTILITY(U,$J,358.3,16210,1,5,0)
 ;;=5^Claw toe, congential
 ;;^UTILITY(U,$J,358.3,16210,2)
 ;;=^117165
 ;;^UTILITY(U,$J,358.3,16211,0)
 ;;=736.71^^119^1018^18
 ;;^UTILITY(U,$J,358.3,16211,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16211,1,3,0)
 ;;=3^736.71
 ;;^UTILITY(U,$J,358.3,16211,1,5,0)
 ;;=5^Clubfoot, acquired
 ;;^UTILITY(U,$J,358.3,16211,2)
 ;;=^272743
 ;;^UTILITY(U,$J,358.3,16212,0)
 ;;=729.89^^119^1018^19
 ;;^UTILITY(U,$J,358.3,16212,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16212,1,3,0)
 ;;=3^729.89
 ;;^UTILITY(U,$J,358.3,16212,1,5,0)
 ;;=5^Cold feet or toes- chronic
 ;;^UTILITY(U,$J,358.3,16212,2)
 ;;=^87720
 ;;^UTILITY(U,$J,358.3,16213,0)
 ;;=958.8^^119^1018^21
 ;;^UTILITY(U,$J,358.3,16213,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16213,1,3,0)
 ;;=3^958.8
 ;;^UTILITY(U,$J,358.3,16213,1,5,0)
 ;;=5^Compartmental syndrome, traumatic
 ;;^UTILITY(U,$J,358.3,16213,2)
 ;;=^87560
 ;;^UTILITY(U,$J,358.3,16214,0)
 ;;=996.67^^119^1018^23
 ;;^UTILITY(U,$J,358.3,16214,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16214,1,3,0)
 ;;=3^996.67
 ;;^UTILITY(U,$J,358.3,16214,1,5,0)
 ;;=5^Complication of internal orthopedic device, implant, and graft; infection and inflammation
 ;;^UTILITY(U,$J,358.3,16214,2)
 ;;=^276290
 ;;^UTILITY(U,$J,358.3,16215,0)
 ;;=996.78^^119^1018^22
 ;;^UTILITY(U,$J,358.3,16215,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16215,1,3,0)
 ;;=3^996.78
 ;;^UTILITY(U,$J,358.3,16215,1,5,0)
 ;;=5^Complication of internal orthepedic device, implant, & graft; bleeding/foreign body/pain/granuloma
 ;;^UTILITY(U,$J,358.3,16215,2)
 ;;=^276301
 ;;^UTILITY(U,$J,358.3,16216,0)
 ;;=718.47^^119^1018^24
 ;;^UTILITY(U,$J,358.3,16216,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16216,1,3,0)
 ;;=3^718.47
 ;;^UTILITY(U,$J,358.3,16216,1,5,0)
 ;;=5^Contracture of ankle/foot/toe joint
 ;;^UTILITY(U,$J,358.3,16216,2)
 ;;=^272324
 ;;^UTILITY(U,$J,358.3,16217,0)
 ;;=924.20^^119^1018^26
 ;;^UTILITY(U,$J,358.3,16217,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16217,1,3,0)
 ;;=3^924.20
 ;;^UTILITY(U,$J,358.3,16217,1,5,0)
 ;;=5^Contusion of foot or heel
 ;;^UTILITY(U,$J,358.3,16217,2)
 ;;=^275423
 ;;^UTILITY(U,$J,358.3,16218,0)
 ;;=924.21^^119^1018^25
 ;;^UTILITY(U,$J,358.3,16218,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16218,1,3,0)
 ;;=3^924.21
 ;;^UTILITY(U,$J,358.3,16218,1,5,0)
 ;;=5^Contusion of ankle
 ;;^UTILITY(U,$J,358.3,16218,2)
 ;;=^275425
 ;;^UTILITY(U,$J,358.3,16219,0)
 ;;=924.10^^119^1018^27
 ;;^UTILITY(U,$J,358.3,16219,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16219,1,3,0)
 ;;=3^924.10
 ;;^UTILITY(U,$J,358.3,16219,1,5,0)
 ;;=5^Contusion of lower leg
 ;;^UTILITY(U,$J,358.3,16219,2)
 ;;=^275419
 ;;^UTILITY(U,$J,358.3,16220,0)
 ;;=924.3^^119^1018^28
 ;;^UTILITY(U,$J,358.3,16220,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16220,1,3,0)
 ;;=3^924.3
 ;;^UTILITY(U,$J,358.3,16220,1,5,0)
 ;;=5^Contusion of toe
 ;;^UTILITY(U,$J,358.3,16220,2)
 ;;=^275427
 ;;^UTILITY(U,$J,358.3,16221,0)
 ;;=700.^^119^1018^29
 ;;^UTILITY(U,$J,358.3,16221,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16221,1,3,0)
 ;;=3^700.
 ;;^UTILITY(U,$J,358.3,16221,1,5,0)
 ;;=5^Corns and callosities
 ;;^UTILITY(U,$J,358.3,16221,2)
 ;;=^18351
 ;;^UTILITY(U,$J,358.3,16222,0)
 ;;=706.2^^119^1018^31
 ;;^UTILITY(U,$J,358.3,16222,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16222,1,3,0)
 ;;=3^706.2
 ;;^UTILITY(U,$J,358.3,16222,1,5,0)
 ;;=5^Cyst of sebaceous
 ;;^UTILITY(U,$J,358.3,16222,2)
 ;;=^41304
 ;;^UTILITY(U,$J,358.3,16223,0)
 ;;=733.20^^119^1018^30
 ;;^UTILITY(U,$J,358.3,16223,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16223,1,3,0)
 ;;=3^733.20
 ;;^UTILITY(U,$J,358.3,16223,1,5,0)
 ;;=5^Cyst of bone
 ;;^UTILITY(U,$J,358.3,16223,2)
 ;;=^30080
 ;;^UTILITY(U,$J,358.3,16224,0)
 ;;=729.99^^119^1018^20
 ;;^UTILITY(U,$J,358.3,16224,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16224,1,3,0)
 ;;=3^729.99
 ;;^UTILITY(U,$J,358.3,16224,1,5,0)
 ;;=5^Compartmental syndrome, nontraumatic
 ;;^UTILITY(U,$J,358.3,16224,2)
 ;;=^336656
 ;;^UTILITY(U,$J,358.3,16225,0)
 ;;=453.89^^119^1018^15
 ;;^UTILITY(U,$J,358.3,16225,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16225,1,3,0)
 ;;=3^453.89
 ;;^UTILITY(U,$J,358.3,16225,1,5,0)
 ;;=5^Claudication, venous
 ;;^UTILITY(U,$J,358.3,16225,2)
 ;;=^338259
 ;;^UTILITY(U,$J,358.3,16226,0)
 ;;=735.8^^119^1019^2
 ;;^UTILITY(U,$J,358.3,16226,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16226,1,3,0)
 ;;=3^735.8
 ;;^UTILITY(U,$J,358.3,16226,1,5,0)
 ;;=5^Deformity, toe- acquired
 ;;^UTILITY(U,$J,358.3,16226,2)
 ;;=^272714
 ;;^UTILITY(U,$J,358.3,16227,0)
 ;;=736.70^^119^1019^3
 ;;^UTILITY(U,$J,358.3,16227,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16227,1,3,0)
 ;;=3^736.70
 ;;^UTILITY(U,$J,358.3,16227,1,5,0)
 ;;=5^Deformity, ankle and foot- acquired
 ;;^UTILITY(U,$J,358.3,16227,2)
 ;;=^123805
 ;;^UTILITY(U,$J,358.3,16228,0)
 ;;=755.66^^119^1019^4
 ;;^UTILITY(U,$J,358.3,16228,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16228,1,3,0)
 ;;=3^755.66
 ;;^UTILITY(U,$J,358.3,16228,1,5,0)
 ;;=5^Deformity, toe- congenital
 ;;^UTILITY(U,$J,358.3,16228,2)
 ;;=^273059
 ;;^UTILITY(U,$J,358.3,16229,0)
 ;;=754.70^^119^1019^5
 ;;^UTILITY(U,$J,358.3,16229,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16229,1,3,0)
 ;;=3^754.70
 ;;^UTILITY(U,$J,358.3,16229,1,5,0)
 ;;=5^Deformity, foot- congenital
 ;;^UTILITY(U,$J,358.3,16229,2)
 ;;=^25440
 ;;^UTILITY(U,$J,358.3,16230,0)
 ;;=755.69^^119^1019^6
 ;;^UTILITY(U,$J,358.3,16230,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16230,1,3,0)
 ;;=3^755.69
 ;;^UTILITY(U,$J,358.3,16230,1,5,0)
 ;;=5^Deformity, ankle- congenital
 ;;^UTILITY(U,$J,358.3,16230,2)
 ;;=^273054
 ;;^UTILITY(U,$J,358.3,16231,0)
 ;;=692.9^^119^1019^7
 ;;^UTILITY(U,$J,358.3,16231,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16231,1,3,0)
 ;;=3^692.9
 ;;^UTILITY(U,$J,358.3,16231,1,5,0)
 ;;=5^Dermatitis (contact/eczema/venenata)
 ;;^UTILITY(U,$J,358.3,16231,2)
 ;;=^27800
 ;;^UTILITY(U,$J,358.3,16232,0)
 ;;=459.81^^119^1019^8
 ;;^UTILITY(U,$J,358.3,16232,1,0)
 ;;=^358.31IA^5^2
