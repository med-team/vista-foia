IBDEI079 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,9541,2)
 ;;=^268988
 ;;^UTILITY(U,$J,358.3,9542,0)
 ;;=370.34^^77^657^38
 ;;^UTILITY(U,$J,358.3,9542,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9542,1,3,0)
 ;;=3^Keratitis, Exposure
 ;;^UTILITY(U,$J,358.3,9542,1,4,0)
 ;;=4^370.34
 ;;^UTILITY(U,$J,358.3,9542,2)
 ;;=Exposure Keratoconjunctivitis^268932
 ;;^UTILITY(U,$J,358.3,9543,0)
 ;;=370.21^^77^657^40
 ;;^UTILITY(U,$J,358.3,9543,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9543,1,3,0)
 ;;=3^Keratitis, Punctate
 ;;^UTILITY(U,$J,358.3,9543,1,4,0)
 ;;=4^370.21
 ;;^UTILITY(U,$J,358.3,9543,2)
 ;;=Keratitis, Punctate^268920
 ;;^UTILITY(U,$J,358.3,9544,0)
 ;;=054.42^^77^657^36
 ;;^UTILITY(U,$J,358.3,9544,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9544,1,3,0)
 ;;=3^Keratitis, Dendritic (HSV)
 ;;^UTILITY(U,$J,358.3,9544,1,4,0)
 ;;=4^054.42
 ;;^UTILITY(U,$J,358.3,9544,2)
 ;;=Dendritic Keratitis^66763
 ;;^UTILITY(U,$J,358.3,9545,0)
 ;;=370.62^^77^657^54
 ;;^UTILITY(U,$J,358.3,9545,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9545,1,3,0)
 ;;=3^Pannus
 ;;^UTILITY(U,$J,358.3,9545,1,4,0)
 ;;=4^370.62
 ;;^UTILITY(U,$J,358.3,9545,2)
 ;;=^268949
 ;;^UTILITY(U,$J,358.3,9546,0)
 ;;=053.21^^77^657^44
 ;;^UTILITY(U,$J,358.3,9546,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9546,1,3,0)
 ;;=3^Keratoconjunctivits, H Zoster
 ;;^UTILITY(U,$J,358.3,9546,1,4,0)
 ;;=4^053.21
 ;;^UTILITY(U,$J,358.3,9546,2)
 ;;=Herp Zost Keratoconjunctivitis^266553
 ;;^UTILITY(U,$J,358.3,9547,0)
 ;;=V42.5^^77^657^19
 ;;^UTILITY(U,$J,358.3,9547,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9547,1,3,0)
 ;;=3^Corneal Transplant
 ;;^UTILITY(U,$J,358.3,9547,1,4,0)
 ;;=4^V42.5
 ;;^UTILITY(U,$J,358.3,9547,2)
 ;;=Corneal Transplant^174117
 ;;^UTILITY(U,$J,358.3,9548,0)
 ;;=996.51^^77^657^62
 ;;^UTILITY(U,$J,358.3,9548,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9548,1,3,0)
 ;;=3^Reject/Failure, Corneal Transp
 ;;^UTILITY(U,$J,358.3,9548,1,4,0)
 ;;=4^996.51
 ;;^UTILITY(U,$J,358.3,9548,2)
 ;;=Rejection/Failure, Corneal Transplant^276277^V42.5
 ;;^UTILITY(U,$J,358.3,9549,0)
 ;;=918.1^^77^657^1
 ;;^UTILITY(U,$J,358.3,9549,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9549,1,3,0)
 ;;=3^Abrasion, Cornea
 ;;^UTILITY(U,$J,358.3,9549,1,4,0)
 ;;=4^918.1
 ;;^UTILITY(U,$J,358.3,9549,2)
 ;;=Corneal Abrasion^115829
 ;;^UTILITY(U,$J,358.3,9550,0)
 ;;=370.49^^77^657^43
 ;;^UTILITY(U,$J,358.3,9550,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9550,1,3,0)
 ;;=3^Keratoconjunctivitis, Other
 ;;^UTILITY(U,$J,358.3,9550,1,4,0)
 ;;=4^370.49
 ;;^UTILITY(U,$J,358.3,9550,2)
 ;;=^87674
 ;;^UTILITY(U,$J,358.3,9551,0)
 ;;=371.41^^77^657^5
 ;;^UTILITY(U,$J,358.3,9551,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9551,1,3,0)
 ;;=3^Arcus, Corneal
 ;;^UTILITY(U,$J,358.3,9551,1,4,0)
 ;;=4^371.41
 ;;^UTILITY(U,$J,358.3,9551,2)
 ;;=Corneal Arcus^109206
 ;;^UTILITY(U,$J,358.3,9552,0)
 ;;=371.10^^77^657^67
 ;;^UTILITY(U,$J,358.3,9552,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9552,1,3,0)
 ;;=3^Toxic Keratopathy, Due to med
 ;;^UTILITY(U,$J,358.3,9552,1,4,0)
 ;;=4^371.10
 ;;^UTILITY(U,$J,358.3,9552,2)
 ;;=Toxic Keratopathy, Due to med^276846
 ;;^UTILITY(U,$J,358.3,9553,0)
 ;;=370.60^^77^657^50
 ;;^UTILITY(U,$J,358.3,9553,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9553,1,3,0)
 ;;=3^Neovascularization, Corneal
 ;;^UTILITY(U,$J,358.3,9553,1,4,0)
 ;;=4^370.60
 ;;^UTILITY(U,$J,358.3,9553,2)
 ;;=Corneal Neovascularization^184274
 ;;^UTILITY(U,$J,358.3,9554,0)
 ;;=371.20^^77^657^22
 ;;^UTILITY(U,$J,358.3,9554,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9554,1,3,0)
 ;;=3^Edema, Cornea
 ;;^UTILITY(U,$J,358.3,9554,1,4,0)
 ;;=4^371.20
 ;;^UTILITY(U,$J,358.3,9554,2)
 ;;=Edema, Cornea^28394
 ;;^UTILITY(U,$J,358.3,9555,0)
 ;;=371.00^^77^657^51
 ;;^UTILITY(U,$J,358.3,9555,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9555,1,3,0)
 ;;=3^Opacity, Corneal
 ;;^UTILITY(U,$J,358.3,9555,1,4,0)
 ;;=4^371.00
 ;;^UTILITY(U,$J,358.3,9555,2)
 ;;=Corneal Opacity^28398
 ;;^UTILITY(U,$J,358.3,9556,0)
 ;;=371.43^^77^657^6
 ;;^UTILITY(U,$J,358.3,9556,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9556,1,3,0)
 ;;=3^Band Keratopathy
 ;;^UTILITY(U,$J,358.3,9556,1,4,0)
 ;;=4^371.43
 ;;^UTILITY(U,$J,358.3,9556,2)
 ;;=Band Keratopathy^268979
 ;;^UTILITY(U,$J,358.3,9557,0)
 ;;=710.2^^77^657^64
 ;;^UTILITY(U,$J,358.3,9557,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9557,1,3,0)
 ;;=3^Sjogren's Disease
 ;;^UTILITY(U,$J,358.3,9557,1,4,0)
 ;;=4^710.2
 ;;^UTILITY(U,$J,358.3,9557,2)
 ;;=Sjogren's Disease^192145
 ;;^UTILITY(U,$J,358.3,9558,0)
 ;;=374.20^^77^657^47
 ;;^UTILITY(U,$J,358.3,9558,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9558,1,3,0)
 ;;=3^Lagophthalmos
 ;;^UTILITY(U,$J,358.3,9558,1,4,0)
 ;;=4^374.20
 ;;^UTILITY(U,$J,358.3,9558,2)
 ;;=Lagophthalmos^265452
 ;;^UTILITY(U,$J,358.3,9559,0)
 ;;=372.72^^77^657^28
 ;;^UTILITY(U,$J,358.3,9559,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9559,1,3,0)
 ;;=3^Hemorrhage, Conjunctival
 ;;^UTILITY(U,$J,358.3,9559,1,4,0)
 ;;=4^372.72
 ;;^UTILITY(U,$J,358.3,9559,2)
 ;;=Hemorrhage, Conjunctival^27538
 ;;^UTILITY(U,$J,358.3,9560,0)
 ;;=372.00^^77^657^13
 ;;^UTILITY(U,$J,358.3,9560,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9560,1,3,0)
 ;;=3^Conjunctivitis, Acute
 ;;^UTILITY(U,$J,358.3,9560,1,4,0)
 ;;=4^372.00
 ;;^UTILITY(U,$J,358.3,9560,2)
 ;;=Conjunctivitis, Acute^269000
 ;;^UTILITY(U,$J,358.3,9561,0)
 ;;=372.05^^77^657^14
 ;;^UTILITY(U,$J,358.3,9561,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9561,1,3,0)
 ;;=3^Conjunctivitis, Atopic Acute
 ;;^UTILITY(U,$J,358.3,9561,1,4,0)
 ;;=4^372.05
 ;;^UTILITY(U,$J,358.3,9561,2)
 ;;=Conjuntivitis, Atopic, Acute^2605
 ;;^UTILITY(U,$J,358.3,9562,0)
 ;;=372.14^^77^657^18
 ;;^UTILITY(U,$J,358.3,9562,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9562,1,3,0)
 ;;=3^Conjuntivitis, Allergic, Chronic
 ;;^UTILITY(U,$J,358.3,9562,1,4,0)
 ;;=4^372.14
 ;;^UTILITY(U,$J,358.3,9562,2)
 ;;=Conjunctivitis, Allergic, Chr^87396
 ;;^UTILITY(U,$J,358.3,9563,0)
 ;;=372.03^^77^657^12
 ;;^UTILITY(U,$J,358.3,9563,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9563,1,3,0)
 ;;=3^Conjuncitivitis, Mucopurulent
 ;;^UTILITY(U,$J,358.3,9563,1,4,0)
 ;;=4^372.03
 ;;^UTILITY(U,$J,358.3,9563,2)
 ;;=Conjuncitivitis, Mucopurulent^87718
 ;;^UTILITY(U,$J,358.3,9564,0)
 ;;=372.10^^77^657^15
 ;;^UTILITY(U,$J,358.3,9564,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9564,1,3,0)
 ;;=3^Conjunctivitis, Chronic
 ;;^UTILITY(U,$J,358.3,9564,1,4,0)
 ;;=4^372.10
 ;;^UTILITY(U,$J,358.3,9564,2)
 ;;=Conjunctivitis, Chronic^269008
 ;;^UTILITY(U,$J,358.3,9565,0)
 ;;=077.8^^77^657^16
 ;;^UTILITY(U,$J,358.3,9565,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9565,1,3,0)
 ;;=3^Conjunctivitis, Viral
 ;;^UTILITY(U,$J,358.3,9565,1,4,0)
 ;;=4^077.8
 ;;^UTILITY(U,$J,358.3,9565,2)
 ;;=Conjunctivitis, Viral^88239
 ;;^UTILITY(U,$J,358.3,9566,0)
 ;;=372.54^^77^657^11
 ;;^UTILITY(U,$J,358.3,9566,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9566,1,3,0)
 ;;=3^Concretions, Conjunctival
 ;;^UTILITY(U,$J,358.3,9566,1,4,0)
 ;;=4^372.54
 ;;^UTILITY(U,$J,358.3,9566,2)
 ;;=...Concretions, Conjunctival^269038
 ;;^UTILITY(U,$J,358.3,9567,0)
 ;;=930.9^^77^657^26
 ;;^UTILITY(U,$J,358.3,9567,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9567,1,3,0)
 ;;=3^Foreign Body, External Eye
 ;;^UTILITY(U,$J,358.3,9567,1,4,0)
 ;;=4^930.9
 ;;^UTILITY(U,$J,358.3,9567,2)
 ;;=Foreign Body, External Eye^275489
 ;;^UTILITY(U,$J,358.3,9568,0)
 ;;=372.51^^77^657^57
 ;;^UTILITY(U,$J,358.3,9568,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9568,1,3,0)
 ;;=3^Pinguecula
 ;;^UTILITY(U,$J,358.3,9568,1,4,0)
 ;;=4^372.51
 ;;^UTILITY(U,$J,358.3,9568,2)
 ;;=Pinguecula^265525
 ;;^UTILITY(U,$J,358.3,9569,0)
 ;;=379.00^^77^657^23
 ;;^UTILITY(U,$J,358.3,9569,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9569,1,3,0)
 ;;=3^Episcleritis
 ;;^UTILITY(U,$J,358.3,9569,1,4,0)
 ;;=4^379.00
 ;;^UTILITY(U,$J,358.3,9569,2)
 ;;=...^108564
 ;;^UTILITY(U,$J,358.3,9570,0)
 ;;=372.20^^77^657^9
 ;;^UTILITY(U,$J,358.3,9570,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9570,1,3,0)
 ;;=3^Blepharoconjunctivitis
 ;;^UTILITY(U,$J,358.3,9570,1,4,0)
 ;;=4^372.20
 ;;^UTILITY(U,$J,358.3,9570,2)
 ;;=Blepharoconjunctivitis^15277
 ;;^UTILITY(U,$J,358.3,9571,0)
 ;;=372.40^^77^657^59
 ;;^UTILITY(U,$J,358.3,9571,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9571,1,3,0)
 ;;=3^Pterygium
 ;;^UTILITY(U,$J,358.3,9571,1,4,0)
 ;;=4^372.40
 ;;^UTILITY(U,$J,358.3,9571,2)
 ;;=Pterygium^100819
 ;;^UTILITY(U,$J,358.3,9572,0)
 ;;=694.4^^77^657^56
 ;;^UTILITY(U,$J,358.3,9572,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9572,1,3,0)
 ;;=3^Pemphigus
 ;;^UTILITY(U,$J,358.3,9572,1,4,0)
 ;;=4^694.4
 ;;^UTILITY(U,$J,358.3,9572,2)
 ;;=Pemphigus^91124
 ;;^UTILITY(U,$J,358.3,9573,0)
 ;;=224.3^^77^657^8
 ;;^UTILITY(U,$J,358.3,9573,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9573,1,3,0)
 ;;=3^Benign Neoplasm Conjunctiva
 ;;^UTILITY(U,$J,358.3,9573,1,4,0)
 ;;=4^224.3
 ;;^UTILITY(U,$J,358.3,9573,2)
 ;;=Benign Neoplasm Conjunctiva^267673
 ;;^UTILITY(U,$J,358.3,9574,0)
 ;;=370.40^^77^657^41
 ;;^UTILITY(U,$J,358.3,9574,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9574,1,3,0)
 ;;=3^Keratoconjunctivitis
 ;;^UTILITY(U,$J,358.3,9574,1,4,0)
 ;;=4^370.40
 ;;^UTILITY(U,$J,358.3,9574,2)
 ;;=^66777
 ;;^UTILITY(U,$J,358.3,9575,0)
 ;;=694.5^^77^657^55
 ;;^UTILITY(U,$J,358.3,9575,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9575,1,3,0)
 ;;=3^Pemphigoid
 ;;^UTILITY(U,$J,358.3,9575,1,4,0)
 ;;=4^694.5
 ;;^UTILITY(U,$J,358.3,9575,2)
 ;;=Pemphigoid^91108
 ;;^UTILITY(U,$J,358.3,9576,0)
 ;;=364.10^^77^657^32
 ;;^UTILITY(U,$J,358.3,9576,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9576,1,3,0)
 ;;=3^Iridocyclitis, Chronic
 ;;^UTILITY(U,$J,358.3,9576,1,4,0)
 ;;=4^364.10
 ;;^UTILITY(U,$J,358.3,9576,2)
 ;;=Iridocyclitis, Chronic^24398
 ;;^UTILITY(U,$J,358.3,9577,0)
 ;;=054.44^^77^657^33
 ;;^UTILITY(U,$J,358.3,9577,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9577,1,3,0)
 ;;=3^Iridocyclitis, H Simplex
