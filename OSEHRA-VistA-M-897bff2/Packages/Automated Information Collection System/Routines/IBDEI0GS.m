IBDEI0GS ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,22609,2)
 ;;=^124436
 ;;^UTILITY(U,$J,358.3,22610,0)
 ;;=597.80^^178^1524^11
 ;;^UTILITY(U,$J,358.3,22610,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22610,1,2,0)
 ;;=2^597.80
 ;;^UTILITY(U,$J,358.3,22610,1,5,0)
 ;;=5^URETHRITIS NOS
 ;;^UTILITY(U,$J,358.3,22610,2)
 ;;=^124214
 ;;^UTILITY(U,$J,358.3,22611,0)
 ;;=788.41^^178^1524^13
 ;;^UTILITY(U,$J,358.3,22611,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22611,1,2,0)
 ;;=2^788.41
 ;;^UTILITY(U,$J,358.3,22611,1,5,0)
 ;;=5^URINARY FREQUENCY
 ;;^UTILITY(U,$J,358.3,22611,2)
 ;;=^124396
 ;;^UTILITY(U,$J,358.3,22612,0)
 ;;=788.30^^178^1524^14
 ;;^UTILITY(U,$J,358.3,22612,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22612,1,2,0)
 ;;=2^788.30
 ;;^UTILITY(U,$J,358.3,22612,1,5,0)
 ;;=5^URINARY INCONTINENCE, UNSPEC
 ;;^UTILITY(U,$J,358.3,22612,2)
 ;;=^124400
 ;;^UTILITY(U,$J,358.3,22613,0)
 ;;=788.20^^178^1524^15
 ;;^UTILITY(U,$J,358.3,22613,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22613,1,2,0)
 ;;=2^788.20
 ;;^UTILITY(U,$J,358.3,22613,1,5,0)
 ;;=5^URINARY RETENTION
 ;;^UTILITY(U,$J,358.3,22613,2)
 ;;=^295812
 ;;^UTILITY(U,$J,358.3,22614,0)
 ;;=532.90^^178^1524^6
 ;;^UTILITY(U,$J,358.3,22614,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22614,1,2,0)
 ;;=2^532.90
 ;;^UTILITY(U,$J,358.3,22614,1,5,0)
 ;;=5^ULCER,DUODENAL
 ;;^UTILITY(U,$J,358.3,22614,2)
 ;;=^37311
 ;;^UTILITY(U,$J,358.3,22615,0)
 ;;=531.90^^178^1524^7
 ;;^UTILITY(U,$J,358.3,22615,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22615,1,2,0)
 ;;=2^531.90
 ;;^UTILITY(U,$J,358.3,22615,1,5,0)
 ;;=5^ULCER,GASTRIC
 ;;^UTILITY(U,$J,358.3,22615,2)
 ;;=^51128
 ;;^UTILITY(U,$J,358.3,22616,0)
 ;;=533.90^^178^1524^9
 ;;^UTILITY(U,$J,358.3,22616,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22616,1,2,0)
 ;;=2^533.90
 ;;^UTILITY(U,$J,358.3,22616,1,5,0)
 ;;=5^ULCER,PEPTIC
 ;;^UTILITY(U,$J,358.3,22616,2)
 ;;=^93051
 ;;^UTILITY(U,$J,358.3,22617,0)
 ;;=707.10^^178^1524^8
 ;;^UTILITY(U,$J,358.3,22617,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22617,1,2,0)
 ;;=2^707.10
 ;;^UTILITY(U,$J,358.3,22617,1,5,0)
 ;;=5^ULCER,LOWER LIMB
 ;;^UTILITY(U,$J,358.3,22617,2)
 ;;=^322142
 ;;^UTILITY(U,$J,358.3,22618,0)
 ;;=707.9^^178^1524^10
 ;;^UTILITY(U,$J,358.3,22618,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22618,1,2,0)
 ;;=2^707.9
 ;;^UTILITY(U,$J,358.3,22618,1,5,0)
 ;;=5^ULCER,SKIN NOS
 ;;^UTILITY(U,$J,358.3,22618,2)
 ;;=^24439
 ;;^UTILITY(U,$J,358.3,22619,0)
 ;;=441.4^^179^1525^8
 ;;^UTILITY(U,$J,358.3,22619,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22619,1,4,0)
 ;;=4^441.4
 ;;^UTILITY(U,$J,358.3,22619,1,5,0)
 ;;=5^Aortic Aneursym, abdominal
 ;;^UTILITY(U,$J,358.3,22619,2)
 ;;=^269769
 ;;^UTILITY(U,$J,358.3,22620,0)
 ;;=441.2^^179^1525^9
 ;;^UTILITY(U,$J,358.3,22620,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22620,1,4,0)
 ;;=4^441.2
 ;;^UTILITY(U,$J,358.3,22620,1,5,0)
 ;;=5^Aortic Aneursym, thoracic
 ;;^UTILITY(U,$J,358.3,22620,2)
 ;;=^269765
 ;;^UTILITY(U,$J,358.3,22621,0)
 ;;=428.0^^179^1525^12
 ;;^UTILITY(U,$J,358.3,22621,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22621,1,4,0)
 ;;=4^428.0
 ;;^UTILITY(U,$J,358.3,22621,1,5,0)
 ;;=5^CHF
 ;;^UTILITY(U,$J,358.3,22621,2)
 ;;=^54758
 ;;^UTILITY(U,$J,358.3,22622,0)
 ;;=433.10^^179^1525^13
 ;;^UTILITY(U,$J,358.3,22622,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22622,1,4,0)
 ;;=4^433.10
 ;;^UTILITY(U,$J,358.3,22622,1,5,0)
 ;;=5^Carotid Artery disease
 ;;^UTILITY(U,$J,358.3,22622,2)
 ;;=^295801
 ;;^UTILITY(U,$J,358.3,22623,0)
 ;;=443.9^^179^1525^25
 ;;^UTILITY(U,$J,358.3,22623,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22623,1,4,0)
 ;;=4^443.9
 ;;^UTILITY(U,$J,358.3,22623,1,5,0)
 ;;=5^PVD
 ;;^UTILITY(U,$J,358.3,22623,2)
 ;;=^184182
 ;;^UTILITY(U,$J,358.3,22624,0)
 ;;=440.21^^179^1525^26
 ;;^UTILITY(U,$J,358.3,22624,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22624,1,4,0)
 ;;=4^440.21
 ;;^UTILITY(U,$J,358.3,22624,1,5,0)
 ;;=5^PVD w/ intermittent claudication
 ;;^UTILITY(U,$J,358.3,22624,2)
 ;;=^293885
 ;;^UTILITY(U,$J,358.3,22625,0)
 ;;=414.00^^179^1525^11
 ;;^UTILITY(U,$J,358.3,22625,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22625,1,4,0)
 ;;=4^414.00
 ;;^UTILITY(U,$J,358.3,22625,1,5,0)
 ;;=5^CAD
 ;;^UTILITY(U,$J,358.3,22625,2)
 ;;=^28512
 ;;^UTILITY(U,$J,358.3,22626,0)
 ;;=433.30^^179^1525^23
 ;;^UTILITY(U,$J,358.3,22626,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22626,1,4,0)
 ;;=4^433.30
 ;;^UTILITY(U,$J,358.3,22626,1,5,0)
 ;;=5^Occ & Sten Mult/Bil Art w/o CRB
 ;;^UTILITY(U,$J,358.3,22626,2)
 ;;=^295803
 ;;^UTILITY(U,$J,358.3,22627,0)
 ;;=433.80^^179^1525^24
 ;;^UTILITY(U,$J,358.3,22627,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22627,1,4,0)
 ;;=4^433.80
 ;;^UTILITY(U,$J,358.3,22627,1,5,0)
 ;;=5^Occ & Sten Precereb Art
 ;;^UTILITY(U,$J,358.3,22627,2)
 ;;=^295804
 ;;^UTILITY(U,$J,358.3,22628,0)
 ;;=444.21^^179^1525^18
 ;;^UTILITY(U,$J,358.3,22628,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22628,1,4,0)
 ;;=4^444.21
 ;;^UTILITY(U,$J,358.3,22628,1,5,0)
 ;;=5^Embolism/Thrombosis UE
 ;;^UTILITY(U,$J,358.3,22628,2)
 ;;=^269789
 ;;^UTILITY(U,$J,358.3,22629,0)
 ;;=444.22^^179^1525^17
 ;;^UTILITY(U,$J,358.3,22629,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22629,1,4,0)
 ;;=4^444.22
 ;;^UTILITY(U,$J,358.3,22629,1,5,0)
 ;;=5^Embolism/Thrombosis LE
 ;;^UTILITY(U,$J,358.3,22629,2)
 ;;=^269790
 ;;^UTILITY(U,$J,358.3,22630,0)
 ;;=454.0^^179^1525^33
 ;;^UTILITY(U,$J,358.3,22630,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22630,1,4,0)
 ;;=4^454.0
 ;;^UTILITY(U,$J,358.3,22630,1,5,0)
 ;;=5^Varicose Vein w/ Ulcer
 ;;^UTILITY(U,$J,358.3,22630,2)
 ;;=^125410
 ;;^UTILITY(U,$J,358.3,22631,0)
 ;;=454.9^^179^1525^32
 ;;^UTILITY(U,$J,358.3,22631,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22631,1,4,0)
 ;;=4^454.9
 ;;^UTILITY(U,$J,358.3,22631,1,5,0)
 ;;=5^Varicose Vein
 ;;^UTILITY(U,$J,358.3,22631,2)
 ;;=^328758
 ;;^UTILITY(U,$J,358.3,22632,0)
 ;;=729.5^^179^1525^21
 ;;^UTILITY(U,$J,358.3,22632,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22632,1,4,0)
 ;;=4^729.5
 ;;^UTILITY(U,$J,358.3,22632,1,5,0)
 ;;=5^Leg Pain
 ;;^UTILITY(U,$J,358.3,22632,2)
 ;;=^89086
 ;;^UTILITY(U,$J,358.3,22633,0)
 ;;=V81.2^^179^1525^28
 ;;^UTILITY(U,$J,358.3,22633,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22633,1,4,0)
 ;;=4^V81.2
 ;;^UTILITY(U,$J,358.3,22633,1,5,0)
 ;;=5^Screen-Cardiovasc NEC
 ;;^UTILITY(U,$J,358.3,22633,2)
 ;;=^295689
 ;;^UTILITY(U,$J,358.3,22634,0)
 ;;=424.0^^179^1525^22
 ;;^UTILITY(U,$J,358.3,22634,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22634,1,4,0)
 ;;=4^424.0
 ;;^UTILITY(U,$J,358.3,22634,1,5,0)
 ;;=5^Mitral Valve Disorders
 ;;^UTILITY(U,$J,358.3,22634,2)
 ;;=^78367
 ;;^UTILITY(U,$J,358.3,22635,0)
 ;;=424.1^^179^1525^10
 ;;^UTILITY(U,$J,358.3,22635,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22635,1,4,0)
 ;;=4^424.1
 ;;^UTILITY(U,$J,358.3,22635,1,5,0)
 ;;=5^Aortic Valve Disorder
 ;;^UTILITY(U,$J,358.3,22635,2)
 ;;=^9330
 ;;^UTILITY(U,$J,358.3,22636,0)
 ;;=424.2^^179^1525^31
 ;;^UTILITY(U,$J,358.3,22636,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22636,1,4,0)
 ;;=4^424.2
 ;;^UTILITY(U,$J,358.3,22636,1,5,0)
 ;;=5^Tricuspid Valve Disorders,nonrheumatic
 ;;^UTILITY(U,$J,358.3,22636,2)
 ;;=^269715
 ;;^UTILITY(U,$J,358.3,22637,0)
 ;;=424.3^^179^1525^27
 ;;^UTILITY(U,$J,358.3,22637,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22637,1,4,0)
 ;;=4^424.3
 ;;^UTILITY(U,$J,358.3,22637,1,5,0)
 ;;=5^Pulmonary Valve Disorder
 ;;^UTILITY(U,$J,358.3,22637,2)
 ;;=^101164
 ;;^UTILITY(U,$J,358.3,22638,0)
 ;;=425.11^^179^1525^19
 ;;^UTILITY(U,$J,358.3,22638,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22638,1,4,0)
 ;;=4^425.11
 ;;^UTILITY(U,$J,358.3,22638,1,5,0)
 ;;=5^Hypertrophic Obs Cardiomyopathy
 ;;^UTILITY(U,$J,358.3,22638,2)
 ;;=^340520
 ;;^UTILITY(U,$J,358.3,22639,0)
 ;;=428.1^^179^1525^20
 ;;^UTILITY(U,$J,358.3,22639,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22639,1,4,0)
 ;;=4^428.1
 ;;^UTILITY(U,$J,358.3,22639,1,5,0)
 ;;=5^Left Heart Failure
 ;;^UTILITY(U,$J,358.3,22639,2)
 ;;=^68721
 ;;^UTILITY(U,$J,358.3,22640,0)
 ;;=428.21^^179^1525^7
 ;;^UTILITY(U,$J,358.3,22640,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22640,1,4,0)
 ;;=4^428.21
 ;;^UTILITY(U,$J,358.3,22640,1,5,0)
 ;;=5^Acute Systolic Ht Failure
 ;;^UTILITY(U,$J,358.3,22640,2)
 ;;=^328494
 ;;^UTILITY(U,$J,358.3,22641,0)
 ;;=428.22^^179^1525^16
 ;;^UTILITY(U,$J,358.3,22641,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22641,1,4,0)
 ;;=4^428.22
 ;;^UTILITY(U,$J,358.3,22641,1,5,0)
 ;;=5^Chr Systolic Ht Failure
 ;;^UTILITY(U,$J,358.3,22641,2)
 ;;=^328495
 ;;^UTILITY(U,$J,358.3,22642,0)
 ;;=428.23^^179^1525^5
 ;;^UTILITY(U,$J,358.3,22642,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22642,1,4,0)
 ;;=4^428.23
 ;;^UTILITY(U,$J,358.3,22642,1,5,0)
 ;;=5^Ac on Chr Sys Ht Failure
 ;;^UTILITY(U,$J,358.3,22642,2)
 ;;=^328496
 ;;^UTILITY(U,$J,358.3,22643,0)
 ;;=428.31^^179^1525^6
 ;;^UTILITY(U,$J,358.3,22643,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22643,1,4,0)
 ;;=4^428.31
 ;;^UTILITY(U,$J,358.3,22643,1,5,0)
 ;;=5^Acute Diastolic Ht Failure
 ;;^UTILITY(U,$J,358.3,22643,2)
 ;;=^328497
 ;;^UTILITY(U,$J,358.3,22644,0)
 ;;=428.32^^179^1525^15
 ;;^UTILITY(U,$J,358.3,22644,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22644,1,4,0)
 ;;=4^428.32
 ;;^UTILITY(U,$J,358.3,22644,1,5,0)
 ;;=5^Chr Diastolic Ht Failure
 ;;^UTILITY(U,$J,358.3,22644,2)
 ;;=^328498
 ;;^UTILITY(U,$J,358.3,22645,0)
 ;;=428.33^^179^1525^4
 ;;^UTILITY(U,$J,358.3,22645,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22645,1,4,0)
 ;;=4^428.33
 ;;^UTILITY(U,$J,358.3,22645,1,5,0)
 ;;=5^Ac on Chr Diastolic Ht Failure
 ;;^UTILITY(U,$J,358.3,22645,2)
 ;;=^328499
 ;;^UTILITY(U,$J,358.3,22646,0)
 ;;=428.41^^179^1525^2
 ;;^UTILITY(U,$J,358.3,22646,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,22646,1,4,0)
 ;;=4^428.41
 ;;^UTILITY(U,$J,358.3,22646,1,5,0)
 ;;=5^Ac Combined Sys & Diast Ht Fail
 ;;^UTILITY(U,$J,358.3,22646,2)
 ;;=^328500
 ;;^UTILITY(U,$J,358.3,22647,0)
 ;;=428.42^^179^1525^14
