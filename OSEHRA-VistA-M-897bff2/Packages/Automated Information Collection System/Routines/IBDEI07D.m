IBDEI07D ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,9684,1,4,0)
 ;;=4^378.53
 ;;^UTILITY(U,$J,358.3,9684,2)
 ;;=Fourth Nerve Palsy^265326
 ;;^UTILITY(U,$J,358.3,9685,0)
 ;;=378.54^^77^660^32
 ;;^UTILITY(U,$J,358.3,9685,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9685,1,3,0)
 ;;=3^Sixth Nerve Palsy
 ;;^UTILITY(U,$J,358.3,9685,1,4,0)
 ;;=4^378.54
 ;;^UTILITY(U,$J,358.3,9685,2)
 ;;=Sixth Nerve Palsy^265334
 ;;^UTILITY(U,$J,358.3,9686,0)
 ;;=378.52^^77^660^33
 ;;^UTILITY(U,$J,358.3,9686,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9686,1,3,0)
 ;;=3^Total Third Nerve Palsy
 ;;^UTILITY(U,$J,358.3,9686,1,4,0)
 ;;=4^378.52
 ;;^UTILITY(U,$J,358.3,9686,2)
 ;;=Total Third Nerve Palsy^269268
 ;;^UTILITY(U,$J,358.3,9687,0)
 ;;=350.9^^77^660^6
 ;;^UTILITY(U,$J,358.3,9687,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9687,1,3,0)
 ;;=3^Fifth Nerve Paresis
 ;;^UTILITY(U,$J,358.3,9687,1,4,0)
 ;;=4^350.9
 ;;^UTILITY(U,$J,358.3,9687,2)
 ;;=Fifth Nerve Palsy^265329
 ;;^UTILITY(U,$J,358.3,9688,0)
 ;;=378.51^^77^660^31
 ;;^UTILITY(U,$J,358.3,9688,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9688,1,3,0)
 ;;=3^Partial Third Nerve Palsy
 ;;^UTILITY(U,$J,358.3,9688,1,4,0)
 ;;=4^378.51
 ;;^UTILITY(U,$J,358.3,9688,2)
 ;;=Partial Third Nerve Palsy^118774
 ;;^UTILITY(U,$J,358.3,9689,0)
 ;;=379.50^^77^660^28
 ;;^UTILITY(U,$J,358.3,9689,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9689,1,3,0)
 ;;=3^Nystagmus
 ;;^UTILITY(U,$J,358.3,9689,1,4,0)
 ;;=4^379.50
 ;;^UTILITY(U,$J,358.3,9689,2)
 ;;=Nystagmus^84761
 ;;^UTILITY(U,$J,358.3,9690,0)
 ;;=802.6^^77^660^15
 ;;^UTILITY(U,$J,358.3,9690,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9690,1,3,0)
 ;;=3^Orbital Floor Fracture
 ;;^UTILITY(U,$J,358.3,9690,1,4,0)
 ;;=4^802.6
 ;;^UTILITY(U,$J,358.3,9690,2)
 ;;=Orbital Floor Fracture^273684
 ;;^UTILITY(U,$J,358.3,9691,0)
 ;;=192.0^^77^660^14
 ;;^UTILITY(U,$J,358.3,9691,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9691,1,3,0)
 ;;=3^Neoplasm, Malign,  Optic Nerve
 ;;^UTILITY(U,$J,358.3,9691,1,4,0)
 ;;=4^192.0
 ;;^UTILITY(U,$J,358.3,9691,2)
 ;;=Mal Neoplasm, Cranial Nerve^267290
 ;;^UTILITY(U,$J,358.3,9692,0)
 ;;=225.1^^77^660^13
 ;;^UTILITY(U,$J,358.3,9692,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9692,1,3,0)
 ;;=3^Neoplasm, Benign, Optic Nerve
 ;;^UTILITY(U,$J,358.3,9692,1,4,0)
 ;;=4^225.1
 ;;^UTILITY(U,$J,358.3,9692,2)
 ;;=Benign Neoplasm Cranial Nerve^13298
 ;;^UTILITY(U,$J,358.3,9693,0)
 ;;=346.90^^77^660^9
 ;;^UTILITY(U,$J,358.3,9693,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9693,1,3,0)
 ;;=3^Headache, Migraine
 ;;^UTILITY(U,$J,358.3,9693,1,4,0)
 ;;=4^346.90
 ;;^UTILITY(U,$J,358.3,9693,2)
 ;;=Headache, Migraine^293880
 ;;^UTILITY(U,$J,358.3,9694,0)
 ;;=784.0^^77^660^8
 ;;^UTILITY(U,$J,358.3,9694,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9694,1,3,0)
 ;;=3^Headache
 ;;^UTILITY(U,$J,358.3,9694,1,4,0)
 ;;=4^784.0
 ;;^UTILITY(U,$J,358.3,9694,2)
 ;;=Headache^54133
 ;;^UTILITY(U,$J,358.3,9695,0)
 ;;=V41.0^^77^660^7
 ;;^UTILITY(U,$J,358.3,9695,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9695,1,3,0)
 ;;=3^Functional Vision Loss
 ;;^UTILITY(U,$J,358.3,9695,1,4,0)
 ;;=4^V41.0
 ;;^UTILITY(U,$J,358.3,9695,2)
 ;;=Problem with sight^295427
 ;;^UTILITY(U,$J,358.3,9696,0)
 ;;=348.2^^77^660^11
 ;;^UTILITY(U,$J,358.3,9696,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9696,1,3,0)
 ;;=3^Intracranial Hypertension
 ;;^UTILITY(U,$J,358.3,9696,1,4,0)
 ;;=4^348.2
 ;;^UTILITY(U,$J,358.3,9696,2)
 ;;=Intracranial Hypertension^100293
 ;;^UTILITY(U,$J,358.3,9697,0)
 ;;=378.50^^77^660^30
 ;;^UTILITY(U,$J,358.3,9697,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9697,1,3,0)
 ;;=3^Ophthalmoplegia, Unspec
 ;;^UTILITY(U,$J,358.3,9697,1,4,0)
 ;;=4^378.50
 ;;^UTILITY(U,$J,358.3,9697,2)
 ;;=Opthalmoplegia, Unspec^265442
 ;;^UTILITY(U,$J,358.3,9698,0)
 ;;=351.8^^77^660^10
 ;;^UTILITY(U,$J,358.3,9698,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9698,1,3,0)
 ;;=3^Hemifacial Spasm
 ;;^UTILITY(U,$J,358.3,9698,1,4,0)
 ;;=4^351.8
 ;;^UTILITY(U,$J,358.3,9698,2)
 ;;=Hemifacial Spasm^87589
 ;;^UTILITY(U,$J,358.3,9699,0)
 ;;=446.5^^77^660^17
 ;;^UTILITY(U,$J,358.3,9699,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9699,1,3,0)
 ;;=3^Temporal Arteritis
 ;;^UTILITY(U,$J,358.3,9699,1,4,0)
 ;;=4^446.5
 ;;^UTILITY(U,$J,358.3,9699,2)
 ;;=Giant Cell Arteritis^117658
 ;;^UTILITY(U,$J,358.3,9700,0)
 ;;=378.73^^77^660^29
 ;;^UTILITY(U,$J,358.3,9700,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9700,1,3,0)
 ;;=3^Ocular Myasthenia Gravis
 ;;^UTILITY(U,$J,358.3,9700,1,4,0)
 ;;=4^378.73
 ;;^UTILITY(U,$J,358.3,9700,2)
 ;;=Ocular Myasthenia Gravis^269274
 ;;^UTILITY(U,$J,358.3,9701,0)
 ;;=377.24^^77^660^25
 ;;^UTILITY(U,$J,358.3,9701,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9701,1,3,0)
 ;;=3^Pseudopapilledema
 ;;^UTILITY(U,$J,358.3,9701,1,4,0)
 ;;=4^377.24
 ;;^UTILITY(U,$J,358.3,9701,2)
 ;;=Pseudopapilledema^269224
 ;;^UTILITY(U,$J,358.3,9702,0)
 ;;=781.0^^77^660^12
 ;;^UTILITY(U,$J,358.3,9702,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9702,1,3,0)
 ;;=3^Lid Myokymia
 ;;^UTILITY(U,$J,358.3,9702,1,4,0)
 ;;=4^781.0
 ;;^UTILITY(U,$J,358.3,9702,2)
 ;;=Lid Myokymia^23827
 ;;^UTILITY(U,$J,358.3,9703,0)
 ;;=368.2^^77^661^7
 ;;^UTILITY(U,$J,358.3,9703,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9703,1,3,0)
 ;;=3^Diplopia
 ;;^UTILITY(U,$J,358.3,9703,1,4,0)
 ;;=4^368.2
 ;;^UTILITY(U,$J,358.3,9703,2)
 ;;=^35208
 ;;^UTILITY(U,$J,358.3,9704,0)
 ;;=695.3^^77^661^1
 ;;^UTILITY(U,$J,358.3,9704,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9704,1,3,0)
 ;;=3^Acne Rosacea
 ;;^UTILITY(U,$J,358.3,9704,1,4,0)
 ;;=4^695.3
 ;;^UTILITY(U,$J,358.3,9704,2)
 ;;=Acne Rosacea^107114
 ;;^UTILITY(U,$J,358.3,9705,0)
 ;;=250.00^^77^661^6
 ;;^UTILITY(U,$J,358.3,9705,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9705,1,3,0)
 ;;=3^DM type II w/o Eye Disease
 ;;^UTILITY(U,$J,358.3,9705,1,4,0)
 ;;=4^250.00
 ;;^UTILITY(U,$J,358.3,9705,2)
 ;;=DM type II w/o Eye Disease^33605
 ;;^UTILITY(U,$J,358.3,9706,0)
 ;;=346.90^^77^661^15
 ;;^UTILITY(U,$J,358.3,9706,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9706,1,3,0)
 ;;=3^Migraine-Unspec.
 ;;^UTILITY(U,$J,358.3,9706,1,4,0)
 ;;=4^346.90
 ;;^UTILITY(U,$J,358.3,9706,2)
 ;;=Migraine without Intract^293880
 ;;^UTILITY(U,$J,358.3,9707,0)
 ;;=376.30^^77^661^8
 ;;^UTILITY(U,$J,358.3,9707,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9707,1,3,0)
 ;;=3^Exophthalmos,NOS
 ;;^UTILITY(U,$J,358.3,9707,1,4,0)
 ;;=4^376.30
 ;;^UTILITY(U,$J,358.3,9707,2)
 ;;=Exophthalmos NOS^43683
 ;;^UTILITY(U,$J,358.3,9708,0)
 ;;=368.8^^77^661^3
 ;;^UTILITY(U,$J,358.3,9708,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9708,1,3,0)
 ;;=3^Blurred Vision
 ;;^UTILITY(U,$J,358.3,9708,1,4,0)
 ;;=4^368.8
 ;;^UTILITY(U,$J,358.3,9708,2)
 ;;=Blurred Vision^88172
 ;;^UTILITY(U,$J,358.3,9709,0)
 ;;=362.34^^77^661^2
 ;;^UTILITY(U,$J,358.3,9709,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9709,1,3,0)
 ;;=3^Amaurosis Fugax
 ;;^UTILITY(U,$J,358.3,9709,1,4,0)
 ;;=4^362.34
 ;;^UTILITY(U,$J,358.3,9709,2)
 ;;=^268622
 ;;^UTILITY(U,$J,358.3,9710,0)
 ;;=368.13^^77^661^17
 ;;^UTILITY(U,$J,358.3,9710,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9710,1,3,0)
 ;;=3^Photophobia
 ;;^UTILITY(U,$J,358.3,9710,1,4,0)
 ;;=4^368.13
 ;;^UTILITY(U,$J,358.3,9710,2)
 ;;=^126851
 ;;^UTILITY(U,$J,358.3,9711,0)
 ;;=368.40^^77^661^23
 ;;^UTILITY(U,$J,358.3,9711,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9711,1,3,0)
 ;;=3^Vis Field Defect
 ;;^UTILITY(U,$J,358.3,9711,1,4,0)
 ;;=4^368.40
 ;;^UTILITY(U,$J,358.3,9711,2)
 ;;=^126859
 ;;^UTILITY(U,$J,358.3,9712,0)
 ;;=369.4^^77^661^14
 ;;^UTILITY(U,$J,358.3,9712,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9712,1,3,0)
 ;;=3^Legal Blindness
 ;;^UTILITY(U,$J,358.3,9712,1,4,0)
 ;;=4^369.4
 ;;^UTILITY(U,$J,358.3,9712,2)
 ;;=Legal Blindness^268887
 ;;^UTILITY(U,$J,358.3,9713,0)
 ;;=250.01^^77^661^5
 ;;^UTILITY(U,$J,358.3,9713,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9713,1,3,0)
 ;;=3^DM Type I, w/o Eye Disease
 ;;^UTILITY(U,$J,358.3,9713,1,4,0)
 ;;=4^250.01
 ;;^UTILITY(U,$J,358.3,9713,2)
 ;;=Diabetes Mellitus Type I^33586
 ;;^UTILITY(U,$J,358.3,9714,0)
 ;;=V08.^^77^661^10
 ;;^UTILITY(U,$J,358.3,9714,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9714,1,3,0)
 ;;=3^HIV Positive
 ;;^UTILITY(U,$J,358.3,9714,1,4,0)
 ;;=4^V08.
 ;;^UTILITY(U,$J,358.3,9714,2)
 ;;=Asymptomatic HIV Status^303392
 ;;^UTILITY(U,$J,358.3,9715,0)
 ;;=921.0^^77^661^4
 ;;^UTILITY(U,$J,358.3,9715,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9715,1,3,0)
 ;;=3^Contusion of Eye
 ;;^UTILITY(U,$J,358.3,9715,1,4,0)
 ;;=4^921.0
 ;;^UTILITY(U,$J,358.3,9715,2)
 ;;=^15052
 ;;^UTILITY(U,$J,358.3,9716,0)
 ;;=379.91^^77^661^16
 ;;^UTILITY(U,$J,358.3,9716,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9716,1,3,0)
 ;;=3^Pain in/around the Eye
 ;;^UTILITY(U,$J,358.3,9716,1,4,0)
 ;;=4^379.91
 ;;^UTILITY(U,$J,358.3,9716,2)
 ;;=Pain in or around eye^89093
 ;;^UTILITY(U,$J,358.3,9717,0)
 ;;=V58.69^^77^661^11
 ;;^UTILITY(U,$J,358.3,9717,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9717,1,3,0)
 ;;=3^Hi-Risk med, L/T
 ;;^UTILITY(U,$J,358.3,9717,1,4,0)
 ;;=4^V58.69
 ;;^UTILITY(U,$J,358.3,9717,2)
 ;;=^303460
 ;;^UTILITY(U,$J,358.3,9718,0)
 ;;=871.4^^77^661^13
 ;;^UTILITY(U,$J,358.3,9718,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9718,1,3,0)
 ;;=3^Laceration of Eyeball
 ;;^UTILITY(U,$J,358.3,9718,1,4,0)
 ;;=4^871.4
 ;;^UTILITY(U,$J,358.3,9718,2)
 ;;=Laceration of Eyeball^274889
 ;;^UTILITY(U,$J,358.3,9719,0)
 ;;=242.90^^77^661^22
 ;;^UTILITY(U,$J,358.3,9719,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9719,1,3,0)
 ;;=3^Thyroid Eye Dis
 ;;^UTILITY(U,$J,358.3,9719,1,4,0)
 ;;=4^242.90
 ;;^UTILITY(U,$J,358.3,9719,2)
 ;;=Thyroid Eye Dis^267811^376.21
 ;;^UTILITY(U,$J,358.3,9720,0)
 ;;=135.^^77^661^20
 ;;^UTILITY(U,$J,358.3,9720,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9720,1,3,0)
 ;;=3^Sarcoidosis
 ;;^UTILITY(U,$J,358.3,9720,1,4,0)
 ;;=4^135.
 ;;^UTILITY(U,$J,358.3,9720,2)
 ;;=Sarcoidosis^107916
