IBDEI09O ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,12836,1,5,0)
 ;;=5^Neurogenic Bladder 
 ;;^UTILITY(U,$J,358.3,12836,2)
 ;;=^304798
 ;;^UTILITY(U,$J,358.3,12837,0)
 ;;=788.43^^105^843^38
 ;;^UTILITY(U,$J,358.3,12837,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12837,1,4,0)
 ;;=4^788.43
 ;;^UTILITY(U,$J,358.3,12837,1,5,0)
 ;;=5^Nocturia
 ;;^UTILITY(U,$J,358.3,12837,2)
 ;;=^84740
 ;;^UTILITY(U,$J,358.3,12838,0)
 ;;=605.^^105^843^43
 ;;^UTILITY(U,$J,358.3,12838,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12838,1,4,0)
 ;;=4^605.
 ;;^UTILITY(U,$J,358.3,12838,1,5,0)
 ;;=5^Phimosis
 ;;^UTILITY(U,$J,358.3,12838,2)
 ;;=^104159
 ;;^UTILITY(U,$J,358.3,12839,0)
 ;;=601.0^^105^843^45
 ;;^UTILITY(U,$J,358.3,12839,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12839,1,4,0)
 ;;=4^601.0
 ;;^UTILITY(U,$J,358.3,12839,1,5,0)
 ;;=5^Prostatitis, Acute
 ;;^UTILITY(U,$J,358.3,12839,2)
 ;;=^259106
 ;;^UTILITY(U,$J,358.3,12840,0)
 ;;=590.10^^105^843^47
 ;;^UTILITY(U,$J,358.3,12840,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12840,1,4,0)
 ;;=4^590.10
 ;;^UTILITY(U,$J,358.3,12840,1,5,0)
 ;;=5^Pyelonephritis, Acute
 ;;^UTILITY(U,$J,358.3,12840,2)
 ;;=^270369
 ;;^UTILITY(U,$J,358.3,12841,0)
 ;;=590.00^^105^843^48
 ;;^UTILITY(U,$J,358.3,12841,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12841,1,4,0)
 ;;=4^590.00
 ;;^UTILITY(U,$J,358.3,12841,1,5,0)
 ;;=5^Pyelonephritis, Chronic
 ;;^UTILITY(U,$J,358.3,12841,2)
 ;;=^270367
 ;;^UTILITY(U,$J,358.3,12842,0)
 ;;=788.7^^105^843^54
 ;;^UTILITY(U,$J,358.3,12842,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12842,1,4,0)
 ;;=4^788.7
 ;;^UTILITY(U,$J,358.3,12842,1,5,0)
 ;;=5^Urethral Discharge
 ;;^UTILITY(U,$J,358.3,12842,2)
 ;;=^265872
 ;;^UTILITY(U,$J,358.3,12843,0)
 ;;=131.02^^105^843^57
 ;;^UTILITY(U,$J,358.3,12843,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12843,1,4,0)
 ;;=4^131.02
 ;;^UTILITY(U,$J,358.3,12843,1,5,0)
 ;;=5^Urethritis, Trichomonal
 ;;^UTILITY(U,$J,358.3,12843,2)
 ;;=^266955
 ;;^UTILITY(U,$J,358.3,12844,0)
 ;;=597.80^^105^843^56
 ;;^UTILITY(U,$J,358.3,12844,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12844,1,4,0)
 ;;=4^597.80
 ;;^UTILITY(U,$J,358.3,12844,1,5,0)
 ;;=5^Urethritis, NOS
 ;;^UTILITY(U,$J,358.3,12844,2)
 ;;=Urethritis, NOS^124214
 ;;^UTILITY(U,$J,358.3,12845,0)
 ;;=788.41^^105^843^58
 ;;^UTILITY(U,$J,358.3,12845,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12845,1,4,0)
 ;;=4^788.41
 ;;^UTILITY(U,$J,358.3,12845,1,5,0)
 ;;=5^Urinary Frequency
 ;;^UTILITY(U,$J,358.3,12845,2)
 ;;=^124396
 ;;^UTILITY(U,$J,358.3,12846,0)
 ;;=788.31^^105^843^61
 ;;^UTILITY(U,$J,358.3,12846,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12846,1,4,0)
 ;;=4^788.31
 ;;^UTILITY(U,$J,358.3,12846,1,5,0)
 ;;=5^Urinary Incontinence, Urge
 ;;^UTILITY(U,$J,358.3,12846,2)
 ;;=^260046
 ;;^UTILITY(U,$J,358.3,12847,0)
 ;;=788.20^^105^843^62
 ;;^UTILITY(U,$J,358.3,12847,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12847,1,4,0)
 ;;=4^788.20
 ;;^UTILITY(U,$J,358.3,12847,1,5,0)
 ;;=5^Urinary Retention
 ;;^UTILITY(U,$J,358.3,12847,2)
 ;;=^295812
 ;;^UTILITY(U,$J,358.3,12848,0)
 ;;=112.2^^105^843^64
 ;;^UTILITY(U,$J,358.3,12848,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12848,1,4,0)
 ;;=4^112.2
 ;;^UTILITY(U,$J,358.3,12848,1,5,0)
 ;;=5^Urogenital Candidiasis
 ;;^UTILITY(U,$J,358.3,12848,2)
 ;;=^266866
 ;;^UTILITY(U,$J,358.3,12849,0)
 ;;=600.01^^105^843^5
 ;;^UTILITY(U,$J,358.3,12849,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12849,1,4,0)
 ;;=4^600.01
 ;;^UTILITY(U,$J,358.3,12849,1,5,0)
 ;;=5^BPH with Obstruction
 ;;^UTILITY(U,$J,358.3,12849,2)
 ;;=^329933
 ;;^UTILITY(U,$J,358.3,12850,0)
 ;;=600.10^^105^843^39
 ;;^UTILITY(U,$J,358.3,12850,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12850,1,4,0)
 ;;=4^600.10
 ;;^UTILITY(U,$J,358.3,12850,1,5,0)
 ;;=5^Nod Prostate W/O UR Obst
 ;;^UTILITY(U,$J,358.3,12850,2)
 ;;=^329934
 ;;^UTILITY(U,$J,358.3,12851,0)
 ;;=585.9^^105^843^8
 ;;^UTILITY(U,$J,358.3,12851,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12851,1,4,0)
 ;;=4^585.9
 ;;^UTILITY(U,$J,358.3,12851,1,5,0)
 ;;=5^Chronic Renal Failure
 ;;^UTILITY(U,$J,358.3,12851,2)
 ;;=^332812
 ;;^UTILITY(U,$J,358.3,12852,0)
 ;;=788.64^^105^843^59
 ;;^UTILITY(U,$J,358.3,12852,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12852,1,4,0)
 ;;=4^788.64
 ;;^UTILITY(U,$J,358.3,12852,1,5,0)
 ;;=5^Urinary Hesitancy
 ;;^UTILITY(U,$J,358.3,12852,2)
 ;;=^334165
 ;;^UTILITY(U,$J,358.3,12853,0)
 ;;=788.65^^105^843^52
 ;;^UTILITY(U,$J,358.3,12853,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12853,1,4,0)
 ;;=4^788.65
 ;;^UTILITY(U,$J,358.3,12853,1,5,0)
 ;;=5^Straining of Urination
 ;;^UTILITY(U,$J,358.3,12853,2)
 ;;=^334166
 ;;^UTILITY(U,$J,358.3,12854,0)
 ;;=600.00^^105^843^4
 ;;^UTILITY(U,$J,358.3,12854,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12854,1,4,0)
 ;;=4^600.00
 ;;^UTILITY(U,$J,358.3,12854,1,5,0)
 ;;=5^BPH w/o Obstruction
 ;;^UTILITY(U,$J,358.3,12854,2)
 ;;=^334276
 ;;^UTILITY(U,$J,358.3,12855,0)
 ;;=599.70^^105^843^17
 ;;^UTILITY(U,$J,358.3,12855,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12855,1,4,0)
 ;;=4^599.70
 ;;^UTILITY(U,$J,358.3,12855,1,5,0)
 ;;=5^Hematuria
 ;;^UTILITY(U,$J,358.3,12855,2)
 ;;=^336751
 ;;^UTILITY(U,$J,358.3,12856,0)
 ;;=599.71^^105^843^18
 ;;^UTILITY(U,$J,358.3,12856,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12856,1,4,0)
 ;;=4^599.71
 ;;^UTILITY(U,$J,358.3,12856,1,5,0)
 ;;=5^Hematuria, Gross
 ;;^UTILITY(U,$J,358.3,12856,2)
 ;;=^336611
 ;;^UTILITY(U,$J,358.3,12857,0)
 ;;=599.72^^105^843^19
 ;;^UTILITY(U,$J,358.3,12857,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12857,1,4,0)
 ;;=4^599.72
 ;;^UTILITY(U,$J,358.3,12857,1,5,0)
 ;;=5^Hematuria, Microscopic
 ;;^UTILITY(U,$J,358.3,12857,2)
 ;;=^336612
 ;;^UTILITY(U,$J,358.3,12858,0)
 ;;=596.81^^105^843^33
 ;;^UTILITY(U,$J,358.3,12858,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12858,1,4,0)
 ;;=4^596.81
 ;;^UTILITY(U,$J,358.3,12858,1,5,0)
 ;;=5^Infection of cystostomy
 ;;^UTILITY(U,$J,358.3,12858,2)
 ;;=^340556
 ;;^UTILITY(U,$J,358.3,12859,0)
 ;;=596.82^^105^843^35
 ;;^UTILITY(U,$J,358.3,12859,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12859,1,4,0)
 ;;=4^596.82
 ;;^UTILITY(U,$J,358.3,12859,1,5,0)
 ;;=5^Mech Comp of Cystostomy
 ;;^UTILITY(U,$J,358.3,12859,2)
 ;;=^340557
 ;;^UTILITY(U,$J,358.3,12860,0)
 ;;=596.83^^105^843^41
 ;;^UTILITY(U,$J,358.3,12860,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12860,1,4,0)
 ;;=4^596.83
 ;;^UTILITY(U,$J,358.3,12860,1,5,0)
 ;;=5^Other Comp of Cystostomy
 ;;^UTILITY(U,$J,358.3,12860,2)
 ;;=^340558
 ;;^UTILITY(U,$J,358.3,12861,0)
 ;;=596.89^^105^843^42
 ;;^UTILITY(U,$J,358.3,12861,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12861,1,4,0)
 ;;=4^596.89
 ;;^UTILITY(U,$J,358.3,12861,1,5,0)
 ;;=5^Other Specified Disorders of Bladder
 ;;^UTILITY(U,$J,358.3,12861,2)
 ;;=^87989
 ;;^UTILITY(U,$J,358.3,12862,0)
 ;;=626.9^^105^844^29
 ;;^UTILITY(U,$J,358.3,12862,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12862,1,4,0)
 ;;=4^626.9
 ;;^UTILITY(U,$J,358.3,12862,1,5,0)
 ;;=5^Menstrual disorder
 ;;^UTILITY(U,$J,358.3,12862,2)
 ;;=^123887
 ;;^UTILITY(U,$J,358.3,12863,0)
 ;;=626.0^^105^844^3
 ;;^UTILITY(U,$J,358.3,12863,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12863,1,4,0)
 ;;=4^626.0
 ;;^UTILITY(U,$J,358.3,12863,1,5,0)
 ;;=5^Amenorrhea
 ;;^UTILITY(U,$J,358.3,12863,2)
 ;;=Amenorrhea^5871
 ;;^UTILITY(U,$J,358.3,12864,0)
 ;;=628.0^^105^844^4
 ;;^UTILITY(U,$J,358.3,12864,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12864,1,4,0)
 ;;=4^628.0
 ;;^UTILITY(U,$J,358.3,12864,1,5,0)
 ;;=5^Anovulatory cyclic bleeding
 ;;^UTILITY(U,$J,358.3,12864,2)
 ;;=^270583
 ;;^UTILITY(U,$J,358.3,12865,0)
 ;;=616.3^^105^844^5
 ;;^UTILITY(U,$J,358.3,12865,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12865,1,4,0)
 ;;=4^616.3
 ;;^UTILITY(U,$J,358.3,12865,1,5,0)
 ;;=5^Bartholin's abscess
 ;;^UTILITY(U,$J,358.3,12865,2)
 ;;=^12748
 ;;^UTILITY(U,$J,358.3,12866,0)
 ;;=610.0^^105^844^8
 ;;^UTILITY(U,$J,358.3,12866,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12866,1,4,0)
 ;;=4^610.0
 ;;^UTILITY(U,$J,358.3,12866,1,5,0)
 ;;=5^Breast, solitary cyst
 ;;^UTILITY(U,$J,358.3,12866,2)
 ;;=^112247
 ;;^UTILITY(U,$J,358.3,12867,0)
 ;;=610.1^^105^844^20
 ;;^UTILITY(U,$J,358.3,12867,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12867,1,4,0)
 ;;=4^610.1
 ;;^UTILITY(U,$J,358.3,12867,1,5,0)
 ;;=5^Fibrocystic breast disease
 ;;^UTILITY(U,$J,358.3,12867,2)
 ;;=^46167
 ;;^UTILITY(U,$J,358.3,12868,0)
 ;;=611.71^^105^844^6
 ;;^UTILITY(U,$J,358.3,12868,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12868,1,4,0)
 ;;=4^611.71
 ;;^UTILITY(U,$J,358.3,12868,1,5,0)
 ;;=5^Breast Pain
 ;;^UTILITY(U,$J,358.3,12868,2)
 ;;=Breast Pain^74467
 ;;^UTILITY(U,$J,358.3,12869,0)
 ;;=611.72^^105^844^7
 ;;^UTILITY(U,$J,358.3,12869,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12869,1,4,0)
 ;;=4^611.72
 ;;^UTILITY(U,$J,358.3,12869,1,5,0)
 ;;=5^Breast, lump/mass
 ;;^UTILITY(U,$J,358.3,12869,2)
 ;;=^72018
 ;;^UTILITY(U,$J,358.3,12870,0)
 ;;=616.0^^105^844^9
 ;;^UTILITY(U,$J,358.3,12870,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12870,1,4,0)
 ;;=4^616.0
 ;;^UTILITY(U,$J,358.3,12870,1,5,0)
 ;;=5^Cervicitis
 ;;^UTILITY(U,$J,358.3,12870,2)
 ;;=Cervicitis^21925
 ;;^UTILITY(U,$J,358.3,12871,0)
 ;;=078.11^^105^844^10
 ;;^UTILITY(U,$J,358.3,12871,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12871,1,4,0)
 ;;=4^078.11
 ;;^UTILITY(U,$J,358.3,12871,1,5,0)
 ;;=5^Condyloma Acuminatum
 ;;^UTILITY(U,$J,358.3,12871,2)
 ;;=^295788
 ;;^UTILITY(U,$J,358.3,12872,0)
 ;;=V25.09^^105^844^11
 ;;^UTILITY(U,$J,358.3,12872,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12872,1,4,0)
 ;;=4^V25.09
 ;;^UTILITY(U,$J,358.3,12872,1,5,0)
 ;;=5^Contraceptive counseling
 ;;^UTILITY(U,$J,358.3,12872,2)
 ;;=^87608
 ;;^UTILITY(U,$J,358.3,12873,0)
 ;;=V25.9^^105^844^12
 ;;^UTILITY(U,$J,358.3,12873,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,12873,1,4,0)
 ;;=4^V25.9
 ;;^UTILITY(U,$J,358.3,12873,1,5,0)
 ;;=5^Contraceptive management
 ;;^UTILITY(U,$J,358.3,12873,2)
 ;;=^276356
