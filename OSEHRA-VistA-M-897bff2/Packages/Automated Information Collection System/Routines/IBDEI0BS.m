IBDEI0BS ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,15686,1,3,0)
 ;;=3^28011
 ;;^UTILITY(U,$J,358.3,15687,0)
 ;;=28020^^114^977^7^^^^1
 ;;^UTILITY(U,$J,358.3,15687,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15687,1,2,0)
 ;;=2^Arthrotomy, including exploration, drainage, or removal of loose or foreign body; intertarsal or tarsometatarsal joint
 ;;^UTILITY(U,$J,358.3,15687,1,3,0)
 ;;=3^28020
 ;;^UTILITY(U,$J,358.3,15688,0)
 ;;=28022^^114^977^8^^^^1
 ;;^UTILITY(U,$J,358.3,15688,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15688,1,2,0)
 ;;=2^Arthrotomy, including exploration, drainage, or removal of loose or foreign body; metatarsophalangeal joint 
 ;;^UTILITY(U,$J,358.3,15688,1,3,0)
 ;;=3^28022
 ;;^UTILITY(U,$J,358.3,15689,0)
 ;;=28024^^114^977^9^^^^1
 ;;^UTILITY(U,$J,358.3,15689,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15689,1,2,0)
 ;;=2^Arthrotomy, including exploration, drainage, or removal of loose or foreign body; interphalangeal joint
 ;;^UTILITY(U,$J,358.3,15689,1,3,0)
 ;;=3^28024
 ;;^UTILITY(U,$J,358.3,15690,0)
 ;;=28035^^114^977^11^^^^1
 ;;^UTILITY(U,$J,358.3,15690,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15690,1,2,0)
 ;;=2^Release, tarsal tunnel
 ;;^UTILITY(U,$J,358.3,15690,1,3,0)
 ;;=3^28035
 ;;^UTILITY(U,$J,358.3,15691,0)
 ;;=28055^^114^977^10^^^^1
 ;;^UTILITY(U,$J,358.3,15691,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15691,1,2,0)
 ;;=2^Neurectomy, Foot
 ;;^UTILITY(U,$J,358.3,15691,1,3,0)
 ;;=3^28055
 ;;^UTILITY(U,$J,358.3,15692,0)
 ;;=28043^^114^978^1^^^^1
 ;;^UTILITY(U,$J,358.3,15692,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15692,1,2,0)
 ;;=2^Excision Tumor-Foot,SQ Tissue >1.5cm
 ;;^UTILITY(U,$J,358.3,15692,1,3,0)
 ;;=3^28043
 ;;^UTILITY(U,$J,358.3,15693,0)
 ;;=28045^^114^978^2^^^^1
 ;;^UTILITY(U,$J,358.3,15693,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15693,1,2,0)
 ;;=2^Excision Tumor-Foot,Deep Subfascial >1.5cm
 ;;^UTILITY(U,$J,358.3,15693,1,3,0)
 ;;=3^28045
 ;;^UTILITY(U,$J,358.3,15694,0)
 ;;=28050^^114^978^3^^^^1
 ;;^UTILITY(U,$J,358.3,15694,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15694,1,2,0)
 ;;=2^Arthrotomy with biopsy; intertarsal or tarsometatarsal joint 
 ;;^UTILITY(U,$J,358.3,15694,1,3,0)
 ;;=3^28050
 ;;^UTILITY(U,$J,358.3,15695,0)
 ;;=28052^^114^978^4^^^^1
 ;;^UTILITY(U,$J,358.3,15695,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15695,1,2,0)
 ;;=2^Arthrotomy with biopsy; metatarsophalangeal joint 
 ;;^UTILITY(U,$J,358.3,15695,1,3,0)
 ;;=3^28052
 ;;^UTILITY(U,$J,358.3,15696,0)
 ;;=28054^^114^978^5^^^^1
 ;;^UTILITY(U,$J,358.3,15696,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15696,1,2,0)
 ;;=2^Arthrotomy with biopsy; interphalangeal joint
 ;;^UTILITY(U,$J,358.3,15696,1,3,0)
 ;;=3^28054
 ;;^UTILITY(U,$J,358.3,15697,0)
 ;;=28060^^114^978^6^^^^1
 ;;^UTILITY(U,$J,358.3,15697,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15697,1,2,0)
 ;;=2^Fasciectomy, plantar fascia; partial 
 ;;^UTILITY(U,$J,358.3,15697,1,3,0)
 ;;=3^28060
 ;;^UTILITY(U,$J,358.3,15698,0)
 ;;=28062^^114^978^7^^^^1
 ;;^UTILITY(U,$J,358.3,15698,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15698,1,2,0)
 ;;=2^Fasciectomy, plantar fascia; radical
 ;;^UTILITY(U,$J,358.3,15698,1,3,0)
 ;;=3^28062
 ;;^UTILITY(U,$J,358.3,15699,0)
 ;;=28080^^114^978^8^^^^1
 ;;^UTILITY(U,$J,358.3,15699,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15699,1,2,0)
 ;;=2^Excision, interdigital (Morton) neuroma, single, each
 ;;^UTILITY(U,$J,358.3,15699,1,3,0)
 ;;=3^28080
 ;;^UTILITY(U,$J,358.3,15700,0)
 ;;=28090^^114^978^9^^^^1
 ;;^UTILITY(U,$J,358.3,15700,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15700,1,2,0)
 ;;=2^Excision of lesion, tendon, tendon sheath, or capsule; foot
 ;;^UTILITY(U,$J,358.3,15700,1,3,0)
 ;;=3^28090
 ;;^UTILITY(U,$J,358.3,15701,0)
 ;;=28092^^114^978^10^^^^1
 ;;^UTILITY(U,$J,358.3,15701,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15701,1,2,0)
 ;;=2^Excision of lesion, tendon, tendon sheath, or capsule; toe(s), each
 ;;^UTILITY(U,$J,358.3,15701,1,3,0)
 ;;=3^28092
 ;;^UTILITY(U,$J,358.3,15702,0)
 ;;=28100^^114^978^11^^^^1
 ;;^UTILITY(U,$J,358.3,15702,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15702,1,2,0)
 ;;=2^Excision or Curettage of bone cyst or benign tumor, talus or calcaneus
 ;;^UTILITY(U,$J,358.3,15702,1,3,0)
 ;;=3^28100
 ;;^UTILITY(U,$J,358.3,15703,0)
 ;;=28102^^114^978^12^^^^1
 ;;^UTILITY(U,$J,358.3,15703,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15703,1,2,0)
 ;;=2^Excision or Curettage of bone cyst or benign tumor, talus or calcaneus; with iliac or other autograft (includes obtaining graft)
 ;;^UTILITY(U,$J,358.3,15703,1,3,0)
 ;;=3^28102
 ;;^UTILITY(U,$J,358.3,15704,0)
 ;;=28103^^114^978^13^^^^1
 ;;^UTILITY(U,$J,358.3,15704,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15704,1,2,0)
 ;;=2^Excision or Curettage of bone cyst or benign tumor, talus or calcaneus; with allograft
 ;;^UTILITY(U,$J,358.3,15704,1,3,0)
 ;;=3^28103
 ;;^UTILITY(U,$J,358.3,15705,0)
 ;;=28104^^114^978^14^^^^1
 ;;^UTILITY(U,$J,358.3,15705,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15705,1,2,0)
 ;;=2^Excision or Curettage of bone cyst or benign tumor, tarsal or metatarsal, except talus or calcaneus 
 ;;^UTILITY(U,$J,358.3,15705,1,3,0)
 ;;=3^28104
 ;;^UTILITY(U,$J,358.3,15706,0)
 ;;=28106^^114^978^15^^^^1
 ;;^UTILITY(U,$J,358.3,15706,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15706,1,2,0)
 ;;=2^Excision or Curettage of bone cyst or benign tumor, tarsal or metatarsal, except talus or calcaneus; with iliac or other autograft
 ;;^UTILITY(U,$J,358.3,15706,1,3,0)
 ;;=3^28106
 ;;^UTILITY(U,$J,358.3,15707,0)
 ;;=28107^^114^978^16^^^^1
 ;;^UTILITY(U,$J,358.3,15707,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15707,1,2,0)
 ;;=2^Excision or Curettage of bone cyst of benign tumor, tarsal or metatarsal, except talus or calcaneus; with allograft
 ;;^UTILITY(U,$J,358.3,15707,1,3,0)
 ;;=3^28107
 ;;^UTILITY(U,$J,358.3,15708,0)
 ;;=28108^^114^978^17^^^^1
 ;;^UTILITY(U,$J,358.3,15708,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15708,1,2,0)
 ;;=2^Excision or Curettage of bone cyst or benign tumor, phalanges of foot
 ;;^UTILITY(U,$J,358.3,15708,1,3,0)
 ;;=3^28108
 ;;^UTILITY(U,$J,358.3,15709,0)
 ;;=28110^^114^978^18^^^^1
 ;;^UTILITY(U,$J,358.3,15709,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15709,1,2,0)
 ;;=2^Ostectomy, partial excision, fifth metararsal head
 ;;^UTILITY(U,$J,358.3,15709,1,3,0)
 ;;=3^28110
 ;;^UTILITY(U,$J,358.3,15710,0)
 ;;=28111^^114^978^19^^^^1
 ;;^UTILITY(U,$J,358.3,15710,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15710,1,2,0)
 ;;=2^Ostectomy, complete excision; 1st metatarsal head
 ;;^UTILITY(U,$J,358.3,15710,1,3,0)
 ;;=3^28111
 ;;^UTILITY(U,$J,358.3,15711,0)
 ;;=28112^^114^978^20^^^^1
 ;;^UTILITY(U,$J,358.3,15711,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15711,1,2,0)
 ;;=2^Ostectomy, complete excision; other metatarsal head (2nd, 3rd, 4th)
 ;;^UTILITY(U,$J,358.3,15711,1,3,0)
 ;;=3^28112
 ;;^UTILITY(U,$J,358.3,15712,0)
 ;;=28113^^114^978^21^^^^1
 ;;^UTILITY(U,$J,358.3,15712,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15712,1,2,0)
 ;;=2^Ostectomy, complete excision; 5th metatarsal head
 ;;^UTILITY(U,$J,358.3,15712,1,3,0)
 ;;=3^28113
 ;;^UTILITY(U,$J,358.3,15713,0)
 ;;=28114^^114^978^22^^^^1
 ;;^UTILITY(U,$J,358.3,15713,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15713,1,2,0)
 ;;=2^Ostectomy, complete excision; all metatarsal heads, with partial proximal phalangectomy, excluding first metatarsal 
 ;;^UTILITY(U,$J,358.3,15713,1,3,0)
 ;;=3^28114
 ;;^UTILITY(U,$J,358.3,15714,0)
 ;;=28140^^114^978^23^^^^1
 ;;^UTILITY(U,$J,358.3,15714,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15714,1,2,0)
 ;;=2^Metatarsectomy
 ;;^UTILITY(U,$J,358.3,15714,1,3,0)
 ;;=3^28140
 ;;^UTILITY(U,$J,358.3,15715,0)
 ;;=28119^^114^978^24^^^^1
 ;;^UTILITY(U,$J,358.3,15715,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15715,1,2,0)
 ;;=2^Ostectomy, calcaneus; for spur, with or without plantar fascial release
 ;;^UTILITY(U,$J,358.3,15715,1,3,0)
 ;;=3^28119
 ;;^UTILITY(U,$J,358.3,15716,0)
 ;;=28120^^114^978^25^^^^1
 ;;^UTILITY(U,$J,358.3,15716,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15716,1,2,0)
 ;;=2^Partial excision bone; talus or calcaneus
 ;;^UTILITY(U,$J,358.3,15716,1,3,0)
 ;;=3^28120
 ;;^UTILITY(U,$J,358.3,15717,0)
 ;;=28122^^114^978^26^^^^1
 ;;^UTILITY(U,$J,358.3,15717,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15717,1,2,0)
 ;;=2^Partial excision bone; tarsal or metatarsal bone, except talus or calcaneus
 ;;^UTILITY(U,$J,358.3,15717,1,3,0)
 ;;=3^28122
 ;;^UTILITY(U,$J,358.3,15718,0)
 ;;=28124^^114^978^27^^^^1
 ;;^UTILITY(U,$J,358.3,15718,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15718,1,2,0)
 ;;=2^Partial excision bone; phalanx of toe
 ;;^UTILITY(U,$J,358.3,15718,1,3,0)
 ;;=3^28124
 ;;^UTILITY(U,$J,358.3,15719,0)
 ;;=28153^^114^978^28^^^^1
 ;;^UTILITY(U,$J,358.3,15719,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15719,1,2,0)
 ;;=2^Resection, condyle(s), distal end of phalanx, each toe
 ;;^UTILITY(U,$J,358.3,15719,1,3,0)
 ;;=3^28153
 ;;^UTILITY(U,$J,358.3,15720,0)
 ;;=28160^^114^978^29^^^^1
 ;;^UTILITY(U,$J,358.3,15720,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15720,1,2,0)
 ;;=2^Hemiphalangectomy or interphalangeal joint excision, toe, proximal end of phalanx, each
 ;;^UTILITY(U,$J,358.3,15720,1,3,0)
 ;;=3^28160
 ;;^UTILITY(U,$J,358.3,15721,0)
 ;;=64774^^114^978^30^^^^1
 ;;^UTILITY(U,$J,358.3,15721,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15721,1,2,0)
 ;;=2^Excision of neuroma; cutaneous nerve, surgically identifiable
 ;;^UTILITY(U,$J,358.3,15721,1,3,0)
 ;;=3^64774
 ;;^UTILITY(U,$J,358.3,15722,0)
 ;;=64776^^114^978^31^^^^1
 ;;^UTILITY(U,$J,358.3,15722,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15722,1,2,0)
 ;;=2^Excision of neuroma; digital nerve, one or both, same digit
 ;;^UTILITY(U,$J,358.3,15722,1,3,0)
 ;;=3^64776
 ;;^UTILITY(U,$J,358.3,15723,0)
 ;;=64778^^114^978^32^^^^1
