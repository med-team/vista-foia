IBDEI0HN ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,23818,0)
 ;;=782.1^^192^1635^7
 ;;^UTILITY(U,$J,358.3,23818,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23818,1,3,0)
 ;;=3^782.1
 ;;^UTILITY(U,$J,358.3,23818,1,4,0)
 ;;=4^Rash, Nonspecif Skin Eruption
 ;;^UTILITY(U,$J,358.3,23818,2)
 ;;=^102948
 ;;^UTILITY(U,$J,358.3,23819,0)
 ;;=707.10^^192^1635^5
 ;;^UTILITY(U,$J,358.3,23819,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23819,1,3,0)
 ;;=3^707.10
 ;;^UTILITY(U,$J,358.3,23819,1,4,0)
 ;;=4^Leg Ulcer, lower limb, chronic
 ;;^UTILITY(U,$J,358.3,23819,2)
 ;;=^322142
 ;;^UTILITY(U,$J,358.3,23820,0)
 ;;=702.0^^192^1635^1
 ;;^UTILITY(U,$J,358.3,23820,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23820,1,3,0)
 ;;=3^702.0
 ;;^UTILITY(U,$J,358.3,23820,1,4,0)
 ;;=4^Actinic Keratosis
 ;;^UTILITY(U,$J,358.3,23820,2)
 ;;=^66900
 ;;^UTILITY(U,$J,358.3,23821,0)
 ;;=696.1^^192^1635^6
 ;;^UTILITY(U,$J,358.3,23821,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23821,1,3,0)
 ;;=3^696.1
 ;;^UTILITY(U,$J,358.3,23821,1,4,0)
 ;;=4^Psoriasis
 ;;^UTILITY(U,$J,358.3,23821,2)
 ;;=^87816
 ;;^UTILITY(U,$J,358.3,23822,0)
 ;;=795.00^^192^1636^2
 ;;^UTILITY(U,$J,358.3,23822,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23822,1,3,0)
 ;;=3^795.00
 ;;^UTILITY(U,$J,358.3,23822,1,4,0)
 ;;=4^Abnormal Pap
 ;;^UTILITY(U,$J,358.3,23822,2)
 ;;=^328609
 ;;^UTILITY(U,$J,358.3,23823,0)
 ;;=V25.09^^192^1636^16
 ;;^UTILITY(U,$J,358.3,23823,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23823,1,3,0)
 ;;=3^V25.09
 ;;^UTILITY(U,$J,358.3,23823,1,4,0)
 ;;=4^Contraceptive Mgmt (1St Visit)
 ;;^UTILITY(U,$J,358.3,23823,2)
 ;;=^87608
 ;;^UTILITY(U,$J,358.3,23824,0)
 ;;=V25.8^^192^1636^17
 ;;^UTILITY(U,$J,358.3,23824,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23824,1,3,0)
 ;;=3^V25.8
 ;;^UTILITY(U,$J,358.3,23824,1,4,0)
 ;;=4^Contraceptive Mgmt (F/U Visit)
 ;;^UTILITY(U,$J,358.3,23824,2)
 ;;=^295353
 ;;^UTILITY(U,$J,358.3,23825,0)
 ;;=626.8^^192^1636^18
 ;;^UTILITY(U,$J,358.3,23825,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23825,1,3,0)
 ;;=3^626.8
 ;;^UTILITY(U,$J,358.3,23825,1,4,0)
 ;;=4^Dysfunctional Bleeding
 ;;^UTILITY(U,$J,358.3,23825,2)
 ;;=^87521
 ;;^UTILITY(U,$J,358.3,23826,0)
 ;;=628.9^^192^1636^25
 ;;^UTILITY(U,$J,358.3,23826,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23826,1,3,0)
 ;;=3^628.9
 ;;^UTILITY(U,$J,358.3,23826,1,4,0)
 ;;=4^Female Infertility Nos
 ;;^UTILITY(U,$J,358.3,23826,2)
 ;;=^62820
 ;;^UTILITY(U,$J,358.3,23827,0)
 ;;=610.1^^192^1636^26
 ;;^UTILITY(U,$J,358.3,23827,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23827,1,3,0)
 ;;=3^610.1
 ;;^UTILITY(U,$J,358.3,23827,1,4,0)
 ;;=4^Fibrocystic Breast
 ;;^UTILITY(U,$J,358.3,23827,2)
 ;;=^46167
 ;;^UTILITY(U,$J,358.3,23828,0)
 ;;=V10.3^^192^1636^29
 ;;^UTILITY(U,$J,358.3,23828,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23828,1,3,0)
 ;;=3^V10.3
 ;;^UTILITY(U,$J,358.3,23828,1,4,0)
 ;;=4^Hx Of Breast Cancer
 ;;^UTILITY(U,$J,358.3,23828,2)
 ;;=^295217
 ;;^UTILITY(U,$J,358.3,23829,0)
 ;;=V10.43^^192^1636^30
 ;;^UTILITY(U,$J,358.3,23829,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23829,1,3,0)
 ;;=3^V10.43
 ;;^UTILITY(U,$J,358.3,23829,1,4,0)
 ;;=4^Hx Of Ovarian Cancer
 ;;^UTILITY(U,$J,358.3,23829,2)
 ;;=^295221
 ;;^UTILITY(U,$J,358.3,23830,0)
 ;;=611.72^^192^1636^32
 ;;^UTILITY(U,$J,358.3,23830,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23830,1,3,0)
 ;;=3^611.72
 ;;^UTILITY(U,$J,358.3,23830,1,4,0)
 ;;=4^Lump Or Mass In Breast
 ;;^UTILITY(U,$J,358.3,23830,2)
 ;;=^72018
 ;;^UTILITY(U,$J,358.3,23831,0)
 ;;=625.9^^192^1636^37
 ;;^UTILITY(U,$J,358.3,23831,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23831,1,3,0)
 ;;=3^625.9
 ;;^UTILITY(U,$J,358.3,23831,1,4,0)
 ;;=4^Pelvic Pain
 ;;^UTILITY(U,$J,358.3,23831,2)
 ;;=^123993
 ;;^UTILITY(U,$J,358.3,23832,0)
 ;;=627.1^^192^1636^38
 ;;^UTILITY(U,$J,358.3,23832,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23832,1,3,0)
 ;;=3^627.1
 ;;^UTILITY(U,$J,358.3,23832,1,4,0)
 ;;=4^Postmenopausal Bleeding
 ;;^UTILITY(U,$J,358.3,23832,2)
 ;;=^97040
 ;;^UTILITY(U,$J,358.3,23833,0)
 ;;=616.10^^192^1636^47
 ;;^UTILITY(U,$J,358.3,23833,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23833,1,3,0)
 ;;=3^616.10
 ;;^UTILITY(U,$J,358.3,23833,1,4,0)
 ;;=4^Vaginitis/Vaginal Infection
 ;;^UTILITY(U,$J,358.3,23833,2)
 ;;=^125233
 ;;^UTILITY(U,$J,358.3,23834,0)
 ;;=626.0^^192^1636^8
 ;;^UTILITY(U,$J,358.3,23834,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23834,1,3,0)
 ;;=3^626.0
 ;;^UTILITY(U,$J,358.3,23834,1,4,0)
 ;;=4^Amenorrhea
 ;;^UTILITY(U,$J,358.3,23834,2)
 ;;=^5871
 ;;^UTILITY(U,$J,358.3,23835,0)
 ;;=783.0^^192^1636^9
 ;;^UTILITY(U,$J,358.3,23835,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23835,1,3,0)
 ;;=3^783.0
 ;;^UTILITY(U,$J,358.3,23835,1,4,0)
 ;;=4^Anorexia
 ;;^UTILITY(U,$J,358.3,23835,2)
 ;;=^7939
 ;;^UTILITY(U,$J,358.3,23836,0)
 ;;=V76.10^^192^1636^11
 ;;^UTILITY(U,$J,358.3,23836,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23836,1,3,0)
 ;;=3^V76.10
 ;;^UTILITY(U,$J,358.3,23836,1,4,0)
 ;;=4^Breast Cancer Screening
 ;;^UTILITY(U,$J,358.3,23836,2)
 ;;=^317968
 ;;^UTILITY(U,$J,358.3,23837,0)
 ;;=616.0^^192^1636^14
 ;;^UTILITY(U,$J,358.3,23837,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23837,1,3,0)
 ;;=3^616.0
 ;;^UTILITY(U,$J,358.3,23837,1,4,0)
 ;;=4^Cervicitis
 ;;^UTILITY(U,$J,358.3,23837,2)
 ;;=^21925
 ;;^UTILITY(U,$J,358.3,23838,0)
 ;;=078.10^^192^1636^27
 ;;^UTILITY(U,$J,358.3,23838,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23838,1,3,0)
 ;;=3^078.10
 ;;^UTILITY(U,$J,358.3,23838,1,4,0)
 ;;=4^Genital Warts
 ;;^UTILITY(U,$J,358.3,23838,2)
 ;;=^295787
 ;;^UTILITY(U,$J,358.3,23839,0)
 ;;=625.3^^192^1636^19
 ;;^UTILITY(U,$J,358.3,23839,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23839,1,3,0)
 ;;=3^625.3
 ;;^UTILITY(U,$J,358.3,23839,1,4,0)
 ;;=4^Dysmenorrhea
 ;;^UTILITY(U,$J,358.3,23839,2)
 ;;=^37592
 ;;^UTILITY(U,$J,358.3,23840,0)
 ;;=625.0^^192^1636^20
 ;;^UTILITY(U,$J,358.3,23840,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23840,1,3,0)
 ;;=3^625.0
 ;;^UTILITY(U,$J,358.3,23840,1,4,0)
 ;;=4^Dyspareunia
 ;;^UTILITY(U,$J,358.3,23840,2)
 ;;=^37604
 ;;^UTILITY(U,$J,358.3,23841,0)
 ;;=788.1^^192^1636^24
 ;;^UTILITY(U,$J,358.3,23841,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23841,1,3,0)
 ;;=3^788.1
 ;;^UTILITY(U,$J,358.3,23841,1,4,0)
 ;;=4^Dysuria
 ;;^UTILITY(U,$J,358.3,23841,2)
 ;;=^37716
 ;;^UTILITY(U,$J,358.3,23842,0)
 ;;=789.30^^192^1636^35
 ;;^UTILITY(U,$J,358.3,23842,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23842,1,3,0)
 ;;=3^789.30
 ;;^UTILITY(U,$J,358.3,23842,1,4,0)
 ;;=4^Pelvic Abdominal Mass
 ;;^UTILITY(U,$J,358.3,23842,2)
 ;;=^917
 ;;^UTILITY(U,$J,358.3,23843,0)
 ;;=614.9^^192^1636^36
 ;;^UTILITY(U,$J,358.3,23843,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23843,1,3,0)
 ;;=3^614.9
 ;;^UTILITY(U,$J,358.3,23843,1,4,0)
 ;;=4^Pelvic Inflammatory Disease
 ;;^UTILITY(U,$J,358.3,23843,2)
 ;;=^3537
 ;;^UTILITY(U,$J,358.3,23844,0)
 ;;=599.0^^192^1636^42
 ;;^UTILITY(U,$J,358.3,23844,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23844,1,3,0)
 ;;=3^599.0
 ;;^UTILITY(U,$J,358.3,23844,1,4,0)
 ;;=4^Urin Tract Infection Nos
 ;;^UTILITY(U,$J,358.3,23844,2)
 ;;=^124436
 ;;^UTILITY(U,$J,358.3,23845,0)
 ;;=218.9^^192^1636^43
 ;;^UTILITY(U,$J,358.3,23845,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23845,1,3,0)
 ;;=3^218.9
 ;;^UTILITY(U,$J,358.3,23845,1,4,0)
 ;;=4^Uterine Fibroids
 ;;^UTILITY(U,$J,358.3,23845,2)
 ;;=^68944
 ;;^UTILITY(U,$J,358.3,23846,0)
 ;;=618.1^^192^1636^44
 ;;^UTILITY(U,$J,358.3,23846,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23846,1,3,0)
 ;;=3^618.1
 ;;^UTILITY(U,$J,358.3,23846,1,4,0)
 ;;=4^Uterine Prolapse
 ;;^UTILITY(U,$J,358.3,23846,2)
 ;;=^124773
 ;;^UTILITY(U,$J,358.3,23847,0)
 ;;=112.1^^192^1636^45
 ;;^UTILITY(U,$J,358.3,23847,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23847,1,3,0)
 ;;=3^112.1
 ;;^UTILITY(U,$J,358.3,23847,1,4,0)
 ;;=4^Vaginitis, Candidal
 ;;^UTILITY(U,$J,358.3,23847,2)
 ;;=^18615
 ;;^UTILITY(U,$J,358.3,23848,0)
 ;;=099.53^^192^1636^48
 ;;^UTILITY(U,$J,358.3,23848,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23848,1,3,0)
 ;;=3^099.53
 ;;^UTILITY(U,$J,358.3,23848,1,4,0)
 ;;=4^Vaginits, Chlamydial
 ;;^UTILITY(U,$J,358.3,23848,2)
 ;;=^293988
 ;;^UTILITY(U,$J,358.3,23849,0)
 ;;=131.01^^192^1636^46
 ;;^UTILITY(U,$J,358.3,23849,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23849,1,3,0)
 ;;=3^131.01
 ;;^UTILITY(U,$J,358.3,23849,1,4,0)
 ;;=4^Vaginitis, Trichonomal
 ;;^UTILITY(U,$J,358.3,23849,2)
 ;;=^121763
 ;;^UTILITY(U,$J,358.3,23850,0)
 ;;=592.0^^192^1636^31
 ;;^UTILITY(U,$J,358.3,23850,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23850,1,3,0)
 ;;=3^592.0
 ;;^UTILITY(U,$J,358.3,23850,1,4,0)
 ;;=4^Kidney Stones
 ;;^UTILITY(U,$J,358.3,23850,2)
 ;;=^67056
 ;;^UTILITY(U,$J,358.3,23851,0)
 ;;=627.2^^192^1636^41
 ;;^UTILITY(U,$J,358.3,23851,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23851,1,3,0)
 ;;=3^627.2
 ;;^UTILITY(U,$J,358.3,23851,1,4,0)
 ;;=4^Symptomatic Menopause
 ;;^UTILITY(U,$J,358.3,23851,2)
 ;;=^328735
 ;;^UTILITY(U,$J,358.3,23852,0)
 ;;=622.10^^192^1636^21
 ;;^UTILITY(U,$J,358.3,23852,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23852,1,3,0)
 ;;=3^622.10
 ;;^UTILITY(U,$J,358.3,23852,1,4,0)
 ;;=4^Dysplasia of Cervix
 ;;^UTILITY(U,$J,358.3,23852,2)
 ;;=^331546
 ;;^UTILITY(U,$J,358.3,23853,0)
 ;;=622.11^^192^1636^22
 ;;^UTILITY(U,$J,358.3,23853,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23853,1,3,0)
 ;;=3^622.11
 ;;^UTILITY(U,$J,358.3,23853,1,4,0)
 ;;=4^Dysplasia of Cervix, Mild
 ;;^UTILITY(U,$J,358.3,23853,2)
 ;;=^331547
 ;;^UTILITY(U,$J,358.3,23854,0)
 ;;=622.12^^192^1636^23
 ;;^UTILITY(U,$J,358.3,23854,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23854,1,3,0)
 ;;=3^622.12
 ;;^UTILITY(U,$J,358.3,23854,1,4,0)
 ;;=4^Dysplasia of Cervix, Moderate
 ;;^UTILITY(U,$J,358.3,23854,2)
 ;;=^331548
 ;;^UTILITY(U,$J,358.3,23855,0)
 ;;=V72.40^^192^1636^39
 ;;^UTILITY(U,$J,358.3,23855,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,23855,1,3,0)
 ;;=3^V72.40
