IBDEI0BY ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,15910,2)
 ;;=^275767
 ;;^UTILITY(U,$J,358.3,15911,0)
 ;;=945.23^^116^992^12
 ;;^UTILITY(U,$J,358.3,15911,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15911,1,3,0)
 ;;=3^945.23
 ;;^UTILITY(U,$J,358.3,15911,1,5,0)
 ;;=5^Burn, 2nd Degree (blister), ankle
 ;;^UTILITY(U,$J,358.3,15911,2)
 ;;=^275768
 ;;^UTILITY(U,$J,358.3,15912,0)
 ;;=945.24^^116^992^14
 ;;^UTILITY(U,$J,358.3,15912,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15912,1,3,0)
 ;;=3^945.24
 ;;^UTILITY(U,$J,358.3,15912,1,5,0)
 ;;=5^Burn, 2nd Degree (blister), lower leg
 ;;^UTILITY(U,$J,358.3,15912,2)
 ;;=^275769
 ;;^UTILITY(U,$J,358.3,15913,0)
 ;;=945.31^^116^992^19
 ;;^UTILITY(U,$J,358.3,15913,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15913,1,3,0)
 ;;=3^945.31
 ;;^UTILITY(U,$J,358.3,15913,1,5,0)
 ;;=5^Burn, 3rd Degree (full thickness), toe(s)(nail)
 ;;^UTILITY(U,$J,358.3,15913,2)
 ;;=^275775
 ;;^UTILITY(U,$J,358.3,15914,0)
 ;;=945.34^^116^992^18
 ;;^UTILITY(U,$J,358.3,15914,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15914,1,3,0)
 ;;=3^945.34
 ;;^UTILITY(U,$J,358.3,15914,1,5,0)
 ;;=5^Burn, 3rd Degree (full thickness), lower leg
 ;;^UTILITY(U,$J,358.3,15914,2)
 ;;=^275778
 ;;^UTILITY(U,$J,358.3,15915,0)
 ;;=945.32^^116^992^17
 ;;^UTILITY(U,$J,358.3,15915,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15915,1,3,0)
 ;;=3^945.32
 ;;^UTILITY(U,$J,358.3,15915,1,5,0)
 ;;=5^Burn, 3rd Degree (full thickness), foot 
 ;;^UTILITY(U,$J,358.3,15915,2)
 ;;=^275776
 ;;^UTILITY(U,$J,358.3,15916,0)
 ;;=945.33^^116^992^16
 ;;^UTILITY(U,$J,358.3,15916,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15916,1,3,0)
 ;;=3^945.33
 ;;^UTILITY(U,$J,358.3,15916,1,5,0)
 ;;=5^Burn, 3rd Degree (full thickness), ankle
 ;;^UTILITY(U,$J,358.3,15916,2)
 ;;=^275777
 ;;^UTILITY(U,$J,358.3,15917,0)
 ;;=948.00^^116^992^21
 ;;^UTILITY(U,$J,358.3,15917,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15917,1,3,0)
 ;;=3^948.00
 ;;^UTILITY(U,$J,358.3,15917,1,5,0)
 ;;=5^Burn, Any Degree, <10% or unspecified of body surface
 ;;^UTILITY(U,$J,358.3,15917,2)
 ;;=^275814
 ;;^UTILITY(U,$J,358.3,15918,0)
 ;;=948.10^^116^992^20
 ;;^UTILITY(U,$J,358.3,15918,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15918,1,3,0)
 ;;=3^948.10
 ;;^UTILITY(U,$J,358.3,15918,1,5,0)
 ;;=5^Burn, Any Degree, 10-19% of body surface
 ;;^UTILITY(U,$J,358.3,15918,2)
 ;;=^275816
 ;;^UTILITY(U,$J,358.3,15919,0)
 ;;=782.0^^116^992^27
 ;;^UTILITY(U,$J,358.3,15919,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15919,1,3,0)
 ;;=3^782.0
 ;;^UTILITY(U,$J,358.3,15919,1,5,0)
 ;;=5^Burning skin sensation
 ;;^UTILITY(U,$J,358.3,15919,2)
 ;;=^35757
 ;;^UTILITY(U,$J,358.3,15920,0)
 ;;=726.79^^116^992^28
 ;;^UTILITY(U,$J,358.3,15920,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15920,1,3,0)
 ;;=3^726.79
 ;;^UTILITY(U,$J,358.3,15920,1,5,0)
 ;;=5^Bursitis-foot/toe/ankle/calcaneal
 ;;^UTILITY(U,$J,358.3,15920,2)
 ;;=^272555
 ;;^UTILITY(U,$J,358.3,15921,0)
 ;;=266.2^^116^992^1
 ;;^UTILITY(U,$J,358.3,15921,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15921,1,3,0)
 ;;=3^266.2
 ;;^UTILITY(U,$J,358.3,15921,1,5,0)
 ;;=5^B12 Deficiency
 ;;^UTILITY(U,$J,358.3,15921,2)
 ;;=^87347
 ;;^UTILITY(U,$J,358.3,15922,0)
 ;;=754.62^^116^993^1
 ;;^UTILITY(U,$J,358.3,15922,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15922,1,3,0)
 ;;=3^754.62
 ;;^UTILITY(U,$J,358.3,15922,1,5,0)
 ;;=5^Calcaneovalgus, talipes
 ;;^UTILITY(U,$J,358.3,15922,2)
 ;;=^265474
 ;;^UTILITY(U,$J,358.3,15923,0)
 ;;=736.76^^116^993^2
 ;;^UTILITY(U,$J,358.3,15923,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15923,1,3,0)
 ;;=3^736.76
 ;;^UTILITY(U,$J,358.3,15923,1,5,0)
 ;;=5^Calcaneovalgus, talipes, acquired
 ;;^UTILITY(U,$J,358.3,15923,2)
 ;;=^272748
 ;;^UTILITY(U,$J,358.3,15924,0)
 ;;=754.59^^116^993^3
 ;;^UTILITY(U,$J,358.3,15924,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15924,1,3,0)
 ;;=3^754.59
 ;;^UTILITY(U,$J,358.3,15924,1,5,0)
 ;;=5^Calcaneovarus, talipes
 ;;^UTILITY(U,$J,358.3,15924,2)
 ;;=^273008
 ;;^UTILITY(U,$J,358.3,15925,0)
 ;;=V64.2^^116^993^6
 ;;^UTILITY(U,$J,358.3,15925,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15925,1,3,0)
 ;;=3^V64.2
 ;;^UTILITY(U,$J,358.3,15925,1,5,0)
 ;;=5^Cancelled surgical or other procedures because of patient decision 
 ;;^UTILITY(U,$J,358.3,15925,2)
 ;;=^295559
 ;;^UTILITY(U,$J,358.3,15926,0)
 ;;=V64.1^^116^993^5
 ;;^UTILITY(U,$J,358.3,15926,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15926,1,3,0)
 ;;=3^V64.1
 ;;^UTILITY(U,$J,358.3,15926,1,5,0)
 ;;=5^Cancelled surgical or other procedure because of contraindication
 ;;^UTILITY(U,$J,358.3,15926,2)
 ;;=^295558
 ;;^UTILITY(U,$J,358.3,15927,0)
 ;;=V64.3^^116^993^4
 ;;^UTILITY(U,$J,358.3,15927,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15927,1,3,0)
 ;;=3^V64.3
 ;;^UTILITY(U,$J,358.3,15927,1,5,0)
 ;;=5^Cancelled procedure because of other reasons
 ;;^UTILITY(U,$J,358.3,15927,2)
 ;;=^295560
 ;;^UTILITY(U,$J,358.3,15928,0)
 ;;=726.79^^116^993^7
 ;;^UTILITY(U,$J,358.3,15928,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15928,1,3,0)
 ;;=3^726.79
 ;;^UTILITY(U,$J,358.3,15928,1,5,0)
 ;;=5^Capsulitis of ankle/toe/foot
 ;;^UTILITY(U,$J,358.3,15928,2)
 ;;=^272555
 ;;^UTILITY(U,$J,358.3,15929,0)
 ;;=736.75^^116^993^8
 ;;^UTILITY(U,$J,358.3,15929,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15929,1,3,0)
 ;;=3^736.75
 ;;^UTILITY(U,$J,358.3,15929,1,5,0)
 ;;=5^Cavovarus deformity of foot/ankle acquired
 ;;^UTILITY(U,$J,358.3,15929,2)
 ;;=^272747
 ;;^UTILITY(U,$J,358.3,15930,0)
 ;;=681.10^^116^993^11
 ;;^UTILITY(U,$J,358.3,15930,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15930,1,3,0)
 ;;=3^681.10
 ;;^UTILITY(U,$J,358.3,15930,1,5,0)
 ;;=5^Cellulitis and abscess, toe, acquired
 ;;^UTILITY(U,$J,358.3,15930,2)
 ;;=^271885
 ;;^UTILITY(U,$J,358.3,15931,0)
 ;;=682.7^^116^993^9
 ;;^UTILITY(U,$J,358.3,15931,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15931,1,3,0)
 ;;=3^682.7
 ;;^UTILITY(U,$J,358.3,15931,1,5,0)
 ;;=5^Cellulitis and abscess, foot, except toes
 ;;^UTILITY(U,$J,358.3,15931,2)
 ;;=^271895
 ;;^UTILITY(U,$J,358.3,15932,0)
 ;;=682.6^^116^993^10
 ;;^UTILITY(U,$J,358.3,15932,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15932,1,3,0)
 ;;=3^682.6
 ;;^UTILITY(U,$J,358.3,15932,1,5,0)
 ;;=5^Cellulitis and abscess, leg, except foot
 ;;^UTILITY(U,$J,358.3,15932,2)
 ;;=^271894
 ;;^UTILITY(U,$J,358.3,15933,0)
 ;;=094.0^^116^993^12
 ;;^UTILITY(U,$J,358.3,15933,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15933,1,3,0)
 ;;=3^094.0
 ;;^UTILITY(U,$J,358.3,15933,1,5,0)
 ;;=5^Charcot's joint disease (713.5)
 ;;^UTILITY(U,$J,358.3,15933,2)
 ;;=^117008
 ;;^UTILITY(U,$J,358.3,15934,0)
 ;;=443.9^^116^993^13
 ;;^UTILITY(U,$J,358.3,15934,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15934,1,3,0)
 ;;=3^443.9
 ;;^UTILITY(U,$J,358.3,15934,1,5,0)
 ;;=5^Claudication, intermittent
 ;;^UTILITY(U,$J,358.3,15934,2)
 ;;=^184182
 ;;^UTILITY(U,$J,358.3,15935,0)
 ;;=440.21^^116^993^14
 ;;^UTILITY(U,$J,358.3,15935,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15935,1,3,0)
 ;;=3^440.21
 ;;^UTILITY(U,$J,358.3,15935,1,5,0)
 ;;=5^Claudication, intermittent due to arteriosclerosis
 ;;^UTILITY(U,$J,358.3,15935,2)
 ;;=^293885
 ;;^UTILITY(U,$J,358.3,15936,0)
 ;;=735.5^^116^993^16
 ;;^UTILITY(U,$J,358.3,15936,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15936,1,3,0)
 ;;=3^735.5
 ;;^UTILITY(U,$J,358.3,15936,1,5,0)
 ;;=5^Claw toe, acquired
 ;;^UTILITY(U,$J,358.3,15936,2)
 ;;=^272713
 ;;^UTILITY(U,$J,358.3,15937,0)
 ;;=754.71^^116^993^17
 ;;^UTILITY(U,$J,358.3,15937,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15937,1,3,0)
 ;;=3^754.71
 ;;^UTILITY(U,$J,358.3,15937,1,5,0)
 ;;=5^Claw toe, congential
 ;;^UTILITY(U,$J,358.3,15937,2)
 ;;=^117165
 ;;^UTILITY(U,$J,358.3,15938,0)
 ;;=736.71^^116^993^18
 ;;^UTILITY(U,$J,358.3,15938,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15938,1,3,0)
 ;;=3^736.71
 ;;^UTILITY(U,$J,358.3,15938,1,5,0)
 ;;=5^Clubfoot, acquired
 ;;^UTILITY(U,$J,358.3,15938,2)
 ;;=^272743
 ;;^UTILITY(U,$J,358.3,15939,0)
 ;;=729.89^^116^993^19
 ;;^UTILITY(U,$J,358.3,15939,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15939,1,3,0)
 ;;=3^729.89
 ;;^UTILITY(U,$J,358.3,15939,1,5,0)
 ;;=5^Cold feet or toes- chronic
 ;;^UTILITY(U,$J,358.3,15939,2)
 ;;=^87720
 ;;^UTILITY(U,$J,358.3,15940,0)
 ;;=958.8^^116^993^21
 ;;^UTILITY(U,$J,358.3,15940,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15940,1,3,0)
 ;;=3^958.8
 ;;^UTILITY(U,$J,358.3,15940,1,5,0)
 ;;=5^Compartmental syndrome, traumatic
 ;;^UTILITY(U,$J,358.3,15940,2)
 ;;=^87560
 ;;^UTILITY(U,$J,358.3,15941,0)
 ;;=996.67^^116^993^23
 ;;^UTILITY(U,$J,358.3,15941,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15941,1,3,0)
 ;;=3^996.67
 ;;^UTILITY(U,$J,358.3,15941,1,5,0)
 ;;=5^Complication of internal orthopedic device, implant, and graft; infection and inflammation
 ;;^UTILITY(U,$J,358.3,15941,2)
 ;;=^276290
 ;;^UTILITY(U,$J,358.3,15942,0)
 ;;=996.78^^116^993^22
 ;;^UTILITY(U,$J,358.3,15942,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15942,1,3,0)
 ;;=3^996.78
 ;;^UTILITY(U,$J,358.3,15942,1,5,0)
 ;;=5^Complication of internal orthepedic device, implant, & graft; bleeding/foreign body/pain/granuloma
 ;;^UTILITY(U,$J,358.3,15942,2)
 ;;=^276301
 ;;^UTILITY(U,$J,358.3,15943,0)
 ;;=718.47^^116^993^24
 ;;^UTILITY(U,$J,358.3,15943,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15943,1,3,0)
 ;;=3^718.47
 ;;^UTILITY(U,$J,358.3,15943,1,5,0)
 ;;=5^Contracture of ankle/foot/toe joint
 ;;^UTILITY(U,$J,358.3,15943,2)
 ;;=^272324
 ;;^UTILITY(U,$J,358.3,15944,0)
 ;;=924.20^^116^993^26
 ;;^UTILITY(U,$J,358.3,15944,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15944,1,3,0)
 ;;=3^924.20
 ;;^UTILITY(U,$J,358.3,15944,1,5,0)
 ;;=5^Contusion of foot or heel
 ;;^UTILITY(U,$J,358.3,15944,2)
 ;;=^275423
 ;;^UTILITY(U,$J,358.3,15945,0)
 ;;=924.21^^116^993^25
 ;;^UTILITY(U,$J,358.3,15945,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15945,1,3,0)
 ;;=3^924.21
 ;;^UTILITY(U,$J,358.3,15945,1,5,0)
 ;;=5^Contusion of ankle
