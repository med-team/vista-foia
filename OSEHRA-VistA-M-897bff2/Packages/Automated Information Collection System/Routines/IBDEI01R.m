IBDEI01R ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,1771,0)
 ;;=722.2^^21^130^21
 ;;^UTILITY(U,$J,358.3,1771,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1771,1,2,0)
 ;;=2^722.2
 ;;^UTILITY(U,$J,358.3,1771,1,5,0)
 ;;=5^HERNIATED DISC
 ;;^UTILITY(U,$J,358.3,1771,2)
 ;;=^35649
 ;;^UTILITY(U,$J,358.3,1772,0)
 ;;=053.9^^21^130^22
 ;;^UTILITY(U,$J,358.3,1772,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1772,1,2,0)
 ;;=2^053.9
 ;;^UTILITY(U,$J,358.3,1772,1,5,0)
 ;;=5^HERPES ZOSTER NOS
 ;;^UTILITY(U,$J,358.3,1772,2)
 ;;=^56946
 ;;^UTILITY(U,$J,358.3,1773,0)
 ;;=V66.7^^21^130^24
 ;;^UTILITY(U,$J,358.3,1773,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1773,1,2,0)
 ;;=2^V66.7
 ;;^UTILITY(U,$J,358.3,1773,1,5,0)
 ;;=5^HOSPICE CARE
 ;;^UTILITY(U,$J,358.3,1773,2)
 ;;=^89209
 ;;^UTILITY(U,$J,358.3,1774,0)
 ;;=V08.^^21^130^23
 ;;^UTILITY(U,$J,358.3,1774,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1774,1,2,0)
 ;;=2^V08.
 ;;^UTILITY(U,$J,358.3,1774,1,5,0)
 ;;=5^HIV,ASYMPTOMATIC
 ;;^UTILITY(U,$J,358.3,1774,2)
 ;;=^303392
 ;;^UTILITY(U,$J,358.3,1775,0)
 ;;=603.9^^21^130^25
 ;;^UTILITY(U,$J,358.3,1775,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1775,1,2,0)
 ;;=2^603.9
 ;;^UTILITY(U,$J,358.3,1775,1,5,0)
 ;;=5^HYDROCELE NOS
 ;;^UTILITY(U,$J,358.3,1775,2)
 ;;=^59548
 ;;^UTILITY(U,$J,358.3,1776,0)
 ;;=272.0^^21^130^26
 ;;^UTILITY(U,$J,358.3,1776,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1776,1,2,0)
 ;;=2^272.0
 ;;^UTILITY(U,$J,358.3,1776,1,5,0)
 ;;=5^HYPERCHOLESTEROLEM
 ;;^UTILITY(U,$J,358.3,1776,2)
 ;;=^59973
 ;;^UTILITY(U,$J,358.3,1777,0)
 ;;=272.4^^21^130^27
 ;;^UTILITY(U,$J,358.3,1777,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1777,1,2,0)
 ;;=2^272.4
 ;;^UTILITY(U,$J,358.3,1777,1,5,0)
 ;;=5^HYPERLIPIDEMIA NEC/NOS
 ;;^UTILITY(U,$J,358.3,1777,2)
 ;;=^87281
 ;;^UTILITY(U,$J,358.3,1778,0)
 ;;=276.7^^21^130^28
 ;;^UTILITY(U,$J,358.3,1778,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1778,1,2,0)
 ;;=2^276.7
 ;;^UTILITY(U,$J,358.3,1778,1,5,0)
 ;;=5^HYPERPOTASSEMIA
 ;;^UTILITY(U,$J,358.3,1778,2)
 ;;=^60042
 ;;^UTILITY(U,$J,358.3,1779,0)
 ;;=401.9^^21^130^29
 ;;^UTILITY(U,$J,358.3,1779,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1779,1,2,0)
 ;;=2^401.9
 ;;^UTILITY(U,$J,358.3,1779,1,5,0)
 ;;=5^HYPERTENSION NOS
 ;;^UTILITY(U,$J,358.3,1779,2)
 ;;=^186630
 ;;^UTILITY(U,$J,358.3,1780,0)
 ;;=401.1^^21^130^30
 ;;^UTILITY(U,$J,358.3,1780,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1780,1,2,0)
 ;;=2^401.1
 ;;^UTILITY(U,$J,358.3,1780,1,5,0)
 ;;=5^HYPERTENSION,BENIGN
 ;;^UTILITY(U,$J,358.3,1780,2)
 ;;=^269591
 ;;^UTILITY(U,$J,358.3,1781,0)
 ;;=401.0^^21^130^31
 ;;^UTILITY(U,$J,358.3,1781,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1781,1,2,0)
 ;;=2^401.0
 ;;^UTILITY(U,$J,358.3,1781,1,5,0)
 ;;=5^HYPERTENSION,MALIGNANT
 ;;^UTILITY(U,$J,358.3,1781,2)
 ;;=^73505
 ;;^UTILITY(U,$J,358.3,1782,0)
 ;;=242.90^^21^130^32
 ;;^UTILITY(U,$J,358.3,1782,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1782,1,2,0)
 ;;=2^242.90
 ;;^UTILITY(U,$J,358.3,1782,1,5,0)
 ;;=5^HYPERTHYROIDISM
 ;;^UTILITY(U,$J,358.3,1782,2)
 ;;=^267811
 ;;^UTILITY(U,$J,358.3,1783,0)
 ;;=272.1^^21^130^33
 ;;^UTILITY(U,$J,358.3,1783,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1783,1,2,0)
 ;;=2^272.1
 ;;^UTILITY(U,$J,358.3,1783,1,5,0)
 ;;=5^HYPERTRIGLYCERIDEMIA
 ;;^UTILITY(U,$J,358.3,1783,2)
 ;;=^101303
 ;;^UTILITY(U,$J,358.3,1784,0)
 ;;=251.2^^21^130^34
 ;;^UTILITY(U,$J,358.3,1784,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1784,1,2,0)
 ;;=2^251.2
 ;;^UTILITY(U,$J,358.3,1784,1,5,0)
 ;;=5^HYPOGLYCEMIA NOS
 ;;^UTILITY(U,$J,358.3,1784,2)
 ;;=^60580
 ;;^UTILITY(U,$J,358.3,1785,0)
 ;;=276.1^^21^130^35
 ;;^UTILITY(U,$J,358.3,1785,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1785,1,2,0)
 ;;=2^276.1
 ;;^UTILITY(U,$J,358.3,1785,1,5,0)
 ;;=5^HYPONATREMIA
 ;;^UTILITY(U,$J,358.3,1785,2)
 ;;=^60722
 ;;^UTILITY(U,$J,358.3,1786,0)
 ;;=276.8^^21^130^36
 ;;^UTILITY(U,$J,358.3,1786,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1786,1,2,0)
 ;;=2^276.8
 ;;^UTILITY(U,$J,358.3,1786,1,5,0)
 ;;=5^HYPOPOTASSEMIA
 ;;^UTILITY(U,$J,358.3,1786,2)
 ;;=^60611
 ;;^UTILITY(U,$J,358.3,1787,0)
 ;;=458.9^^21^130^37
 ;;^UTILITY(U,$J,358.3,1787,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1787,1,2,0)
 ;;=2^458.9
 ;;^UTILITY(U,$J,358.3,1787,1,5,0)
 ;;=5^HYPOTENSION NOS
 ;;^UTILITY(U,$J,358.3,1787,2)
 ;;=^60729
 ;;^UTILITY(U,$J,358.3,1788,0)
 ;;=244.9^^21^130^38
 ;;^UTILITY(U,$J,358.3,1788,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1788,1,2,0)
 ;;=2^244.9
 ;;^UTILITY(U,$J,358.3,1788,1,5,0)
 ;;=5^HYPOTHYROIDISM NOS
 ;;^UTILITY(U,$J,358.3,1788,2)
 ;;=^123752
 ;;^UTILITY(U,$J,358.3,1789,0)
 ;;=380.4^^21^131^1
 ;;^UTILITY(U,$J,358.3,1789,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1789,1,2,0)
 ;;=2^380.4
 ;;^UTILITY(U,$J,358.3,1789,1,5,0)
 ;;=5^IMPACTED CERUMEN
 ;;^UTILITY(U,$J,358.3,1789,2)
 ;;=^62061
 ;;^UTILITY(U,$J,358.3,1790,0)
 ;;=607.84^^21^131^2
 ;;^UTILITY(U,$J,358.3,1790,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1790,1,2,0)
 ;;=2^607.84
 ;;^UTILITY(U,$J,358.3,1790,1,5,0)
 ;;=5^IMPOTENCE
 ;;^UTILITY(U,$J,358.3,1790,2)
 ;;=^270441
 ;;^UTILITY(U,$J,358.3,1791,0)
 ;;=998.59^^21^131^3
 ;;^UTILITY(U,$J,358.3,1791,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1791,1,2,0)
 ;;=2^998.59
 ;;^UTILITY(U,$J,358.3,1791,1,5,0)
 ;;=5^INFECTION,POST-OP
 ;;^UTILITY(U,$J,358.3,1791,2)
 ;;=^97081
 ;;^UTILITY(U,$J,358.3,1792,0)
 ;;=134.9^^21^131^4
 ;;^UTILITY(U,$J,358.3,1792,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1792,1,2,0)
 ;;=2^134.9
 ;;^UTILITY(U,$J,358.3,1792,1,5,0)
 ;;=5^INFECTION,SKIN
 ;;^UTILITY(U,$J,358.3,1792,2)
 ;;=^90104
 ;;^UTILITY(U,$J,358.3,1793,0)
 ;;=487.1^^21^131^5
 ;;^UTILITY(U,$J,358.3,1793,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1793,1,2,0)
 ;;=2^487.1
 ;;^UTILITY(U,$J,358.3,1793,1,5,0)
 ;;=5^INFLUENZA
 ;;^UTILITY(U,$J,358.3,1793,2)
 ;;=^63125
 ;;^UTILITY(U,$J,358.3,1794,0)
 ;;=780.52^^21^131^6
 ;;^UTILITY(U,$J,358.3,1794,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1794,1,2,0)
 ;;=2^780.52
 ;;^UTILITY(U,$J,358.3,1794,1,5,0)
 ;;=5^INSOMNIA NOS
 ;;^UTILITY(U,$J,358.3,1794,2)
 ;;=^332924
 ;;^UTILITY(U,$J,358.3,1795,0)
 ;;=564.1^^21^131^7
 ;;^UTILITY(U,$J,358.3,1795,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1795,1,2,0)
 ;;=2^564.1
 ;;^UTILITY(U,$J,358.3,1795,1,5,0)
 ;;=5^IRRITABLE BOWEL SYNDROME
 ;;^UTILITY(U,$J,358.3,1795,2)
 ;;=^65682
 ;;^UTILITY(U,$J,358.3,1796,0)
 ;;=782.4^^21^132^1
 ;;^UTILITY(U,$J,358.3,1796,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1796,1,2,0)
 ;;=2^782.4
 ;;^UTILITY(U,$J,358.3,1796,1,5,0)
 ;;=5^JAUNDICE NOS
 ;;^UTILITY(U,$J,358.3,1796,2)
 ;;=^66155
 ;;^UTILITY(U,$J,358.3,1797,0)
 ;;=110.3^^21^132^2
 ;;^UTILITY(U,$J,358.3,1797,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1797,1,2,0)
 ;;=2^110.3
 ;;^UTILITY(U,$J,358.3,1797,1,5,0)
 ;;=5^JOCK ITCH
 ;;^UTILITY(U,$J,358.3,1797,2)
 ;;=^33171
 ;;^UTILITY(U,$J,358.3,1798,0)
 ;;=464.00^^21^132^4
 ;;^UTILITY(U,$J,358.3,1798,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1798,1,2,0)
 ;;=2^464.00
 ;;^UTILITY(U,$J,358.3,1798,1,5,0)
 ;;=5^LARYNGITIS,AC w/o OBS
 ;;^UTILITY(U,$J,358.3,1798,2)
 ;;=^323469
 ;;^UTILITY(U,$J,358.3,1799,0)
 ;;=464.01^^21^132^3
 ;;^UTILITY(U,$J,358.3,1799,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1799,1,2,0)
 ;;=2^464.01
 ;;^UTILITY(U,$J,358.3,1799,1,5,0)
 ;;=5^LARYNGITIS,AC w/ OBS
 ;;^UTILITY(U,$J,358.3,1799,2)
 ;;=^323470
 ;;^UTILITY(U,$J,358.3,1800,0)
 ;;=729.82^^21^132^5
 ;;^UTILITY(U,$J,358.3,1800,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1800,1,2,0)
 ;;=2^729.82
 ;;^UTILITY(U,$J,358.3,1800,1,5,0)
 ;;=5^LEG CRAMPS
 ;;^UTILITY(U,$J,358.3,1800,2)
 ;;=^29062
 ;;^UTILITY(U,$J,358.3,1801,0)
 ;;=709.9^^21^132^6
 ;;^UTILITY(U,$J,358.3,1801,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1801,1,2,0)
 ;;=2^709.9
 ;;^UTILITY(U,$J,358.3,1801,1,5,0)
 ;;=5^LESION, SKIN NOS
 ;;^UTILITY(U,$J,358.3,1801,2)
 ;;=^111083
 ;;^UTILITY(U,$J,358.3,1802,0)
 ;;=214.9^^21^132^7
 ;;^UTILITY(U,$J,358.3,1802,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1802,1,2,0)
 ;;=2^214.9
 ;;^UTILITY(U,$J,358.3,1802,1,5,0)
 ;;=5^LIPOMA NOS
 ;;^UTILITY(U,$J,358.3,1802,2)
 ;;=^71155
 ;;^UTILITY(U,$J,358.3,1803,0)
 ;;=724.02^^21^132^8
 ;;^UTILITY(U,$J,358.3,1803,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1803,1,2,0)
 ;;=2^724.02
 ;;^UTILITY(U,$J,358.3,1803,1,5,0)
 ;;=5^LUMBAR STENOSIS
 ;;^UTILITY(U,$J,358.3,1803,2)
 ;;=^339732
 ;;^UTILITY(U,$J,358.3,1804,0)
 ;;=786.6^^21^132^9
 ;;^UTILITY(U,$J,358.3,1804,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1804,1,2,0)
 ;;=2^786.6
 ;;^UTILITY(U,$J,358.3,1804,1,5,0)
 ;;=5^LUNG MASS
 ;;^UTILITY(U,$J,358.3,1804,2)
 ;;=^273380
 ;;^UTILITY(U,$J,358.3,1805,0)
 ;;=289.89^^21^133^1
 ;;^UTILITY(U,$J,358.3,1805,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1805,1,2,0)
 ;;=2^289.89
 ;;^UTILITY(U,$J,358.3,1805,1,5,0)
 ;;=5^MACROCYTOSIS
 ;;^UTILITY(U,$J,358.3,1805,2)
 ;;=^329887
 ;;^UTILITY(U,$J,358.3,1806,0)
 ;;=263.9^^21^133^2
 ;;^UTILITY(U,$J,358.3,1806,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1806,1,2,0)
 ;;=2^263.9
 ;;^UTILITY(U,$J,358.3,1806,1,5,0)
 ;;=5^MALNUTRITION
 ;;^UTILITY(U,$J,358.3,1806,2)
 ;;=^267900
 ;;^UTILITY(U,$J,358.3,1807,0)
 ;;=424.0^^21^133^5
 ;;^UTILITY(U,$J,358.3,1807,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1807,1,2,0)
 ;;=2^424.0
 ;;^UTILITY(U,$J,358.3,1807,1,5,0)
 ;;=5^MITRAL VALVE PROLAPSE
 ;;^UTILITY(U,$J,358.3,1807,2)
 ;;=^78367
 ;;^UTILITY(U,$J,358.3,1808,0)
 ;;=340.^^21^133^6
 ;;^UTILITY(U,$J,358.3,1808,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1808,1,2,0)
 ;;=2^340.
 ;;^UTILITY(U,$J,358.3,1808,1,5,0)
 ;;=5^MULTIPLE SCLEROSIS
 ;;^UTILITY(U,$J,358.3,1808,2)
 ;;=^79761
 ;;^UTILITY(U,$J,358.3,1809,0)
 ;;=410.90^^21^133^7
 ;;^UTILITY(U,$J,358.3,1809,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1809,1,2,0)
 ;;=2^410.90
 ;;^UTILITY(U,$J,358.3,1809,1,5,0)
 ;;=5^MYOCARDIAL INFARCTION
 ;;^UTILITY(U,$J,358.3,1809,2)
 ;;=^269673
 ;;^UTILITY(U,$J,358.3,1810,0)
 ;;=424.0^^21^133^4
 ;;^UTILITY(U,$J,358.3,1810,1,0)
 ;;=^358.31IA^5^2
