IBDEI0AJ ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,13986,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13986,1,4,0)
 ;;=4^787.91
 ;;^UTILITY(U,$J,358.3,13986,1,5,0)
 ;;=5^Diarrhea
 ;;^UTILITY(U,$J,358.3,13986,2)
 ;;=^33921
 ;;^UTILITY(U,$J,358.3,13987,0)
 ;;=562.11^^107^868^15
 ;;^UTILITY(U,$J,358.3,13987,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13987,1,4,0)
 ;;=4^562.11
 ;;^UTILITY(U,$J,358.3,13987,1,5,0)
 ;;=5^Diverticulitis, Colon
 ;;^UTILITY(U,$J,358.3,13987,2)
 ;;=^270274
 ;;^UTILITY(U,$J,358.3,13988,0)
 ;;=562.10^^107^868^16
 ;;^UTILITY(U,$J,358.3,13988,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13988,1,4,0)
 ;;=4^562.10
 ;;^UTILITY(U,$J,358.3,13988,1,5,0)
 ;;=5^Diverticulosis, Colon
 ;;^UTILITY(U,$J,358.3,13988,2)
 ;;=^35917
 ;;^UTILITY(U,$J,358.3,13989,0)
 ;;=532.90^^107^868^17
 ;;^UTILITY(U,$J,358.3,13989,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13989,1,4,0)
 ;;=4^532.90
 ;;^UTILITY(U,$J,358.3,13989,1,5,0)
 ;;=5^Duodenal Ulcer Nos
 ;;^UTILITY(U,$J,358.3,13989,2)
 ;;=^37311
 ;;^UTILITY(U,$J,358.3,13990,0)
 ;;=536.8^^107^868^18
 ;;^UTILITY(U,$J,358.3,13990,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13990,1,4,0)
 ;;=4^536.8
 ;;^UTILITY(U,$J,358.3,13990,1,5,0)
 ;;=5^Dyspepsia
 ;;^UTILITY(U,$J,358.3,13990,2)
 ;;=^37612
 ;;^UTILITY(U,$J,358.3,13991,0)
 ;;=571.0^^107^868^34
 ;;^UTILITY(U,$J,358.3,13991,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13991,1,4,0)
 ;;=4^571.0
 ;;^UTILITY(U,$J,358.3,13991,1,5,0)
 ;;=5^Fatty Liver W/ Alcohol
 ;;^UTILITY(U,$J,358.3,13991,2)
 ;;=^45182
 ;;^UTILITY(U,$J,358.3,13992,0)
 ;;=571.3^^107^868^33
 ;;^UTILITY(U,$J,358.3,13992,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13992,1,4,0)
 ;;=4^571.3
 ;;^UTILITY(U,$J,358.3,13992,1,5,0)
 ;;=5^ETOH Liver Disease
 ;;^UTILITY(U,$J,358.3,13992,2)
 ;;=ETOH Liver Disease^4638
 ;;^UTILITY(U,$J,358.3,13993,0)
 ;;=530.10^^107^868^31
 ;;^UTILITY(U,$J,358.3,13993,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13993,1,4,0)
 ;;=4^530.10
 ;;^UTILITY(U,$J,358.3,13993,1,5,0)
 ;;=5^Esophagitis, Unsp.
 ;;^UTILITY(U,$J,358.3,13993,2)
 ;;=^295809
 ;;^UTILITY(U,$J,358.3,13994,0)
 ;;=530.81^^107^868^43
 ;;^UTILITY(U,$J,358.3,13994,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13994,1,4,0)
 ;;=4^530.81
 ;;^UTILITY(U,$J,358.3,13994,1,5,0)
 ;;=5^GERD
 ;;^UTILITY(U,$J,358.3,13994,2)
 ;;=^295749
 ;;^UTILITY(U,$J,358.3,13995,0)
 ;;=456.1^^107^868^30
 ;;^UTILITY(U,$J,358.3,13995,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13995,1,4,0)
 ;;=4^456.1
 ;;^UTILITY(U,$J,358.3,13995,1,5,0)
 ;;=5^Esoph Varices W/O Bleed
 ;;^UTILITY(U,$J,358.3,13995,2)
 ;;=^269836
 ;;^UTILITY(U,$J,358.3,13996,0)
 ;;=571.8^^107^868^35
 ;;^UTILITY(U,$J,358.3,13996,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13996,1,4,0)
 ;;=4^571.8
 ;;^UTILITY(U,$J,358.3,13996,1,5,0)
 ;;=5^Fatty Liver W/O Alcohol
 ;;^UTILITY(U,$J,358.3,13996,2)
 ;;=^87404
 ;;^UTILITY(U,$J,358.3,13997,0)
 ;;=792.1^^107^868^46
 ;;^UTILITY(U,$J,358.3,13997,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13997,1,4,0)
 ;;=4^792.1
 ;;^UTILITY(U,$J,358.3,13997,1,5,0)
 ;;=5^Heme+Stool
 ;;^UTILITY(U,$J,358.3,13997,2)
 ;;=^273412
 ;;^UTILITY(U,$J,358.3,13998,0)
 ;;=564.5^^107^868^36
 ;;^UTILITY(U,$J,358.3,13998,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13998,1,4,0)
 ;;=4^564.5
 ;;^UTILITY(U,$J,358.3,13998,1,5,0)
 ;;=5^Functional Diarrhea
 ;;^UTILITY(U,$J,358.3,13998,2)
 ;;=^270281
 ;;^UTILITY(U,$J,358.3,13999,0)
 ;;=578.9^^107^868^44
 ;;^UTILITY(U,$J,358.3,13999,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13999,1,4,0)
 ;;=4^578.9
 ;;^UTILITY(U,$J,358.3,13999,1,5,0)
 ;;=5^GI Bleed
 ;;^UTILITY(U,$J,358.3,13999,2)
 ;;=GI Bleed^49525
 ;;^UTILITY(U,$J,358.3,14000,0)
 ;;=531.70^^107^868^38
 ;;^UTILITY(U,$J,358.3,14000,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14000,1,4,0)
 ;;=4^531.70
 ;;^UTILITY(U,$J,358.3,14000,1,5,0)
 ;;=5^Gastric Ulcer, Chronic
 ;;^UTILITY(U,$J,358.3,14000,2)
 ;;=^270086
 ;;^UTILITY(U,$J,358.3,14001,0)
 ;;=535.50^^107^868^39
 ;;^UTILITY(U,$J,358.3,14001,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14001,1,4,0)
 ;;=4^535.50
 ;;^UTILITY(U,$J,358.3,14001,1,5,0)
 ;;=5^Gastritis
 ;;^UTILITY(U,$J,358.3,14001,2)
 ;;=^270181
 ;;^UTILITY(U,$J,358.3,14002,0)
 ;;=041.86^^107^868^45
 ;;^UTILITY(U,$J,358.3,14002,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14002,1,4,0)
 ;;=4^041.86
 ;;^UTILITY(U,$J,358.3,14002,1,5,0)
 ;;=5^H. Pylori Infection
 ;;^UTILITY(U,$J,358.3,14002,2)
 ;;=^303246
 ;;^UTILITY(U,$J,358.3,14003,0)
 ;;=455.6^^107^868^47
 ;;^UTILITY(U,$J,358.3,14003,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14003,1,4,0)
 ;;=4^455.6
 ;;^UTILITY(U,$J,358.3,14003,1,5,0)
 ;;=5^Hemorrhoids Nos
 ;;^UTILITY(U,$J,358.3,14003,2)
 ;;=^123922
 ;;^UTILITY(U,$J,358.3,14004,0)
 ;;=789.1^^107^868^58
 ;;^UTILITY(U,$J,358.3,14004,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14004,1,4,0)
 ;;=4^789.1
 ;;^UTILITY(U,$J,358.3,14004,1,5,0)
 ;;=5^Hepatomegaly
 ;;^UTILITY(U,$J,358.3,14004,2)
 ;;=^56494
 ;;^UTILITY(U,$J,358.3,14005,0)
 ;;=553.3^^107^868^62
 ;;^UTILITY(U,$J,358.3,14005,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14005,1,4,0)
 ;;=4^553.3
 ;;^UTILITY(U,$J,358.3,14005,1,5,0)
 ;;=5^Hiatal Hernia
 ;;^UTILITY(U,$J,358.3,14005,2)
 ;;=^33903
 ;;^UTILITY(U,$J,358.3,14006,0)
 ;;=550.92^^107^868^60
 ;;^UTILITY(U,$J,358.3,14006,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14006,1,4,0)
 ;;=4^550.92
 ;;^UTILITY(U,$J,358.3,14006,1,5,0)
 ;;=5^Hernia, Inguinal, Bilat
 ;;^UTILITY(U,$J,358.3,14006,2)
 ;;=^270212
 ;;^UTILITY(U,$J,358.3,14007,0)
 ;;=550.90^^107^868^61
 ;;^UTILITY(U,$J,358.3,14007,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14007,1,4,0)
 ;;=4^550.90
 ;;^UTILITY(U,$J,358.3,14007,1,5,0)
 ;;=5^Hernia, Inguinal, Unilat
 ;;^UTILITY(U,$J,358.3,14007,2)
 ;;=^63302
 ;;^UTILITY(U,$J,358.3,14008,0)
 ;;=553.9^^107^868^59
 ;;^UTILITY(U,$J,358.3,14008,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14008,1,4,0)
 ;;=4^553.9
 ;;^UTILITY(U,$J,358.3,14008,1,5,0)
 ;;=5^Hernia Nos
 ;;^UTILITY(U,$J,358.3,14008,2)
 ;;=^56659
 ;;^UTILITY(U,$J,358.3,14009,0)
 ;;=564.1^^107^868^65
 ;;^UTILITY(U,$J,358.3,14009,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14009,1,4,0)
 ;;=4^564.1
 ;;^UTILITY(U,$J,358.3,14009,1,5,0)
 ;;=5^Irritable Bowel Syndrome
 ;;^UTILITY(U,$J,358.3,14009,2)
 ;;=^65682^909.2
 ;;^UTILITY(U,$J,358.3,14010,0)
 ;;=787.02^^107^868^71
 ;;^UTILITY(U,$J,358.3,14010,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14010,1,4,0)
 ;;=4^787.02
 ;;^UTILITY(U,$J,358.3,14010,1,5,0)
 ;;=5^Nausea
 ;;^UTILITY(U,$J,358.3,14010,2)
 ;;=^81639
 ;;^UTILITY(U,$J,358.3,14011,0)
 ;;=787.01^^107^868^72
 ;;^UTILITY(U,$J,358.3,14011,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14011,1,4,0)
 ;;=4^787.01
 ;;^UTILITY(U,$J,358.3,14011,1,5,0)
 ;;=5^Nausea W/ Vomiting
 ;;^UTILITY(U,$J,358.3,14011,2)
 ;;=^81644
 ;;^UTILITY(U,$J,358.3,14012,0)
 ;;=577.2^^107^868^73
 ;;^UTILITY(U,$J,358.3,14012,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14012,1,4,0)
 ;;=4^577.2
 ;;^UTILITY(U,$J,358.3,14012,1,5,0)
 ;;=5^Pancreatic Pseudocyst
 ;;^UTILITY(U,$J,358.3,14012,2)
 ;;=^30078
 ;;^UTILITY(U,$J,358.3,14013,0)
 ;;=577.0^^107^868^74
 ;;^UTILITY(U,$J,358.3,14013,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14013,1,4,0)
 ;;=4^577.0
 ;;^UTILITY(U,$J,358.3,14013,1,5,0)
 ;;=5^Pancreatitis, Acute
 ;;^UTILITY(U,$J,358.3,14013,2)
 ;;=^2643
 ;;^UTILITY(U,$J,358.3,14014,0)
 ;;=577.1^^107^868^75
 ;;^UTILITY(U,$J,358.3,14014,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14014,1,4,0)
 ;;=4^577.1
 ;;^UTILITY(U,$J,358.3,14014,1,5,0)
 ;;=5^Pancreatitis, Chronic
 ;;^UTILITY(U,$J,358.3,14014,2)
 ;;=^259100
 ;;^UTILITY(U,$J,358.3,14015,0)
 ;;=536.9^^107^868^76
 ;;^UTILITY(U,$J,358.3,14015,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14015,1,4,0)
 ;;=4^536.9
 ;;^UTILITY(U,$J,358.3,14015,1,5,0)
 ;;=5^Peptic Disease
 ;;^UTILITY(U,$J,358.3,14015,2)
 ;;=^270185
 ;;^UTILITY(U,$J,358.3,14016,0)
 ;;=V12.71^^107^868^63
 ;;^UTILITY(U,$J,358.3,14016,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14016,1,4,0)
 ;;=4^V12.71
 ;;^UTILITY(U,$J,358.3,14016,1,5,0)
 ;;=5^Hx Of Pud
 ;;^UTILITY(U,$J,358.3,14016,2)
 ;;=^303400
 ;;^UTILITY(U,$J,358.3,14017,0)
 ;;=566.^^107^868^78
 ;;^UTILITY(U,$J,358.3,14017,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14017,1,4,0)
 ;;=4^566.
 ;;^UTILITY(U,$J,358.3,14017,1,5,0)
 ;;=5^Peri-Rectal Abscess
 ;;^UTILITY(U,$J,358.3,14017,2)
 ;;=^270285
 ;;^UTILITY(U,$J,358.3,14018,0)
 ;;=698.0^^107^868^81
 ;;^UTILITY(U,$J,358.3,14018,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14018,1,4,0)
 ;;=4^698.0
 ;;^UTILITY(U,$J,358.3,14018,1,5,0)
 ;;=5^Pruritus Ani
 ;;^UTILITY(U,$J,358.3,14018,2)
 ;;=^100061
 ;;^UTILITY(U,$J,358.3,14019,0)
 ;;=789.2^^107^868^86
 ;;^UTILITY(U,$J,358.3,14019,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14019,1,4,0)
 ;;=4^789.2
 ;;^UTILITY(U,$J,358.3,14019,1,5,0)
 ;;=5^Splenomegaly
 ;;^UTILITY(U,$J,358.3,14019,2)
 ;;=^113452
 ;;^UTILITY(U,$J,358.3,14020,0)
 ;;=289.51^^107^868^87
 ;;^UTILITY(U,$J,358.3,14020,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14020,1,4,0)
 ;;=4^289.51
 ;;^UTILITY(U,$J,358.3,14020,1,5,0)
 ;;=5^Splenomegaly, Chronic
 ;;^UTILITY(U,$J,358.3,14020,2)
 ;;=^268000
 ;;^UTILITY(U,$J,358.3,14021,0)
 ;;=556.9^^107^868^88
 ;;^UTILITY(U,$J,358.3,14021,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14021,1,4,0)
 ;;=4^556.9
 ;;^UTILITY(U,$J,358.3,14021,1,5,0)
 ;;=5^Ulcerative Colitis
 ;;^UTILITY(U,$J,358.3,14021,2)
 ;;=^26044
 ;;^UTILITY(U,$J,358.3,14022,0)
 ;;=787.03^^107^868^89
 ;;^UTILITY(U,$J,358.3,14022,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14022,1,4,0)
 ;;=4^787.03
 ;;^UTILITY(U,$J,358.3,14022,1,5,0)
 ;;=5^Vomiting Alone
 ;;^UTILITY(U,$J,358.3,14022,2)
 ;;=^127237
 ;;^UTILITY(U,$J,358.3,14023,0)
 ;;=789.07^^107^868^42
 ;;^UTILITY(U,$J,358.3,14023,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,14023,1,4,0)
 ;;=4^789.07
 ;;^UTILITY(U,$J,358.3,14023,1,5,0)
 ;;=5^Generalized Abdominal Pain
 ;;^UTILITY(U,$J,358.3,14023,2)
 ;;=Generalized Abdominal Pain^303324
 ;;^UTILITY(U,$J,358.3,14024,0)
 ;;=564.00^^107^868^12
