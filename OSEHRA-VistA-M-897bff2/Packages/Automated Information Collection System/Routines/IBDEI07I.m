IBDEI07I ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,9871,1,4,0)
 ;;=4^367.0
 ;;^UTILITY(U,$J,358.3,9871,2)
 ;;=Hypermetropia^60139
 ;;^UTILITY(U,$J,358.3,9872,0)
 ;;=367.1^^79^671^6
 ;;^UTILITY(U,$J,358.3,9872,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9872,1,3,0)
 ;;=3^Myopia
 ;;^UTILITY(U,$J,358.3,9872,1,4,0)
 ;;=4^367.1
 ;;^UTILITY(U,$J,358.3,9872,2)
 ;;=Myopia^80671
 ;;^UTILITY(U,$J,358.3,9873,0)
 ;;=367.4^^79^671^0
 ;;^UTILITY(U,$J,358.3,9873,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9873,1,3,0)
 ;;=3^Presbyopia
 ;;^UTILITY(U,$J,358.3,9873,1,4,0)
 ;;=4^367.4
 ;;^UTILITY(U,$J,358.3,9873,2)
 ;;=Presbyopia^98095
 ;;^UTILITY(U,$J,358.3,9874,0)
 ;;=368.40^^79^671^7
 ;;^UTILITY(U,$J,358.3,9874,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9874,1,3,0)
 ;;=3^Visual Field Defect
 ;;^UTILITY(U,$J,358.3,9874,1,4,0)
 ;;=4^368.40
 ;;^UTILITY(U,$J,358.3,9874,2)
 ;;=^126859
 ;;^UTILITY(U,$J,358.3,9875,0)
 ;;=V67.09^^79^671^4
 ;;^UTILITY(U,$J,358.3,9875,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9875,1,3,0)
 ;;=3^F/U Vistreos Surgery
 ;;^UTILITY(U,$J,358.3,9875,1,4,0)
 ;;=4^V67.09
 ;;^UTILITY(U,$J,358.3,9875,2)
 ;;=^322080
 ;;^UTILITY(U,$J,358.3,9876,0)
 ;;=373.32^^79^672^1
 ;;^UTILITY(U,$J,358.3,9876,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9876,1,3,0)
 ;;=3^Allerg Dermatitis, Eyelid
 ;;^UTILITY(U,$J,358.3,9876,1,4,0)
 ;;=4^373.32
 ;;^UTILITY(U,$J,358.3,9876,2)
 ;;=^269061
 ;;^UTILITY(U,$J,358.3,9877,0)
 ;;=373.00^^79^672^4
 ;;^UTILITY(U,$J,358.3,9877,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9877,1,3,0)
 ;;=3^Blepharitis
 ;;^UTILITY(U,$J,358.3,9877,1,4,0)
 ;;=4^373.00
 ;;^UTILITY(U,$J,358.3,9877,2)
 ;;=Blepharitis^15271
 ;;^UTILITY(U,$J,358.3,9878,0)
 ;;=373.2^^79^672^7
 ;;^UTILITY(U,$J,358.3,9878,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9878,1,3,0)
 ;;=3^Chalazion
 ;;^UTILITY(U,$J,358.3,9878,1,4,0)
 ;;=4^373.2
 ;;^UTILITY(U,$J,358.3,9878,2)
 ;;=^22156
 ;;^UTILITY(U,$J,358.3,9879,0)
 ;;=374.84^^79^672^9
 ;;^UTILITY(U,$J,358.3,9879,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9879,1,3,0)
 ;;=3^Cysts Of Eyelids
 ;;^UTILITY(U,$J,358.3,9879,1,4,0)
 ;;=4^374.84
 ;;^UTILITY(U,$J,358.3,9879,2)
 ;;=^269119
 ;;^UTILITY(U,$J,358.3,9880,0)
 ;;=374.87^^79^672^11
 ;;^UTILITY(U,$J,358.3,9880,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9880,1,3,0)
 ;;=3^Dermatochalasis
 ;;^UTILITY(U,$J,358.3,9880,1,4,0)
 ;;=4^374.87
 ;;^UTILITY(U,$J,358.3,9880,2)
 ;;=^269123
 ;;^UTILITY(U,$J,358.3,9881,0)
 ;;=375.15^^79^672^12
 ;;^UTILITY(U,$J,358.3,9881,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9881,1,3,0)
 ;;=3^Dry Eye Syndrome
 ;;^UTILITY(U,$J,358.3,9881,1,4,0)
 ;;=4^375.15
 ;;^UTILITY(U,$J,358.3,9881,2)
 ;;=Dry Eye Syndrome^37168
 ;;^UTILITY(U,$J,358.3,9882,0)
 ;;=374.01^^79^672^21
 ;;^UTILITY(U,$J,358.3,9882,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9882,1,3,0)
 ;;=3^Entropion, Senile
 ;;^UTILITY(U,$J,358.3,9882,1,4,0)
 ;;=4^374.01
 ;;^UTILITY(U,$J,358.3,9882,2)
 ;;=Entropion, Senile^269074
 ;;^UTILITY(U,$J,358.3,9883,0)
 ;;=375.20^^79^672^24
 ;;^UTILITY(U,$J,358.3,9883,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9883,1,3,0)
 ;;=3^Epiphora
 ;;^UTILITY(U,$J,358.3,9883,1,4,0)
 ;;=4^375.20
 ;;^UTILITY(U,$J,358.3,9883,2)
 ;;=Epiphora^269136
 ;;^UTILITY(U,$J,358.3,9884,0)
 ;;=373.11^^79^672^31
 ;;^UTILITY(U,$J,358.3,9884,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9884,1,3,0)
 ;;=3^Hordeolum Externum
 ;;^UTILITY(U,$J,358.3,9884,1,4,0)
 ;;=4^373.11
 ;;^UTILITY(U,$J,358.3,9884,2)
 ;;=^58510
 ;;^UTILITY(U,$J,358.3,9885,0)
 ;;=373.12^^79^672^32
 ;;^UTILITY(U,$J,358.3,9885,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9885,1,3,0)
 ;;=3^Hordeolum Internum
 ;;^UTILITY(U,$J,358.3,9885,1,4,0)
 ;;=4^373.12
 ;;^UTILITY(U,$J,358.3,9885,2)
 ;;=^58512
 ;;^UTILITY(U,$J,358.3,9886,0)
 ;;=216.1^^79^672^40
 ;;^UTILITY(U,$J,358.3,9886,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9886,1,3,0)
 ;;=3^Neoplasm, Benign of Lid
 ;;^UTILITY(U,$J,358.3,9886,1,4,0)
 ;;=4^216.1
 ;;^UTILITY(U,$J,358.3,9886,2)
 ;;=Neoplasm, Benign of Lid^267630
 ;;^UTILITY(U,$J,358.3,9887,0)
 ;;=374.30^^79^672^47
 ;;^UTILITY(U,$J,358.3,9887,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9887,1,3,0)
 ;;=3^Ptosis Of Eyelid
 ;;^UTILITY(U,$J,358.3,9887,1,4,0)
 ;;=4^374.30
 ;;^UTILITY(U,$J,358.3,9887,2)
 ;;=^15283
 ;;^UTILITY(U,$J,358.3,9888,0)
 ;;=375.30^^79^672^10
 ;;^UTILITY(U,$J,358.3,9888,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9888,1,3,0)
 ;;=3^Dacryocystitis
 ;;^UTILITY(U,$J,358.3,9888,1,4,0)
 ;;=4^375.30
 ;;^UTILITY(U,$J,358.3,9888,2)
 ;;=Dacrocystitis^30880
 ;;^UTILITY(U,$J,358.3,9889,0)
 ;;=921.1^^79^672^8
 ;;^UTILITY(U,$J,358.3,9889,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9889,1,3,0)
 ;;=3^Contusion Eyelid
 ;;^UTILITY(U,$J,358.3,9889,1,4,0)
 ;;=4^921.1
 ;;^UTILITY(U,$J,358.3,9889,2)
 ;;=^275367
 ;;^UTILITY(U,$J,358.3,9890,0)
 ;;=374.51^^79^672^53
 ;;^UTILITY(U,$J,358.3,9890,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9890,1,3,0)
 ;;=3^Xanthalasma
 ;;^UTILITY(U,$J,358.3,9890,1,4,0)
 ;;=4^374.51
 ;;^UTILITY(U,$J,358.3,9890,2)
 ;;=^269107
 ;;^UTILITY(U,$J,358.3,9891,0)
 ;;=374.11^^79^672^15
 ;;^UTILITY(U,$J,358.3,9891,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9891,1,3,0)
 ;;=3^Ectropion, Senile
 ;;^UTILITY(U,$J,358.3,9891,1,4,0)
 ;;=4^374.11
 ;;^UTILITY(U,$J,358.3,9891,2)
 ;;=Ectropion, Senile^269083
 ;;^UTILITY(U,$J,358.3,9892,0)
 ;;=333.81^^79^672^6
 ;;^UTILITY(U,$J,358.3,9892,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9892,1,3,0)
 ;;=3^Blepharospasm
 ;;^UTILITY(U,$J,358.3,9892,1,4,0)
 ;;=4^333.81
 ;;^UTILITY(U,$J,358.3,9892,2)
 ;;=Blepharospasm^15293
 ;;^UTILITY(U,$J,358.3,9893,0)
 ;;=351.8^^79^672^30
 ;;^UTILITY(U,$J,358.3,9893,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9893,1,3,0)
 ;;=3^Hemifacial Spasm
 ;;^UTILITY(U,$J,358.3,9893,1,4,0)
 ;;=4^351.8
 ;;^UTILITY(U,$J,358.3,9893,2)
 ;;=Hemifacial Spasm^87589
 ;;^UTILITY(U,$J,358.3,9894,0)
 ;;=374.10^^79^672^17
 ;;^UTILITY(U,$J,358.3,9894,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9894,1,3,0)
 ;;=3^Ectropion, Unspecified
 ;;^UTILITY(U,$J,358.3,9894,1,4,0)
 ;;=4^374.10
 ;;^UTILITY(U,$J,358.3,9894,2)
 ;;=Ectropion, Unspecified^38326
 ;;^UTILITY(U,$J,358.3,9895,0)
 ;;=374.14^^79^672^13
 ;;^UTILITY(U,$J,358.3,9895,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9895,1,3,0)
 ;;=3^Ectropion, Cicatricial
 ;;^UTILITY(U,$J,358.3,9895,1,4,0)
 ;;=4^374.14
 ;;^UTILITY(U,$J,358.3,9895,2)
 ;;=Edctropion, Cicatricial^269089
 ;;^UTILITY(U,$J,358.3,9896,0)
 ;;=374.12^^79^672^14
 ;;^UTILITY(U,$J,358.3,9896,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9896,1,3,0)
 ;;=3^Ectropion, Mechanical
 ;;^UTILITY(U,$J,358.3,9896,1,4,0)
 ;;=4^374.12
 ;;^UTILITY(U,$J,358.3,9896,2)
 ;;=Ectropion, Mechanical^269085
 ;;^UTILITY(U,$J,358.3,9897,0)
 ;;=374.13^^79^672^16
 ;;^UTILITY(U,$J,358.3,9897,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9897,1,3,0)
 ;;=3^Ectropion, Spastic
 ;;^UTILITY(U,$J,358.3,9897,1,4,0)
 ;;=4^374.13
 ;;^UTILITY(U,$J,358.3,9897,2)
 ;;=Ectropion, Spastic^269087
 ;;^UTILITY(U,$J,358.3,9898,0)
 ;;=374.00^^79^672^23
 ;;^UTILITY(U,$J,358.3,9898,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9898,1,3,0)
 ;;=3^Entropion, Unspec
 ;;^UTILITY(U,$J,358.3,9898,1,4,0)
 ;;=4^374.00
 ;;^UTILITY(U,$J,358.3,9898,2)
 ;;=Entropion, Unspec^41016
 ;;^UTILITY(U,$J,358.3,9899,0)
 ;;=374.04^^79^672^19
 ;;^UTILITY(U,$J,358.3,9899,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9899,1,3,0)
 ;;=3^Entropion, Cicatricial
 ;;^UTILITY(U,$J,358.3,9899,1,4,0)
 ;;=4^374.04
 ;;^UTILITY(U,$J,358.3,9899,2)
 ;;=Entropion, Cicatricial^269080
 ;;^UTILITY(U,$J,358.3,9900,0)
 ;;=374.02^^79^672^20
 ;;^UTILITY(U,$J,358.3,9900,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9900,1,3,0)
 ;;=3^Entropion, Mechanical
 ;;^UTILITY(U,$J,358.3,9900,1,4,0)
 ;;=4^374.02
 ;;^UTILITY(U,$J,358.3,9900,2)
 ;;=Entropion, Mechanical^269076
 ;;^UTILITY(U,$J,358.3,9901,0)
 ;;=374.03^^79^672^22
 ;;^UTILITY(U,$J,358.3,9901,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9901,1,3,0)
 ;;=3^Entropion, Spastic
 ;;^UTILITY(U,$J,358.3,9901,1,4,0)
 ;;=4^374.03
 ;;^UTILITY(U,$J,358.3,9901,2)
 ;;=Spastic Entropion^269078
 ;;^UTILITY(U,$J,358.3,9902,0)
 ;;=870.0^^79^672^33
 ;;^UTILITY(U,$J,358.3,9902,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9902,1,3,0)
 ;;=3^Laceration, Eyelid
 ;;^UTILITY(U,$J,358.3,9902,1,4,0)
 ;;=4^870.0
 ;;^UTILITY(U,$J,358.3,9902,2)
 ;;=Laceration, Eyelid^274879
 ;;^UTILITY(U,$J,358.3,9903,0)
 ;;=374.20^^79^672^37
 ;;^UTILITY(U,$J,358.3,9903,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9903,1,3,0)
 ;;=3^Lagophthalmos
 ;;^UTILITY(U,$J,358.3,9903,1,4,0)
 ;;=4^374.20
 ;;^UTILITY(U,$J,358.3,9903,2)
 ;;=Lagophthalmos^265452
 ;;^UTILITY(U,$J,358.3,9904,0)
 ;;=378.9^^79^672^49
 ;;^UTILITY(U,$J,358.3,9904,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9904,1,3,0)
 ;;=3^Strabismus
 ;;^UTILITY(U,$J,358.3,9904,1,4,0)
 ;;=4^378.9
 ;;^UTILITY(U,$J,358.3,9904,2)
 ;;=Strabismus^123833
 ;;^UTILITY(U,$J,358.3,9905,0)
 ;;=242.90^^79^672^50
 ;;^UTILITY(U,$J,358.3,9905,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9905,1,3,0)
 ;;=3^Thyroid Eye Disease
 ;;^UTILITY(U,$J,358.3,9905,1,4,0)
 ;;=4^242.90
 ;;^UTILITY(U,$J,358.3,9905,2)
 ;;=Thyroid Eye Disease^267811^376.21
 ;;^UTILITY(U,$J,358.3,9906,0)
 ;;=374.05^^79^672^51
 ;;^UTILITY(U,$J,358.3,9906,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9906,1,3,0)
 ;;=3^Trichiasis
 ;;^UTILITY(U,$J,358.3,9906,1,4,0)
 ;;=4^374.05
 ;;^UTILITY(U,$J,358.3,9906,2)
 ;;=Trichiasis^269082
 ;;^UTILITY(U,$J,358.3,9907,0)
 ;;=368.40^^79^672^52
 ;;^UTILITY(U,$J,358.3,9907,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9907,1,3,0)
 ;;=3^Visual Field Defect
 ;;^UTILITY(U,$J,358.3,9907,1,4,0)
 ;;=4^368.40
 ;;^UTILITY(U,$J,358.3,9907,2)
 ;;=Visual Field Defect^126859
 ;;^UTILITY(U,$J,358.3,9908,0)
 ;;=375.21^^79^672^26
 ;;^UTILITY(U,$J,358.3,9908,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9908,1,3,0)
 ;;=3^Epiphora, excess lacrimation
 ;;^UTILITY(U,$J,358.3,9908,1,4,0)
 ;;=4^375.21
