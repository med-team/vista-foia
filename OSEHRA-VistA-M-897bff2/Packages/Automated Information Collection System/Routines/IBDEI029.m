IBDEI029 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,2478,0)
 ;;=V71.09^^25^197^1
 ;;^UTILITY(U,$J,358.3,2478,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,2478,1,2,0)
 ;;=2^V71.09
 ;;^UTILITY(U,$J,358.3,2478,1,5,0)
 ;;=5^OBSERV-MENTAL COND NEC
 ;;^UTILITY(U,$J,358.3,2478,2)
 ;;=^295604
 ;;^UTILITY(U,$J,358.3,2479,0)
 ;;=99499^^26^198^1
 ;;^UTILITY(U,$J,358.3,2479,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,2479,1,1,0)
 ;;=1^Office Visit
 ;;^UTILITY(U,$J,358.3,2479,1,2,0)
 ;;=2^99499
 ;;^UTILITY(U,$J,358.3,2480,0)
 ;;=99499^^26^199^1
 ;;^UTILITY(U,$J,358.3,2480,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,2480,1,1,0)
 ;;=1^F-T-F Intervention
 ;;^UTILITY(U,$J,358.3,2480,1,2,0)
 ;;=2^99499
 ;;^UTILITY(U,$J,358.3,2481,0)
 ;;=99499^^26^200^1
 ;;^UTILITY(U,$J,358.3,2481,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,2481,1,1,0)
 ;;=1^CCHT Video Visit
 ;;^UTILITY(U,$J,358.3,2481,1,2,0)
 ;;=2^99499
 ;;^UTILITY(U,$J,358.3,2482,0)
 ;;=99078^^27^201^3^^^^1
 ;;^UTILITY(U,$J,358.3,2482,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2482,1,2,0)
 ;;=2^99078
 ;;^UTILITY(U,$J,358.3,2482,1,3,0)
 ;;=3^GROUP HEALTH EDUCATION
 ;;^UTILITY(U,$J,358.3,2483,0)
 ;;=99080^^27^201^8^^^^1
 ;;^UTILITY(U,$J,358.3,2483,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2483,1,2,0)
 ;;=2^99080
 ;;^UTILITY(U,$J,358.3,2483,1,3,0)
 ;;=3^SPECIAL REPORTS OR FORMS
 ;;^UTILITY(U,$J,358.3,2484,0)
 ;;=97535^^27^201^7^^^^1
 ;;^UTILITY(U,$J,358.3,2484,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2484,1,2,0)
 ;;=2^97535
 ;;^UTILITY(U,$J,358.3,2484,1,3,0)
 ;;=3^SELF CARE MNGMENT TRAINING
 ;;^UTILITY(U,$J,358.3,2485,0)
 ;;=99366^^27^201^9^^^^1
 ;;^UTILITY(U,$J,358.3,2485,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2485,1,2,0)
 ;;=2^99366
 ;;^UTILITY(U,$J,358.3,2485,1,3,0)
 ;;=3^TEAM CONF W/PAT BY HC PRO
 ;;^UTILITY(U,$J,358.3,2486,0)
 ;;=99499^^27^201^10^^^^1
 ;;^UTILITY(U,$J,358.3,2486,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2486,1,2,0)
 ;;=2^99499
 ;;^UTILITY(U,$J,358.3,2486,1,3,0)
 ;;=3^UNLISTED E&M SERVICE
 ;;^UTILITY(U,$J,358.3,2487,0)
 ;;=G0108^^27^201^2^^^^1
 ;;^UTILITY(U,$J,358.3,2487,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2487,1,2,0)
 ;;=2^G0108
 ;;^UTILITY(U,$J,358.3,2487,1,3,0)
 ;;=3^DIAB MANAGE TRN PER INDIV
 ;;^UTILITY(U,$J,358.3,2488,0)
 ;;=G0109^^27^201^1^^^^1
 ;;^UTILITY(U,$J,358.3,2488,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2488,1,2,0)
 ;;=2^G0109
 ;;^UTILITY(U,$J,358.3,2488,1,3,0)
 ;;=3^DIAB MANAGE TRN GROUP
 ;;^UTILITY(U,$J,358.3,2489,0)
 ;;=A9279^^27^201^5^^^^1
 ;;^UTILITY(U,$J,358.3,2489,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2489,1,2,0)
 ;;=2^A9279
 ;;^UTILITY(U,$J,358.3,2489,1,3,0)
 ;;=3^MONITORING FEATURE/DEVICE NOC
 ;;^UTILITY(U,$J,358.3,2490,0)
 ;;=H0034^^27^201^4^^^^1
 ;;^UTILITY(U,$J,358.3,2490,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2490,1,2,0)
 ;;=2^H0034
 ;;^UTILITY(U,$J,358.3,2490,1,3,0)
 ;;=3^MED TRNG & SUPPORT PER 15MIN
 ;;^UTILITY(U,$J,358.3,2491,0)
 ;;=S9445^^27^202^1^^^^1
 ;;^UTILITY(U,$J,358.3,2491,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2491,1,2,0)
 ;;=2^S9445
 ;;^UTILITY(U,$J,358.3,2491,1,3,0)
 ;;=3^PT EDUCATION NOC INDIVID
 ;;^UTILITY(U,$J,358.3,2492,0)
 ;;=99091^^27^203^1^^^^1
 ;;^UTILITY(U,$J,358.3,2492,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2492,1,2,0)
 ;;=2^99091
 ;;^UTILITY(U,$J,358.3,2492,1,3,0)
 ;;=3^COLLECT/REVIEW DATA FROM PT
 ;;^UTILITY(U,$J,358.3,2493,0)
 ;;=G0155^^27^204^1^^^^1
 ;;^UTILITY(U,$J,358.3,2493,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2493,1,2,0)
 ;;=2^G0155
 ;;^UTILITY(U,$J,358.3,2493,1,3,0)
 ;;=3^HOME VISIT BY CSW,EA 15 MIN
 ;;^UTILITY(U,$J,358.3,2494,0)
 ;;=90847^^27^205^1^^^^1
 ;;^UTILITY(U,$J,358.3,2494,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2494,1,2,0)
 ;;=2^90847
 ;;^UTILITY(U,$J,358.3,2494,1,3,0)
 ;;=3^FAMILY PSYTX W/PATIENT
 ;;^UTILITY(U,$J,358.3,2495,0)
 ;;=90849^^27^205^3^^^^1
 ;;^UTILITY(U,$J,358.3,2495,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2495,1,2,0)
 ;;=2^90849
 ;;^UTILITY(U,$J,358.3,2495,1,3,0)
 ;;=3^MULTIPLE FAMILY GROUP PSYTX
 ;;^UTILITY(U,$J,358.3,2496,0)
 ;;=90853^^27^205^2^^^^1
 ;;^UTILITY(U,$J,358.3,2496,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2496,1,2,0)
 ;;=2^90853
 ;;^UTILITY(U,$J,358.3,2496,1,3,0)
 ;;=3^GROUP PSYCHOTHERAPY
 ;;^UTILITY(U,$J,358.3,2497,0)
 ;;=90832^^27^205^4^^^^1
 ;;^UTILITY(U,$J,358.3,2497,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2497,1,2,0)
 ;;=2^90832
 ;;^UTILITY(U,$J,358.3,2497,1,3,0)
 ;;=3^PSYTX PT OR FAMILY 17-32 MIN
 ;;^UTILITY(U,$J,358.3,2498,0)
 ;;=90834^^27^205^5^^^^1
 ;;^UTILITY(U,$J,358.3,2498,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2498,1,2,0)
 ;;=2^90834
 ;;^UTILITY(U,$J,358.3,2498,1,3,0)
 ;;=3^PSYTX PT OR FAMILY 38-52 MIN
 ;;^UTILITY(U,$J,358.3,2499,0)
 ;;=90837^^27^205^6^^^^1
 ;;^UTILITY(U,$J,358.3,2499,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,2499,1,2,0)
 ;;=2^90837
 ;;^UTILITY(U,$J,358.3,2499,1,3,0)
 ;;=3^PSYTX PT OR FAMILY 53+ MIN
 ;;^UTILITY(U,$J,358.3,2500,0)
 ;;=V71.89^^28^206^1
 ;;^UTILITY(U,$J,358.3,2500,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,2500,1,3,0)
 ;;=3^ENROLLMENT IN CCHT PROGRAM
 ;;^UTILITY(U,$J,358.3,2500,1,4,0)
 ;;=4^V71.89
 ;;^UTILITY(U,$J,358.3,2500,2)
 ;;=^322082
 ;;^UTILITY(U,$J,358.3,2501,0)
 ;;=284.9^^29^207^6
 ;;^UTILITY(U,$J,358.3,2501,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2501,1,1,0)
 ;;=1^284.9
 ;;^UTILITY(U,$J,358.3,2501,1,8,0)
 ;;=8^Aplastic Anemia,unspec
 ;;^UTILITY(U,$J,358.3,2501,2)
 ;;=^7020
 ;;^UTILITY(U,$J,358.3,2502,0)
 ;;=282.9^^29^207^10
 ;;^UTILITY(U,$J,358.3,2502,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2502,1,1,0)
 ;;=1^282.9
 ;;^UTILITY(U,$J,358.3,2502,1,8,0)
 ;;=8^Hemolytic Anemia,Hered
 ;;^UTILITY(U,$J,358.3,2502,2)
 ;;=^56578
 ;;^UTILITY(U,$J,358.3,2503,0)
 ;;=283.9^^29^207^8
 ;;^UTILITY(U,$J,358.3,2503,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2503,1,1,0)
 ;;=1^283.9
 ;;^UTILITY(U,$J,358.3,2503,1,8,0)
 ;;=8^Hemolytic Anemia,Acquired
 ;;^UTILITY(U,$J,358.3,2503,2)
 ;;=^7071
 ;;^UTILITY(U,$J,358.3,2504,0)
 ;;=283.0^^29^207^9
 ;;^UTILITY(U,$J,358.3,2504,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2504,1,1,0)
 ;;=1^283.0
 ;;^UTILITY(U,$J,358.3,2504,1,8,0)
 ;;=8^Hemolytic Anemia,Autoimmun
 ;;^UTILITY(U,$J,358.3,2504,2)
 ;;=^7079
 ;;^UTILITY(U,$J,358.3,2505,0)
 ;;=283.19^^29^207^11
 ;;^UTILITY(U,$J,358.3,2505,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2505,1,1,0)
 ;;=1^283.19
 ;;^UTILITY(U,$J,358.3,2505,1,8,0)
 ;;=8^Hemolytic Anemia,Microangiopathic
 ;;^UTILITY(U,$J,358.3,2505,2)
 ;;=^293664
 ;;^UTILITY(U,$J,358.3,2506,0)
 ;;=280.9^^29^207^12
 ;;^UTILITY(U,$J,358.3,2506,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2506,1,1,0)
 ;;=1^280.9
 ;;^UTILITY(U,$J,358.3,2506,1,8,0)
 ;;=8^Iron Defic Anemia
 ;;^UTILITY(U,$J,358.3,2506,2)
 ;;=^276946
 ;;^UTILITY(U,$J,358.3,2507,0)
 ;;=281.0^^29^207^17
 ;;^UTILITY(U,$J,358.3,2507,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2507,1,1,0)
 ;;=1^281.0
 ;;^UTILITY(U,$J,358.3,2507,1,8,0)
 ;;=8^Pernicious Anemia
 ;;^UTILITY(U,$J,358.3,2507,2)
 ;;=^7161
 ;;^UTILITY(U,$J,358.3,2508,0)
 ;;=285.9^^29^207^19
 ;;^UTILITY(U,$J,358.3,2508,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2508,1,1,0)
 ;;=1^285.9
 ;;^UTILITY(U,$J,358.3,2508,1,8,0)
 ;;=8^Unspecified Anemia
 ;;^UTILITY(U,$J,358.3,2508,2)
 ;;=^7007
 ;;^UTILITY(U,$J,358.3,2509,0)
 ;;=281.8^^29^207^14
 ;;^UTILITY(U,$J,358.3,2509,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2509,1,1,0)
 ;;=1^281.8
 ;;^UTILITY(U,$J,358.3,2509,1,8,0)
 ;;=8^Nutritional Anemia
 ;;^UTILITY(U,$J,358.3,2509,2)
 ;;=^267979
 ;;^UTILITY(U,$J,358.3,2510,0)
 ;;=285.21^^29^207^2
 ;;^UTILITY(U,$J,358.3,2510,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2510,1,1,0)
 ;;=1^285.21
 ;;^UTILITY(U,$J,358.3,2510,1,8,0)
 ;;=8^Anemia End-Stage Renal Disease
 ;;^UTILITY(U,$J,358.3,2510,2)
 ;;=^321977
 ;;^UTILITY(U,$J,358.3,2511,0)
 ;;=285.22^^29^207^3
 ;;^UTILITY(U,$J,358.3,2511,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2511,1,1,0)
 ;;=1^285.22
 ;;^UTILITY(U,$J,358.3,2511,1,8,0)
 ;;=8^Anemia In Neoplastic Disease
 ;;^UTILITY(U,$J,358.3,2511,2)
 ;;=^321978
 ;;^UTILITY(U,$J,358.3,2512,0)
 ;;=285.29^^29^207^4
 ;;^UTILITY(U,$J,358.3,2512,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2512,1,1,0)
 ;;=1^285.29
 ;;^UTILITY(U,$J,358.3,2512,1,8,0)
 ;;=8^Anemia Oth Chronic Illness
 ;;^UTILITY(U,$J,358.3,2512,2)
 ;;=^321979
 ;;^UTILITY(U,$J,358.3,2513,0)
 ;;=285.1^^29^207^1
 ;;^UTILITY(U,$J,358.3,2513,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2513,1,1,0)
 ;;=1^285.1
 ;;^UTILITY(U,$J,358.3,2513,1,8,0)
 ;;=8^Ac Posthemorrhag Anemia
 ;;^UTILITY(U,$J,358.3,2513,2)
 ;;=^267986
 ;;^UTILITY(U,$J,358.3,2514,0)
 ;;=284.2^^29^207^13
 ;;^UTILITY(U,$J,358.3,2514,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2514,1,1,0)
 ;;=1^284.2
 ;;^UTILITY(U,$J,358.3,2514,1,8,0)
 ;;=8^Myelophthisic Anemia
 ;;^UTILITY(U,$J,358.3,2514,2)
 ;;=^334037
 ;;^UTILITY(U,$J,358.3,2515,0)
 ;;=281.9^^29^207^7
 ;;^UTILITY(U,$J,358.3,2515,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2515,1,1,0)
 ;;=1^281.9
 ;;^UTILITY(U,$J,358.3,2515,1,8,0)
 ;;=8^Deficiency Anemia
 ;;^UTILITY(U,$J,358.3,2515,2)
 ;;=^123801
 ;;^UTILITY(U,$J,358.3,2516,0)
 ;;=282.41^^29^207^18
 ;;^UTILITY(U,$J,358.3,2516,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2516,1,1,0)
 ;;=1^282.41
 ;;^UTILITY(U,$J,358.3,2516,1,8,0)
 ;;=8^Sickle Cell Thalassemia w/o crisis
 ;;^UTILITY(U,$J,358.3,2516,2)
 ;;=^329908
 ;;^UTILITY(U,$J,358.3,2517,0)
 ;;=284.11^^29^207^5
 ;;^UTILITY(U,$J,358.3,2517,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2517,1,1,0)
 ;;=1^284.11
 ;;^UTILITY(U,$J,358.3,2517,1,8,0)
 ;;=8^Antineoplastic Chemo Indcd Panycyt
 ;;^UTILITY(U,$J,358.3,2517,2)
 ;;=^340499
 ;;^UTILITY(U,$J,358.3,2518,0)
 ;;=284.12^^29^207^15
 ;;^UTILITY(U,$J,358.3,2518,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2518,1,1,0)
 ;;=1^284.12
 ;;^UTILITY(U,$J,358.3,2518,1,8,0)
 ;;=8^Oth Drug Indcd Pancytopenia
 ;;^UTILITY(U,$J,358.3,2518,2)
 ;;=^340500
 ;;^UTILITY(U,$J,358.3,2519,0)
 ;;=284.19^^29^207^16
