IBDEI06K ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,8578,2)
 ;;=^303345
 ;;^UTILITY(U,$J,358.3,8579,0)
 ;;=789.64^^74^630^71
 ;;^UTILITY(U,$J,358.3,8579,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8579,1,4,0)
 ;;=4^789.64
 ;;^UTILITY(U,$J,358.3,8579,1,5,0)
 ;;=5^Llq Abdominal Tenderness
 ;;^UTILITY(U,$J,358.3,8579,2)
 ;;=^303346
 ;;^UTILITY(U,$J,358.3,8580,0)
 ;;=789.65^^74^630^84
 ;;^UTILITY(U,$J,358.3,8580,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8580,1,4,0)
 ;;=4^789.65
 ;;^UTILITY(U,$J,358.3,8580,1,5,0)
 ;;=5^Periumbilical Tenderness
 ;;^UTILITY(U,$J,358.3,8580,2)
 ;;=^303347
 ;;^UTILITY(U,$J,358.3,8581,0)
 ;;=789.66^^74^630^32
 ;;^UTILITY(U,$J,358.3,8581,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8581,1,4,0)
 ;;=4^789.66
 ;;^UTILITY(U,$J,358.3,8581,1,5,0)
 ;;=5^Epigastric Tenderness
 ;;^UTILITY(U,$J,358.3,8581,2)
 ;;=^303348
 ;;^UTILITY(U,$J,358.3,8582,0)
 ;;=070.1^^74^630^52
 ;;^UTILITY(U,$J,358.3,8582,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8582,1,4,0)
 ;;=4^070.1
 ;;^UTILITY(U,$J,358.3,8582,1,5,0)
 ;;=5^Hepatitis A
 ;;^UTILITY(U,$J,358.3,8582,2)
 ;;=^126486
 ;;^UTILITY(U,$J,358.3,8583,0)
 ;;=070.30^^74^630^53
 ;;^UTILITY(U,$J,358.3,8583,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8583,1,4,0)
 ;;=4^070.30
 ;;^UTILITY(U,$J,358.3,8583,1,5,0)
 ;;=5^Hepatitis B, Acute
 ;;^UTILITY(U,$J,358.3,8583,2)
 ;;=^266626
 ;;^UTILITY(U,$J,358.3,8584,0)
 ;;=070.32^^74^630^54
 ;;^UTILITY(U,$J,358.3,8584,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8584,1,4,0)
 ;;=4^070.32
 ;;^UTILITY(U,$J,358.3,8584,1,5,0)
 ;;=5^Hepatitis B, Chronic
 ;;^UTILITY(U,$J,358.3,8584,2)
 ;;=^303249
 ;;^UTILITY(U,$J,358.3,8585,0)
 ;;=070.51^^74^630^55
 ;;^UTILITY(U,$J,358.3,8585,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8585,1,4,0)
 ;;=4^070.51
 ;;^UTILITY(U,$J,358.3,8585,1,5,0)
 ;;=5^Hepatitis C, Acute
 ;;^UTILITY(U,$J,358.3,8585,2)
 ;;=^266632
 ;;^UTILITY(U,$J,358.3,8586,0)
 ;;=070.54^^74^630^56
 ;;^UTILITY(U,$J,358.3,8586,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8586,1,4,0)
 ;;=4^070.54
 ;;^UTILITY(U,$J,358.3,8586,1,5,0)
 ;;=5^Hepatitis C, Chronic
 ;;^UTILITY(U,$J,358.3,8586,2)
 ;;=^303252
 ;;^UTILITY(U,$J,358.3,8587,0)
 ;;=571.41^^74^630^58
 ;;^UTILITY(U,$J,358.3,8587,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8587,1,4,0)
 ;;=4^571.41
 ;;^UTILITY(U,$J,358.3,8587,1,5,0)
 ;;=5^Hepatitis, Chronic Persist
 ;;^UTILITY(U,$J,358.3,8587,2)
 ;;=^259093
 ;;^UTILITY(U,$J,358.3,8588,0)
 ;;=571.1^^74^630^59
 ;;^UTILITY(U,$J,358.3,8588,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8588,1,4,0)
 ;;=4^571.1
 ;;^UTILITY(U,$J,358.3,8588,1,5,0)
 ;;=5^Hepatitis, Etoh Acute
 ;;^UTILITY(U,$J,358.3,8588,2)
 ;;=^2597
 ;;^UTILITY(U,$J,358.3,8589,0)
 ;;=070.59^^74^630^61
 ;;^UTILITY(U,$J,358.3,8589,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8589,1,4,0)
 ;;=4^070.59
 ;;^UTILITY(U,$J,358.3,8589,1,5,0)
 ;;=5^Hepatitis, Other Viral
 ;;^UTILITY(U,$J,358.3,8589,2)
 ;;=^266631
 ;;^UTILITY(U,$J,358.3,8590,0)
 ;;=573.3^^74^630^60
 ;;^UTILITY(U,$J,358.3,8590,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8590,1,4,0)
 ;;=4^573.3
 ;;^UTILITY(U,$J,358.3,8590,1,5,0)
 ;;=5^Hepatitis, Other
 ;;^UTILITY(U,$J,358.3,8590,2)
 ;;=^56268
 ;;^UTILITY(U,$J,358.3,8591,0)
 ;;=555.9^^74^630^16
 ;;^UTILITY(U,$J,358.3,8591,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8591,1,4,0)
 ;;=4^555.9
 ;;^UTILITY(U,$J,358.3,8591,1,5,0)
 ;;=5^Crohn'S Disease
 ;;^UTILITY(U,$J,358.3,8591,2)
 ;;=Crohn's Disease^29356
 ;;^UTILITY(U,$J,358.3,8592,0)
 ;;=787.91^^74^630^17
 ;;^UTILITY(U,$J,358.3,8592,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8592,1,4,0)
 ;;=4^787.91
 ;;^UTILITY(U,$J,358.3,8592,1,5,0)
 ;;=5^Diarrhea
 ;;^UTILITY(U,$J,358.3,8592,2)
 ;;=^33921
 ;;^UTILITY(U,$J,358.3,8593,0)
 ;;=562.11^^74^630^18
 ;;^UTILITY(U,$J,358.3,8593,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8593,1,4,0)
 ;;=4^562.11
 ;;^UTILITY(U,$J,358.3,8593,1,5,0)
 ;;=5^Diverticulitis, Colon
 ;;^UTILITY(U,$J,358.3,8593,2)
 ;;=^270274
 ;;^UTILITY(U,$J,358.3,8594,0)
 ;;=562.10^^74^630^19
 ;;^UTILITY(U,$J,358.3,8594,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8594,1,4,0)
 ;;=4^562.10
 ;;^UTILITY(U,$J,358.3,8594,1,5,0)
 ;;=5^Diverticulosis, Colon
 ;;^UTILITY(U,$J,358.3,8594,2)
 ;;=^35917
 ;;^UTILITY(U,$J,358.3,8595,0)
 ;;=532.90^^74^630^20
 ;;^UTILITY(U,$J,358.3,8595,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8595,1,4,0)
 ;;=4^532.90
 ;;^UTILITY(U,$J,358.3,8595,1,5,0)
 ;;=5^Duodenal Ulcer Nos
 ;;^UTILITY(U,$J,358.3,8595,2)
 ;;=^37311
 ;;^UTILITY(U,$J,358.3,8596,0)
 ;;=536.8^^74^630^21
 ;;^UTILITY(U,$J,358.3,8596,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8596,1,4,0)
 ;;=4^536.8
 ;;^UTILITY(U,$J,358.3,8596,1,5,0)
 ;;=5^Dyspepsia
 ;;^UTILITY(U,$J,358.3,8596,2)
 ;;=^37612
 ;;^UTILITY(U,$J,358.3,8597,0)
 ;;=571.0^^74^630^37
 ;;^UTILITY(U,$J,358.3,8597,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8597,1,4,0)
 ;;=4^571.0
 ;;^UTILITY(U,$J,358.3,8597,1,5,0)
 ;;=5^Fatty Liver W/ Alcohol
 ;;^UTILITY(U,$J,358.3,8597,2)
 ;;=^45182
 ;;^UTILITY(U,$J,358.3,8598,0)
 ;;=571.3^^74^630^36
 ;;^UTILITY(U,$J,358.3,8598,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8598,1,4,0)
 ;;=4^571.3
 ;;^UTILITY(U,$J,358.3,8598,1,5,0)
 ;;=5^Etoh Liver Disease
 ;;^UTILITY(U,$J,358.3,8598,2)
 ;;=ETOH Liver Disease^4638
 ;;^UTILITY(U,$J,358.3,8599,0)
 ;;=530.10^^74^630^34
 ;;^UTILITY(U,$J,358.3,8599,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8599,1,4,0)
 ;;=4^530.10
 ;;^UTILITY(U,$J,358.3,8599,1,5,0)
 ;;=5^Esophagitis, Unsp.
 ;;^UTILITY(U,$J,358.3,8599,2)
 ;;=^295809
 ;;^UTILITY(U,$J,358.3,8600,0)
 ;;=530.81^^74^630^47
 ;;^UTILITY(U,$J,358.3,8600,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8600,1,4,0)
 ;;=4^530.81
 ;;^UTILITY(U,$J,358.3,8600,1,5,0)
 ;;=5^Gerd
 ;;^UTILITY(U,$J,358.3,8600,2)
 ;;=^295749
 ;;^UTILITY(U,$J,358.3,8601,0)
 ;;=456.1^^74^630^33
 ;;^UTILITY(U,$J,358.3,8601,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8601,1,4,0)
 ;;=4^456.1
 ;;^UTILITY(U,$J,358.3,8601,1,5,0)
 ;;=5^Esoph Varices W/O Bleed
 ;;^UTILITY(U,$J,358.3,8601,2)
 ;;=^269836
 ;;^UTILITY(U,$J,358.3,8602,0)
 ;;=571.8^^74^630^38
 ;;^UTILITY(U,$J,358.3,8602,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8602,1,4,0)
 ;;=4^571.8
 ;;^UTILITY(U,$J,358.3,8602,1,5,0)
 ;;=5^Fatty Liver W/O Alcohol
 ;;^UTILITY(U,$J,358.3,8602,2)
 ;;=^87404
 ;;^UTILITY(U,$J,358.3,8603,0)
 ;;=792.1^^74^630^50
 ;;^UTILITY(U,$J,358.3,8603,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8603,1,4,0)
 ;;=4^792.1
 ;;^UTILITY(U,$J,358.3,8603,1,5,0)
 ;;=5^Heme+Stool
 ;;^UTILITY(U,$J,358.3,8603,2)
 ;;=^273412
 ;;^UTILITY(U,$J,358.3,8604,0)
 ;;=564.5^^74^630^39
 ;;^UTILITY(U,$J,358.3,8604,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8604,1,4,0)
 ;;=4^564.5
 ;;^UTILITY(U,$J,358.3,8604,1,5,0)
 ;;=5^Functional Diarrhea
 ;;^UTILITY(U,$J,358.3,8604,2)
 ;;=^270281
 ;;^UTILITY(U,$J,358.3,8605,0)
 ;;=578.9^^74^630^48
 ;;^UTILITY(U,$J,358.3,8605,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8605,1,4,0)
 ;;=4^578.9
 ;;^UTILITY(U,$J,358.3,8605,1,5,0)
 ;;=5^Gi Bleed
 ;;^UTILITY(U,$J,358.3,8605,2)
 ;;=GI Bleed^49525
 ;;^UTILITY(U,$J,358.3,8606,0)
 ;;=531.70^^74^630^41
 ;;^UTILITY(U,$J,358.3,8606,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8606,1,4,0)
 ;;=4^531.70
 ;;^UTILITY(U,$J,358.3,8606,1,5,0)
 ;;=5^Gastric Ulcer, Chronic
 ;;^UTILITY(U,$J,358.3,8606,2)
 ;;=^270086
 ;;^UTILITY(U,$J,358.3,8607,0)
 ;;=535.50^^74^630^42
 ;;^UTILITY(U,$J,358.3,8607,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8607,1,4,0)
 ;;=4^535.50
 ;;^UTILITY(U,$J,358.3,8607,1,5,0)
 ;;=5^Gastritis
 ;;^UTILITY(U,$J,358.3,8607,2)
 ;;=^270181
 ;;^UTILITY(U,$J,358.3,8608,0)
 ;;=041.86^^74^630^49
 ;;^UTILITY(U,$J,358.3,8608,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8608,1,4,0)
 ;;=4^041.86
 ;;^UTILITY(U,$J,358.3,8608,1,5,0)
 ;;=5^H. Pylori Infection
 ;;^UTILITY(U,$J,358.3,8608,2)
 ;;=^303246
 ;;^UTILITY(U,$J,358.3,8609,0)
 ;;=455.6^^74^630^51
 ;;^UTILITY(U,$J,358.3,8609,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8609,1,4,0)
 ;;=4^455.6
 ;;^UTILITY(U,$J,358.3,8609,1,5,0)
 ;;=5^Hemorrhoids Nos
 ;;^UTILITY(U,$J,358.3,8609,2)
 ;;=^123922
 ;;^UTILITY(U,$J,358.3,8610,0)
 ;;=789.1^^74^630^62
 ;;^UTILITY(U,$J,358.3,8610,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8610,1,4,0)
 ;;=4^789.1
 ;;^UTILITY(U,$J,358.3,8610,1,5,0)
 ;;=5^Hepatomegaly
 ;;^UTILITY(U,$J,358.3,8610,2)
 ;;=^56494
 ;;^UTILITY(U,$J,358.3,8611,0)
 ;;=553.3^^74^630^66
 ;;^UTILITY(U,$J,358.3,8611,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8611,1,4,0)
 ;;=4^553.3
 ;;^UTILITY(U,$J,358.3,8611,1,5,0)
 ;;=5^Hiatal Hernia
 ;;^UTILITY(U,$J,358.3,8611,2)
 ;;=^33903
 ;;^UTILITY(U,$J,358.3,8612,0)
 ;;=550.92^^74^630^64
 ;;^UTILITY(U,$J,358.3,8612,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8612,1,4,0)
 ;;=4^550.92
 ;;^UTILITY(U,$J,358.3,8612,1,5,0)
 ;;=5^Hernia, Inguinal, Bilat
 ;;^UTILITY(U,$J,358.3,8612,2)
 ;;=^270212
 ;;^UTILITY(U,$J,358.3,8613,0)
 ;;=550.90^^74^630^65
 ;;^UTILITY(U,$J,358.3,8613,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8613,1,4,0)
 ;;=4^550.90
 ;;^UTILITY(U,$J,358.3,8613,1,5,0)
 ;;=5^Hernia, Inguinal, Unilat
 ;;^UTILITY(U,$J,358.3,8613,2)
 ;;=^63302
 ;;^UTILITY(U,$J,358.3,8614,0)
 ;;=553.9^^74^630^63
 ;;^UTILITY(U,$J,358.3,8614,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8614,1,4,0)
 ;;=4^553.9
 ;;^UTILITY(U,$J,358.3,8614,1,5,0)
 ;;=5^Hernia Nos
 ;;^UTILITY(U,$J,358.3,8614,2)
 ;;=^56659
 ;;^UTILITY(U,$J,358.3,8615,0)
 ;;=564.1^^74^630^69
 ;;^UTILITY(U,$J,358.3,8615,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8615,1,4,0)
 ;;=4^564.1
 ;;^UTILITY(U,$J,358.3,8615,1,5,0)
 ;;=5^Irritable Bowel Syndrome
 ;;^UTILITY(U,$J,358.3,8615,2)
 ;;=^65682^909.2
 ;;^UTILITY(U,$J,358.3,8616,0)
 ;;=787.02^^74^630^75
 ;;^UTILITY(U,$J,358.3,8616,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8616,1,4,0)
 ;;=4^787.02
 ;;^UTILITY(U,$J,358.3,8616,1,5,0)
 ;;=5^Nausea
 ;;^UTILITY(U,$J,358.3,8616,2)
 ;;=^81639
 ;;^UTILITY(U,$J,358.3,8617,0)
 ;;=787.01^^74^630^76
 ;;^UTILITY(U,$J,358.3,8617,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8617,1,4,0)
 ;;=4^787.01
