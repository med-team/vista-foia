IBDEI02W ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,3389,1,5,0)
 ;;=5^Joint Effusion
 ;;^UTILITY(U,$J,358.3,3389,2)
 ;;=^38674
 ;;^UTILITY(U,$J,358.3,3390,0)
 ;;=790.1^^40^252^30
 ;;^UTILITY(U,$J,358.3,3390,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3390,1,4,0)
 ;;=4^790.1
 ;;^UTILITY(U,$J,358.3,3390,1,5,0)
 ;;=5^Elevated Esr
 ;;^UTILITY(U,$J,358.3,3390,2)
 ;;=^39339
 ;;^UTILITY(U,$J,358.3,3391,0)
 ;;=726.32^^40^252^72
 ;;^UTILITY(U,$J,358.3,3391,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3391,1,4,0)
 ;;=4^726.32
 ;;^UTILITY(U,$J,358.3,3391,1,5,0)
 ;;=5^Tennis Elbow
 ;;^UTILITY(U,$J,358.3,3391,2)
 ;;=^117801
 ;;^UTILITY(U,$J,358.3,3392,0)
 ;;=274.9^^40^252^32
 ;;^UTILITY(U,$J,358.3,3392,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3392,1,4,0)
 ;;=4^274.9
 ;;^UTILITY(U,$J,358.3,3392,1,5,0)
 ;;=5^Gout, Unspecified
 ;;^UTILITY(U,$J,358.3,3392,2)
 ;;=^52625
 ;;^UTILITY(U,$J,358.3,3393,0)
 ;;=714.9^^40^252^35
 ;;^UTILITY(U,$J,358.3,3393,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3393,1,4,0)
 ;;=4^714.9
 ;;^UTILITY(U,$J,358.3,3393,1,5,0)
 ;;=5^Inflammatory Polyarthropathy
 ;;^UTILITY(U,$J,358.3,3393,2)
 ;;=^272122
 ;;^UTILITY(U,$J,358.3,3394,0)
 ;;=724.4^^40^252^39
 ;;^UTILITY(U,$J,358.3,3394,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3394,1,4,0)
 ;;=4^724.4
 ;;^UTILITY(U,$J,358.3,3394,1,5,0)
 ;;=5^Lumbar Radiculopathy
 ;;^UTILITY(U,$J,358.3,3394,2)
 ;;=^272510
 ;;^UTILITY(U,$J,358.3,3395,0)
 ;;=728.85^^40^252^60
 ;;^UTILITY(U,$J,358.3,3395,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3395,1,4,0)
 ;;=4^728.85
 ;;^UTILITY(U,$J,358.3,3395,1,5,0)
 ;;=5^Spasm Of Muscle
 ;;^UTILITY(U,$J,358.3,3395,2)
 ;;=^112558
 ;;^UTILITY(U,$J,358.3,3396,0)
 ;;=733.00^^40^252^44
 ;;^UTILITY(U,$J,358.3,3396,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3396,1,4,0)
 ;;=4^733.00
 ;;^UTILITY(U,$J,358.3,3396,1,5,0)
 ;;=5^Osteoporosis Nos
 ;;^UTILITY(U,$J,358.3,3396,2)
 ;;=^87159
 ;;^UTILITY(U,$J,358.3,3397,0)
 ;;=728.71^^40^252^50
 ;;^UTILITY(U,$J,358.3,3397,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3397,1,4,0)
 ;;=4^728.71
 ;;^UTILITY(U,$J,358.3,3397,1,5,0)
 ;;=5^Plantar Fascitis
 ;;^UTILITY(U,$J,358.3,3397,2)
 ;;=^272598
 ;;^UTILITY(U,$J,358.3,3398,0)
 ;;=696.0^^40^252^51
 ;;^UTILITY(U,$J,358.3,3398,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3398,1,4,0)
 ;;=4^696.0
 ;;^UTILITY(U,$J,358.3,3398,1,5,0)
 ;;=5^Psoriatic Arthritis
 ;;^UTILITY(U,$J,358.3,3398,2)
 ;;=^100320
 ;;^UTILITY(U,$J,358.3,3399,0)
 ;;=337.20^^40^252^53
 ;;^UTILITY(U,$J,358.3,3399,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3399,1,4,0)
 ;;=4^337.20
 ;;^UTILITY(U,$J,358.3,3399,1,5,0)
 ;;=5^Reflex Symp. Dyst
 ;;^UTILITY(U,$J,358.3,3399,2)
 ;;=^295799
 ;;^UTILITY(U,$J,358.3,3400,0)
 ;;=099.3^^40^252^54
 ;;^UTILITY(U,$J,358.3,3400,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3400,1,4,0)
 ;;=4^099.3
 ;;^UTILITY(U,$J,358.3,3400,1,5,0)
 ;;=5^Reiter's Disease
 ;;^UTILITY(U,$J,358.3,3400,2)
 ;;=Reiter's Disease^104534^711.10
 ;;^UTILITY(U,$J,358.3,3401,0)
 ;;=714.0^^40^252^55
 ;;^UTILITY(U,$J,358.3,3401,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3401,1,4,0)
 ;;=4^714.0
 ;;^UTILITY(U,$J,358.3,3401,1,5,0)
 ;;=5^Rheumatoid Arthritis
 ;;^UTILITY(U,$J,358.3,3401,2)
 ;;=^10473
 ;;^UTILITY(U,$J,358.3,3402,0)
 ;;=726.10^^40^252^56
 ;;^UTILITY(U,$J,358.3,3402,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3402,1,4,0)
 ;;=4^726.10
 ;;^UTILITY(U,$J,358.3,3402,1,5,0)
 ;;=5^Rotator Cuff Synd
 ;;^UTILITY(U,$J,358.3,3402,2)
 ;;=^272523
 ;;^UTILITY(U,$J,358.3,3403,0)
 ;;=720.2^^40^252^57
 ;;^UTILITY(U,$J,358.3,3403,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3403,1,4,0)
 ;;=4^720.2
 ;;^UTILITY(U,$J,358.3,3403,1,5,0)
 ;;=5^Sacroiliitis Nec
 ;;^UTILITY(U,$J,358.3,3403,2)
 ;;=^259118
 ;;^UTILITY(U,$J,358.3,3404,0)
 ;;=724.3^^40^252^58
 ;;^UTILITY(U,$J,358.3,3404,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3404,1,4,0)
 ;;=4^724.3
 ;;^UTILITY(U,$J,358.3,3404,1,5,0)
 ;;=5^Sciatica
 ;;^UTILITY(U,$J,358.3,3404,2)
 ;;=^108484
 ;;^UTILITY(U,$J,358.3,3405,0)
 ;;=724.00^^40^252^61
 ;;^UTILITY(U,$J,358.3,3405,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3405,1,4,0)
 ;;=4^724.00
 ;;^UTILITY(U,$J,358.3,3405,1,5,0)
 ;;=5^Spinal Stenosis
 ;;^UTILITY(U,$J,358.3,3405,2)
 ;;=^113279
 ;;^UTILITY(U,$J,358.3,3406,0)
 ;;=848.9^^40^252^63
 ;;^UTILITY(U,$J,358.3,3406,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3406,1,4,0)
 ;;=4^848.9
 ;;^UTILITY(U,$J,358.3,3406,1,5,0)
 ;;=5^Sprain Or Strain Nos
 ;;^UTILITY(U,$J,358.3,3406,2)
 ;;=^123990
 ;;^UTILITY(U,$J,358.3,3407,0)
 ;;=845.00^^40^252^65
 ;;^UTILITY(U,$J,358.3,3407,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3407,1,4,0)
 ;;=4^845.00
 ;;^UTILITY(U,$J,358.3,3407,1,5,0)
 ;;=5^Sprain, Ankle
 ;;^UTILITY(U,$J,358.3,3407,2)
 ;;=^274507
 ;;^UTILITY(U,$J,358.3,3408,0)
 ;;=842.00^^40^252^67
 ;;^UTILITY(U,$J,358.3,3408,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3408,1,4,0)
 ;;=4^842.00
 ;;^UTILITY(U,$J,358.3,3408,1,5,0)
 ;;=5^Sprain, Wrist
 ;;^UTILITY(U,$J,358.3,3408,2)
 ;;=^274483
 ;;^UTILITY(U,$J,358.3,3409,0)
 ;;=847.0^^40^252^66
 ;;^UTILITY(U,$J,358.3,3409,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3409,1,4,0)
 ;;=4^847.0
 ;;^UTILITY(U,$J,358.3,3409,1,5,0)
 ;;=5^Sprain, Cervical
 ;;^UTILITY(U,$J,358.3,3409,2)
 ;;=^81735
 ;;^UTILITY(U,$J,358.3,3410,0)
 ;;=847.2^^40^252^62
 ;;^UTILITY(U,$J,358.3,3410,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3410,1,4,0)
 ;;=4^847.2
 ;;^UTILITY(U,$J,358.3,3410,1,5,0)
 ;;=5^Sprain Lumbar
 ;;^UTILITY(U,$J,358.3,3410,2)
 ;;=^274527
 ;;^UTILITY(U,$J,358.3,3411,0)
 ;;=847.1^^40^252^64
 ;;^UTILITY(U,$J,358.3,3411,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3411,1,4,0)
 ;;=4^847.1
 ;;^UTILITY(U,$J,358.3,3411,1,5,0)
 ;;=5^Sprain Thoracic
 ;;^UTILITY(U,$J,358.3,3411,2)
 ;;=^274526
 ;;^UTILITY(U,$J,358.3,3412,0)
 ;;=729.81^^40^252^68
 ;;^UTILITY(U,$J,358.3,3412,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3412,1,4,0)
 ;;=4^729.81
 ;;^UTILITY(U,$J,358.3,3412,1,5,0)
 ;;=5^Swelling Of Limb
 ;;^UTILITY(U,$J,358.3,3412,2)
 ;;=^272609
 ;;^UTILITY(U,$J,358.3,3413,0)
 ;;=710.0^^40^252^69
 ;;^UTILITY(U,$J,358.3,3413,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3413,1,4,0)
 ;;=4^710.0
 ;;^UTILITY(U,$J,358.3,3413,1,5,0)
 ;;=5^Syst Lupus Erythematosis
 ;;^UTILITY(U,$J,358.3,3413,2)
 ;;=^72159
 ;;^UTILITY(U,$J,358.3,3414,0)
 ;;=524.60^^40^252^70
 ;;^UTILITY(U,$J,358.3,3414,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3414,1,4,0)
 ;;=4^524.60
 ;;^UTILITY(U,$J,358.3,3414,1,5,0)
 ;;=5^TMJ Syndrome
 ;;^UTILITY(U,$J,358.3,3414,2)
 ;;=^117722
 ;;^UTILITY(U,$J,358.3,3415,0)
 ;;=726.90^^40^252^71
 ;;^UTILITY(U,$J,358.3,3415,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3415,1,4,0)
 ;;=4^726.90
 ;;^UTILITY(U,$J,358.3,3415,1,5,0)
 ;;=5^Tendonitis
 ;;^UTILITY(U,$J,358.3,3415,2)
 ;;=^41010
 ;;^UTILITY(U,$J,358.3,3416,0)
 ;;=354.2^^40^252^73
 ;;^UTILITY(U,$J,358.3,3416,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3416,1,4,0)
 ;;=4^354.2
 ;;^UTILITY(U,$J,358.3,3416,1,5,0)
 ;;=5^Ulnar Nerve Entrapment
 ;;^UTILITY(U,$J,358.3,3416,2)
 ;;=^268506
 ;;^UTILITY(U,$J,358.3,3417,0)
 ;;=715.97^^40^252^16
 ;;^UTILITY(U,$J,358.3,3417,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3417,1,4,0)
 ;;=4^715.97
 ;;^UTILITY(U,$J,358.3,3417,1,5,0)
 ;;=5^DJD Ankle/Foot
 ;;^UTILITY(U,$J,358.3,3417,2)
 ;;=DJD Ankle/Foot^272168
 ;;^UTILITY(U,$J,358.3,3418,0)
 ;;=731.0^^40^252^47
 ;;^UTILITY(U,$J,358.3,3418,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3418,1,4,0)
 ;;=4^731.0
 ;;^UTILITY(U,$J,358.3,3418,1,5,0)
 ;;=5^Paget's Disease
 ;;^UTILITY(U,$J,358.3,3418,2)
 ;;=Paget's Disease^86892
 ;;^UTILITY(U,$J,358.3,3419,0)
 ;;=733.01^^40^252^46
 ;;^UTILITY(U,$J,358.3,3419,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3419,1,4,0)
 ;;=4^733.01
 ;;^UTILITY(U,$J,358.3,3419,1,5,0)
 ;;=5^Osteoporosis, Senile
 ;;^UTILITY(U,$J,358.3,3419,2)
 ;;=Osteoporosis, Senile^87188
 ;;^UTILITY(U,$J,358.3,3420,0)
 ;;=733.02^^40^252^45
 ;;^UTILITY(U,$J,358.3,3420,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3420,1,4,0)
 ;;=4^733.02
 ;;^UTILITY(U,$J,358.3,3420,1,5,0)
 ;;=5^Osteoporosis, Idiopathic
 ;;^UTILITY(U,$J,358.3,3420,2)
 ;;=Osteoporosis, Idiopathic^272692
 ;;^UTILITY(U,$J,358.3,3421,0)
 ;;=733.90^^40^252^43
 ;;^UTILITY(U,$J,358.3,3421,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3421,1,4,0)
 ;;=4^733.90
 ;;^UTILITY(U,$J,358.3,3421,1,5,0)
 ;;=5^Osteopenia
 ;;^UTILITY(U,$J,358.3,3421,2)
 ;;=Osteopenia^35593
 ;;^UTILITY(U,$J,358.3,3422,0)
 ;;=733.13^^40^252^11
 ;;^UTILITY(U,$J,358.3,3422,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3422,1,4,0)
 ;;=4^733.13
 ;;^UTILITY(U,$J,358.3,3422,1,5,0)
 ;;=5^Compression Fx of Spine
 ;;^UTILITY(U,$J,358.3,3422,2)
 ;;=Compression Fx of Spine^295754
 ;;^UTILITY(U,$J,358.3,3423,0)
 ;;=274.00^^40^252^33
 ;;^UTILITY(U,$J,358.3,3423,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3423,1,4,0)
 ;;=4^274.00
 ;;^UTILITY(U,$J,358.3,3423,1,5,0)
 ;;=5^Gouty Arthritis NOS
 ;;^UTILITY(U,$J,358.3,3423,2)
 ;;=^338313
 ;;^UTILITY(U,$J,358.3,3424,0)
 ;;=722.93^^40^252^38
 ;;^UTILITY(U,$J,358.3,3424,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3424,1,4,0)
 ;;=4^722.93
 ;;^UTILITY(U,$J,358.3,3424,1,5,0)
 ;;=5^L-S DISC DISEASE
 ;;^UTILITY(U,$J,358.3,3424,2)
 ;;=^272495
 ;;^UTILITY(U,$J,358.3,3425,0)
 ;;=807.00^^40^252^52
 ;;^UTILITY(U,$J,358.3,3425,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3425,1,4,0)
 ;;=4^807.00
 ;;^UTILITY(U,$J,358.3,3425,1,5,0)
 ;;=5^RIB RX-CLOSED,NOS
 ;;^UTILITY(U,$J,358.3,3425,2)
 ;;=^25317
 ;;^UTILITY(U,$J,358.3,3426,0)
 ;;=781.0^^40^253^42
 ;;^UTILITY(U,$J,358.3,3426,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3426,1,4,0)
 ;;=4^781.0
 ;;^UTILITY(U,$J,358.3,3426,1,5,0)
 ;;=5^Tremor
 ;;^UTILITY(U,$J,358.3,3426,2)
 ;;=^23827
 ;;^UTILITY(U,$J,358.3,3427,0)
 ;;=351.0^^40^253^1
 ;;^UTILITY(U,$J,358.3,3427,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3427,1,4,0)
 ;;=4^351.0
 ;;^UTILITY(U,$J,358.3,3427,1,5,0)
 ;;=5^Bell's Palsy
 ;;^UTILITY(U,$J,358.3,3427,2)
 ;;=Bell's Palsy^13238
 ;;^UTILITY(U,$J,358.3,3428,0)
 ;;=386.11^^40^253^45
