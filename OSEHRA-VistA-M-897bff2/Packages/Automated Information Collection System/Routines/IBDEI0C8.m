IBDEI0C8 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,16267,0)
 ;;=824.8^^119^1021^11
 ;;^UTILITY(U,$J,358.3,16267,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16267,1,3,0)
 ;;=3^824.8
 ;;^UTILITY(U,$J,358.3,16267,1,5,0)
 ;;=5^Fracture of ankle; unspecified, closed
 ;;^UTILITY(U,$J,358.3,16267,2)
 ;;=^274256
 ;;^UTILITY(U,$J,358.3,16268,0)
 ;;=824.4^^119^1021^12
 ;;^UTILITY(U,$J,358.3,16268,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16268,1,3,0)
 ;;=3^824.4
 ;;^UTILITY(U,$J,358.3,16268,1,5,0)
 ;;=5^Fracture of ankle; bimalleolar, closed
 ;;^UTILITY(U,$J,358.3,16268,2)
 ;;=^14370
 ;;^UTILITY(U,$J,358.3,16269,0)
 ;;=824.2^^119^1021^13
 ;;^UTILITY(U,$J,358.3,16269,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16269,1,3,0)
 ;;=3^824.2
 ;;^UTILITY(U,$J,358.3,16269,1,5,0)
 ;;=5^Fracture of ankle; lateral malleolus, closed
 ;;^UTILITY(U,$J,358.3,16269,2)
 ;;=^274247
 ;;^UTILITY(U,$J,358.3,16270,0)
 ;;=824.0^^119^1021^14
 ;;^UTILITY(U,$J,358.3,16270,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16270,1,3,0)
 ;;=3^824.0
 ;;^UTILITY(U,$J,358.3,16270,1,5,0)
 ;;=5^Fracture of ankle; medial malleolus, closed
 ;;^UTILITY(U,$J,358.3,16270,2)
 ;;=^274245
 ;;^UTILITY(U,$J,358.3,16271,0)
 ;;=824.6^^119^1021^15
 ;;^UTILITY(U,$J,358.3,16271,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16271,1,3,0)
 ;;=3^824.6
 ;;^UTILITY(U,$J,358.3,16271,1,5,0)
 ;;=5^Fracture of ankle; trimalleolar, closed
 ;;^UTILITY(U,$J,358.3,16271,2)
 ;;=^274251
 ;;^UTILITY(U,$J,358.3,16272,0)
 ;;=825.20^^119^1021^16
 ;;^UTILITY(U,$J,358.3,16272,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16272,1,3,0)
 ;;=3^825.20
 ;;^UTILITY(U,$J,358.3,16272,1,5,0)
 ;;=5^Fracture of other tarsal & metatarsal bone, closed; unspecified bone(s) of foot (except toes)
 ;;^UTILITY(U,$J,358.3,16272,2)
 ;;=^274265
 ;;^UTILITY(U,$J,358.3,16273,0)
 ;;=825.0^^119^1021^17
 ;;^UTILITY(U,$J,358.3,16273,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16273,1,3,0)
 ;;=3^825.0
 ;;^UTILITY(U,$J,358.3,16273,1,5,0)
 ;;=5^Fracture of one or more tarsal & metatarsal bone, closed; calcaneus, closed
 ;;^UTILITY(U,$J,358.3,16273,2)
 ;;=^274259
 ;;^UTILITY(U,$J,358.3,16274,0)
 ;;=825.23^^119^1021^18
 ;;^UTILITY(U,$J,358.3,16274,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16274,1,3,0)
 ;;=3^825.23
 ;;^UTILITY(U,$J,358.3,16274,1,5,0)
 ;;=5^Fracture of other tarsal & metatarsal bones, closed; Cuboid
 ;;^UTILITY(U,$J,358.3,16274,2)
 ;;=^274270
 ;;^UTILITY(U,$J,358.3,16275,0)
 ;;=825.24^^119^1021^19
 ;;^UTILITY(U,$J,358.3,16275,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16275,1,3,0)
 ;;=3^825.24
 ;;^UTILITY(U,$J,358.3,16275,1,5,0)
 ;;=5^Fracture of other tarsal & metatarsal bones, closed; Cuneiform, foot &
 ;;^UTILITY(U,$J,358.3,16275,2)
 ;;=^274271
 ;;^UTILITY(U,$J,358.3,16276,0)
 ;;=825.25^^119^1021^20
 ;;^UTILITY(U,$J,358.3,16276,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16276,1,3,0)
 ;;=3^825.25
 ;;^UTILITY(U,$J,358.3,16276,1,5,0)
 ;;=5^Fracture of other tarsal & metatarsal bones, closed; Jones 
 ;;^UTILITY(U,$J,358.3,16276,2)
 ;;=^274272
 ;;^UTILITY(U,$J,358.3,16277,0)
 ;;=825.22^^119^1021^21
 ;;^UTILITY(U,$J,358.3,16277,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16277,1,3,0)
 ;;=3^825.22
 ;;^UTILITY(U,$J,358.3,16277,1,5,0)
 ;;=5^Fracture of other tarsal & metatarsal bones, closed; Navicular (scapiod), foot & metatarsal
 ;;^UTILITY(U,$J,358.3,16277,2)
 ;;=^274269
 ;;^UTILITY(U,$J,358.3,16278,0)
 ;;=825.21^^119^1021^22
 ;;^UTILITY(U,$J,358.3,16278,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16278,1,3,0)
 ;;=3^825.21
 ;;^UTILITY(U,$J,358.3,16278,1,5,0)
 ;;=5^Fracture of other tarsal & metatarsal bones, closed; Astragalus, talus
 ;;^UTILITY(U,$J,358.3,16278,2)
 ;;=^274266
 ;;^UTILITY(U,$J,358.3,16279,0)
 ;;=825.29^^119^1021^23
 ;;^UTILITY(U,$J,358.3,16279,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16279,1,3,0)
 ;;=3^825.29
 ;;^UTILITY(U,$J,358.3,16279,1,5,0)
 ;;=5^Fracture of other tarsal & metatarsal bones, closed; Tarsal or tarsal w/metatarsal
 ;;^UTILITY(U,$J,358.3,16279,2)
 ;;=^274263
 ;;^UTILITY(U,$J,358.3,16280,0)
 ;;=826.0^^119^1021^24
 ;;^UTILITY(U,$J,358.3,16280,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16280,1,3,0)
 ;;=3^826.0
 ;;^UTILITY(U,$J,358.3,16280,1,5,0)
 ;;=5^Fracture of one or more phalanges of foot/toe(s), closed
 ;;^UTILITY(U,$J,358.3,16280,2)
 ;;=^274282
 ;;^UTILITY(U,$J,358.3,16281,0)
 ;;=733.16^^119^1021^25
 ;;^UTILITY(U,$J,358.3,16281,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16281,1,3,0)
 ;;=3^733.16
 ;;^UTILITY(U,$J,358.3,16281,1,5,0)
 ;;=5^Fracture-stress/pathologic/spontaneous; tibia or fibula
 ;;^UTILITY(U,$J,358.3,16281,2)
 ;;=^295757
 ;;^UTILITY(U,$J,358.3,16282,0)
 ;;=733.19^^119^1021^26
 ;;^UTILITY(U,$J,358.3,16282,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16282,1,3,0)
 ;;=3^733.19
 ;;^UTILITY(U,$J,358.3,16282,1,5,0)
 ;;=5^Fracture-stress/pathologic/spontanous; foot, toe, other of lower extremities
 ;;^UTILITY(U,$J,358.3,16282,2)
 ;;=^295758
 ;;^UTILITY(U,$J,358.3,16283,0)
 ;;=991.2^^119^1021^27
 ;;^UTILITY(U,$J,358.3,16283,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16283,1,3,0)
 ;;=3^991.2
 ;;^UTILITY(U,$J,358.3,16283,1,5,0)
 ;;=5^Frostbite of foot
 ;;^UTILITY(U,$J,358.3,16283,2)
 ;;=^276237
 ;;^UTILITY(U,$J,358.3,16284,0)
 ;;=909.4^^119^1021^28
 ;;^UTILITY(U,$J,358.3,16284,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16284,1,3,0)
 ;;=3^909.4
 ;;^UTILITY(U,$J,358.3,16284,1,5,0)
 ;;=5^Frostbite of foot; late effect of certain other external causes
 ;;^UTILITY(U,$J,358.3,16284,2)
 ;;=^275260
 ;;^UTILITY(U,$J,358.3,16285,0)
 ;;=781.2^^119^1022^1
 ;;^UTILITY(U,$J,358.3,16285,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16285,1,3,0)
 ;;=3^781.2
 ;;^UTILITY(U,$J,358.3,16285,1,5,0)
 ;;=5^Gait abnormality
 ;;^UTILITY(U,$J,358.3,16285,2)
 ;;=^48820
 ;;^UTILITY(U,$J,358.3,16286,0)
 ;;=727.41^^119^1022^2
 ;;^UTILITY(U,$J,358.3,16286,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16286,1,3,0)
 ;;=3^727.41
 ;;^UTILITY(U,$J,358.3,16286,1,5,0)
 ;;=5^Ganglion of joint
 ;;^UTILITY(U,$J,358.3,16286,2)
 ;;=^272567
 ;;^UTILITY(U,$J,358.3,16287,0)
 ;;=727.42^^119^1022^3
 ;;^UTILITY(U,$J,358.3,16287,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16287,1,3,0)
 ;;=3^727.42
 ;;^UTILITY(U,$J,358.3,16287,1,5,0)
 ;;=5^Ganglion of tendon sheath
 ;;^UTILITY(U,$J,358.3,16287,2)
 ;;=^272569
 ;;^UTILITY(U,$J,358.3,16288,0)
 ;;=727.43^^119^1022^4
 ;;^UTILITY(U,$J,358.3,16288,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16288,1,3,0)
 ;;=3^727.43
 ;;^UTILITY(U,$J,358.3,16288,1,5,0)
 ;;=5^Ganglion, unspecified
 ;;^UTILITY(U,$J,358.3,16288,2)
 ;;=^186803
 ;;^UTILITY(U,$J,358.3,16289,0)
 ;;=785.4^^119^1022^5
 ;;^UTILITY(U,$J,358.3,16289,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16289,1,3,0)
 ;;=3^785.4
 ;;^UTILITY(U,$J,358.3,16289,1,5,0)
 ;;=5^Gangrene
 ;;^UTILITY(U,$J,358.3,16289,2)
 ;;=^49194
 ;;^UTILITY(U,$J,358.3,16290,0)
 ;;=274.9^^119^1022^7
 ;;^UTILITY(U,$J,358.3,16290,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16290,1,3,0)
 ;;=3^274.9
 ;;^UTILITY(U,$J,358.3,16290,1,5,0)
 ;;=5^Gout, unspecified
 ;;^UTILITY(U,$J,358.3,16290,2)
 ;;=^52625
 ;;^UTILITY(U,$J,358.3,16291,0)
 ;;=274.00^^119^1022^6
 ;;^UTILITY(U,$J,358.3,16291,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16291,1,3,0)
 ;;=3^274.00
 ;;^UTILITY(U,$J,358.3,16291,1,5,0)
 ;;=5^Gout,joint or gouty arthritis
 ;;^UTILITY(U,$J,358.3,16291,2)
 ;;=^338313
 ;;^UTILITY(U,$J,358.3,16292,0)
 ;;=732.5^^119^1023^1
 ;;^UTILITY(U,$J,358.3,16292,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16292,1,3,0)
 ;;=3^732.5
 ;;^UTILITY(U,$J,358.3,16292,1,5,0)
 ;;=5^Haglunds disease
 ;;^UTILITY(U,$J,358.3,16292,2)
 ;;=^272687
 ;;^UTILITY(U,$J,358.3,16293,0)
 ;;=755.66^^119^1023^2
 ;;^UTILITY(U,$J,358.3,16293,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16293,1,3,0)
 ;;=3^755.66
 ;;^UTILITY(U,$J,358.3,16293,1,5,0)
 ;;=5^Hallux, congenital (any type)
 ;;^UTILITY(U,$J,358.3,16293,2)
 ;;=^273059
 ;;^UTILITY(U,$J,358.3,16294,0)
 ;;=735.3^^119^1023^3
 ;;^UTILITY(U,$J,358.3,16294,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16294,1,3,0)
 ;;=3^735.3
 ;;^UTILITY(U,$J,358.3,16294,1,5,0)
 ;;=5^Hallux malleus
 ;;^UTILITY(U,$J,358.3,16294,2)
 ;;=^272710
 ;;^UTILITY(U,$J,358.3,16295,0)
 ;;=735.2^^119^1023^4
 ;;^UTILITY(U,$J,358.3,16295,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16295,1,3,0)
 ;;=3^735.2
 ;;^UTILITY(U,$J,358.3,16295,1,5,0)
 ;;=5^Hallux rigidus
 ;;^UTILITY(U,$J,358.3,16295,2)
 ;;=^265483
 ;;^UTILITY(U,$J,358.3,16296,0)
 ;;=735.0^^119^1023^5
 ;;^UTILITY(U,$J,358.3,16296,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16296,1,3,0)
 ;;=3^735.0
 ;;^UTILITY(U,$J,358.3,16296,1,5,0)
 ;;=5^Hallux valgus (acquired)
 ;;^UTILITY(U,$J,358.3,16296,2)
 ;;=^53764
 ;;^UTILITY(U,$J,358.3,16297,0)
 ;;=735.1^^119^1023^6
 ;;^UTILITY(U,$J,358.3,16297,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16297,1,3,0)
 ;;=3^735.1
 ;;^UTILITY(U,$J,358.3,16297,1,5,0)
 ;;=5^Hallux varus (acquired)
 ;;^UTILITY(U,$J,358.3,16297,2)
 ;;=^272709
 ;;^UTILITY(U,$J,358.3,16298,0)
 ;;=735.8^^119^1023^7
 ;;^UTILITY(U,$J,358.3,16298,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16298,1,3,0)
 ;;=3^735.8
 ;;^UTILITY(U,$J,358.3,16298,1,5,0)
 ;;=5^Hallux other (flex, extensus, etc)
 ;;^UTILITY(U,$J,358.3,16298,2)
 ;;=^272714
 ;;^UTILITY(U,$J,358.3,16299,0)
 ;;=735.4^^119^1023^8
 ;;^UTILITY(U,$J,358.3,16299,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16299,1,3,0)
 ;;=3^735.4
 ;;^UTILITY(U,$J,358.3,16299,1,5,0)
 ;;=5^Hammertoe
 ;;^UTILITY(U,$J,358.3,16299,2)
 ;;=^272712
 ;;^UTILITY(U,$J,358.3,16300,0)
 ;;=726.73^^119^1023^9
 ;;^UTILITY(U,$J,358.3,16300,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16300,1,3,0)
 ;;=3^726.73
 ;;^UTILITY(U,$J,358.3,16300,1,5,0)
 ;;=5^Heel spur
 ;;^UTILITY(U,$J,358.3,16300,2)
 ;;=^272553
 ;;^UTILITY(U,$J,358.3,16301,0)
 ;;=924.21^^119^1023^10
 ;;^UTILITY(U,$J,358.3,16301,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16301,1,3,0)
 ;;=3^924.21
 ;;^UTILITY(U,$J,358.3,16301,1,5,0)
 ;;=5^Hematoma, ankle
