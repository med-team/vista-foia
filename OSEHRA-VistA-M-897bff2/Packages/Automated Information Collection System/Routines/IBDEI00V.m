IBDEI00V ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,480,1,5,0)
 ;;=5^Bronchopneumonia
 ;;^UTILITY(U,$J,358.3,480,2)
 ;;=^17194
 ;;^UTILITY(U,$J,358.3,481,0)
 ;;=490.^^7^35^17
 ;;^UTILITY(U,$J,358.3,481,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,481,1,4,0)
 ;;=4^490.
 ;;^UTILITY(U,$J,358.3,481,1,5,0)
 ;;=5^Bronchitis
 ;;^UTILITY(U,$J,358.3,481,2)
 ;;=^17164
 ;;^UTILITY(U,$J,358.3,482,0)
 ;;=491.0^^7^35^21
 ;;^UTILITY(U,$J,358.3,482,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,482,1,4,0)
 ;;=4^491.0
 ;;^UTILITY(U,$J,358.3,482,1,5,0)
 ;;=5^Bronchitis,Chronic Simple
 ;;^UTILITY(U,$J,358.3,482,2)
 ;;=^269946
 ;;^UTILITY(U,$J,358.3,483,0)
 ;;=491.20^^7^35^19
 ;;^UTILITY(U,$J,358.3,483,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,483,1,4,0)
 ;;=4^491.20
 ;;^UTILITY(U,$J,358.3,483,1,5,0)
 ;;=5^Bronchitis,Asthmatic
 ;;^UTILITY(U,$J,358.3,483,2)
 ;;=^330083
 ;;^UTILITY(U,$J,358.3,484,0)
 ;;=491.21^^7^35^22
 ;;^UTILITY(U,$J,358.3,484,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,484,1,4,0)
 ;;=4^491.21
 ;;^UTILITY(U,$J,358.3,484,1,5,0)
 ;;=5^Bronchitis,Exacerbation
 ;;^UTILITY(U,$J,358.3,484,2)
 ;;=^330084
 ;;^UTILITY(U,$J,358.3,485,0)
 ;;=491.9^^7^35^20
 ;;^UTILITY(U,$J,358.3,485,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,485,1,4,0)
 ;;=4^491.9
 ;;^UTILITY(U,$J,358.3,485,1,5,0)
 ;;=5^Bronchitis,Chronic NOS
 ;;^UTILITY(U,$J,358.3,485,2)
 ;;=^24359
 ;;^UTILITY(U,$J,358.3,486,0)
 ;;=492.8^^7^35^44
 ;;^UTILITY(U,$J,358.3,486,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,486,1,4,0)
 ;;=4^492.8
 ;;^UTILITY(U,$J,358.3,486,1,5,0)
 ;;=5^Emphysema
 ;;^UTILITY(U,$J,358.3,486,2)
 ;;=^87569
 ;;^UTILITY(U,$J,358.3,487,0)
 ;;=493.00^^7^35^13
 ;;^UTILITY(U,$J,358.3,487,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,487,1,4,0)
 ;;=4^493.00
 ;;^UTILITY(U,$J,358.3,487,1,5,0)
 ;;=5^Asthma,Extrinsic
 ;;^UTILITY(U,$J,358.3,487,2)
 ;;=^330085
 ;;^UTILITY(U,$J,358.3,488,0)
 ;;=493.10^^7^35^14
 ;;^UTILITY(U,$J,358.3,488,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,488,1,4,0)
 ;;=4^493.10
 ;;^UTILITY(U,$J,358.3,488,1,5,0)
 ;;=5^Asthma,Intrinsic
 ;;^UTILITY(U,$J,358.3,488,2)
 ;;=^330087
 ;;^UTILITY(U,$J,358.3,489,0)
 ;;=493.82^^7^35^12
 ;;^UTILITY(U,$J,358.3,489,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,489,1,4,0)
 ;;=4^493.82
 ;;^UTILITY(U,$J,358.3,489,1,5,0)
 ;;=5^Asthma,Cough Variant
 ;;^UTILITY(U,$J,358.3,489,2)
 ;;=^329927
 ;;^UTILITY(U,$J,358.3,490,0)
 ;;=493.90^^7^35^9
 ;;^UTILITY(U,$J,358.3,490,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,490,1,4,0)
 ;;=4^493.90
 ;;^UTILITY(U,$J,358.3,490,1,5,0)
 ;;=5^Asthma NOS
 ;;^UTILITY(U,$J,358.3,490,2)
 ;;=^330091
 ;;^UTILITY(U,$J,358.3,491,0)
 ;;=493.91^^7^35^11
 ;;^UTILITY(U,$J,358.3,491,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,491,1,4,0)
 ;;=4^493.91
 ;;^UTILITY(U,$J,358.3,491,1,5,0)
 ;;=5^Asthma w/Status Asthmaticus
 ;;^UTILITY(U,$J,358.3,491,2)
 ;;=^269967
 ;;^UTILITY(U,$J,358.3,492,0)
 ;;=493.92^^7^35^10
 ;;^UTILITY(U,$J,358.3,492,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,492,1,4,0)
 ;;=4^493.92
 ;;^UTILITY(U,$J,358.3,492,1,5,0)
 ;;=5^Asthma w/Acute Exacerbation
 ;;^UTILITY(U,$J,358.3,492,2)
 ;;=^330092
 ;;^UTILITY(U,$J,358.3,493,0)
 ;;=494.0^^7^35^16
 ;;^UTILITY(U,$J,358.3,493,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,493,1,4,0)
 ;;=4^494.0
 ;;^UTILITY(U,$J,358.3,493,1,5,0)
 ;;=5^Bronchiectasis
 ;;^UTILITY(U,$J,358.3,493,2)
 ;;=^321990
 ;;^UTILITY(U,$J,358.3,494,0)
 ;;=496.^^7^35^24
 ;;^UTILITY(U,$J,358.3,494,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,494,1,4,0)
 ;;=4^496.
 ;;^UTILITY(U,$J,358.3,494,1,5,0)
 ;;=5^COPD
 ;;^UTILITY(U,$J,358.3,494,2)
 ;;=^24355
 ;;^UTILITY(U,$J,358.3,495,0)
 ;;=530.11^^7^35^45
 ;;^UTILITY(U,$J,358.3,495,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,495,1,4,0)
 ;;=4^530.11
 ;;^UTILITY(U,$J,358.3,495,1,5,0)
 ;;=5^Esophagitis,Reflux
 ;;^UTILITY(U,$J,358.3,495,2)
 ;;=^295747
 ;;^UTILITY(U,$J,358.3,496,0)
 ;;=530.81^^7^35^52
 ;;^UTILITY(U,$J,358.3,496,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,496,1,4,0)
 ;;=4^530.81
 ;;^UTILITY(U,$J,358.3,496,1,5,0)
 ;;=5^GERD
 ;;^UTILITY(U,$J,358.3,496,2)
 ;;=^295749
 ;;^UTILITY(U,$J,358.3,497,0)
 ;;=535.50^^7^35^53
 ;;^UTILITY(U,$J,358.3,497,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,497,1,4,0)
 ;;=4^535.50
 ;;^UTILITY(U,$J,358.3,497,1,5,0)
 ;;=5^Gastritis
 ;;^UTILITY(U,$J,358.3,497,2)
 ;;=^270181
 ;;^UTILITY(U,$J,358.3,498,0)
 ;;=682.9^^7^35^26
 ;;^UTILITY(U,$J,358.3,498,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,498,1,4,0)
 ;;=4^682.9
 ;;^UTILITY(U,$J,358.3,498,1,5,0)
 ;;=5^Cellulitis
 ;;^UTILITY(U,$J,358.3,498,2)
 ;;=^21160
 ;;^UTILITY(U,$J,358.3,499,0)
 ;;=691.8^^7^35^34
 ;;^UTILITY(U,$J,358.3,499,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,499,1,4,0)
 ;;=4^691.8
 ;;^UTILITY(U,$J,358.3,499,1,5,0)
 ;;=5^Dermatitis,Atopic
 ;;^UTILITY(U,$J,358.3,499,2)
 ;;=^87342
 ;;^UTILITY(U,$J,358.3,500,0)
 ;;=692.9^^7^35^35
 ;;^UTILITY(U,$J,358.3,500,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,500,1,4,0)
 ;;=4^692.9
 ;;^UTILITY(U,$J,358.3,500,1,5,0)
 ;;=5^Dermatitis,Contact
 ;;^UTILITY(U,$J,358.3,500,2)
 ;;=^27800
 ;;^UTILITY(U,$J,358.3,501,0)
 ;;=692.9^^7^35^42
 ;;^UTILITY(U,$J,358.3,501,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,501,1,4,0)
 ;;=4^692.9
 ;;^UTILITY(U,$J,358.3,501,1,5,0)
 ;;=5^Eczema
 ;;^UTILITY(U,$J,358.3,501,2)
 ;;=^27800
 ;;^UTILITY(U,$J,358.3,502,0)
 ;;=698.9^^7^35^67
 ;;^UTILITY(U,$J,358.3,502,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,502,1,4,0)
 ;;=4^698.9
 ;;^UTILITY(U,$J,358.3,502,1,5,0)
 ;;=5^Pruritis
 ;;^UTILITY(U,$J,358.3,502,2)
 ;;=^123977
 ;;^UTILITY(U,$J,358.3,503,0)
 ;;=705.81^^7^35^43
 ;;^UTILITY(U,$J,358.3,503,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,503,1,4,0)
 ;;=4^705.81
 ;;^UTILITY(U,$J,358.3,503,1,5,0)
 ;;=5^Eczema,Dyshydrotic
 ;;^UTILITY(U,$J,358.3,503,2)
 ;;=^96545
 ;;^UTILITY(U,$J,358.3,504,0)
 ;;=706.1^^7^35^2
 ;;^UTILITY(U,$J,358.3,504,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,504,1,4,0)
 ;;=4^706.1
 ;;^UTILITY(U,$J,358.3,504,1,5,0)
 ;;=5^Acne
 ;;^UTILITY(U,$J,358.3,504,2)
 ;;=^87239
 ;;^UTILITY(U,$J,358.3,505,0)
 ;;=708.3^^7^35^36
 ;;^UTILITY(U,$J,358.3,505,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,505,1,4,0)
 ;;=4^708.3
 ;;^UTILITY(U,$J,358.3,505,1,5,0)
 ;;=5^Dermographism
 ;;^UTILITY(U,$J,358.3,505,2)
 ;;=^265291
 ;;^UTILITY(U,$J,358.3,506,0)
 ;;=708.5^^7^35^28
 ;;^UTILITY(U,$J,358.3,506,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,506,1,4,0)
 ;;=4^708.5
 ;;^UTILITY(U,$J,358.3,506,1,5,0)
 ;;=5^Cholinergic Urticaria
 ;;^UTILITY(U,$J,358.3,506,2)
 ;;=^265460
 ;;^UTILITY(U,$J,358.3,507,0)
 ;;=708.9^^7^35^77
 ;;^UTILITY(U,$J,358.3,507,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,507,1,4,0)
 ;;=4^708.9
 ;;^UTILITY(U,$J,358.3,507,1,5,0)
 ;;=5^Urticaria
 ;;^UTILITY(U,$J,358.3,507,2)
 ;;=^124650
 ;;^UTILITY(U,$J,358.3,508,0)
 ;;=780.61^^7^35^48
 ;;^UTILITY(U,$J,358.3,508,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,508,1,4,0)
 ;;=4^780.61
 ;;^UTILITY(U,$J,358.3,508,1,5,0)
 ;;=5^Fever in Oth Diseases
 ;;^UTILITY(U,$J,358.3,508,2)
 ;;=^336667
 ;;^UTILITY(U,$J,358.3,509,0)
 ;;=780.62^^7^35^49
 ;;^UTILITY(U,$J,358.3,509,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,509,1,4,0)
 ;;=4^780.62
 ;;^UTILITY(U,$J,358.3,509,1,5,0)
 ;;=5^Fever,Postprocedural
 ;;^UTILITY(U,$J,358.3,509,2)
 ;;=^336668
 ;;^UTILITY(U,$J,358.3,510,0)
 ;;=780.63^^7^35^50
 ;;^UTILITY(U,$J,358.3,510,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,510,1,4,0)
 ;;=4^780.63
 ;;^UTILITY(U,$J,358.3,510,1,5,0)
 ;;=5^Fever,Postvaccination
 ;;^UTILITY(U,$J,358.3,510,2)
 ;;=^336669
 ;;^UTILITY(U,$J,358.3,511,0)
 ;;=780.64^^7^35^27
 ;;^UTILITY(U,$J,358.3,511,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,511,1,4,0)
 ;;=4^780.64
 ;;^UTILITY(U,$J,358.3,511,1,5,0)
 ;;=5^Chills w/o Fever
 ;;^UTILITY(U,$J,358.3,511,2)
 ;;=^336670
 ;;^UTILITY(U,$J,358.3,512,0)
 ;;=780.65^^7^35^63
 ;;^UTILITY(U,$J,358.3,512,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,512,1,4,0)
 ;;=4^780.65
 ;;^UTILITY(U,$J,358.3,512,1,5,0)
 ;;=5^Hypothrm w/o Low Env Temp
 ;;^UTILITY(U,$J,358.3,512,2)
 ;;=^336671
 ;;^UTILITY(U,$J,358.3,513,0)
 ;;=789.00^^7^35^1
 ;;^UTILITY(U,$J,358.3,513,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,513,1,4,0)
 ;;=4^789.00
 ;;^UTILITY(U,$J,358.3,513,1,5,0)
 ;;=5^Abdominal Pain
 ;;^UTILITY(U,$J,358.3,513,2)
 ;;=^303317
 ;;^UTILITY(U,$J,358.3,514,0)
 ;;=995.0^^7^35^4
 ;;^UTILITY(U,$J,358.3,514,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,514,1,4,0)
 ;;=4^995.0
 ;;^UTILITY(U,$J,358.3,514,1,5,0)
 ;;=5^Anaphylactic Reaction
 ;;^UTILITY(U,$J,358.3,514,2)
 ;;=^340653
 ;;^UTILITY(U,$J,358.3,515,0)
 ;;=995.1^^7^35^7
 ;;^UTILITY(U,$J,358.3,515,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,515,1,4,0)
 ;;=4^995.1
 ;;^UTILITY(U,$J,358.3,515,1,5,0)
 ;;=5^Angioedema
 ;;^UTILITY(U,$J,358.3,515,2)
 ;;=^7527
 ;;^UTILITY(U,$J,358.3,516,0)
 ;;=995.1^^7^35^8
 ;;^UTILITY(U,$J,358.3,516,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,516,1,4,0)
 ;;=4^995.1
 ;;^UTILITY(U,$J,358.3,516,1,5,0)
 ;;=5^Angioneurotic Edema
 ;;^UTILITY(U,$J,358.3,516,2)
 ;;=^7527
 ;;^UTILITY(U,$J,358.3,517,0)
 ;;=995.20^^7^35^39
 ;;^UTILITY(U,$J,358.3,517,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,517,1,4,0)
 ;;=4^995.20
 ;;^UTILITY(U,$J,358.3,517,1,5,0)
 ;;=5^Drug Reaction
 ;;^UTILITY(U,$J,358.3,517,2)
 ;;=^334208
 ;;^UTILITY(U,$J,358.3,518,0)
 ;;=995.27^^7^35^38
 ;;^UTILITY(U,$J,358.3,518,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,518,1,4,0)
 ;;=4^995.27
 ;;^UTILITY(U,$J,358.3,518,1,5,0)
 ;;=5^Drug Allergy NEC
 ;;^UTILITY(U,$J,358.3,518,2)
 ;;=^334180
 ;;^UTILITY(U,$J,358.3,519,0)
 ;;=995.60^^7^35^51
 ;;^UTILITY(U,$J,358.3,519,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,519,1,4,0)
 ;;=4^995.60
 ;;^UTILITY(U,$J,358.3,519,1,5,0)
 ;;=5^Food Reaction
 ;;^UTILITY(U,$J,358.3,519,2)
 ;;=^340654
 ;;^UTILITY(U,$J,358.3,520,0)
 ;;=V04.81^^7^35^78
 ;;^UTILITY(U,$J,358.3,520,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,520,1,4,0)
 ;;=4^V04.81
 ;;^UTILITY(U,$J,358.3,520,1,5,0)
 ;;=5^Vaccin for Influenza
 ;;^UTILITY(U,$J,358.3,520,2)
 ;;=^329964
