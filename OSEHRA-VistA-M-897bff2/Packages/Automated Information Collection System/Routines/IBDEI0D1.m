IBDEI0D1 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,17361,1,5,0)
 ;;=5^Lyme Disease
 ;;^UTILITY(U,$J,358.3,17361,2)
 ;;=^72315
 ;;^UTILITY(U,$J,358.3,17362,0)
 ;;=785.6^^125^1076^42
 ;;^UTILITY(U,$J,358.3,17362,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17362,1,4,0)
 ;;=4^785.6
 ;;^UTILITY(U,$J,358.3,17362,1,5,0)
 ;;=5^Lymphadenopathy
 ;;^UTILITY(U,$J,358.3,17362,2)
 ;;=^72368
 ;;^UTILITY(U,$J,358.3,17363,0)
 ;;=031.0^^125^1076^43
 ;;^UTILITY(U,$J,358.3,17363,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17363,1,4,0)
 ;;=4^031.0
 ;;^UTILITY(U,$J,358.3,17363,1,5,0)
 ;;=5^Mycobacteria,Atyp,Pulmonary
 ;;^UTILITY(U,$J,358.3,17363,2)
 ;;=^101018
 ;;^UTILITY(U,$J,358.3,17364,0)
 ;;=730.00^^125^1076^45
 ;;^UTILITY(U,$J,358.3,17364,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17364,1,4,0)
 ;;=4^730.00
 ;;^UTILITY(U,$J,358.3,17364,1,5,0)
 ;;=5^Osteomyelitis,Acute Unsp
 ;;^UTILITY(U,$J,358.3,17364,2)
 ;;=^272612
 ;;^UTILITY(U,$J,358.3,17365,0)
 ;;=730.10^^125^1076^46
 ;;^UTILITY(U,$J,358.3,17365,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17365,1,4,0)
 ;;=4^730.10
 ;;^UTILITY(U,$J,358.3,17365,1,5,0)
 ;;=5^Osteomyelitis,Chron Unsp
 ;;^UTILITY(U,$J,358.3,17365,2)
 ;;=^24427
 ;;^UTILITY(U,$J,358.3,17366,0)
 ;;=380.10^^125^1076^47
 ;;^UTILITY(U,$J,358.3,17366,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17366,1,4,0)
 ;;=4^380.10
 ;;^UTILITY(U,$J,358.3,17366,1,5,0)
 ;;=5^Otitis Externa 
 ;;^UTILITY(U,$J,358.3,17366,2)
 ;;=380.10^62807
 ;;^UTILITY(U,$J,358.3,17367,0)
 ;;=382.9^^125^1076^48
 ;;^UTILITY(U,$J,358.3,17367,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17367,1,4,0)
 ;;=4^382.9
 ;;^UTILITY(U,$J,358.3,17367,1,5,0)
 ;;=5^Otitis Media
 ;;^UTILITY(U,$J,358.3,17367,2)
 ;;=^123967
 ;;^UTILITY(U,$J,358.3,17368,0)
 ;;=614.9^^125^1076^49
 ;;^UTILITY(U,$J,358.3,17368,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17368,1,4,0)
 ;;=4^614.9
 ;;^UTILITY(U,$J,358.3,17368,1,5,0)
 ;;=5^Pelvic Inflammatory Disease
 ;;^UTILITY(U,$J,358.3,17368,2)
 ;;=^3537
 ;;^UTILITY(U,$J,358.3,17369,0)
 ;;=486.^^125^1076^51
 ;;^UTILITY(U,$J,358.3,17369,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17369,1,4,0)
 ;;=4^486.
 ;;^UTILITY(U,$J,358.3,17369,1,5,0)
 ;;=5^Pneumonia
 ;;^UTILITY(U,$J,358.3,17369,2)
 ;;=^95632
 ;;^UTILITY(U,$J,358.3,17370,0)
 ;;=136.3^^125^1076^50
 ;;^UTILITY(U,$J,358.3,17370,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17370,1,4,0)
 ;;=4^136.3
 ;;^UTILITY(U,$J,358.3,17370,1,5,0)
 ;;=5^Pneumocystosis
 ;;^UTILITY(U,$J,358.3,17370,2)
 ;;=^95635
 ;;^UTILITY(U,$J,358.3,17371,0)
 ;;=601.9^^125^1076^53
 ;;^UTILITY(U,$J,358.3,17371,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17371,1,4,0)
 ;;=4^601.9
 ;;^UTILITY(U,$J,358.3,17371,1,5,0)
 ;;=5^Prostatitis Nos
 ;;^UTILITY(U,$J,358.3,17371,2)
 ;;=^99489
 ;;^UTILITY(U,$J,358.3,17372,0)
 ;;=590.80^^125^1076^54
 ;;^UTILITY(U,$J,358.3,17372,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17372,1,4,0)
 ;;=4^590.80
 ;;^UTILITY(U,$J,358.3,17372,1,5,0)
 ;;=5^Pyelonephritis Nos
 ;;^UTILITY(U,$J,358.3,17372,2)
 ;;=^101463
 ;;^UTILITY(U,$J,358.3,17373,0)
 ;;=711.00^^125^1076^55
 ;;^UTILITY(U,$J,358.3,17373,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17373,1,4,0)
 ;;=4^711.00
 ;;^UTILITY(U,$J,358.3,17373,1,5,0)
 ;;=5^Septic Joint
 ;;^UTILITY(U,$J,358.3,17373,2)
 ;;=^263780
 ;;^UTILITY(U,$J,358.3,17374,0)
 ;;=461.9^^125^1076^57
 ;;^UTILITY(U,$J,358.3,17374,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17374,1,4,0)
 ;;=4^461.9
 ;;^UTILITY(U,$J,358.3,17374,1,5,0)
 ;;=5^Sinusitis, Acute
 ;;^UTILITY(U,$J,358.3,17374,2)
 ;;=^259080
 ;;^UTILITY(U,$J,358.3,17375,0)
 ;;=473.9^^125^1076^56
 ;;^UTILITY(U,$J,358.3,17375,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17375,1,4,0)
 ;;=4^473.9
 ;;^UTILITY(U,$J,358.3,17375,1,5,0)
 ;;=5^Sinusitis Chronic
 ;;^UTILITY(U,$J,358.3,17375,2)
 ;;=^123985
 ;;^UTILITY(U,$J,358.3,17376,0)
 ;;=091.0^^125^1076^62
 ;;^UTILITY(U,$J,358.3,17376,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17376,1,4,0)
 ;;=4^091.0
 ;;^UTILITY(U,$J,358.3,17376,1,5,0)
 ;;=5^Syphilis,Primary Genital
 ;;^UTILITY(U,$J,358.3,17376,2)
 ;;=^50581
 ;;^UTILITY(U,$J,358.3,17377,0)
 ;;=097.0^^125^1076^60
 ;;^UTILITY(U,$J,358.3,17377,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17377,1,4,0)
 ;;=4^097.0
 ;;^UTILITY(U,$J,358.3,17377,1,5,0)
 ;;=5^Syphilis,Latent
 ;;^UTILITY(U,$J,358.3,17377,2)
 ;;=^266781
 ;;^UTILITY(U,$J,358.3,17378,0)
 ;;=094.9^^125^1076^61
 ;;^UTILITY(U,$J,358.3,17378,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17378,1,4,0)
 ;;=4^094.9
 ;;^UTILITY(U,$J,358.3,17378,1,5,0)
 ;;=5^Syphilis,Neurosyphilis
 ;;^UTILITY(U,$J,358.3,17378,2)
 ;;=^83026
 ;;^UTILITY(U,$J,358.3,17379,0)
 ;;=097.9^^125^1076^59
 ;;^UTILITY(U,$J,358.3,17379,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17379,1,4,0)
 ;;=4^097.9
 ;;^UTILITY(U,$J,358.3,17379,1,5,0)
 ;;=5^Syphilis Nos
 ;;^UTILITY(U,$J,358.3,17379,2)
 ;;=^116808
 ;;^UTILITY(U,$J,358.3,17380,0)
 ;;=463.^^125^1076^63
 ;;^UTILITY(U,$J,358.3,17380,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17380,1,4,0)
 ;;=4^463.
 ;;^UTILITY(U,$J,358.3,17380,1,5,0)
 ;;=5^Tonsillitis
 ;;^UTILITY(U,$J,358.3,17380,2)
 ;;=^2695
 ;;^UTILITY(U,$J,358.3,17381,0)
 ;;=130.9^^125^1076^64
 ;;^UTILITY(U,$J,358.3,17381,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17381,1,4,0)
 ;;=4^130.9
 ;;^UTILITY(U,$J,358.3,17381,1,5,0)
 ;;=5^Toxoplasmosis Nos
 ;;^UTILITY(U,$J,358.3,17381,2)
 ;;=^120695
 ;;^UTILITY(U,$J,358.3,17382,0)
 ;;=011.90^^125^1076^65
 ;;^UTILITY(U,$J,358.3,17382,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17382,1,4,0)
 ;;=4^011.90
 ;;^UTILITY(U,$J,358.3,17382,1,5,0)
 ;;=5^Tuberculosis
 ;;^UTILITY(U,$J,358.3,17382,2)
 ;;=^122756
 ;;^UTILITY(U,$J,358.3,17383,0)
 ;;=614.2^^125^1076^66
 ;;^UTILITY(U,$J,358.3,17383,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17383,1,4,0)
 ;;=4^614.2
 ;;^UTILITY(U,$J,358.3,17383,1,5,0)
 ;;=5^Tubo-Ovarian Abcess
 ;;^UTILITY(U,$J,358.3,17383,2)
 ;;=^107806
 ;;^UTILITY(U,$J,358.3,17384,0)
 ;;=788.7^^125^1076^69
 ;;^UTILITY(U,$J,358.3,17384,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17384,1,4,0)
 ;;=4^788.7
 ;;^UTILITY(U,$J,358.3,17384,1,5,0)
 ;;=5^Urethral Discharge
 ;;^UTILITY(U,$J,358.3,17384,2)
 ;;=^265872
 ;;^UTILITY(U,$J,358.3,17385,0)
 ;;=131.02^^125^1076^70
 ;;^UTILITY(U,$J,358.3,17385,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17385,1,4,0)
 ;;=4^131.02
 ;;^UTILITY(U,$J,358.3,17385,1,5,0)
 ;;=5^Urethritis, Trichomonal
 ;;^UTILITY(U,$J,358.3,17385,2)
 ;;=^266955
 ;;^UTILITY(U,$J,358.3,17386,0)
 ;;=597.80^^125^1076^71
 ;;^UTILITY(U,$J,358.3,17386,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17386,1,4,0)
 ;;=4^597.80
 ;;^UTILITY(U,$J,358.3,17386,1,5,0)
 ;;=5^Urethritis, Unsp
 ;;^UTILITY(U,$J,358.3,17386,2)
 ;;=^124214
 ;;^UTILITY(U,$J,358.3,17387,0)
 ;;=079.99^^125^1076^75
 ;;^UTILITY(U,$J,358.3,17387,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17387,1,4,0)
 ;;=4^079.99
 ;;^UTILITY(U,$J,358.3,17387,1,5,0)
 ;;=5^Viral Syndrome
 ;;^UTILITY(U,$J,358.3,17387,2)
 ;;=^295798
 ;;^UTILITY(U,$J,358.3,17388,0)
 ;;=616.10^^125^1076^74
 ;;^UTILITY(U,$J,358.3,17388,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17388,1,4,0)
 ;;=4^616.10
 ;;^UTILITY(U,$J,358.3,17388,1,5,0)
 ;;=5^Vaginitis, Unsp Cause
 ;;^UTILITY(U,$J,358.3,17388,2)
 ;;=^125233
 ;;^UTILITY(U,$J,358.3,17389,0)
 ;;=131.01^^125^1076^73
 ;;^UTILITY(U,$J,358.3,17389,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17389,1,4,0)
 ;;=4^131.01
 ;;^UTILITY(U,$J,358.3,17389,1,5,0)
 ;;=5^Vaginitis, Trichomonas
 ;;^UTILITY(U,$J,358.3,17389,2)
 ;;=^121763
 ;;^UTILITY(U,$J,358.3,17390,0)
 ;;=682.0^^125^1076^4
 ;;^UTILITY(U,$J,358.3,17390,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17390,1,4,0)
 ;;=4^682.0
 ;;^UTILITY(U,$J,358.3,17390,1,5,0)
 ;;=5^Cellulitis Of Face
 ;;^UTILITY(U,$J,358.3,17390,2)
 ;;=^271888
 ;;^UTILITY(U,$J,358.3,17391,0)
 ;;=681.00^^125^1076^11
 ;;^UTILITY(U,$J,358.3,17391,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17391,1,4,0)
 ;;=4^681.00
 ;;^UTILITY(U,$J,358.3,17391,1,5,0)
 ;;=5^Cellulitis, Finger 
 ;;^UTILITY(U,$J,358.3,17391,2)
 ;;=^271883
 ;;^UTILITY(U,$J,358.3,17392,0)
 ;;=682.7^^125^1076^5
 ;;^UTILITY(U,$J,358.3,17392,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17392,1,4,0)
 ;;=4^682.7
 ;;^UTILITY(U,$J,358.3,17392,1,5,0)
 ;;=5^Cellulitis Of Foot
 ;;^UTILITY(U,$J,358.3,17392,2)
 ;;=^271895
 ;;^UTILITY(U,$J,358.3,17393,0)
 ;;=682.4^^125^1076^6
 ;;^UTILITY(U,$J,358.3,17393,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17393,1,4,0)
 ;;=4^682.4
 ;;^UTILITY(U,$J,358.3,17393,1,5,0)
 ;;=5^Cellulitis Of Hand
 ;;^UTILITY(U,$J,358.3,17393,2)
 ;;=^271892
 ;;^UTILITY(U,$J,358.3,17394,0)
 ;;=682.6^^125^1076^7
 ;;^UTILITY(U,$J,358.3,17394,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17394,1,4,0)
 ;;=4^682.6
 ;;^UTILITY(U,$J,358.3,17394,1,5,0)
 ;;=5^Cellulitis Of Leg
 ;;^UTILITY(U,$J,358.3,17394,2)
 ;;=^271894
 ;;^UTILITY(U,$J,358.3,17395,0)
 ;;=682.1^^125^1076^8
 ;;^UTILITY(U,$J,358.3,17395,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17395,1,4,0)
 ;;=4^682.1
 ;;^UTILITY(U,$J,358.3,17395,1,5,0)
 ;;=5^Cellulitis Of Neck
 ;;^UTILITY(U,$J,358.3,17395,2)
 ;;=^271889
 ;;^UTILITY(U,$J,358.3,17396,0)
 ;;=376.01^^125^1076^9
 ;;^UTILITY(U,$J,358.3,17396,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17396,1,4,0)
 ;;=4^376.01
 ;;^UTILITY(U,$J,358.3,17396,1,5,0)
 ;;=5^Cellulitis Of Orbit
 ;;^UTILITY(U,$J,358.3,17396,2)
 ;;=^259068
 ;;^UTILITY(U,$J,358.3,17397,0)
 ;;=681.10^^125^1076^12
 ;;^UTILITY(U,$J,358.3,17397,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17397,1,4,0)
 ;;=4^681.10
 ;;^UTILITY(U,$J,358.3,17397,1,5,0)
 ;;=5^Cellulitis, Toe Nos
 ;;^UTILITY(U,$J,358.3,17397,2)
 ;;=^271885
 ;;^UTILITY(U,$J,358.3,17398,0)
 ;;=682.2^^125^1076^10
 ;;^UTILITY(U,$J,358.3,17398,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,17398,1,4,0)
 ;;=4^682.2
 ;;^UTILITY(U,$J,358.3,17398,1,5,0)
 ;;=5^Cellulitis Of Trunk
 ;;^UTILITY(U,$J,358.3,17398,2)
 ;;=^271890
 ;;^UTILITY(U,$J,358.3,17399,0)
 ;;=070.1^^125^1076^26
 ;;^UTILITY(U,$J,358.3,17399,1,0)
 ;;=^358.31IA^5^2
