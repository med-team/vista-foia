IBDEI0FI ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,20827,1,2,0)
 ;;=2^EMG Thor Paraspinal
 ;;^UTILITY(U,$J,358.3,20827,1,3,0)
 ;;=3^95869
 ;;^UTILITY(U,$J,358.3,20828,0)
 ;;=95905^^158^1364^12^^^^1
 ;;^UTILITY(U,$J,358.3,20828,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20828,1,2,0)
 ;;=2^Motor/Sens Nerv Conduct-ea limb w/F-wv
 ;;^UTILITY(U,$J,358.3,20828,1,3,0)
 ;;=3^95905
 ;;^UTILITY(U,$J,358.3,20829,0)
 ;;=95937^^158^1364^27^^^^1
 ;;^UTILITY(U,$J,358.3,20829,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20829,1,2,0)
 ;;=2^Neuromuscular Junction Test
 ;;^UTILITY(U,$J,358.3,20829,1,3,0)
 ;;=3^95937
 ;;^UTILITY(U,$J,358.3,20830,0)
 ;;=95907^^158^1364^20^^^^1
 ;;^UTILITY(U,$J,358.3,20830,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20830,1,2,0)
 ;;=2^Nerve conduction studies; 1-2 studies
 ;;^UTILITY(U,$J,358.3,20830,1,3,0)
 ;;=3^95907
 ;;^UTILITY(U,$J,358.3,20831,0)
 ;;=95908^^158^1364^21^^^^1
 ;;^UTILITY(U,$J,358.3,20831,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20831,1,2,0)
 ;;=2^Nerve conduction studies; 3-4 studies
 ;;^UTILITY(U,$J,358.3,20831,1,3,0)
 ;;=3^95908
 ;;^UTILITY(U,$J,358.3,20832,0)
 ;;=95909^^158^1364^22^^^^1
 ;;^UTILITY(U,$J,358.3,20832,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20832,1,2,0)
 ;;=2^Nerve conduction studies; 5-6 studies
 ;;^UTILITY(U,$J,358.3,20832,1,3,0)
 ;;=3^95909
 ;;^UTILITY(U,$J,358.3,20833,0)
 ;;=95910^^158^1364^23^^^^1
 ;;^UTILITY(U,$J,358.3,20833,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20833,1,2,0)
 ;;=2^Nerve conduction studies; 7-8 studies
 ;;^UTILITY(U,$J,358.3,20833,1,3,0)
 ;;=3^95910
 ;;^UTILITY(U,$J,358.3,20834,0)
 ;;=95911^^158^1364^24^^^^1
 ;;^UTILITY(U,$J,358.3,20834,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20834,1,2,0)
 ;;=2^Nerve conduction studies; 9-10 studies
 ;;^UTILITY(U,$J,358.3,20834,1,3,0)
 ;;=3^95911
 ;;^UTILITY(U,$J,358.3,20835,0)
 ;;=95912^^158^1364^25^^^^1
 ;;^UTILITY(U,$J,358.3,20835,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20835,1,2,0)
 ;;=2^Nerve conduction studies; 11-12 studies
 ;;^UTILITY(U,$J,358.3,20835,1,3,0)
 ;;=3^95912
 ;;^UTILITY(U,$J,358.3,20836,0)
 ;;=95913^^158^1364^26^^^^1
 ;;^UTILITY(U,$J,358.3,20836,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20836,1,2,0)
 ;;=2^Nerve conduction studies;13 or > studies
 ;;^UTILITY(U,$J,358.3,20836,1,3,0)
 ;;=3^95913
 ;;^UTILITY(U,$J,358.3,20837,0)
 ;;=99366^^158^1365^1^^^^1
 ;;^UTILITY(U,$J,358.3,20837,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20837,1,2,0)
 ;;=2^Interdisc Team Conf HCP w/pt-fam >30min
 ;;^UTILITY(U,$J,358.3,20837,1,3,0)
 ;;=3^99366
 ;;^UTILITY(U,$J,358.3,20838,0)
 ;;=99368^^158^1365^2^^^^1
 ;;^UTILITY(U,$J,358.3,20838,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20838,1,2,0)
 ;;=2^Interdis Team Conf HCP w/o pt/fam >30min
 ;;^UTILITY(U,$J,358.3,20838,1,3,0)
 ;;=3^99368
 ;;^UTILITY(U,$J,358.3,20839,0)
 ;;=99234^^158^1366^1^^^^1
 ;;^UTILITY(U,$J,358.3,20839,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20839,1,2,0)
 ;;=2^Observ-Detail,Low Complexity
 ;;^UTILITY(U,$J,358.3,20839,1,3,0)
 ;;=3^99234
 ;;^UTILITY(U,$J,358.3,20840,0)
 ;;=99235^^158^1366^2^^^^1
 ;;^UTILITY(U,$J,358.3,20840,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20840,1,2,0)
 ;;=2^Observ-Comp,High Complex
 ;;^UTILITY(U,$J,358.3,20840,1,3,0)
 ;;=3^99235
 ;;^UTILITY(U,$J,358.3,20841,0)
 ;;=99236^^158^1366^3^^^^1
 ;;^UTILITY(U,$J,358.3,20841,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,20841,1,2,0)
 ;;=2^Observ-Comp,Mod Complex
 ;;^UTILITY(U,$J,358.3,20841,1,3,0)
 ;;=3^99236
 ;;^UTILITY(U,$J,358.3,20842,0)
 ;;=V57.0^^159^1367^2
 ;;^UTILITY(U,$J,358.3,20842,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20842,1,3,0)
 ;;=3^Breathing Exercises
 ;;^UTILITY(U,$J,358.3,20842,1,4,0)
 ;;=4^V57.0
 ;;^UTILITY(U,$J,358.3,20842,2)
 ;;=^19759
 ;;^UTILITY(U,$J,358.3,20843,0)
 ;;=V52.0^^159^1367^4
 ;;^UTILITY(U,$J,358.3,20843,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20843,1,3,0)
 ;;=3^Fitting Artificial Arm
 ;;^UTILITY(U,$J,358.3,20843,1,4,0)
 ;;=4^V52.0
 ;;^UTILITY(U,$J,358.3,20843,2)
 ;;=^295496
 ;;^UTILITY(U,$J,358.3,20844,0)
 ;;=V52.1^^159^1367^5
 ;;^UTILITY(U,$J,358.3,20844,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20844,1,3,0)
 ;;=3^Fitting Artificial Leg
 ;;^UTILITY(U,$J,358.3,20844,1,4,0)
 ;;=4^V52.1
 ;;^UTILITY(U,$J,358.3,20844,2)
 ;;=^295497
 ;;^UTILITY(U,$J,358.3,20845,0)
 ;;=V53.7^^159^1367^3
 ;;^UTILITY(U,$J,358.3,20845,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20845,1,3,0)
 ;;=3^Fit Orthopedic Devices
 ;;^UTILITY(U,$J,358.3,20845,1,4,0)
 ;;=4^V53.7
 ;;^UTILITY(U,$J,358.3,20845,2)
 ;;=^295510
 ;;^UTILITY(U,$J,358.3,20846,0)
 ;;=V57.81^^159^1367^8
 ;;^UTILITY(U,$J,358.3,20846,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20846,1,3,0)
 ;;=3^Orthotic Training
 ;;^UTILITY(U,$J,358.3,20846,1,4,0)
 ;;=4^V57.81
 ;;^UTILITY(U,$J,358.3,20846,2)
 ;;=^295527
 ;;^UTILITY(U,$J,358.3,20847,0)
 ;;=V52.9^^159^1367^6
 ;;^UTILITY(U,$J,358.3,20847,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20847,1,3,0)
 ;;=3^Fitting Prosthesis Nos
 ;;^UTILITY(U,$J,358.3,20847,1,4,0)
 ;;=4^V52.9
 ;;^UTILITY(U,$J,358.3,20847,2)
 ;;=^295502
 ;;^UTILITY(U,$J,358.3,20848,0)
 ;;=V57.9^^159^1367^10
 ;;^UTILITY(U,$J,358.3,20848,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20848,1,3,0)
 ;;=3^Rehabilitation Proc Nos
 ;;^UTILITY(U,$J,358.3,20848,1,4,0)
 ;;=4^V57.9
 ;;^UTILITY(U,$J,358.3,20848,2)
 ;;=^19769
 ;;^UTILITY(U,$J,358.3,20849,0)
 ;;=V53.8^^159^1367^1
 ;;^UTILITY(U,$J,358.3,20849,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20849,1,3,0)
 ;;=3^Adjustment Of Wheelchair
 ;;^UTILITY(U,$J,358.3,20849,1,4,0)
 ;;=4^V53.8
 ;;^UTILITY(U,$J,358.3,20849,2)
 ;;=^295511
 ;;^UTILITY(U,$J,358.3,20850,0)
 ;;=V54.89^^159^1367^9
 ;;^UTILITY(U,$J,358.3,20850,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20850,1,3,0)
 ;;=3^Other Ortho Aftercare
 ;;^UTILITY(U,$J,358.3,20850,1,4,0)
 ;;=4^V54.89
 ;;^UTILITY(U,$J,358.3,20850,2)
 ;;=Other Ortho Aftercare^1
 ;;^UTILITY(U,$J,358.3,20851,0)
 ;;=V52.8^^159^1367^7
 ;;^UTILITY(U,$J,358.3,20851,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20851,1,3,0)
 ;;=3^Fitting/Adj Oth Spec Prosth Device
 ;;^UTILITY(U,$J,358.3,20851,1,4,0)
 ;;=4^V52.8
 ;;^UTILITY(U,$J,358.3,20851,2)
 ;;=^295501
 ;;^UTILITY(U,$J,358.3,20852,0)
 ;;=V49.76^^159^1368^2
 ;;^UTILITY(U,$J,358.3,20852,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20852,1,3,0)
 ;;=3^Above Knee Amputat Stat
 ;;^UTILITY(U,$J,358.3,20852,1,4,0)
 ;;=4^V49.76
 ;;^UTILITY(U,$J,358.3,20852,2)
 ;;=^303444
 ;;^UTILITY(U,$J,358.3,20853,0)
 ;;=V49.74^^159^1368^3
 ;;^UTILITY(U,$J,358.3,20853,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20853,1,3,0)
 ;;=3^Ankle Amputat Stat
 ;;^UTILITY(U,$J,358.3,20853,1,4,0)
 ;;=4^V49.74
 ;;^UTILITY(U,$J,358.3,20853,2)
 ;;=^303442
 ;;^UTILITY(U,$J,358.3,20854,0)
 ;;=V49.75^^159^1368^5
 ;;^UTILITY(U,$J,358.3,20854,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20854,1,3,0)
 ;;=3^Below Knee Amputat Stat
 ;;^UTILITY(U,$J,358.3,20854,1,4,0)
 ;;=4^V49.75
 ;;^UTILITY(U,$J,358.3,20854,2)
 ;;=^303443
 ;;^UTILITY(U,$J,358.3,20855,0)
 ;;=V49.73^^159^1368^6
 ;;^UTILITY(U,$J,358.3,20855,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20855,1,3,0)
 ;;=3^Foot Amputat Stat
 ;;^UTILITY(U,$J,358.3,20855,1,4,0)
 ;;=4^V49.73
 ;;^UTILITY(U,$J,358.3,20855,2)
 ;;=^303441
 ;;^UTILITY(U,$J,358.3,20856,0)
 ;;=V49.70^^159^1368^14
 ;;^UTILITY(U,$J,358.3,20856,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20856,1,3,0)
 ;;=3^Unsp L Limb Amput Stat
 ;;^UTILITY(U,$J,358.3,20856,1,4,0)
 ;;=4^V49.70
 ;;^UTILITY(U,$J,358.3,20856,2)
 ;;=^303438
 ;;^UTILITY(U,$J,358.3,20857,0)
 ;;=V49.71^^159^1368^7
 ;;^UTILITY(U,$J,358.3,20857,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20857,1,3,0)
 ;;=3^Great Toe Amputat Stat
 ;;^UTILITY(U,$J,358.3,20857,1,4,0)
 ;;=4^V49.71
 ;;^UTILITY(U,$J,358.3,20857,2)
 ;;=^303439
 ;;^UTILITY(U,$J,358.3,20858,0)
 ;;=V49.72^^159^1368^11
 ;;^UTILITY(U,$J,358.3,20858,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20858,1,3,0)
 ;;=3^Oth Toe(S) Amputat Stat
 ;;^UTILITY(U,$J,358.3,20858,1,4,0)
 ;;=4^V49.72
 ;;^UTILITY(U,$J,358.3,20858,2)
 ;;=^303440
 ;;^UTILITY(U,$J,358.3,20859,0)
 ;;=V49.60^^159^1368^15
 ;;^UTILITY(U,$J,358.3,20859,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20859,1,3,0)
 ;;=3^Unsp Lev U Limb Amput
 ;;^UTILITY(U,$J,358.3,20859,1,4,0)
 ;;=4^V49.60
 ;;^UTILITY(U,$J,358.3,20859,2)
 ;;=^303427
 ;;^UTILITY(U,$J,358.3,20860,0)
 ;;=V49.61^^159^1368^13
 ;;^UTILITY(U,$J,358.3,20860,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20860,1,3,0)
 ;;=3^Thumb Amput Status
 ;;^UTILITY(U,$J,358.3,20860,1,4,0)
 ;;=4^V49.61
 ;;^UTILITY(U,$J,358.3,20860,2)
 ;;=^303428
 ;;^UTILITY(U,$J,358.3,20861,0)
 ;;=V49.62^^159^1368^10
 ;;^UTILITY(U,$J,358.3,20861,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20861,1,3,0)
 ;;=3^Oth Finger(s) Amput Status
 ;;^UTILITY(U,$J,358.3,20861,1,4,0)
 ;;=4^V49.62
 ;;^UTILITY(U,$J,358.3,20861,2)
 ;;=^303429
 ;;^UTILITY(U,$J,358.3,20862,0)
 ;;=V49.63^^159^1368^8
 ;;^UTILITY(U,$J,358.3,20862,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20862,1,3,0)
 ;;=3^Hand Amput Status
 ;;^UTILITY(U,$J,358.3,20862,1,4,0)
 ;;=4^V49.63
 ;;^UTILITY(U,$J,358.3,20862,2)
 ;;=^303430
 ;;^UTILITY(U,$J,358.3,20863,0)
 ;;=V49.64^^159^1368^16
 ;;^UTILITY(U,$J,358.3,20863,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20863,1,3,0)
 ;;=3^Wrist Amput Status
 ;;^UTILITY(U,$J,358.3,20863,1,4,0)
 ;;=4^V49.64
 ;;^UTILITY(U,$J,358.3,20863,2)
 ;;=^303431
 ;;^UTILITY(U,$J,358.3,20864,0)
 ;;=V49.65^^159^1368^4
 ;;^UTILITY(U,$J,358.3,20864,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20864,1,3,0)
 ;;=3^Below Elbow Amput Status
 ;;^UTILITY(U,$J,358.3,20864,1,4,0)
 ;;=4^V49.65
 ;;^UTILITY(U,$J,358.3,20864,2)
 ;;=^303432
 ;;^UTILITY(U,$J,358.3,20865,0)
 ;;=V49.66^^159^1368^1
 ;;^UTILITY(U,$J,358.3,20865,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,20865,1,3,0)
 ;;=3^Above Elbow Amput Status
 ;;^UTILITY(U,$J,358.3,20865,1,4,0)
 ;;=4^V49.66
 ;;^UTILITY(U,$J,358.3,20865,2)
 ;;=^303433
 ;;^UTILITY(U,$J,358.3,20866,0)
 ;;=V49.67^^159^1368^12
