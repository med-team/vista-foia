IBDEI0BU ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,15761,1,2,0)
 ;;=2^Closed TX of fracture, phalanx or phalanges, other than great toe; without manipulation, each
 ;;^UTILITY(U,$J,358.3,15761,1,3,0)
 ;;=3^28510
 ;;^UTILITY(U,$J,358.3,15762,0)
 ;;=28515^^114^980^23^^^^1
 ;;^UTILITY(U,$J,358.3,15762,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15762,1,2,0)
 ;;=2^Closed TX of fracture, phalanx or phalanges, other than great toe; with manipulation, each
 ;;^UTILITY(U,$J,358.3,15762,1,3,0)
 ;;=3^28515
 ;;^UTILITY(U,$J,358.3,15763,0)
 ;;=28525^^114^980^24^^^^1
 ;;^UTILITY(U,$J,358.3,15763,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15763,1,2,0)
 ;;=2^Open TX of fracture, phalanx or phalanges, other than great toe, with or without internal or external fixation, each
 ;;^UTILITY(U,$J,358.3,15763,1,3,0)
 ;;=3^28525
 ;;^UTILITY(U,$J,358.3,15764,0)
 ;;=28530^^114^980^25^^^^1
 ;;^UTILITY(U,$J,358.3,15764,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15764,1,2,0)
 ;;=2^Closed TX of sesamiod fracture 
 ;;^UTILITY(U,$J,358.3,15764,1,3,0)
 ;;=3^28530
 ;;^UTILITY(U,$J,358.3,15765,0)
 ;;=28531^^114^980^26^^^^1
 ;;^UTILITY(U,$J,358.3,15765,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15765,1,2,0)
 ;;=2^Open TX of sesamiod fracture, with or without internal fixation
 ;;^UTILITY(U,$J,358.3,15765,1,3,0)
 ;;=3^28531
 ;;^UTILITY(U,$J,358.3,15766,0)
 ;;=27760^^114^980^27^^^^1
 ;;^UTILITY(U,$J,358.3,15766,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15766,1,2,0)
 ;;=2^Closed TX of medial malleolus fracture; without manipulation 
 ;;^UTILITY(U,$J,358.3,15766,1,3,0)
 ;;=3^27760
 ;;^UTILITY(U,$J,358.3,15767,0)
 ;;=27762^^114^980^28^^^^1
 ;;^UTILITY(U,$J,358.3,15767,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15767,1,2,0)
 ;;=2^Closed TX of medial malleolus fracture; with manipulation, with or without skin or skeletal traction
 ;;^UTILITY(U,$J,358.3,15767,1,3,0)
 ;;=3^27762
 ;;^UTILITY(U,$J,358.3,15768,0)
 ;;=27766^^114^980^29^^^^1
 ;;^UTILITY(U,$J,358.3,15768,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15768,1,2,0)
 ;;=2^Open/Closed TX of medial matteolus FX,w/ internal fixation
 ;;^UTILITY(U,$J,358.3,15768,1,3,0)
 ;;=3^27766
 ;;^UTILITY(U,$J,358.3,15769,0)
 ;;=27786^^114^980^30^^^^1
 ;;^UTILITY(U,$J,358.3,15769,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15769,1,2,0)
 ;;=2^Closed TX of distal fibular fracture (lateral malleolus); without manipulation
 ;;^UTILITY(U,$J,358.3,15769,1,3,0)
 ;;=3^27786
 ;;^UTILITY(U,$J,358.3,15770,0)
 ;;=27788^^114^980^31^^^^1
 ;;^UTILITY(U,$J,358.3,15770,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15770,1,2,0)
 ;;=2^Closed TX of distal fibular (lateral malleolus); with manipulation
 ;;^UTILITY(U,$J,358.3,15770,1,3,0)
 ;;=3^27788
 ;;^UTILITY(U,$J,358.3,15771,0)
 ;;=27792^^114^980^32^^^^1
 ;;^UTILITY(U,$J,358.3,15771,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15771,1,2,0)
 ;;=2^Open TX of distal fibular fracture (lateral malleolus), with or without internal or external fixation
 ;;^UTILITY(U,$J,358.3,15771,1,3,0)
 ;;=3^27792
 ;;^UTILITY(U,$J,358.3,15772,0)
 ;;=27808^^114^980^33^^^^1
 ;;^UTILITY(U,$J,358.3,15772,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15772,1,2,0)
 ;;=2^Closed TX of bimalleolar ankle fracture, (including Potts); without manipulation
 ;;^UTILITY(U,$J,358.3,15772,1,3,0)
 ;;=3^27808
 ;;^UTILITY(U,$J,358.3,15773,0)
 ;;=27810^^114^980^34^^^^1
 ;;^UTILITY(U,$J,358.3,15773,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15773,1,2,0)
 ;;=2^Closed TX of bimalleolar ankle fracture, (including Potts); with manipulation
 ;;^UTILITY(U,$J,358.3,15773,1,3,0)
 ;;=3^27810
 ;;^UTILITY(U,$J,358.3,15774,0)
 ;;=27814^^114^980^35^^^^1
 ;;^UTILITY(U,$J,358.3,15774,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15774,1,2,0)
 ;;=2^Open TX of bimalleolar ankle fracture, with or without internal or external fixation
 ;;^UTILITY(U,$J,358.3,15774,1,3,0)
 ;;=3^27814
 ;;^UTILITY(U,$J,358.3,15775,0)
 ;;=27816^^114^980^36^^^^1
 ;;^UTILITY(U,$J,358.3,15775,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15775,1,2,0)
 ;;=2^Closed TX of trimalleolar ankle fracture; without manipulation
 ;;^UTILITY(U,$J,358.3,15775,1,3,0)
 ;;=3^27816
 ;;^UTILITY(U,$J,358.3,15776,0)
 ;;=27818^^114^980^37^^^^1
 ;;^UTILITY(U,$J,358.3,15776,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15776,1,2,0)
 ;;=2^Closed TX of trimalleolar ankle fracture; with manipulation
 ;;^UTILITY(U,$J,358.3,15776,1,3,0)
 ;;=3^27818
 ;;^UTILITY(U,$J,358.3,15777,0)
 ;;=27822^^114^980^38^^^^1
 ;;^UTILITY(U,$J,358.3,15777,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15777,1,2,0)
 ;;=2^Open Tx of trimalleolar ankle fracture, with or w/o internal or external fixation of, medial and/or lateral malleolus; w/o fixation of posterior lip
 ;;^UTILITY(U,$J,358.3,15777,1,3,0)
 ;;=3^27822
 ;;^UTILITY(U,$J,358.3,15778,0)
 ;;=27823^^114^980^39^^^^1
 ;;^UTILITY(U,$J,358.3,15778,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15778,1,2,0)
 ;;=2^Open Tx of trimalleolar ankle fracture, with or w/o internal or external fixation, medial and/or lateral malleolus; with fixation of posterior lip  
 ;;^UTILITY(U,$J,358.3,15778,1,3,0)
 ;;=3^27823
 ;;^UTILITY(U,$J,358.3,15779,0)
 ;;=27824^^114^980^40^^^^1
 ;;^UTILITY(U,$J,358.3,15779,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15779,1,2,0)
 ;;=2^Closed Tx of fracture of weight bearing articular portion of distal tibia, with or w/o anesthesia; w/o manipulation 
 ;;^UTILITY(U,$J,358.3,15779,1,3,0)
 ;;=3^27824
 ;;^UTILITY(U,$J,358.3,15780,0)
 ;;=27825^^114^980^41^^^^1
 ;;^UTILITY(U,$J,358.3,15780,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15780,1,2,0)
 ;;=2^Closed Tx of fracture of weight bearing articular portion of distal tibia, with or w/o anesthesia; with skeletal traction &/or requiring manipulation
 ;;^UTILITY(U,$J,358.3,15780,1,3,0)
 ;;=3^27825
 ;;^UTILITY(U,$J,358.3,15781,0)
 ;;=27826^^114^980^42^^^^1
 ;;^UTILITY(U,$J,358.3,15781,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15781,1,2,0)
 ;;=2^Open Tx of fracture of weight bearing articular surface/portion of distal tibia, w/ internal or external fixation; of fibula only
 ;;^UTILITY(U,$J,358.3,15781,1,3,0)
 ;;=3^27826
 ;;^UTILITY(U,$J,358.3,15782,0)
 ;;=27827^^114^980^43^^^^1
 ;;^UTILITY(U,$J,358.3,15782,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15782,1,2,0)
 ;;=2^Open Tx of fracture of weight bearing articular surface/portion of distal tibia, w/ internal or external fixation; of tibia only
 ;;^UTILITY(U,$J,358.3,15782,1,3,0)
 ;;=3^27827
 ;;^UTILITY(U,$J,358.3,15783,0)
 ;;=27828^^114^980^44^^^^1
 ;;^UTILITY(U,$J,358.3,15783,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15783,1,2,0)
 ;;=2^Open Tx of fracture of weight bearing articular surface/portionof distal tibia, w/ internal or external fixation; of both tibia & fibula
 ;;^UTILITY(U,$J,358.3,15783,1,3,0)
 ;;=3^27828
 ;;^UTILITY(U,$J,358.3,15784,0)
 ;;=27829^^114^980^45^^^^1
 ;;^UTILITY(U,$J,358.3,15784,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15784,1,2,0)
 ;;=2^Open Tx of distal tibiofibular joint disruption, with or w/o internal or external fixation
 ;;^UTILITY(U,$J,358.3,15784,1,3,0)
 ;;=3^27829
 ;;^UTILITY(U,$J,358.3,15785,0)
 ;;=28540^^114^980^46^^^^1
 ;;^UTILITY(U,$J,358.3,15785,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15785,1,2,0)
 ;;=2^Closed Tx of tarsal bone dislocation, other than talotarsal; w/o anesthesia
 ;;^UTILITY(U,$J,358.3,15785,1,3,0)
 ;;=3^28540
 ;;^UTILITY(U,$J,358.3,15786,0)
 ;;=28545^^114^980^47^^^^1
 ;;^UTILITY(U,$J,358.3,15786,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15786,1,2,0)
 ;;=2^Closed Tx of tarsal bone dislocation, other than talotarsal; requiring anesthesia
 ;;^UTILITY(U,$J,358.3,15786,1,3,0)
 ;;=3^28545
 ;;^UTILITY(U,$J,358.3,15787,0)
 ;;=28546^^114^980^48^^^^1
 ;;^UTILITY(U,$J,358.3,15787,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15787,1,2,0)
 ;;=2^Perc Fixation Tarsal Bone Dislocation
 ;;^UTILITY(U,$J,358.3,15787,1,3,0)
 ;;=3^28546
 ;;^UTILITY(U,$J,358.3,15788,0)
 ;;=28555^^114^980^49^^^^1
 ;;^UTILITY(U,$J,358.3,15788,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15788,1,2,0)
 ;;=2^Open TX of tarsal bone disloc,w/ internal fixation
 ;;^UTILITY(U,$J,358.3,15788,1,3,0)
 ;;=3^28555
 ;;^UTILITY(U,$J,358.3,15789,0)
 ;;=28570^^114^980^50^^^^1
 ;;^UTILITY(U,$J,358.3,15789,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15789,1,2,0)
 ;;=2^Closed Tx of talotarsal joint dislocation; w/o anesthesia
 ;;^UTILITY(U,$J,358.3,15789,1,3,0)
 ;;=3^28570
 ;;^UTILITY(U,$J,358.3,15790,0)
 ;;=28575^^114^980^51^^^^1
 ;;^UTILITY(U,$J,358.3,15790,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15790,1,2,0)
 ;;=2^Closed Tx of talotarsal joint dislocation; requiring anesthesia
 ;;^UTILITY(U,$J,358.3,15790,1,3,0)
 ;;=3^28575
 ;;^UTILITY(U,$J,358.3,15791,0)
 ;;=28576^^114^980^52^^^^1
 ;;^UTILITY(U,$J,358.3,15791,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15791,1,2,0)
 ;;=2^Perc Fixation Talotarsal Joint Dislocation
 ;;^UTILITY(U,$J,358.3,15791,1,3,0)
 ;;=3^28576
 ;;^UTILITY(U,$J,358.3,15792,0)
 ;;=28585^^114^980^53^^^^1
 ;;^UTILITY(U,$J,358.3,15792,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15792,1,2,0)
 ;;=2^Open Tx of talotarsal joint dislocation, with or w/o internal or external fixation
 ;;^UTILITY(U,$J,358.3,15792,1,3,0)
 ;;=3^28585
 ;;^UTILITY(U,$J,358.3,15793,0)
 ;;=28600^^114^980^55^^^^1
 ;;^UTILITY(U,$J,358.3,15793,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15793,1,2,0)
 ;;=2^Closed Tx of tarsometatarsal joint dislocation; requiring anesthesia
 ;;^UTILITY(U,$J,358.3,15793,1,3,0)
 ;;=3^28600
 ;;^UTILITY(U,$J,358.3,15794,0)
 ;;=28606^^114^980^56^^^^1
 ;;^UTILITY(U,$J,358.3,15794,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15794,1,2,0)
 ;;=2^Perc Fixation Tarsometatarsal Joint Dislocation
 ;;^UTILITY(U,$J,358.3,15794,1,3,0)
 ;;=3^28606
 ;;^UTILITY(U,$J,358.3,15795,0)
 ;;=28615^^114^980^57^^^^1
 ;;^UTILITY(U,$J,358.3,15795,1,0)
 ;;=^358.31IA^3^2
