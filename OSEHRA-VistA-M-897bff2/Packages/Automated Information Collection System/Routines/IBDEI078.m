IBDEI078 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,9505,2)
 ;;=Trichiasis^269082
 ;;^UTILITY(U,$J,358.3,9506,0)
 ;;=368.40^^77^656^52
 ;;^UTILITY(U,$J,358.3,9506,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9506,1,3,0)
 ;;=3^Visual Field Defect
 ;;^UTILITY(U,$J,358.3,9506,1,4,0)
 ;;=4^368.40
 ;;^UTILITY(U,$J,358.3,9506,2)
 ;;=Visual Field Defect^126859
 ;;^UTILITY(U,$J,358.3,9507,0)
 ;;=375.21^^77^656^26
 ;;^UTILITY(U,$J,358.3,9507,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9507,1,3,0)
 ;;=3^Epiphora, excess lacrimation
 ;;^UTILITY(U,$J,358.3,9507,1,4,0)
 ;;=4^375.21
 ;;^UTILITY(U,$J,358.3,9507,2)
 ;;=Epiphora, excess lacrimation^269137
 ;;^UTILITY(U,$J,358.3,9508,0)
 ;;=375.22^^77^656^25
 ;;^UTILITY(U,$J,358.3,9508,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9508,1,3,0)
 ;;=3^Epiphora, Insuff Drainage
 ;;^UTILITY(U,$J,358.3,9508,1,4,0)
 ;;=4^375.22
 ;;^UTILITY(U,$J,358.3,9508,2)
 ;;=Epiphora, Insuff Drainage^269138
 ;;^UTILITY(U,$J,358.3,9509,0)
 ;;=375.53^^77^656^34
 ;;^UTILITY(U,$J,358.3,9509,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9509,1,3,0)
 ;;=3^Lacrimal Canal Stenosis
 ;;^UTILITY(U,$J,358.3,9509,1,4,0)
 ;;=4^375.53
 ;;^UTILITY(U,$J,358.3,9509,2)
 ;;=Lacrimal Canal Stenosis^269154
 ;;^UTILITY(U,$J,358.3,9510,0)
 ;;=375.51^^77^656^35
 ;;^UTILITY(U,$J,358.3,9510,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9510,1,3,0)
 ;;=3^Lacrimal Punctum Eversion
 ;;^UTILITY(U,$J,358.3,9510,1,4,0)
 ;;=4^375.51
 ;;^UTILITY(U,$J,358.3,9510,2)
 ;;=Lacrimal Punctum Eversion^269150
 ;;^UTILITY(U,$J,358.3,9511,0)
 ;;=375.54^^77^656^36
 ;;^UTILITY(U,$J,358.3,9511,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9511,1,3,0)
 ;;=3^Lacrimal Punctum Stenosis
 ;;^UTILITY(U,$J,358.3,9511,1,4,0)
 ;;=4^375.54
 ;;^UTILITY(U,$J,358.3,9511,2)
 ;;=Lacrimal Punctum Stenosis^269156
 ;;^UTILITY(U,$J,358.3,9512,0)
 ;;=375.56^^77^656^41
 ;;^UTILITY(U,$J,358.3,9512,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9512,1,3,0)
 ;;=3^Obstruction, Nasolacrimal duct
 ;;^UTILITY(U,$J,358.3,9512,1,4,0)
 ;;=4^375.56
 ;;^UTILITY(U,$J,358.3,9512,2)
 ;;=Obstruction, Nasolacrimal duct^269159
 ;;^UTILITY(U,$J,358.3,9513,0)
 ;;=376.30^^77^656^27
 ;;^UTILITY(U,$J,358.3,9513,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9513,1,3,0)
 ;;=3^Exophalmos
 ;;^UTILITY(U,$J,358.3,9513,1,4,0)
 ;;=4^376.30
 ;;^UTILITY(U,$J,358.3,9513,2)
 ;;=Exophthalmos^43683
 ;;^UTILITY(U,$J,358.3,9514,0)
 ;;=802.8^^77^656^29
 ;;^UTILITY(U,$J,358.3,9514,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9514,1,3,0)
 ;;=3^Fracture of Orbit
 ;;^UTILITY(U,$J,358.3,9514,1,4,0)
 ;;=4^802.8
 ;;^UTILITY(U,$J,358.3,9514,2)
 ;;=Fracture of Orbit^25315
 ;;^UTILITY(U,$J,358.3,9515,0)
 ;;=870.4^^77^656^28
 ;;^UTILITY(U,$J,358.3,9515,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9515,1,3,0)
 ;;=3^Foreign Body in Orbit
 ;;^UTILITY(U,$J,358.3,9515,1,4,0)
 ;;=4^870.4
 ;;^UTILITY(U,$J,358.3,9515,2)
 ;;=Foreign Body in Orbit^274883
 ;;^UTILITY(U,$J,358.3,9516,0)
 ;;=V45.78^^77^656^2
 ;;^UTILITY(U,$J,358.3,9516,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9516,1,3,0)
 ;;=3^Anophthalmos, Surgical
 ;;^UTILITY(U,$J,358.3,9516,1,4,0)
 ;;=4^V45.78
 ;;^UTILITY(U,$J,358.3,9516,2)
 ;;=^322068
 ;;^UTILITY(U,$J,358.3,9517,0)
 ;;=376.10^^77^656^43
 ;;^UTILITY(U,$J,358.3,9517,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9517,1,3,0)
 ;;=3^Orbital Inflammation
 ;;^UTILITY(U,$J,358.3,9517,1,4,0)
 ;;=4^376.10
 ;;^UTILITY(U,$J,358.3,9517,2)
 ;;=Orbital Inflammation^269175
 ;;^UTILITY(U,$J,358.3,9518,0)
 ;;=360.41^^77^656^45
 ;;^UTILITY(U,$J,358.3,9518,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9518,1,3,0)
 ;;=3^Phthisis
 ;;^UTILITY(U,$J,358.3,9518,1,4,0)
 ;;=4^360.41
 ;;^UTILITY(U,$J,358.3,9518,2)
 ;;=Phthisis^268564
 ;;^UTILITY(U,$J,358.3,9519,0)
 ;;=376.50^^77^656^18
 ;;^UTILITY(U,$J,358.3,9519,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9519,1,3,0)
 ;;=3^Enopthalmos
 ;;^UTILITY(U,$J,358.3,9519,1,4,0)
 ;;=4^376.50
 ;;^UTILITY(U,$J,358.3,9519,2)
 ;;=Enopthalmos^40801
 ;;^UTILITY(U,$J,358.3,9520,0)
 ;;=372.20^^77^656^5
 ;;^UTILITY(U,$J,358.3,9520,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9520,1,3,0)
 ;;=3^Blepharoconjunctivitis
 ;;^UTILITY(U,$J,358.3,9520,1,4,0)
 ;;=4^372.20
 ;;^UTILITY(U,$J,358.3,9520,2)
 ;;=Blepharoconjunctivitis^15277
 ;;^UTILITY(U,$J,358.3,9521,0)
 ;;=239.2^^77^656^44
 ;;^UTILITY(U,$J,358.3,9521,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9521,1,3,0)
 ;;=3^Orbital Tumor
 ;;^UTILITY(U,$J,358.3,9521,1,4,0)
 ;;=4^239.2
 ;;^UTILITY(U,$J,358.3,9521,2)
 ;;=Orbital Tumor^267783
 ;;^UTILITY(U,$J,358.3,9522,0)
 ;;=239.7^^77^656^42
 ;;^UTILITY(U,$J,358.3,9522,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9522,1,3,0)
 ;;=3^Optic Nerve Tumor
 ;;^UTILITY(U,$J,358.3,9522,1,4,0)
 ;;=4^239.7
 ;;^UTILITY(U,$J,358.3,9522,2)
 ;;=Optic Nerve Tumor^267785
 ;;^UTILITY(U,$J,358.3,9523,0)
 ;;=V52.2^^77^656^46
 ;;^UTILITY(U,$J,358.3,9523,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9523,1,3,0)
 ;;=3^Prosthethic Eye Check
 ;;^UTILITY(U,$J,358.3,9523,1,4,0)
 ;;=4^V52.2
 ;;^UTILITY(U,$J,358.3,9523,2)
 ;;=Prosthethic Eye Check^295498^V43.0
 ;;^UTILITY(U,$J,358.3,9524,0)
 ;;=173.10^^77^656^39
 ;;^UTILITY(U,$J,358.3,9524,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9524,1,3,0)
 ;;=3^Malig Neopl Eyelid/Canthus NOS
 ;;^UTILITY(U,$J,358.3,9524,1,4,0)
 ;;=4^173.10
 ;;^UTILITY(U,$J,358.3,9524,2)
 ;;=^340597
 ;;^UTILITY(U,$J,358.3,9525,0)
 ;;=173.11^^77^656^3
 ;;^UTILITY(U,$J,358.3,9525,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9525,1,3,0)
 ;;=3^BCC of Eyelid/Canthus
 ;;^UTILITY(U,$J,358.3,9525,1,4,0)
 ;;=4^173.11
 ;;^UTILITY(U,$J,358.3,9525,2)
 ;;=^340467
 ;;^UTILITY(U,$J,358.3,9526,0)
 ;;=173.12^^77^656^48
 ;;^UTILITY(U,$J,358.3,9526,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9526,1,3,0)
 ;;=3^SCC of Eyelid/Canthus
 ;;^UTILITY(U,$J,358.3,9526,1,4,0)
 ;;=4^173.12
 ;;^UTILITY(U,$J,358.3,9526,2)
 ;;=^340468
 ;;^UTILITY(U,$J,358.3,9527,0)
 ;;=173.19^^77^656^38
 ;;^UTILITY(U,$J,358.3,9527,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9527,1,3,0)
 ;;=3^Malig Neopl Eyelid/Canthus NEC
 ;;^UTILITY(U,$J,358.3,9527,1,4,0)
 ;;=4^173.19
 ;;^UTILITY(U,$J,358.3,9527,2)
 ;;=^340469
 ;;^UTILITY(U,$J,358.3,9528,0)
 ;;=371.23^^77^657^10
 ;;^UTILITY(U,$J,358.3,9528,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9528,1,3,0)
 ;;=3^Bullous Keratopathy
 ;;^UTILITY(U,$J,358.3,9528,1,4,0)
 ;;=4^371.23
 ;;^UTILITY(U,$J,358.3,9528,2)
 ;;=^268967
 ;;^UTILITY(U,$J,358.3,9529,0)
 ;;=371.50^^77^657^21
 ;;^UTILITY(U,$J,358.3,9529,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9529,1,3,0)
 ;;=3^Dystrophy, Corneal
 ;;^UTILITY(U,$J,358.3,9529,1,4,0)
 ;;=4^371.50
 ;;^UTILITY(U,$J,358.3,9529,2)
 ;;=Dystrophy, Corneal^28381
 ;;^UTILITY(U,$J,358.3,9530,0)
 ;;=930.0^^77^657^25
 ;;^UTILITY(U,$J,358.3,9530,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9530,1,3,0)
 ;;=3^Foreign Body, Cornea
 ;;^UTILITY(U,$J,358.3,9530,1,4,0)
 ;;=4^930.0
 ;;^UTILITY(U,$J,358.3,9530,2)
 ;;=Corneal Foreign Body^275485
 ;;^UTILITY(U,$J,358.3,9531,0)
 ;;=054.43^^77^657^37
 ;;^UTILITY(U,$J,358.3,9531,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9531,1,3,0)
 ;;=3^Keratitis, Disciform (HSV)
 ;;^UTILITY(U,$J,358.3,9531,1,4,0)
 ;;=4^054.43
 ;;^UTILITY(U,$J,358.3,9531,2)
 ;;=Herpes Simplex Keratitis^266564
 ;;^UTILITY(U,$J,358.3,9532,0)
 ;;=370.23^^77^657^39
 ;;^UTILITY(U,$J,358.3,9532,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9532,1,3,0)
 ;;=3^Keratitis, Filamentary
 ;;^UTILITY(U,$J,358.3,9532,1,4,0)
 ;;=4^370.23
 ;;^UTILITY(U,$J,358.3,9532,2)
 ;;=^268924
 ;;^UTILITY(U,$J,358.3,9533,0)
 ;;=370.33^^77^657^42
 ;;^UTILITY(U,$J,358.3,9533,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9533,1,3,0)
 ;;=3^Keratoconjunctivitis Sicca
 ;;^UTILITY(U,$J,358.3,9533,1,4,0)
 ;;=4^370.33
 ;;^UTILITY(U,$J,358.3,9533,2)
 ;;=Keratoconjunctivitis Sicca^268931
 ;;^UTILITY(U,$J,358.3,9534,0)
 ;;=371.60^^77^657^45
 ;;^UTILITY(U,$J,358.3,9534,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9534,1,3,0)
 ;;=3^Keratoconus
 ;;^UTILITY(U,$J,358.3,9534,1,4,0)
 ;;=4^371.60
 ;;^UTILITY(U,$J,358.3,9534,2)
 ;;=Keratoconus^66799
 ;;^UTILITY(U,$J,358.3,9535,0)
 ;;=371.13^^77^657^46
 ;;^UTILITY(U,$J,358.3,9535,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9535,1,3,0)
 ;;=3^Krunkenberg's Spindle
 ;;^UTILITY(U,$J,358.3,9535,1,4,0)
 ;;=4^371.13
 ;;^UTILITY(U,$J,358.3,9535,2)
 ;;=^268961
 ;;^UTILITY(U,$J,358.3,9536,0)
 ;;=371.03^^77^657^52
 ;;^UTILITY(U,$J,358.3,9536,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9536,1,3,0)
 ;;=3^Opacity, Corneal, Central
 ;;^UTILITY(U,$J,358.3,9536,1,4,0)
 ;;=4^371.03
 ;;^UTILITY(U,$J,358.3,9536,2)
 ;;=Corneal Opacity, Central^21253
 ;;^UTILITY(U,$J,358.3,9537,0)
 ;;=371.02^^77^657^53
 ;;^UTILITY(U,$J,358.3,9537,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9537,1,3,0)
 ;;=3^Opacity, Periph, Corneal
 ;;^UTILITY(U,$J,358.3,9537,1,4,0)
 ;;=4^371.02
 ;;^UTILITY(U,$J,358.3,9537,2)
 ;;=Opacity, Peripheral^268955
 ;;^UTILITY(U,$J,358.3,9538,0)
 ;;=371.42^^77^657^61
 ;;^UTILITY(U,$J,358.3,9538,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9538,1,3,0)
 ;;=3^Recurrent Erosion, Cornea
 ;;^UTILITY(U,$J,358.3,9538,1,4,0)
 ;;=4^371.42
 ;;^UTILITY(U,$J,358.3,9538,2)
 ;;=Recurrent Cornea Erosion^268978
 ;;^UTILITY(U,$J,358.3,9539,0)
 ;;=370.03^^77^657^68
 ;;^UTILITY(U,$J,358.3,9539,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9539,1,3,0)
 ;;=3^Ulcer, Central Cornea
 ;;^UTILITY(U,$J,358.3,9539,1,4,0)
 ;;=4^370.03
 ;;^UTILITY(U,$J,358.3,9539,2)
 ;;=Corneal Ulcer, Central^268910
 ;;^UTILITY(U,$J,358.3,9540,0)
 ;;=370.01^^77^657^69
 ;;^UTILITY(U,$J,358.3,9540,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9540,1,3,0)
 ;;=3^Ulcer, Marginal Cornea
 ;;^UTILITY(U,$J,358.3,9540,1,4,0)
 ;;=4^370.01
 ;;^UTILITY(U,$J,358.3,9540,2)
 ;;=Corneal Ulcer,Marginal^268908
 ;;^UTILITY(U,$J,358.3,9541,0)
 ;;=371.57^^77^657^27
 ;;^UTILITY(U,$J,358.3,9541,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9541,1,3,0)
 ;;=3^Guttata
 ;;^UTILITY(U,$J,358.3,9541,1,4,0)
 ;;=4^371.57
