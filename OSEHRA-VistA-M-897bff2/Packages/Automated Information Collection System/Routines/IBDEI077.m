IBDEI077 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,9468,1,3,0)
 ;;=3^Anisometropia
 ;;^UTILITY(U,$J,358.3,9468,1,4,0)
 ;;=4^367.31
 ;;^UTILITY(U,$J,358.3,9468,2)
 ;;=Anisometropia^7839
 ;;^UTILITY(U,$J,358.3,9469,0)
 ;;=367.20^^77^655^3
 ;;^UTILITY(U,$J,358.3,9469,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9469,1,3,0)
 ;;=3^Astigmatism, NOS
 ;;^UTILITY(U,$J,358.3,9469,1,4,0)
 ;;=4^367.20
 ;;^UTILITY(U,$J,358.3,9469,2)
 ;;=Astigmatism, NOS^11141
 ;;^UTILITY(U,$J,358.3,9470,0)
 ;;=367.0^^77^655^5
 ;;^UTILITY(U,$J,358.3,9470,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9470,1,3,0)
 ;;=3^Hypermetropia
 ;;^UTILITY(U,$J,358.3,9470,1,4,0)
 ;;=4^367.0
 ;;^UTILITY(U,$J,358.3,9470,2)
 ;;=Hypermetropia^60139
 ;;^UTILITY(U,$J,358.3,9471,0)
 ;;=367.1^^77^655^6
 ;;^UTILITY(U,$J,358.3,9471,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9471,1,3,0)
 ;;=3^Myopia
 ;;^UTILITY(U,$J,358.3,9471,1,4,0)
 ;;=4^367.1
 ;;^UTILITY(U,$J,358.3,9471,2)
 ;;=Myopia^80671
 ;;^UTILITY(U,$J,358.3,9472,0)
 ;;=367.4^^77^655^0
 ;;^UTILITY(U,$J,358.3,9472,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9472,1,3,0)
 ;;=3^Presbyopia
 ;;^UTILITY(U,$J,358.3,9472,1,4,0)
 ;;=4^367.4
 ;;^UTILITY(U,$J,358.3,9472,2)
 ;;=Presbyopia^98095
 ;;^UTILITY(U,$J,358.3,9473,0)
 ;;=368.40^^77^655^7
 ;;^UTILITY(U,$J,358.3,9473,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9473,1,3,0)
 ;;=3^Visual Field Defect
 ;;^UTILITY(U,$J,358.3,9473,1,4,0)
 ;;=4^368.40
 ;;^UTILITY(U,$J,358.3,9473,2)
 ;;=^126859
 ;;^UTILITY(U,$J,358.3,9474,0)
 ;;=V67.09^^77^655^4
 ;;^UTILITY(U,$J,358.3,9474,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9474,1,3,0)
 ;;=3^F/U Vistreos Surgery
 ;;^UTILITY(U,$J,358.3,9474,1,4,0)
 ;;=4^V67.09
 ;;^UTILITY(U,$J,358.3,9474,2)
 ;;=^322080
 ;;^UTILITY(U,$J,358.3,9475,0)
 ;;=373.32^^77^656^1
 ;;^UTILITY(U,$J,358.3,9475,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9475,1,3,0)
 ;;=3^Allerg Dermatitis, Eyelid
 ;;^UTILITY(U,$J,358.3,9475,1,4,0)
 ;;=4^373.32
 ;;^UTILITY(U,$J,358.3,9475,2)
 ;;=^269061
 ;;^UTILITY(U,$J,358.3,9476,0)
 ;;=373.00^^77^656^4
 ;;^UTILITY(U,$J,358.3,9476,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9476,1,3,0)
 ;;=3^Blepharitis
 ;;^UTILITY(U,$J,358.3,9476,1,4,0)
 ;;=4^373.00
 ;;^UTILITY(U,$J,358.3,9476,2)
 ;;=Blepharitis^15271
 ;;^UTILITY(U,$J,358.3,9477,0)
 ;;=373.2^^77^656^7
 ;;^UTILITY(U,$J,358.3,9477,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9477,1,3,0)
 ;;=3^Chalazion
 ;;^UTILITY(U,$J,358.3,9477,1,4,0)
 ;;=4^373.2
 ;;^UTILITY(U,$J,358.3,9477,2)
 ;;=^22156
 ;;^UTILITY(U,$J,358.3,9478,0)
 ;;=374.84^^77^656^9
 ;;^UTILITY(U,$J,358.3,9478,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9478,1,3,0)
 ;;=3^Cysts Of Eyelids
 ;;^UTILITY(U,$J,358.3,9478,1,4,0)
 ;;=4^374.84
 ;;^UTILITY(U,$J,358.3,9478,2)
 ;;=^269119
 ;;^UTILITY(U,$J,358.3,9479,0)
 ;;=374.87^^77^656^11
 ;;^UTILITY(U,$J,358.3,9479,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9479,1,3,0)
 ;;=3^Dermatochalasis
 ;;^UTILITY(U,$J,358.3,9479,1,4,0)
 ;;=4^374.87
 ;;^UTILITY(U,$J,358.3,9479,2)
 ;;=^269123
 ;;^UTILITY(U,$J,358.3,9480,0)
 ;;=375.15^^77^656^12
 ;;^UTILITY(U,$J,358.3,9480,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9480,1,3,0)
 ;;=3^Dry Eye Syndrome
 ;;^UTILITY(U,$J,358.3,9480,1,4,0)
 ;;=4^375.15
 ;;^UTILITY(U,$J,358.3,9480,2)
 ;;=Dry Eye Syndrome^37168
 ;;^UTILITY(U,$J,358.3,9481,0)
 ;;=374.01^^77^656^21
 ;;^UTILITY(U,$J,358.3,9481,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9481,1,3,0)
 ;;=3^Entropion, Senile
 ;;^UTILITY(U,$J,358.3,9481,1,4,0)
 ;;=4^374.01
 ;;^UTILITY(U,$J,358.3,9481,2)
 ;;=Entropion, Senile^269074
 ;;^UTILITY(U,$J,358.3,9482,0)
 ;;=375.20^^77^656^24
 ;;^UTILITY(U,$J,358.3,9482,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9482,1,3,0)
 ;;=3^Epiphora
 ;;^UTILITY(U,$J,358.3,9482,1,4,0)
 ;;=4^375.20
 ;;^UTILITY(U,$J,358.3,9482,2)
 ;;=Epiphora^269136
 ;;^UTILITY(U,$J,358.3,9483,0)
 ;;=373.11^^77^656^31
 ;;^UTILITY(U,$J,358.3,9483,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9483,1,3,0)
 ;;=3^Hordeolum Externum
 ;;^UTILITY(U,$J,358.3,9483,1,4,0)
 ;;=4^373.11
 ;;^UTILITY(U,$J,358.3,9483,2)
 ;;=^58510
 ;;^UTILITY(U,$J,358.3,9484,0)
 ;;=373.12^^77^656^32
 ;;^UTILITY(U,$J,358.3,9484,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9484,1,3,0)
 ;;=3^Hordeolum Internum
 ;;^UTILITY(U,$J,358.3,9484,1,4,0)
 ;;=4^373.12
 ;;^UTILITY(U,$J,358.3,9484,2)
 ;;=^58512
 ;;^UTILITY(U,$J,358.3,9485,0)
 ;;=216.1^^77^656^40
 ;;^UTILITY(U,$J,358.3,9485,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9485,1,3,0)
 ;;=3^Neoplasm, Benign of Lid
 ;;^UTILITY(U,$J,358.3,9485,1,4,0)
 ;;=4^216.1
 ;;^UTILITY(U,$J,358.3,9485,2)
 ;;=Neoplasm, Benign of Lid^267630
 ;;^UTILITY(U,$J,358.3,9486,0)
 ;;=374.30^^77^656^47
 ;;^UTILITY(U,$J,358.3,9486,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9486,1,3,0)
 ;;=3^Ptosis Of Eyelid
 ;;^UTILITY(U,$J,358.3,9486,1,4,0)
 ;;=4^374.30
 ;;^UTILITY(U,$J,358.3,9486,2)
 ;;=^15283
 ;;^UTILITY(U,$J,358.3,9487,0)
 ;;=375.30^^77^656^10
 ;;^UTILITY(U,$J,358.3,9487,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9487,1,3,0)
 ;;=3^Dacryocystitis
 ;;^UTILITY(U,$J,358.3,9487,1,4,0)
 ;;=4^375.30
 ;;^UTILITY(U,$J,358.3,9487,2)
 ;;=Dacrocystitis^30880
 ;;^UTILITY(U,$J,358.3,9488,0)
 ;;=921.1^^77^656^8
 ;;^UTILITY(U,$J,358.3,9488,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9488,1,3,0)
 ;;=3^Contusion Eyelid
 ;;^UTILITY(U,$J,358.3,9488,1,4,0)
 ;;=4^921.1
 ;;^UTILITY(U,$J,358.3,9488,2)
 ;;=^275367
 ;;^UTILITY(U,$J,358.3,9489,0)
 ;;=374.51^^77^656^53
 ;;^UTILITY(U,$J,358.3,9489,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9489,1,3,0)
 ;;=3^Xanthalasma
 ;;^UTILITY(U,$J,358.3,9489,1,4,0)
 ;;=4^374.51
 ;;^UTILITY(U,$J,358.3,9489,2)
 ;;=^269107
 ;;^UTILITY(U,$J,358.3,9490,0)
 ;;=374.11^^77^656^15
 ;;^UTILITY(U,$J,358.3,9490,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9490,1,3,0)
 ;;=3^Ectropion, Senile
 ;;^UTILITY(U,$J,358.3,9490,1,4,0)
 ;;=4^374.11
 ;;^UTILITY(U,$J,358.3,9490,2)
 ;;=Ectropion, Senile^269083
 ;;^UTILITY(U,$J,358.3,9491,0)
 ;;=333.81^^77^656^6
 ;;^UTILITY(U,$J,358.3,9491,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9491,1,3,0)
 ;;=3^Blepharospasm
 ;;^UTILITY(U,$J,358.3,9491,1,4,0)
 ;;=4^333.81
 ;;^UTILITY(U,$J,358.3,9491,2)
 ;;=Blepharospasm^15293
 ;;^UTILITY(U,$J,358.3,9492,0)
 ;;=351.8^^77^656^30
 ;;^UTILITY(U,$J,358.3,9492,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9492,1,3,0)
 ;;=3^Hemifacial Spasm
 ;;^UTILITY(U,$J,358.3,9492,1,4,0)
 ;;=4^351.8
 ;;^UTILITY(U,$J,358.3,9492,2)
 ;;=Hemifacial Spasm^87589
 ;;^UTILITY(U,$J,358.3,9493,0)
 ;;=374.10^^77^656^17
 ;;^UTILITY(U,$J,358.3,9493,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9493,1,3,0)
 ;;=3^Ectropion, Unspecified
 ;;^UTILITY(U,$J,358.3,9493,1,4,0)
 ;;=4^374.10
 ;;^UTILITY(U,$J,358.3,9493,2)
 ;;=Ectropion, Unspecified^38326
 ;;^UTILITY(U,$J,358.3,9494,0)
 ;;=374.14^^77^656^13
 ;;^UTILITY(U,$J,358.3,9494,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9494,1,3,0)
 ;;=3^Ectropion, Cicatricial
 ;;^UTILITY(U,$J,358.3,9494,1,4,0)
 ;;=4^374.14
 ;;^UTILITY(U,$J,358.3,9494,2)
 ;;=Edctropion, Cicatricial^269089
 ;;^UTILITY(U,$J,358.3,9495,0)
 ;;=374.12^^77^656^14
 ;;^UTILITY(U,$J,358.3,9495,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9495,1,3,0)
 ;;=3^Ectropion, Mechanical
 ;;^UTILITY(U,$J,358.3,9495,1,4,0)
 ;;=4^374.12
 ;;^UTILITY(U,$J,358.3,9495,2)
 ;;=Ectropion, Mechanical^269085
 ;;^UTILITY(U,$J,358.3,9496,0)
 ;;=374.13^^77^656^16
 ;;^UTILITY(U,$J,358.3,9496,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9496,1,3,0)
 ;;=3^Ectropion, Spastic
 ;;^UTILITY(U,$J,358.3,9496,1,4,0)
 ;;=4^374.13
 ;;^UTILITY(U,$J,358.3,9496,2)
 ;;=Ectropion, Spastic^269087
 ;;^UTILITY(U,$J,358.3,9497,0)
 ;;=374.00^^77^656^23
 ;;^UTILITY(U,$J,358.3,9497,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9497,1,3,0)
 ;;=3^Entropion, Unspec
 ;;^UTILITY(U,$J,358.3,9497,1,4,0)
 ;;=4^374.00
 ;;^UTILITY(U,$J,358.3,9497,2)
 ;;=Entropion, Unspec^41016
 ;;^UTILITY(U,$J,358.3,9498,0)
 ;;=374.04^^77^656^19
 ;;^UTILITY(U,$J,358.3,9498,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9498,1,3,0)
 ;;=3^Entropion, Cicatricial
 ;;^UTILITY(U,$J,358.3,9498,1,4,0)
 ;;=4^374.04
 ;;^UTILITY(U,$J,358.3,9498,2)
 ;;=Entropion, Cicatricial^269080
 ;;^UTILITY(U,$J,358.3,9499,0)
 ;;=374.02^^77^656^20
 ;;^UTILITY(U,$J,358.3,9499,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9499,1,3,0)
 ;;=3^Entropion, Mechanical
 ;;^UTILITY(U,$J,358.3,9499,1,4,0)
 ;;=4^374.02
 ;;^UTILITY(U,$J,358.3,9499,2)
 ;;=Entropion, Mechanical^269076
 ;;^UTILITY(U,$J,358.3,9500,0)
 ;;=374.03^^77^656^22
 ;;^UTILITY(U,$J,358.3,9500,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9500,1,3,0)
 ;;=3^Entropion, Spastic
 ;;^UTILITY(U,$J,358.3,9500,1,4,0)
 ;;=4^374.03
 ;;^UTILITY(U,$J,358.3,9500,2)
 ;;=Spastic Entropion^269078
 ;;^UTILITY(U,$J,358.3,9501,0)
 ;;=870.0^^77^656^33
 ;;^UTILITY(U,$J,358.3,9501,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9501,1,3,0)
 ;;=3^Laceration, Eyelid
 ;;^UTILITY(U,$J,358.3,9501,1,4,0)
 ;;=4^870.0
 ;;^UTILITY(U,$J,358.3,9501,2)
 ;;=Laceration, Eyelid^274879
 ;;^UTILITY(U,$J,358.3,9502,0)
 ;;=374.20^^77^656^37
 ;;^UTILITY(U,$J,358.3,9502,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9502,1,3,0)
 ;;=3^Lagophthalmos
 ;;^UTILITY(U,$J,358.3,9502,1,4,0)
 ;;=4^374.20
 ;;^UTILITY(U,$J,358.3,9502,2)
 ;;=Lagophthalmos^265452
 ;;^UTILITY(U,$J,358.3,9503,0)
 ;;=378.9^^77^656^49
 ;;^UTILITY(U,$J,358.3,9503,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9503,1,3,0)
 ;;=3^Strabismus
 ;;^UTILITY(U,$J,358.3,9503,1,4,0)
 ;;=4^378.9
 ;;^UTILITY(U,$J,358.3,9503,2)
 ;;=Strabismus^123833
 ;;^UTILITY(U,$J,358.3,9504,0)
 ;;=242.90^^77^656^50
 ;;^UTILITY(U,$J,358.3,9504,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9504,1,3,0)
 ;;=3^Thyroid Eye Disease
 ;;^UTILITY(U,$J,358.3,9504,1,4,0)
 ;;=4^242.90
 ;;^UTILITY(U,$J,358.3,9504,2)
 ;;=Thyroid Eye Disease^267811^376.21
 ;;^UTILITY(U,$J,358.3,9505,0)
 ;;=374.05^^77^656^51
 ;;^UTILITY(U,$J,358.3,9505,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9505,1,3,0)
 ;;=3^Trichiasis
 ;;^UTILITY(U,$J,358.3,9505,1,4,0)
 ;;=4^374.05
