IBDEI07L ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,9980,1,3,0)
 ;;=3^Rubeosis Iridis
 ;;^UTILITY(U,$J,358.3,9980,1,4,0)
 ;;=4^364.42
 ;;^UTILITY(U,$J,358.3,9980,2)
 ;;=Rubeosis Iridis^268716
 ;;^UTILITY(U,$J,358.3,9981,0)
 ;;=364.77^^79^673^2
 ;;^UTILITY(U,$J,358.3,9981,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9981,1,3,0)
 ;;=3^Angle Recession, Iris
 ;;^UTILITY(U,$J,358.3,9981,1,4,0)
 ;;=4^364.77
 ;;^UTILITY(U,$J,358.3,9981,2)
 ;;=Angle Recession, Iris^268743
 ;;^UTILITY(U,$J,358.3,9982,0)
 ;;=364.59^^79^673^35
 ;;^UTILITY(U,$J,358.3,9982,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9982,1,3,0)
 ;;=3^Iris Atrophy
 ;;^UTILITY(U,$J,358.3,9982,1,4,0)
 ;;=4^364.59
 ;;^UTILITY(U,$J,358.3,9982,2)
 ;;=Iris Atrophy^268731
 ;;^UTILITY(U,$J,358.3,9983,0)
 ;;=224.0^^79^673^7
 ;;^UTILITY(U,$J,358.3,9983,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9983,1,3,0)
 ;;=3^Benign Neopl of Iris
 ;;^UTILITY(U,$J,358.3,9983,1,4,0)
 ;;=4^224.0
 ;;^UTILITY(U,$J,358.3,9983,2)
 ;;=^267670
 ;;^UTILITY(U,$J,358.3,9984,0)
 ;;=364.72^^79^673^4
 ;;^UTILITY(U,$J,358.3,9984,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9984,1,3,0)
 ;;=3^Anterior Synechiae
 ;;^UTILITY(U,$J,358.3,9984,1,4,0)
 ;;=4^364.72
 ;;^UTILITY(U,$J,358.3,9984,2)
 ;;=Anterior Synechiae^265517
 ;;^UTILITY(U,$J,358.3,9985,0)
 ;;=364.71^^79^673^58
 ;;^UTILITY(U,$J,358.3,9985,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9985,1,3,0)
 ;;=3^Posterior Synechiae
 ;;^UTILITY(U,$J,358.3,9985,1,4,0)
 ;;=4^364.71
 ;;^UTILITY(U,$J,358.3,9985,2)
 ;;=Posterior Synechiae^265519
 ;;^UTILITY(U,$J,358.3,9986,0)
 ;;=364.00^^79^673^31
 ;;^UTILITY(U,$J,358.3,9986,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9986,1,3,0)
 ;;=3^Iridocyclitis, Acute
 ;;^UTILITY(U,$J,358.3,9986,1,4,0)
 ;;=4^364.00
 ;;^UTILITY(U,$J,358.3,9986,2)
 ;;=Iridocyclitis, Acute^268703
 ;;^UTILITY(U,$J,358.3,9987,0)
 ;;=379.40^^79^673^60
 ;;^UTILITY(U,$J,358.3,9987,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9987,1,3,0)
 ;;=3^Pupil, Abnormal function
 ;;^UTILITY(U,$J,358.3,9987,1,4,0)
 ;;=4^379.40
 ;;^UTILITY(U,$J,358.3,9987,2)
 ;;=Pupil, Abnormal function^101288
 ;;^UTILITY(U,$J,358.3,9988,0)
 ;;=190.0^^79^673^49
 ;;^UTILITY(U,$J,358.3,9988,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9988,1,3,0)
 ;;=3^Malig Neopl of Iris
 ;;^UTILITY(U,$J,358.3,9988,1,4,0)
 ;;=4^190.0
 ;;^UTILITY(U,$J,358.3,9988,2)
 ;;=Malig Neopl of Iris^267271
 ;;^UTILITY(U,$J,358.3,9989,0)
 ;;=190.3^^79^673^48
 ;;^UTILITY(U,$J,358.3,9989,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9989,1,3,0)
 ;;=3^Malig Neopl Conjunctiva
 ;;^UTILITY(U,$J,358.3,9989,1,4,0)
 ;;=4^190.3
 ;;^UTILITY(U,$J,358.3,9989,2)
 ;;=Malig Neopl Conjunctiva^267274
 ;;^UTILITY(U,$J,358.3,9990,0)
 ;;=364.41^^79^673^29
 ;;^UTILITY(U,$J,358.3,9990,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9990,1,3,0)
 ;;=3^Hyphema
 ;;^UTILITY(U,$J,358.3,9990,1,4,0)
 ;;=4^364.41
 ;;^UTILITY(U,$J,358.3,9990,2)
 ;;=Hyphema^60498
 ;;^UTILITY(U,$J,358.3,9991,0)
 ;;=364.05^^79^673^30
 ;;^UTILITY(U,$J,358.3,9991,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9991,1,3,0)
 ;;=3^Hypopyon
 ;;^UTILITY(U,$J,358.3,9991,1,4,0)
 ;;=4^364.05
 ;;^UTILITY(U,$J,358.3,9991,2)
 ;;=Hypopion^60720
 ;;^UTILITY(U,$J,358.3,9992,0)
 ;;=365.02^^79^673^3
 ;;^UTILITY(U,$J,358.3,9992,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9992,1,3,0)
 ;;=3^Angle, Narrow
 ;;^UTILITY(U,$J,358.3,9992,1,4,0)
 ;;=4^365.02
 ;;^UTILITY(U,$J,358.3,9992,2)
 ;;=Angle, Narrow^268748
 ;;^UTILITY(U,$J,358.3,9993,0)
 ;;=372.63^^79^673^65
 ;;^UTILITY(U,$J,358.3,9993,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9993,1,3,0)
 ;;=3^Symblepharon
 ;;^UTILITY(U,$J,358.3,9993,1,4,0)
 ;;=4^372.63
 ;;^UTILITY(U,$J,358.3,9993,2)
 ;;=Symblepharon^265885
 ;;^UTILITY(U,$J,358.3,9994,0)
 ;;=372.75^^79^673^20
 ;;^UTILITY(U,$J,358.3,9994,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9994,1,3,0)
 ;;=3^Cysts, Conjunctival
 ;;^UTILITY(U,$J,358.3,9994,1,4,0)
 ;;=4^372.75
 ;;^UTILITY(U,$J,358.3,9994,2)
 ;;=Cysts, Conjunctival^269049
 ;;^UTILITY(U,$J,358.3,9995,0)
 ;;=364.81^^79^673^24
 ;;^UTILITY(U,$J,358.3,9995,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9995,1,3,0)
 ;;=3^Floppy Iris Syndrome
 ;;^UTILITY(U,$J,358.3,9995,1,4,0)
 ;;=4^364.81
 ;;^UTILITY(U,$J,358.3,9995,2)
 ;;=^335254
 ;;^UTILITY(U,$J,358.3,9996,0)
 ;;=372.06^^79^673^17
 ;;^UTILITY(U,$J,358.3,9996,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9996,1,3,0)
 ;;=3^Conjunctivitis,Chemical Acute
 ;;^UTILITY(U,$J,358.3,9996,1,4,0)
 ;;=4^372.06
 ;;^UTILITY(U,$J,358.3,9996,2)
 ;;=^338237
 ;;^UTILITY(U,$J,358.3,9997,0)
 ;;=379.31^^79^674^1
 ;;^UTILITY(U,$J,358.3,9997,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9997,1,3,0)
 ;;=3^Aphakia
 ;;^UTILITY(U,$J,358.3,9997,1,4,0)
 ;;=4^379.31
 ;;^UTILITY(U,$J,358.3,9997,2)
 ;;=^9445
 ;;^UTILITY(U,$J,358.3,9998,0)
 ;;=366.15^^79^674^3
 ;;^UTILITY(U,$J,358.3,9998,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9998,1,3,0)
 ;;=3^Cataract, Cortical
 ;;^UTILITY(U,$J,358.3,9998,1,4,0)
 ;;=4^366.15
 ;;^UTILITY(U,$J,358.3,9998,2)
 ;;=Cortical Senile Cataract^268797
 ;;^UTILITY(U,$J,358.3,9999,0)
 ;;=366.16^^79^674^5
 ;;^UTILITY(U,$J,358.3,9999,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9999,1,3,0)
 ;;=3^Cataract, Nuclear Sclerosis
 ;;^UTILITY(U,$J,358.3,9999,1,4,0)
 ;;=4^366.16
 ;;^UTILITY(U,$J,358.3,9999,2)
 ;;=366.16^268800
 ;;^UTILITY(U,$J,358.3,10000,0)
 ;;=366.14^^79^674^6
 ;;^UTILITY(U,$J,358.3,10000,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10000,1,3,0)
 ;;=3^Cataract, PSC/Post Subcapsular
 ;;^UTILITY(U,$J,358.3,10000,1,4,0)
 ;;=4^366.14
 ;;^UTILITY(U,$J,358.3,10000,2)
 ;;=Post Subcapsular Senile Cataract^268796
 ;;^UTILITY(U,$J,358.3,10001,0)
 ;;=V43.1^^79^674^16
 ;;^UTILITY(U,$J,358.3,10001,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10001,1,3,0)
 ;;=3^Pseudophakia
 ;;^UTILITY(U,$J,358.3,10001,1,4,0)
 ;;=4^V43.1
 ;;^UTILITY(U,$J,358.3,10001,2)
 ;;=^69114^V45.61
 ;;^UTILITY(U,$J,358.3,10002,0)
 ;;=366.20^^79^674^11
 ;;^UTILITY(U,$J,358.3,10002,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10002,1,3,0)
 ;;=3^Cataract, Traumatic
 ;;^UTILITY(U,$J,358.3,10002,1,4,0)
 ;;=4^366.20
 ;;^UTILITY(U,$J,358.3,10002,2)
 ;;=Traumatic Cataract, NOS^268802
 ;;^UTILITY(U,$J,358.3,10003,0)
 ;;=366.52^^79^674^7
 ;;^UTILITY(U,$J,358.3,10003,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10003,1,3,0)
 ;;=3^Cataract, Post Capsular-not obscuring vision
 ;;^UTILITY(U,$J,358.3,10003,1,4,0)
 ;;=4^366.52
 ;;^UTILITY(U,$J,358.3,10003,2)
 ;;=Posterior Capsular Fibrosis Not Obscuring Vision^268822
 ;;^UTILITY(U,$J,358.3,10004,0)
 ;;=366.53^^79^674^8
 ;;^UTILITY(U,$J,358.3,10004,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10004,1,3,0)
 ;;=3^Cataract, Post Capsular-obscuring vision
 ;;^UTILITY(U,$J,358.3,10004,1,4,0)
 ;;=4^366.53
 ;;^UTILITY(U,$J,358.3,10004,2)
 ;;=Post Capsular Fibrosis, Obscuring Vision^268823
 ;;^UTILITY(U,$J,358.3,10005,0)
 ;;=366.11^^79^674^9
 ;;^UTILITY(U,$J,358.3,10005,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10005,1,3,0)
 ;;=3^Cataract, Pseudoexfoliation
 ;;^UTILITY(U,$J,358.3,10005,1,4,0)
 ;;=4^366.11
 ;;^UTILITY(U,$J,358.3,10005,2)
 ;;=Pseudoexfoliation^265538
 ;;^UTILITY(U,$J,358.3,10006,0)
 ;;=366.17^^79^674^4
 ;;^UTILITY(U,$J,358.3,10006,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10006,1,3,0)
 ;;=3^Cataract, Mature
 ;;^UTILITY(U,$J,358.3,10006,1,4,0)
 ;;=4^366.17
 ;;^UTILITY(U,$J,358.3,10006,2)
 ;;=Mature Cataract^265530
 ;;^UTILITY(U,$J,358.3,10007,0)
 ;;=366.45^^79^674^10
 ;;^UTILITY(U,$J,358.3,10007,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10007,1,3,0)
 ;;=3^Cataract, Toxic
 ;;^UTILITY(U,$J,358.3,10007,1,4,0)
 ;;=4^366.45
 ;;^UTILITY(U,$J,358.3,10007,2)
 ;;=Toxic Cataract^268819
 ;;^UTILITY(U,$J,358.3,10008,0)
 ;;=362.53^^79^674^14
 ;;^UTILITY(U,$J,358.3,10008,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10008,1,3,0)
 ;;=3^Cystoid Macular Edema (CME)
 ;;^UTILITY(U,$J,358.3,10008,1,4,0)
 ;;=4^362.53
 ;;^UTILITY(U,$J,358.3,10008,2)
 ;;=^268638^996.79
 ;;^UTILITY(U,$J,358.3,10009,0)
 ;;=743.30^^79^674^2
 ;;^UTILITY(U,$J,358.3,10009,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10009,1,3,0)
 ;;=3^Cataract, Congenital
 ;;^UTILITY(U,$J,358.3,10009,1,4,0)
 ;;=4^743.30
 ;;^UTILITY(U,$J,358.3,10009,2)
 ;;=Congenital Cataract^27422
 ;;^UTILITY(U,$J,358.3,10010,0)
 ;;=996.53^^79^674^13
 ;;^UTILITY(U,$J,358.3,10010,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10010,1,3,0)
 ;;=3^Complication of Lens Implant 
 ;;^UTILITY(U,$J,358.3,10010,1,4,0)
 ;;=4^996.53
 ;;^UTILITY(U,$J,358.3,10010,2)
 ;;=Complication of Lens Implant (dislocation)^276279
 ;;^UTILITY(U,$J,358.3,10011,0)
 ;;=366.9^^79^674^12
 ;;^UTILITY(U,$J,358.3,10011,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10011,1,3,0)
 ;;=3^Cataract, Unspecified
 ;;^UTILITY(U,$J,358.3,10011,1,4,0)
 ;;=4^366.9
 ;;^UTILITY(U,$J,358.3,10011,2)
 ;;=^20266
 ;;^UTILITY(U,$J,358.3,10012,0)
 ;;=996.69^^79^674^15
 ;;^UTILITY(U,$J,358.3,10012,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10012,1,3,0)
 ;;=3^Post Op Endophthalmitis
 ;;^UTILITY(U,$J,358.3,10012,1,4,0)
 ;;=4^996.69
 ;;^UTILITY(U,$J,358.3,10012,2)
 ;;=Post Op Endophthalmitis^276291
 ;;^UTILITY(U,$J,358.3,10013,0)
 ;;=362.36^^79^675^5
 ;;^UTILITY(U,$J,358.3,10013,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10013,1,3,0)
 ;;=3^Brvo/Branch Retinal Vein Occlusion
 ;;^UTILITY(U,$J,358.3,10013,1,4,0)
 ;;=4^362.36
 ;;^UTILITY(U,$J,358.3,10013,2)
 ;;=Branch Retina Vein Occlusion^268626
 ;;^UTILITY(U,$J,358.3,10014,0)
 ;;=362.31^^79^675^12
 ;;^UTILITY(U,$J,358.3,10014,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10014,1,3,0)
 ;;=3^Crao/Central Retinal Artery Occlusion
 ;;^UTILITY(U,$J,358.3,10014,1,4,0)
 ;;=4^362.31
 ;;^UTILITY(U,$J,358.3,10014,2)
 ;;=Central Retinal Artery Occulusion^21255
 ;;^UTILITY(U,$J,358.3,10015,0)
 ;;=362.35^^79^675^13
 ;;^UTILITY(U,$J,358.3,10015,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10015,1,3,0)
 ;;=3^Crvo/Central Retinal Vein Occlusion
 ;;^UTILITY(U,$J,358.3,10015,1,4,0)
 ;;=4^362.35
