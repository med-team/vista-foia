IBDEI014 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,847,2)
 ;;=^268837
 ;;^UTILITY(U,$J,358.3,848,0)
 ;;=368.9^^13^64^43
 ;;^UTILITY(U,$J,358.3,848,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,848,1,3,0)
 ;;=3^368.9
 ;;^UTILITY(U,$J,358.3,848,1,4,0)
 ;;=4^Unspecified Visual Disturbance
 ;;^UTILITY(U,$J,358.3,848,2)
 ;;=^124001
 ;;^UTILITY(U,$J,358.3,849,0)
 ;;=367.51^^13^64^3
 ;;^UTILITY(U,$J,358.3,849,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,849,1,3,0)
 ;;=3^367.51
 ;;^UTILITY(U,$J,358.3,849,1,4,0)
 ;;=4^Accommodation Deficiency
 ;;^UTILITY(U,$J,358.3,849,2)
 ;;=^265376
 ;;^UTILITY(U,$J,358.3,850,0)
 ;;=378.9^^13^64^42
 ;;^UTILITY(U,$J,358.3,850,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,850,1,3,0)
 ;;=3^378.9
 ;;^UTILITY(U,$J,358.3,850,1,4,0)
 ;;=4^Unspec Disorder of Eye Movement
 ;;^UTILITY(U,$J,358.3,850,2)
 ;;=^123833
 ;;^UTILITY(U,$J,358.3,851,0)
 ;;=368.46^^13^64^34
 ;;^UTILITY(U,$J,358.3,851,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,851,1,3,0)
 ;;=3^368.46
 ;;^UTILITY(U,$J,358.3,851,1,4,0)
 ;;=4^Homonymous Bilateral Field Defects
 ;;^UTILITY(U,$J,358.3,851,2)
 ;;=^58452
 ;;^UTILITY(U,$J,358.3,852,0)
 ;;=368.47^^13^64^33
 ;;^UTILITY(U,$J,358.3,852,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,852,1,3,0)
 ;;=3^368.47
 ;;^UTILITY(U,$J,358.3,852,1,4,0)
 ;;=4^Heteronymous Bilateral Field Defects
 ;;^UTILITY(U,$J,358.3,852,2)
 ;;=^268847
 ;;^UTILITY(U,$J,358.3,853,0)
 ;;=378.85^^13^64^5
 ;;^UTILITY(U,$J,358.3,853,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,853,1,3,0)
 ;;=3^378.85
 ;;^UTILITY(U,$J,358.3,853,1,4,0)
 ;;=4^Anomalies of Divergence
 ;;^UTILITY(U,$J,358.3,853,2)
 ;;=^269279
 ;;^UTILITY(U,$J,358.3,854,0)
 ;;=369.10^^13^64^20
 ;;^UTILITY(U,$J,358.3,854,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,854,1,3,0)
 ;;=3^369.10
 ;;^UTILITY(U,$J,358.3,854,1,4,0)
 ;;=4^BE:Blindness;LE:Low Vision
 ;;^UTILITY(U,$J,358.3,854,2)
 ;;=^268870
 ;;^UTILITY(U,$J,358.3,855,0)
 ;;=369.20^^13^64^24
 ;;^UTILITY(U,$J,358.3,855,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,855,1,3,0)
 ;;=3^369.20
 ;;^UTILITY(U,$J,358.3,855,1,4,0)
 ;;=4^BE:Low Vision;LE:Same
 ;;^UTILITY(U,$J,358.3,855,2)
 ;;=^71924
 ;;^UTILITY(U,$J,358.3,856,0)
 ;;=905.0^^13^65^1
 ;;^UTILITY(U,$J,358.3,856,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,856,1,3,0)
 ;;=3^905.0
 ;;^UTILITY(U,$J,358.3,856,1,4,0)
 ;;=4^TBI Late Effect w/ Skull Fracture
 ;;^UTILITY(U,$J,358.3,856,2)
 ;;=^275214
 ;;^UTILITY(U,$J,358.3,857,0)
 ;;=907.0^^13^65^2
 ;;^UTILITY(U,$J,358.3,857,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,857,1,3,0)
 ;;=3^907.0
 ;;^UTILITY(U,$J,358.3,857,1,4,0)
 ;;=4^TBI Late Effect w/o Skull Fracture
 ;;^UTILITY(U,$J,358.3,857,2)
 ;;=^68489
 ;;^UTILITY(U,$J,358.3,858,0)
 ;;=V15.52^^13^65^4
 ;;^UTILITY(U,$J,358.3,858,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,858,1,3,0)
 ;;=3^V15.52
 ;;^UTILITY(U,$J,358.3,858,1,4,0)
 ;;=4^Personal History of TBI
 ;;^UTILITY(U,$J,358.3,858,2)
 ;;=^338495
 ;;^UTILITY(U,$J,358.3,859,0)
 ;;=950.9^^13^65^3
 ;;^UTILITY(U,$J,358.3,859,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,859,1,3,0)
 ;;=3^950.9
 ;;^UTILITY(U,$J,358.3,859,1,4,0)
 ;;=4^Other Traumatic Blindness
 ;;^UTILITY(U,$J,358.3,859,2)
 ;;=^275894
 ;;^UTILITY(U,$J,358.3,860,0)
 ;;=V57.89^^14^66^1
 ;;^UTILITY(U,$J,358.3,860,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,860,1,1,0)
 ;;=1^V57.89
 ;;^UTILITY(U,$J,358.3,860,1,2,0)
 ;;=2^Other Rehabilitation Procedure
 ;;^UTILITY(U,$J,358.3,860,2)
 ;;=^177367
 ;;^UTILITY(U,$J,358.3,861,0)
 ;;=96150^^15^67^1^^^^1
 ;;^UTILITY(U,$J,358.3,861,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,861,1,2,0)
 ;;=2^96150
 ;;^UTILITY(U,$J,358.3,861,1,3,0)
 ;;=3^Initial Assess,Ea 15 min
 ;;^UTILITY(U,$J,358.3,862,0)
 ;;=96151^^15^67^2^^^^1
 ;;^UTILITY(U,$J,358.3,862,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,862,1,2,0)
 ;;=2^96151
 ;;^UTILITY(U,$J,358.3,862,1,3,0)
 ;;=3^Re-Assess, Ea 15 min
 ;;^UTILITY(U,$J,358.3,863,0)
 ;;=97755^^15^67^5^^^^1
 ;;^UTILITY(U,$J,358.3,863,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,863,1,2,0)
 ;;=2^97755
 ;;^UTILITY(U,$J,358.3,863,1,3,0)
 ;;=3^Assistive Technology Assess,ea 15min
 ;;^UTILITY(U,$J,358.3,864,0)
 ;;=96152^^15^67^4^^^^1
 ;;^UTILITY(U,$J,358.3,864,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,864,1,2,0)
 ;;=2^96152
 ;;^UTILITY(U,$J,358.3,864,1,3,0)
 ;;=3^Adjustment Counseling, Ea 15min
 ;;^UTILITY(U,$J,358.3,865,0)
 ;;=G9012^^15^67^3^^^^1
 ;;^UTILITY(U,$J,358.3,865,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,865,1,2,0)
 ;;=2^G9012
 ;;^UTILITY(U,$J,358.3,865,1,3,0)
 ;;=3^Other Case Management
 ;;^UTILITY(U,$J,358.3,866,0)
 ;;=97110^^15^68^6^^^^1
 ;;^UTILITY(U,$J,358.3,866,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,866,1,2,0)
 ;;=2^97110
 ;;^UTILITY(U,$J,358.3,866,1,3,0)
 ;;=3^Therapeutic Exercise,1> areas,ea 15min
 ;;^UTILITY(U,$J,358.3,867,0)
 ;;=97116^^15^68^4^^^^1
 ;;^UTILITY(U,$J,358.3,867,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,867,1,2,0)
 ;;=2^97116
 ;;^UTILITY(U,$J,358.3,867,1,3,0)
 ;;=3^Basic Mobility/Gait Training,Ea 15 Min
 ;;^UTILITY(U,$J,358.3,868,0)
 ;;=97532^^15^68^5^^^^1
 ;;^UTILITY(U,$J,358.3,868,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,868,1,2,0)
 ;;=2^97532
 ;;^UTILITY(U,$J,358.3,868,1,3,0)
 ;;=3^Cognitive Skills Development,Ea 15 Min
 ;;^UTILITY(U,$J,358.3,869,0)
 ;;=97533^^15^68^1^^^^1
 ;;^UTILITY(U,$J,358.3,869,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,869,1,2,0)
 ;;=2^97533
 ;;^UTILITY(U,$J,358.3,869,1,3,0)
 ;;=3^Sensory Integration Techniques,Ea 15 Min
 ;;^UTILITY(U,$J,358.3,870,0)
 ;;=97535^^15^68^2^^^^1
 ;;^UTILITY(U,$J,358.3,870,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,870,1,2,0)
 ;;=2^97535
 ;;^UTILITY(U,$J,358.3,870,1,3,0)
 ;;=3^Self Care Mgmt/ADL,Ea 15 Min
 ;;^UTILITY(U,$J,358.3,871,0)
 ;;=97537^^15^68^3^^^^1
 ;;^UTILITY(U,$J,358.3,871,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,871,1,2,0)
 ;;=2^97537
 ;;^UTILITY(U,$J,358.3,871,1,3,0)
 ;;=3^Community/Occupation Trng,Ea 15 Min
 ;;^UTILITY(U,$J,358.3,872,0)
 ;;=96153^^15^69^2^^^^1
 ;;^UTILITY(U,$J,358.3,872,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,872,1,2,0)
 ;;=2^96153
 ;;^UTILITY(U,$J,358.3,872,1,3,0)
 ;;=3^H&B Intervention Grp 2>,Ea 15min
 ;;^UTILITY(U,$J,358.3,873,0)
 ;;=96154^^15^69^3^^^^1
 ;;^UTILITY(U,$J,358.3,873,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,873,1,2,0)
 ;;=2^96154
 ;;^UTILITY(U,$J,358.3,873,1,3,0)
 ;;=3^H&B Intervention Indv w/pt/fam,Ea 15min
 ;;^UTILITY(U,$J,358.3,874,0)
 ;;=96155^^15^69^4^^^^1
 ;;^UTILITY(U,$J,358.3,874,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,874,1,2,0)
 ;;=2^96155
 ;;^UTILITY(U,$J,358.3,874,1,3,0)
 ;;=3^H&B Intervention Fam w/o patient,Ea 15min
 ;;^UTILITY(U,$J,358.3,875,0)
 ;;=99366^^15^70^1^^^^1
 ;;^UTILITY(U,$J,358.3,875,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,875,1,2,0)
 ;;=2^99366
 ;;^UTILITY(U,$J,358.3,875,1,3,0)
 ;;=3^Interdisc. Team Mtg. w/Pt w/o Physician
 ;;^UTILITY(U,$J,358.3,876,0)
 ;;=99368^^15^70^3^^^^1
 ;;^UTILITY(U,$J,358.3,876,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,876,1,2,0)
 ;;=2^99368
 ;;^UTILITY(U,$J,358.3,876,1,3,0)
 ;;=3^Interdisc. Team Mtg. w/o Pt w/o Phys
 ;;^UTILITY(U,$J,358.3,877,0)
 ;;=99367^^15^70^2^^^^1
 ;;^UTILITY(U,$J,358.3,877,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,877,1,2,0)
 ;;=2^99367
 ;;^UTILITY(U,$J,358.3,877,1,3,0)
 ;;=3^Interdisc. Team Mtg. w/o Pt w/Phys
 ;;^UTILITY(U,$J,358.3,878,0)
 ;;=99082^^15^71^1^^^^1
 ;;^UTILITY(U,$J,358.3,878,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,878,1,2,0)
 ;;=2^99082
 ;;^UTILITY(U,$J,358.3,878,1,3,0)
 ;;=3^Transportation/Escort of Patient
 ;;^UTILITY(U,$J,358.3,879,0)
 ;;=99600^^15^72^1^^^^1
 ;;^UTILITY(U,$J,358.3,879,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,879,1,2,0)
 ;;=2^99600
 ;;^UTILITY(U,$J,358.3,879,1,3,0)
 ;;=3^Home Visit by Nonphysician
 ;;^UTILITY(U,$J,358.3,880,0)
 ;;=G0155^^15^72^2^^^^1
 ;;^UTILITY(U,$J,358.3,880,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,880,1,2,0)
 ;;=2^G0155
 ;;^UTILITY(U,$J,358.3,880,1,3,0)
 ;;=3^Home Visit by CSW,ea 15 min
 ;;^UTILITY(U,$J,358.3,881,0)
 ;;=410.01^^16^73^1
 ;;^UTILITY(U,$J,358.3,881,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,881,1,4,0)
 ;;=4^410.01
 ;;^UTILITY(U,$J,358.3,881,1,5,0)
 ;;=5^Acute MI, Anterolateral, Initial
 ;;^UTILITY(U,$J,358.3,881,2)
 ;;=Acute MI, Anterolateral, Initial^269639
 ;;^UTILITY(U,$J,358.3,882,0)
 ;;=410.02^^16^73^2
 ;;^UTILITY(U,$J,358.3,882,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,882,1,4,0)
 ;;=4^410.02
 ;;^UTILITY(U,$J,358.3,882,1,5,0)
 ;;=5^Acute MI Anterolateral, Subsequent
 ;;^UTILITY(U,$J,358.3,882,2)
 ;;=Acute MI Anterolateral, Subsequent^269640
 ;;^UTILITY(U,$J,358.3,883,0)
 ;;=410.11^^16^73^3
 ;;^UTILITY(U,$J,358.3,883,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,883,1,4,0)
 ;;=4^410.11
 ;;^UTILITY(U,$J,358.3,883,1,5,0)
 ;;=5^Acute MI, Anterior, Initial
 ;;^UTILITY(U,$J,358.3,883,2)
 ;;=Acute MI, Anterior, Initial^269643
 ;;^UTILITY(U,$J,358.3,884,0)
 ;;=410.12^^16^73^4
 ;;^UTILITY(U,$J,358.3,884,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,884,1,4,0)
 ;;=4^410.12
 ;;^UTILITY(U,$J,358.3,884,1,5,0)
 ;;=5^Acute MI, Anterior, Subsequent
 ;;^UTILITY(U,$J,358.3,884,2)
 ;;=Acute MI, Anterior, Subsequent^269644
 ;;^UTILITY(U,$J,358.3,885,0)
 ;;=410.21^^16^73^5
 ;;^UTILITY(U,$J,358.3,885,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,885,1,4,0)
 ;;=4^410.21
 ;;^UTILITY(U,$J,358.3,885,1,5,0)
 ;;=5^Acute MI, Inferolateral, Initial
 ;;^UTILITY(U,$J,358.3,885,2)
 ;;=Acute MI, Inferolateral, Initial^269647
 ;;^UTILITY(U,$J,358.3,886,0)
 ;;=410.22^^16^73^6
 ;;^UTILITY(U,$J,358.3,886,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,886,1,4,0)
 ;;=4^410.22
 ;;^UTILITY(U,$J,358.3,886,1,5,0)
 ;;=5^Acute MI, Inferior, Subsequent
 ;;^UTILITY(U,$J,358.3,886,2)
 ;;=Acute MI, Inferior, Subsequent^269648
 ;;^UTILITY(U,$J,358.3,887,0)
 ;;=410.31^^16^73^7
 ;;^UTILITY(U,$J,358.3,887,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,887,1,4,0)
 ;;=4^410.31
 ;;^UTILITY(U,$J,358.3,887,1,5,0)
 ;;=5^Acute MI, Inferopostior, Initial
