IBDEI01U ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,1890,0)
 ;;=592.0^^21^140^4
 ;;^UTILITY(U,$J,358.3,1890,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1890,1,2,0)
 ;;=2^592.0
 ;;^UTILITY(U,$J,358.3,1890,1,5,0)
 ;;=5^RENAL CALCULI
 ;;^UTILITY(U,$J,358.3,1890,2)
 ;;=^67056
 ;;^UTILITY(U,$J,358.3,1891,0)
 ;;=585.9^^21^140^6
 ;;^UTILITY(U,$J,358.3,1891,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1891,1,2,0)
 ;;=2^585.9
 ;;^UTILITY(U,$J,358.3,1891,1,5,0)
 ;;=5^RENAL FAILURE,CHRONIC
 ;;^UTILITY(U,$J,358.3,1891,2)
 ;;=^332812
 ;;^UTILITY(U,$J,358.3,1892,0)
 ;;=584.9^^21^140^5
 ;;^UTILITY(U,$J,358.3,1892,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1892,1,2,0)
 ;;=2^584.9
 ;;^UTILITY(U,$J,358.3,1892,1,5,0)
 ;;=5^RENAL FAILURE,ACUTE
 ;;^UTILITY(U,$J,358.3,1892,2)
 ;;=^338532
 ;;^UTILITY(U,$J,358.3,1893,0)
 ;;=714.0^^21^140^7
 ;;^UTILITY(U,$J,358.3,1893,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1893,1,2,0)
 ;;=2^714.0
 ;;^UTILITY(U,$J,358.3,1893,1,5,0)
 ;;=5^RHEUMATOID ARTHRITIS
 ;;^UTILITY(U,$J,358.3,1893,2)
 ;;=^10473
 ;;^UTILITY(U,$J,358.3,1894,0)
 ;;=460.^^21^140^8
 ;;^UTILITY(U,$J,358.3,1894,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1894,1,2,0)
 ;;=2^460.
 ;;^UTILITY(U,$J,358.3,1894,1,5,0)
 ;;=5^RHINITIS,ACUTE
 ;;^UTILITY(U,$J,358.3,1894,2)
 ;;=^26543
 ;;^UTILITY(U,$J,358.3,1895,0)
 ;;=472.0^^21^140^9
 ;;^UTILITY(U,$J,358.3,1895,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1895,1,2,0)
 ;;=2^472.0
 ;;^UTILITY(U,$J,358.3,1895,1,5,0)
 ;;=5^RHINITIS,CHRONIC
 ;;^UTILITY(U,$J,358.3,1895,2)
 ;;=^24434
 ;;^UTILITY(U,$J,358.3,1896,0)
 ;;=726.90^^21^141^1
 ;;^UTILITY(U,$J,358.3,1896,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1896,1,2,0)
 ;;=2^726.90
 ;;^UTILITY(U,$J,358.3,1896,1,5,0)
 ;;=5^TENDONITIS
 ;;^UTILITY(U,$J,358.3,1896,2)
 ;;=^41010
 ;;^UTILITY(U,$J,358.3,1897,0)
 ;;=451.19^^21^141^2
 ;;^UTILITY(U,$J,358.3,1897,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1897,1,2,0)
 ;;=2^451.19
 ;;^UTILITY(U,$J,358.3,1897,1,5,0)
 ;;=5^THROMBOSIS/DVT
 ;;^UTILITY(U,$J,358.3,1897,2)
 ;;=^269812
 ;;^UTILITY(U,$J,358.3,1898,0)
 ;;=110.3^^21^141^3
 ;;^UTILITY(U,$J,358.3,1898,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1898,1,2,0)
 ;;=2^110.3
 ;;^UTILITY(U,$J,358.3,1898,1,5,0)
 ;;=5^TINEA CRURIS
 ;;^UTILITY(U,$J,358.3,1898,2)
 ;;=^33171
 ;;^UTILITY(U,$J,358.3,1899,0)
 ;;=110.4^^21^141^4
 ;;^UTILITY(U,$J,358.3,1899,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1899,1,2,0)
 ;;=2^110.4
 ;;^UTILITY(U,$J,358.3,1899,1,5,0)
 ;;=5^TINEA PEDIS
 ;;^UTILITY(U,$J,358.3,1899,2)
 ;;=^33168
 ;;^UTILITY(U,$J,358.3,1900,0)
 ;;=525.9^^21^141^6
 ;;^UTILITY(U,$J,358.3,1900,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1900,1,2,0)
 ;;=2^525.9
 ;;^UTILITY(U,$J,358.3,1900,1,5,0)
 ;;=5^TOOTHACHE
 ;;^UTILITY(U,$J,358.3,1900,2)
 ;;=^123871
 ;;^UTILITY(U,$J,358.3,1901,0)
 ;;=465.9^^21^141^13
 ;;^UTILITY(U,$J,358.3,1901,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1901,1,2,0)
 ;;=2^465.9
 ;;^UTILITY(U,$J,358.3,1901,1,5,0)
 ;;=5^URI,ACUTE
 ;;^UTILITY(U,$J,358.3,1901,2)
 ;;=^269878
 ;;^UTILITY(U,$J,358.3,1902,0)
 ;;=599.0^^21^141^17
 ;;^UTILITY(U,$J,358.3,1902,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1902,1,2,0)
 ;;=2^599.0
 ;;^UTILITY(U,$J,358.3,1902,1,5,0)
 ;;=5^UTI
 ;;^UTILITY(U,$J,358.3,1902,2)
 ;;=^124436
 ;;^UTILITY(U,$J,358.3,1903,0)
 ;;=597.80^^21^141^12
 ;;^UTILITY(U,$J,358.3,1903,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1903,1,2,0)
 ;;=2^597.80
 ;;^UTILITY(U,$J,358.3,1903,1,5,0)
 ;;=5^URETHRITIS NOS
 ;;^UTILITY(U,$J,358.3,1903,2)
 ;;=^124214
 ;;^UTILITY(U,$J,358.3,1904,0)
 ;;=788.41^^21^141^14
 ;;^UTILITY(U,$J,358.3,1904,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1904,1,2,0)
 ;;=2^788.41
 ;;^UTILITY(U,$J,358.3,1904,1,5,0)
 ;;=5^URINARY FREQUENCY
 ;;^UTILITY(U,$J,358.3,1904,2)
 ;;=^124396
 ;;^UTILITY(U,$J,358.3,1905,0)
 ;;=788.30^^21^141^15
 ;;^UTILITY(U,$J,358.3,1905,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1905,1,2,0)
 ;;=2^788.30
 ;;^UTILITY(U,$J,358.3,1905,1,5,0)
 ;;=5^URINARY INCONTINENCE, UNSPEC
 ;;^UTILITY(U,$J,358.3,1905,2)
 ;;=^124400
 ;;^UTILITY(U,$J,358.3,1906,0)
 ;;=788.20^^21^141^16
 ;;^UTILITY(U,$J,358.3,1906,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1906,1,2,0)
 ;;=2^788.20
 ;;^UTILITY(U,$J,358.3,1906,1,5,0)
 ;;=5^URINARY RETENTION
 ;;^UTILITY(U,$J,358.3,1906,2)
 ;;=^295812
 ;;^UTILITY(U,$J,358.3,1907,0)
 ;;=532.90^^21^141^7
 ;;^UTILITY(U,$J,358.3,1907,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1907,1,2,0)
 ;;=2^532.90
 ;;^UTILITY(U,$J,358.3,1907,1,5,0)
 ;;=5^ULCER,DUODENAL
 ;;^UTILITY(U,$J,358.3,1907,2)
 ;;=^37311
 ;;^UTILITY(U,$J,358.3,1908,0)
 ;;=531.90^^21^141^8
 ;;^UTILITY(U,$J,358.3,1908,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1908,1,2,0)
 ;;=2^531.90
 ;;^UTILITY(U,$J,358.3,1908,1,5,0)
 ;;=5^ULCER,GASTRIC
 ;;^UTILITY(U,$J,358.3,1908,2)
 ;;=^51128
 ;;^UTILITY(U,$J,358.3,1909,0)
 ;;=533.90^^21^141^10
 ;;^UTILITY(U,$J,358.3,1909,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1909,1,2,0)
 ;;=2^533.90
 ;;^UTILITY(U,$J,358.3,1909,1,5,0)
 ;;=5^ULCER,PEPTIC
 ;;^UTILITY(U,$J,358.3,1909,2)
 ;;=^93051
 ;;^UTILITY(U,$J,358.3,1910,0)
 ;;=707.10^^21^141^9
 ;;^UTILITY(U,$J,358.3,1910,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1910,1,2,0)
 ;;=2^707.10
 ;;^UTILITY(U,$J,358.3,1910,1,5,0)
 ;;=5^ULCER,LOWER LIMB
 ;;^UTILITY(U,$J,358.3,1910,2)
 ;;=^322142
 ;;^UTILITY(U,$J,358.3,1911,0)
 ;;=707.9^^21^141^11
 ;;^UTILITY(U,$J,358.3,1911,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1911,1,2,0)
 ;;=2^707.9
 ;;^UTILITY(U,$J,358.3,1911,1,5,0)
 ;;=5^ULCER,SKIN NOS
 ;;^UTILITY(U,$J,358.3,1911,2)
 ;;=^24439
 ;;^UTILITY(U,$J,358.3,1912,0)
 ;;=388.30^^21^141^5
 ;;^UTILITY(U,$J,358.3,1912,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1912,1,2,0)
 ;;=2^388.30
 ;;^UTILITY(U,$J,358.3,1912,1,5,0)
 ;;=5^TINNITUS NOS
 ;;^UTILITY(U,$J,358.3,1912,2)
 ;;=^119771
 ;;^UTILITY(U,$J,358.3,1913,0)
 ;;=309.9^^21^142^1
 ;;^UTILITY(U,$J,358.3,1913,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1913,1,2,0)
 ;;=2^309.9
 ;;^UTILITY(U,$J,358.3,1913,1,5,0)
 ;;=5^ADJUSTMENT REACTION NOS
 ;;^UTILITY(U,$J,358.3,1913,2)
 ;;=^123757
 ;;^UTILITY(U,$J,358.3,1914,0)
 ;;=309.0^^21^142^2
 ;;^UTILITY(U,$J,358.3,1914,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1914,1,2,0)
 ;;=2^309.0
 ;;^UTILITY(U,$J,358.3,1914,1,5,0)
 ;;=5^ADJUSTMNT DIS W DEPRESSN
 ;;^UTILITY(U,$J,358.3,1914,2)
 ;;=^331948
 ;;^UTILITY(U,$J,358.3,1915,0)
 ;;=300.22^^21^143^1
 ;;^UTILITY(U,$J,358.3,1915,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1915,1,2,0)
 ;;=2^300.22
 ;;^UTILITY(U,$J,358.3,1915,1,5,0)
 ;;=5^AGORAPHOBIA W/O PANIC
 ;;^UTILITY(U,$J,358.3,1915,2)
 ;;=^4218
 ;;^UTILITY(U,$J,358.3,1916,0)
 ;;=300.00^^21^143^2
 ;;^UTILITY(U,$J,358.3,1916,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1916,1,2,0)
 ;;=2^300.00
 ;;^UTILITY(U,$J,358.3,1916,1,5,0)
 ;;=5^ANXIETY STATE NOS
 ;;^UTILITY(U,$J,358.3,1916,2)
 ;;=^9200
 ;;^UTILITY(U,$J,358.3,1917,0)
 ;;=300.02^^21^143^3
 ;;^UTILITY(U,$J,358.3,1917,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1917,1,2,0)
 ;;=2^300.02
 ;;^UTILITY(U,$J,358.3,1917,1,5,0)
 ;;=5^GENERALIZED ANXIETY DIS
 ;;^UTILITY(U,$J,358.3,1917,2)
 ;;=^50059
 ;;^UTILITY(U,$J,358.3,1918,0)
 ;;=300.3^^21^143^4
 ;;^UTILITY(U,$J,358.3,1918,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1918,1,2,0)
 ;;=2^300.3
 ;;^UTILITY(U,$J,358.3,1918,1,5,0)
 ;;=5^OBSESSIVE-COMPULSIVE DIS
 ;;^UTILITY(U,$J,358.3,1918,2)
 ;;=^84904
 ;;^UTILITY(U,$J,358.3,1919,0)
 ;;=300.21^^21^143^5
 ;;^UTILITY(U,$J,358.3,1919,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1919,1,2,0)
 ;;=2^300.21
 ;;^UTILITY(U,$J,358.3,1919,1,5,0)
 ;;=5^PANIC D/O W/ AGOROPHOBIA
 ;;^UTILITY(U,$J,358.3,1919,2)
 ;;=^331911
 ;;^UTILITY(U,$J,358.3,1920,0)
 ;;=300.01^^21^143^6
 ;;^UTILITY(U,$J,358.3,1920,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1920,1,2,0)
 ;;=2^300.01
 ;;^UTILITY(U,$J,358.3,1920,1,5,0)
 ;;=5^PANIC DIS W/O AGORPHOBIA
 ;;^UTILITY(U,$J,358.3,1920,2)
 ;;=^331906
 ;;^UTILITY(U,$J,358.3,1921,0)
 ;;=309.81^^21^143^7
 ;;^UTILITY(U,$J,358.3,1921,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1921,1,2,0)
 ;;=2^309.81
 ;;^UTILITY(U,$J,358.3,1921,1,5,0)
 ;;=5^POSTTRAUMATIC STRESS D/O
 ;;^UTILITY(U,$J,358.3,1921,2)
 ;;=^114692
 ;;^UTILITY(U,$J,358.3,1922,0)
 ;;=296.60^^21^144^4
 ;;^UTILITY(U,$J,358.3,1922,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1922,1,2,0)
 ;;=2^296.60
 ;;^UTILITY(U,$J,358.3,1922,1,5,0)
 ;;=5^BIPOLAR I D/O MIXED
 ;;^UTILITY(U,$J,358.3,1922,2)
 ;;=^331884
 ;;^UTILITY(U,$J,358.3,1923,0)
 ;;=296.50^^21^144^2
 ;;^UTILITY(U,$J,358.3,1923,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1923,1,2,0)
 ;;=2^296.50
 ;;^UTILITY(U,$J,358.3,1923,1,5,0)
 ;;=5^BIPOLAR I D/O DEPRES NOS
 ;;^UTILITY(U,$J,358.3,1923,2)
 ;;=^331877
 ;;^UTILITY(U,$J,358.3,1924,0)
 ;;=296.40^^21^144^3
 ;;^UTILITY(U,$J,358.3,1924,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1924,1,2,0)
 ;;=2^296.40
 ;;^UTILITY(U,$J,358.3,1924,1,5,0)
 ;;=5^BIPOLAR I D/O MANIC NOS
 ;;^UTILITY(U,$J,358.3,1924,2)
 ;;=^331870
 ;;^UTILITY(U,$J,358.3,1925,0)
 ;;=296.80^^21^144^1
 ;;^UTILITY(U,$J,358.3,1925,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1925,1,2,0)
 ;;=2^296.80
 ;;^UTILITY(U,$J,358.3,1925,1,5,0)
 ;;=5^BIPOLAR DISORDER NOS
 ;;^UTILITY(U,$J,358.3,1925,2)
 ;;=^331892
 ;;^UTILITY(U,$J,358.3,1926,0)
 ;;=331.0^^21^145^1
 ;;^UTILITY(U,$J,358.3,1926,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1926,1,2,0)
 ;;=2^331.0
 ;;^UTILITY(U,$J,358.3,1926,1,5,0)
 ;;=5^DEMENTIA,ALZHEIMER'S DISEASE
 ;;^UTILITY(U,$J,358.3,1926,2)
 ;;=^5679^294.11
 ;;^UTILITY(U,$J,358.3,1927,0)
 ;;=290.10^^21^145^2
 ;;^UTILITY(U,$J,358.3,1927,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1927,1,2,0)
 ;;=2^290.10
 ;;^UTILITY(U,$J,358.3,1927,1,5,0)
 ;;=5^DEMENTIA,PRESENILE
 ;;^UTILITY(U,$J,358.3,1927,2)
 ;;=^31674
 ;;^UTILITY(U,$J,358.3,1928,0)
 ;;=290.0^^21^145^3
 ;;^UTILITY(U,$J,358.3,1928,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,1928,1,2,0)
 ;;=2^290.0
 ;;^UTILITY(U,$J,358.3,1928,1,5,0)
 ;;=5^DEMENTIA,SENILE UNCOMP
 ;;^UTILITY(U,$J,358.3,1928,2)
 ;;=^31703
