IBDEI003 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358,37,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,38,0)
 ;;=NATL MH PSYCHOLOGIST FY13-Q2^0^National Mental Health Psychologist February 2013^1^0^1^1^^133^80^3^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,38,2,0)
 ;;=^358.02I^2^2
 ;;^UTILITY(U,$J,358,38,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,38,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,39,0)
 ;;=NATIONAL PODIATRY FY13-Q2^0^National Podiatry January 2013^1^0^1^1^^133^80^5^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,39,2,0)
 ;;=^358.02I^6^6
 ;;^UTILITY(U,$J,358,39,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,39,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,39,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,39,2,4,0)
 ;;=4^1
 ;;^UTILITY(U,$J,358,39,2,5,0)
 ;;=5^1
 ;;^UTILITY(U,$J,358,39,2,6,0)
 ;;=6^1
 ;;^UTILITY(U,$J,358,40,0)
 ;;=NATIONAL POD NAIL NURS FY13-Q2^0^National Podiatry Nail Clinic Nursing January 2013^1^0^1^1^^133^80^1^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,40,2,0)
 ;;=^358.02I^6^6
 ;;^UTILITY(U,$J,358,40,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,40,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,40,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,40,2,4,0)
 ;;=4^1
 ;;^UTILITY(U,$J,358,40,2,5,0)
 ;;=5^1
 ;;^UTILITY(U,$J,358,40,2,6,0)
 ;;=6^1
 ;;^UTILITY(U,$J,358,41,0)
 ;;=NATIONAL POLYTRAUMA FY13-Q2^1^National Polytrauma February 2013^1^0^1^1^^133^80^2^1^^1^p^1^2.1
 ;;^UTILITY(U,$J,358,41,2,0)
 ;;=^358.02I^6^6
 ;;^UTILITY(U,$J,358,41,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,41,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,41,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,41,2,4,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,41,2,5,0)
 ;;=4^1
 ;;^UTILITY(U,$J,358,41,2,6,0)
 ;;=5^1
 ;;^UTILITY(U,$J,358,42,0)
 ;;=NATIONAL PRIMARY CARE FY13-Q2^1^National Primary Care Form December 2012^1^0^1^1^^133^80^9^1^^1^p^1^2.1
 ;;^UTILITY(U,$J,358,42,2,0)
 ;;=^358.02I^6^6
 ;;^UTILITY(U,$J,358,42,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,42,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,42,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,42,2,4,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,42,2,5,0)
 ;;=4^1
 ;;^UTILITY(U,$J,358,42,2,6,0)
 ;;=5^1
 ;;^UTILITY(U,$J,358,43,0)
 ;;=NATIONAL PULMONARY FY13-Q2^0^National Pulmonary February 2013^1^0^0^1^^133^80^2^1^^1^p^1
 ;;^UTILITY(U,$J,358,43,2,0)
 ;;=^358.02I^2^2
 ;;^UTILITY(U,$J,358,43,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,43,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,44,0)
 ;;=NATL RADIATION THERAPY FY13-Q2^2^NATIONAL RADIATION THERAPY January 2013^1^0^1^1^^133^80^3^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,44,2,0)
 ;;=^358.02I^3^3
 ;;^UTILITY(U,$J,358,44,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,44,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,44,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,45,0)
 ;;=NATL REC THERAPY GRP FY13-Q2^2^National Recreation Therapy Group January 2013^1^0^1^1^^133^80^1^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,45,2,0)
 ;;=^358.02I^3^3
 ;;^UTILITY(U,$J,358,45,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,45,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,45,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,46,0)
 ;;=NATL REC THERAPY IND FY13-Q2^2^National Recreation Therapy January 2013^1^0^1^1^^133^80^1^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,46,2,0)
 ;;=^358.02I^3^3
 ;;^UTILITY(U,$J,358,46,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,46,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,46,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,47,0)
 ;;=NATIONAL MH MHICM FY13-Q2^1^National MH Intensive Program February 2013^1^0^1^1^^133^80^3^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,47,2,0)
 ;;=^358.02I^3^3
 ;;^UTILITY(U,$J,358,47,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,47,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,47,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,48,0)
 ;;=NATIONAL NEUROLOGY FY13-Q2^0^National Neurology February 2013^1^0^1^1^^133^80^2^1^^1^p^1
 ;;^UTILITY(U,$J,358,49,0)
 ;;=NATL NURSING CLINIC FY13-Q2^1^National Nursing Clinic EEF-December 2012^1^0^1^1^^133^80^3^1^^1^p^1^2.1
 ;;^UTILITY(U,$J,358,49,2,0)
 ;;=^358.02I^4^4
 ;;^UTILITY(U,$J,358,49,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,49,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,49,2,3,0)
 ;;=4^1
 ;;^UTILITY(U,$J,358,49,2,4,0)
 ;;=4^1
 ;;^UTILITY(U,$J,358,50,0)
 ;;=NATIONAL OB/GYN FY13-Q2^2^NATIONAL OB/GYN January 2013^1^0^1^1^^133^80^3^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,51,0)
 ;;=NATIONAL ORTHOPEDICS FY13-Q2^0^National Orthopedics February 2013^1^0^1^1^^133^80^5^1^^1^p^1^2.1
 ;;^UTILITY(U,$J,358,51,2,0)
 ;;=^358.02I^6^6
 ;;^UTILITY(U,$J,358,51,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,51,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,51,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,51,2,4,0)
 ;;=4^1
 ;;^UTILITY(U,$J,358,51,2,5,0)
 ;;=5^1
 ;;^UTILITY(U,$J,358,51,2,6,0)
 ;;=5^1
 ;;^UTILITY(U,$J,358,52,0)
 ;;=NATL PHYSIATRIST INPT FY13-Q2^2^National Inpatient Rehab Physiatrist February 2013^1^0^1^1^^133^80^3^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,52,2,0)
 ;;=^358.02I^3^3
 ;;^UTILITY(U,$J,358,52,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,52,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,52,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,53,0)
 ;;=NATL PHYSIATRIST OTPT FY13-Q2^2^National Rehab Physiatrist Outpatient February 2013^1^0^1^1^^133^80^3^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,53,2,0)
 ;;=^358.02I^3^3
 ;;^UTILITY(U,$J,358,53,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,53,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,53,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,54,0)
 ;;=NATL REHAB-PT/OT/KT FY13-Q2^2^National Rehab PT/OT/KT February 2013^1^0^1^1^^133^80^2^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,54,2,0)
 ;;=^358.02I^3^3
 ;;^UTILITY(U,$J,358,54,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,54,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,54,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,55,0)
 ;;=NATIONAL RESP THERAPY FY13-Q2^0^National Repiratory Therapy (PFT/Sleep/Oxygen) February 2013^1^0^1^1^^133^80^2^1^^1^p^1^2.1
 ;;^UTILITY(U,$J,358,55,2,0)
 ;;=^358.02I^2^2
 ;;^UTILITY(U,$J,358,55,2,1,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,55,2,2,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,56,0)
 ;;=NATIONAL SCI FY13-Q2^1^National Spinal Cord Injury January 2013^1^0^1^1^^133^80^2^1^^1^p^1^2.1
 ;;^UTILITY(U,$J,358,56,2,0)
 ;;=^358.02I^6^6
 ;;^UTILITY(U,$J,358,56,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,56,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,56,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,56,2,4,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,56,2,5,0)
 ;;=4^1
 ;;^UTILITY(U,$J,358,56,2,6,0)
 ;;=5^1
 ;;^UTILITY(U,$J,358,57,0)
 ;;=NATIONAL SPEECH FY13-Q2^1^National Speech February 2013^1^0^1^1^^133^80^1^1^^1^p^1
 ;;^UTILITY(U,$J,358,57,2,0)
 ;;=^358.02I^3^3
 ;;^UTILITY(U,$J,358,57,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,57,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,57,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,58,0)
 ;;=NATIONAL SWS MH FY13-Q2^0^National Social Work Service Mental Health February 2013^1^0^1^1^^133^80^3^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,58,2,0)
 ;;=^358.02I^2^2
 ;;^UTILITY(U,$J,358,58,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,58,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,59,0)
 ;;=NATIONAL TBI FY13-Q2^1^National Traumatic Brain Injury January 2013^1^0^1^1^^133^80^2^1^^1^p^1^2.1
 ;;^UTILITY(U,$J,358,59,2,0)
 ;;=^358.02I^6^6
 ;;^UTILITY(U,$J,358,59,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,59,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,59,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,59,2,4,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,59,2,5,0)
 ;;=4^1
 ;;^UTILITY(U,$J,358,59,2,6,0)
 ;;=5^1
 ;;^UTILITY(U,$J,358,60,0)
 ;;=NATL TELEHLTH-PT SITE FY13-Q2^0^National Telehealth-Patient Site February 2013^1^0^^1^^133^80^2^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,61,0)
 ;;=NATIONAL THORACIC SURG FY13-Q2^1^National Thoracic Surgery February 2013^1^0^1^1^^133^80^2^1^^1^p^1^2.1
 ;;^UTILITY(U,$J,358,61,2,0)
 ;;=^358.02I^6^6
 ;;^UTILITY(U,$J,358,61,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,61,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,61,2,3,0)
 ;;=3^1
 ;;^UTILITY(U,$J,358,61,2,4,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,61,2,5,0)
 ;;=4^1
 ;;^UTILITY(U,$J,358,61,2,6,0)
 ;;=5^1
 ;;^UTILITY(U,$J,358,62,0)
 ;;=NATIONAL TRANSPLANT FY13-Q2^0^National Transplant February 2013^1^0^1^1^^133^80^2^1^^1^p^1^2.1
 ;;^UTILITY(U,$J,358,63,0)
 ;;=NATIONAL UROLOGY FY13-Q2^0^National Urology Form February 2013^1^0^1^1^^133^80^3^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,63,2,0)
 ;;=^358.02I^2^2
 ;;^UTILITY(U,$J,358,63,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,63,2,2,0)
 ;;=2^1
 ;;^UTILITY(U,$J,358,64,0)
 ;;=NATIONAL VASCULAR SURG FY13-Q2^0^NATIONAL VASCULAR SURGERY-November 2012^1^0^1^1^^133^80^2^1^^1^p^1^2.1
 ;;^UTILITY(U,$J,358,65,0)
 ;;=NATIONAL WOMENS HEALTH FY13-Q2^2^NATIONAL WOMENS HEALTH-December 2012^1^0^1^1^^133^80^3^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,66,0)
 ;;=NATIONAL ONCOLOGY FY13-Q2^0^National Oncology February 2013^1^0^1^1^^133^80^4^1^^1^p^1^3
 ;;^UTILITY(U,$J,358,66,2,0)
 ;;=^358.02I^2^2
 ;;^UTILITY(U,$J,358,66,2,1,0)
 ;;=1^1
 ;;^UTILITY(U,$J,358,66,2,2,0)
 ;;=2^1
