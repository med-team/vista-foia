IBDEI08B ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,10961,0)
 ;;=784.3^^87^732^1
 ;;^UTILITY(U,$J,358.3,10961,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10961,1,3,0)
 ;;=3^Aphasia
 ;;^UTILITY(U,$J,358.3,10961,1,4,0)
 ;;=4^784.3
 ;;^UTILITY(U,$J,358.3,10961,2)
 ;;=^9453
 ;;^UTILITY(U,$J,358.3,10962,0)
 ;;=334.3^^87^732^2
 ;;^UTILITY(U,$J,358.3,10962,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10962,1,3,0)
 ;;=3^Cerebellar Ataxia Nec
 ;;^UTILITY(U,$J,358.3,10962,1,4,0)
 ;;=4^334.3
 ;;^UTILITY(U,$J,358.3,10962,2)
 ;;=^87376
 ;;^UTILITY(U,$J,358.3,10963,0)
 ;;=438.11^^87^732^4
 ;;^UTILITY(U,$J,358.3,10963,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10963,1,3,0)
 ;;=3^Late Eff Cereb/Vasc,Aphasia
 ;;^UTILITY(U,$J,358.3,10963,1,4,0)
 ;;=4^438.11
 ;;^UTILITY(U,$J,358.3,10963,2)
 ;;=^317907
 ;;^UTILITY(U,$J,358.3,10964,0)
 ;;=438.0^^87^732^3
 ;;^UTILITY(U,$J,358.3,10964,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10964,1,3,0)
 ;;=3^Late Eff Cereb/Vasc, Cog Def
 ;;^UTILITY(U,$J,358.3,10964,1,4,0)
 ;;=4^438.0
 ;;^UTILITY(U,$J,358.3,10964,2)
 ;;=^317905
 ;;^UTILITY(U,$J,358.3,10965,0)
 ;;=438.20^^87^732^8
 ;;^UTILITY(U,$J,358.3,10965,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10965,1,3,0)
 ;;=3^Late Eff Cereb/Vasc,Hemipl,Uns
 ;;^UTILITY(U,$J,358.3,10965,1,4,0)
 ;;=4^438.20
 ;;^UTILITY(U,$J,358.3,10965,2)
 ;;=^317910
 ;;^UTILITY(U,$J,358.3,10966,0)
 ;;=438.40^^87^732^12
 ;;^UTILITY(U,$J,358.3,10966,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10966,1,3,0)
 ;;=3^Monoplegia of LL, Side NOS
 ;;^UTILITY(U,$J,358.3,10966,1,4,0)
 ;;=4^438.40
 ;;^UTILITY(U,$J,358.3,10966,2)
 ;;=^317916
 ;;^UTILITY(U,$J,358.3,10967,0)
 ;;=438.9^^87^732^16
 ;;^UTILITY(U,$J,358.3,10967,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10967,1,3,0)
 ;;=3^Unspec Late Eff Cerebro Diseas
 ;;^UTILITY(U,$J,358.3,10967,1,4,0)
 ;;=4^438.9
 ;;^UTILITY(U,$J,358.3,10967,2)
 ;;=^269757
 ;;^UTILITY(U,$J,358.3,10968,0)
 ;;=907.0^^87^732^9
 ;;^UTILITY(U,$J,358.3,10968,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10968,1,3,0)
 ;;=3^Lt Eff Intracranial Inj w/o skull frac
 ;;^UTILITY(U,$J,358.3,10968,1,4,0)
 ;;=4^907.0
 ;;^UTILITY(U,$J,358.3,10968,2)
 ;;=^68489
 ;;^UTILITY(U,$J,358.3,10969,0)
 ;;=340.^^87^732^13
 ;;^UTILITY(U,$J,358.3,10969,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10969,1,3,0)
 ;;=3^Multiple Sclerosis
 ;;^UTILITY(U,$J,358.3,10969,1,4,0)
 ;;=4^340.
 ;;^UTILITY(U,$J,358.3,10969,2)
 ;;=^79761
 ;;^UTILITY(U,$J,358.3,10970,0)
 ;;=332.0^^87^732^15
 ;;^UTILITY(U,$J,358.3,10970,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10970,1,3,0)
 ;;=3^Paralysis Agitans
 ;;^UTILITY(U,$J,358.3,10970,1,4,0)
 ;;=4^332.0
 ;;^UTILITY(U,$J,358.3,10970,2)
 ;;=^304847
 ;;^UTILITY(U,$J,358.3,10971,0)
 ;;=780.39^^87^732^14
 ;;^UTILITY(U,$J,358.3,10971,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10971,1,3,0)
 ;;=3^Other Convulsions
 ;;^UTILITY(U,$J,358.3,10971,1,4,0)
 ;;=4^780.39
 ;;^UTILITY(U,$J,358.3,10971,2)
 ;;=^28162
 ;;^UTILITY(U,$J,358.3,10972,0)
 ;;=438.12^^87^732^5
 ;;^UTILITY(U,$J,358.3,10972,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10972,1,3,0)
 ;;=3^Late Eff Cereb/Vasc,Dysphasia
 ;;^UTILITY(U,$J,358.3,10972,1,4,0)
 ;;=4^438.12
 ;;^UTILITY(U,$J,358.3,10972,2)
 ;;=^317908
 ;;^UTILITY(U,$J,358.3,10973,0)
 ;;=438.21^^87^732^6
 ;;^UTILITY(U,$J,358.3,10973,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10973,1,3,0)
 ;;=3^Late Eff Cereb/Vasc,Hemipl,Dom
 ;;^UTILITY(U,$J,358.3,10973,1,4,0)
 ;;=4^438.21
 ;;^UTILITY(U,$J,358.3,10973,2)
 ;;=^317911
 ;;^UTILITY(U,$J,358.3,10974,0)
 ;;=438.22^^87^732^7
 ;;^UTILITY(U,$J,358.3,10974,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10974,1,3,0)
 ;;=3^Late Eff Cereb/Vasc,Hemipl,NonDom
 ;;^UTILITY(U,$J,358.3,10974,1,4,0)
 ;;=4^438.22
 ;;^UTILITY(U,$J,358.3,10974,2)
 ;;=^317912
 ;;^UTILITY(U,$J,358.3,10975,0)
 ;;=438.41^^87^732^10
 ;;^UTILITY(U,$J,358.3,10975,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10975,1,3,0)
 ;;=3^Monoplegia of LL, Dom Side
 ;;^UTILITY(U,$J,358.3,10975,1,4,0)
 ;;=4^438.41
 ;;^UTILITY(U,$J,358.3,10975,2)
 ;;=^317917
 ;;^UTILITY(U,$J,358.3,10976,0)
 ;;=438.42^^87^732^11
 ;;^UTILITY(U,$J,358.3,10976,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10976,1,3,0)
 ;;=3^Monoplegia of LL, NonDom Side
 ;;^UTILITY(U,$J,358.3,10976,1,4,0)
 ;;=4^438.42
 ;;^UTILITY(U,$J,358.3,10976,2)
 ;;=^317918
 ;;^UTILITY(U,$J,358.3,10977,0)
 ;;=427.31^^87^733^3
 ;;^UTILITY(U,$J,358.3,10977,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10977,1,3,0)
 ;;=3^Atrial Fibrillation
 ;;^UTILITY(U,$J,358.3,10977,1,4,0)
 ;;=4^427.31
 ;;^UTILITY(U,$J,358.3,10977,2)
 ;;=^11378
 ;;^UTILITY(U,$J,358.3,10978,0)
 ;;=V45.81^^87^733^2
 ;;^UTILITY(U,$J,358.3,10978,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10978,1,3,0)
 ;;=3^Aortocoronary Bypass
 ;;^UTILITY(U,$J,358.3,10978,1,4,0)
 ;;=4^V45.81
 ;;^UTILITY(U,$J,358.3,10978,2)
 ;;=^97129
 ;;^UTILITY(U,$J,358.3,10979,0)
 ;;=414.00^^87^733^9
 ;;^UTILITY(U,$J,358.3,10979,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10979,1,3,0)
 ;;=3^Cor Atheroscl Unsp Typ-Ves
 ;;^UTILITY(U,$J,358.3,10979,1,4,0)
 ;;=4^414.00
 ;;^UTILITY(U,$J,358.3,10979,2)
 ;;=^28512
 ;;^UTILITY(U,$J,358.3,10980,0)
 ;;=414.01^^87^733^8
 ;;^UTILITY(U,$J,358.3,10980,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10980,1,3,0)
 ;;=3^Cor Atheroscl Native Coronary Artery
 ;;^UTILITY(U,$J,358.3,10980,1,4,0)
 ;;=4^414.01
 ;;^UTILITY(U,$J,358.3,10980,2)
 ;;=^303281
 ;;^UTILITY(U,$J,358.3,10981,0)
 ;;=425.4^^87^733^17
 ;;^UTILITY(U,$J,358.3,10981,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10981,1,3,0)
 ;;=3^Primary Cardiomyopathy NEC
 ;;^UTILITY(U,$J,358.3,10981,1,4,0)
 ;;=4^425.4
 ;;^UTILITY(U,$J,358.3,10981,2)
 ;;=^87808
 ;;^UTILITY(U,$J,358.3,10982,0)
 ;;=428.0^^87^733^7
 ;;^UTILITY(U,$J,358.3,10982,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10982,1,3,0)
 ;;=3^Congestive Heart Failure
 ;;^UTILITY(U,$J,358.3,10982,1,4,0)
 ;;=4^428.0
 ;;^UTILITY(U,$J,358.3,10982,2)
 ;;=^54758
 ;;^UTILITY(U,$J,358.3,10983,0)
 ;;=496.^^87^733^5
 ;;^UTILITY(U,$J,358.3,10983,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10983,1,3,0)
 ;;=3^Chr Airway Obstruct Nec
 ;;^UTILITY(U,$J,358.3,10983,1,4,0)
 ;;=4^496.
 ;;^UTILITY(U,$J,358.3,10983,2)
 ;;=^24355
 ;;^UTILITY(U,$J,358.3,10984,0)
 ;;=491.21^^87^733^13
 ;;^UTILITY(U,$J,358.3,10984,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10984,1,3,0)
 ;;=3^Ob Ch Bronchitis W Exacerb
 ;;^UTILITY(U,$J,358.3,10984,1,4,0)
 ;;=4^491.21
 ;;^UTILITY(U,$J,358.3,10984,2)
 ;;=^269954
 ;;^UTILITY(U,$J,358.3,10985,0)
 ;;=493.20^^87^733^6
 ;;^UTILITY(U,$J,358.3,10985,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10985,1,3,0)
 ;;=3^Chr Obs Asthma W/O Stat Asth/A
 ;;^UTILITY(U,$J,358.3,10985,1,4,0)
 ;;=4^493.20
 ;;^UTILITY(U,$J,358.3,10985,2)
 ;;=^269964
 ;;^UTILITY(U,$J,358.3,10986,0)
 ;;=491.20^^87^733^14
 ;;^UTILITY(U,$J,358.3,10986,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10986,1,3,0)
 ;;=3^Ob Ch Bronchitis W/O Exacerb
 ;;^UTILITY(U,$J,358.3,10986,1,4,0)
 ;;=4^491.20
 ;;^UTILITY(U,$J,358.3,10986,2)
 ;;=^269953
 ;;^UTILITY(U,$J,358.3,10987,0)
 ;;=492.8^^87^733^10
 ;;^UTILITY(U,$J,358.3,10987,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10987,1,3,0)
 ;;=3^Emphysema Nec
 ;;^UTILITY(U,$J,358.3,10987,1,4,0)
 ;;=4^492.8
 ;;^UTILITY(U,$J,358.3,10987,2)
 ;;=^87569
 ;;^UTILITY(U,$J,358.3,10988,0)
 ;;=V17.3^^87^733^12
 ;;^UTILITY(U,$J,358.3,10988,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10988,1,3,0)
 ;;=3^Fam Hx-Ischem Heart Dis
 ;;^UTILITY(U,$J,358.3,10988,1,4,0)
 ;;=4^V17.3
 ;;^UTILITY(U,$J,358.3,10988,2)
 ;;=^295305
 ;;^UTILITY(U,$J,358.3,10989,0)
 ;;=410.92^^87^733^1
 ;;^UTILITY(U,$J,358.3,10989,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10989,1,3,0)
 ;;=3^AMI NOS, Subsequent
 ;;^UTILITY(U,$J,358.3,10989,1,4,0)
 ;;=4^410.92
 ;;^UTILITY(U,$J,358.3,10989,2)
 ;;=^269675
 ;;^UTILITY(U,$J,358.3,10990,0)
 ;;=412.^^87^733^15
 ;;^UTILITY(U,$J,358.3,10990,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10990,1,3,0)
 ;;=3^Old Myocardial Infarction
 ;;^UTILITY(U,$J,358.3,10990,1,4,0)
 ;;=4^412.
 ;;^UTILITY(U,$J,358.3,10990,2)
 ;;=^259884
 ;;^UTILITY(U,$J,358.3,10991,0)
 ;;=V45.01^^87^733^4
 ;;^UTILITY(U,$J,358.3,10991,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10991,1,3,0)
 ;;=3^Cardiac Pacemaker In Situ
 ;;^UTILITY(U,$J,358.3,10991,1,4,0)
 ;;=4^V45.01
 ;;^UTILITY(U,$J,358.3,10991,2)
 ;;=^303419
 ;;^UTILITY(U,$J,358.3,10992,0)
 ;;=519.9^^87^733^18
 ;;^UTILITY(U,$J,358.3,10992,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10992,1,3,0)
 ;;=3^Resp System Disease Nos
 ;;^UTILITY(U,$J,358.3,10992,1,4,0)
 ;;=4^519.9
 ;;^UTILITY(U,$J,358.3,10992,2)
 ;;=^105137
 ;;^UTILITY(U,$J,358.3,10993,0)
 ;;=V17.49^^87^733^11
 ;;^UTILITY(U,$J,358.3,10993,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10993,1,3,0)
 ;;=3^Fam Hx-Cardiovas Dis Nec
 ;;^UTILITY(U,$J,358.3,10993,1,4,0)
 ;;=4^V17.49
 ;;^UTILITY(U,$J,358.3,10993,2)
 ;;=^295306
 ;;^UTILITY(U,$J,358.3,10994,0)
 ;;=V45.82^^87^733^16
 ;;^UTILITY(U,$J,358.3,10994,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10994,1,3,0)
 ;;=3^PTCA Status
 ;;^UTILITY(U,$J,358.3,10994,1,4,0)
 ;;=4^V45.82
 ;;^UTILITY(U,$J,358.3,10994,2)
 ;;=^303425
 ;;^UTILITY(U,$J,358.3,10995,0)
 ;;=996.77^^87^734^1
 ;;^UTILITY(U,$J,358.3,10995,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10995,1,3,0)
 ;;=3^Comp-Internal Joint Pros
 ;;^UTILITY(U,$J,358.3,10995,1,4,0)
 ;;=4^996.77
 ;;^UTILITY(U,$J,358.3,10995,2)
 ;;=^276300
 ;;^UTILITY(U,$J,358.3,10996,0)
 ;;=996.78^^87^734^2
 ;;^UTILITY(U,$J,358.3,10996,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10996,1,3,0)
 ;;=3^Comp-Oth Int Ortho Device
 ;;^UTILITY(U,$J,358.3,10996,1,4,0)
 ;;=4^996.78
 ;;^UTILITY(U,$J,358.3,10996,2)
 ;;=^276301
 ;;^UTILITY(U,$J,358.3,10997,0)
 ;;=905.3^^87^735^1
 ;;^UTILITY(U,$J,358.3,10997,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,10997,1,3,0)
 ;;=3^Late Eff Femoral Neck Fx
 ;;^UTILITY(U,$J,358.3,10997,1,4,0)
 ;;=4^905.3
 ;;^UTILITY(U,$J,358.3,10997,2)
 ;;=^275217
 ;;^UTILITY(U,$J,358.3,10998,0)
 ;;=905.4^^87^735^4
