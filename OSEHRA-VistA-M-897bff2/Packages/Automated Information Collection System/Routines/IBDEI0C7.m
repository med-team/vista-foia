IBDEI0C7 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,16232,1,3,0)
 ;;=3^459.81
 ;;^UTILITY(U,$J,358.3,16232,1,5,0)
 ;;=5^Dermatitis, Stasis (w/o varicose veins) 
 ;;^UTILITY(U,$J,358.3,16232,2)
 ;;=^125826
 ;;^UTILITY(U,$J,358.3,16233,0)
 ;;=454.1^^119^1019^9
 ;;^UTILITY(U,$J,358.3,16233,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16233,1,3,0)
 ;;=3^454.1
 ;;^UTILITY(U,$J,358.3,16233,1,5,0)
 ;;=5^Dermatitis, Status due to varicose veins 
 ;;^UTILITY(U,$J,358.3,16233,2)
 ;;=^125435
 ;;^UTILITY(U,$J,358.3,16234,0)
 ;;=454.2^^119^1019^11
 ;;^UTILITY(U,$J,358.3,16234,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16234,1,3,0)
 ;;=3^454.2
 ;;^UTILITY(U,$J,358.3,16234,1,5,0)
 ;;=5^Dermatitis, Stasis with ulcer/ulcerated
 ;;^UTILITY(U,$J,358.3,16234,2)
 ;;=^269821
 ;;^UTILITY(U,$J,358.3,16235,0)
 ;;=110.4^^119^1019^12
 ;;^UTILITY(U,$J,358.3,16235,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16235,1,3,0)
 ;;=3^110.4
 ;;^UTILITY(U,$J,358.3,16235,1,5,0)
 ;;=5^Dermatophytosis of foot
 ;;^UTILITY(U,$J,358.3,16235,2)
 ;;=^33168
 ;;^UTILITY(U,$J,358.3,16236,0)
 ;;=250.00^^119^1019^13
 ;;^UTILITY(U,$J,358.3,16236,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16236,1,3,0)
 ;;=3^250.00
 ;;^UTILITY(U,$J,358.3,16236,1,5,0)
 ;;=5^DM II w/o complication 
 ;;^UTILITY(U,$J,358.3,16236,2)
 ;;=^33605
 ;;^UTILITY(U,$J,358.3,16237,0)
 ;;=250.01^^119^1019^39
 ;;^UTILITY(U,$J,358.3,16237,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16237,1,3,0)
 ;;=3^250.01
 ;;^UTILITY(U,$J,358.3,16237,1,5,0)
 ;;=5^DM I w/o complication 
 ;;^UTILITY(U,$J,358.3,16237,2)
 ;;=^33586
 ;;^UTILITY(U,$J,358.3,16238,0)
 ;;=838.00^^119^1019^65
 ;;^UTILITY(U,$J,358.3,16238,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16238,1,3,0)
 ;;=3^838.00
 ;;^UTILITY(U,$J,358.3,16238,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; foot, unspecified
 ;;^UTILITY(U,$J,358.3,16238,2)
 ;;=^274391
 ;;^UTILITY(U,$J,358.3,16239,0)
 ;;=838.01^^119^1019^66
 ;;^UTILITY(U,$J,358.3,16239,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16239,1,3,0)
 ;;=3^838.01
 ;;^UTILITY(U,$J,358.3,16239,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; tarsal(bone), joint unspecified 
 ;;^UTILITY(U,$J,358.3,16239,2)
 ;;=^274394
 ;;^UTILITY(U,$J,358.3,16240,0)
 ;;=838.02^^119^1019^67
 ;;^UTILITY(U,$J,358.3,16240,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16240,1,3,0)
 ;;=3^838.02
 ;;^UTILITY(U,$J,358.3,16240,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; midtarsal (joint)
 ;;^UTILITY(U,$J,358.3,16240,2)
 ;;=^274395
 ;;^UTILITY(U,$J,358.3,16241,0)
 ;;=838.03^^119^1019^68
 ;;^UTILITY(U,$J,358.3,16241,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16241,1,3,0)
 ;;=3^838.03
 ;;^UTILITY(U,$J,358.3,16241,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; tarsometatarsal (joint)
 ;;^UTILITY(U,$J,358.3,16241,2)
 ;;=^274396
 ;;^UTILITY(U,$J,358.3,16242,0)
 ;;=838.04^^119^1019^69
 ;;^UTILITY(U,$J,358.3,16242,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16242,1,3,0)
 ;;=3^838.04
 ;;^UTILITY(U,$J,358.3,16242,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; metatarsal(bone), joint unspecified
 ;;^UTILITY(U,$J,358.3,16242,2)
 ;;=^274397
 ;;^UTILITY(U,$J,358.3,16243,0)
 ;;=838.05^^119^1019^70
 ;;^UTILITY(U,$J,358.3,16243,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16243,1,3,0)
 ;;=3^838.05
 ;;^UTILITY(U,$J,358.3,16243,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; metatarsophalangeal(joint)
 ;;^UTILITY(U,$J,358.3,16243,2)
 ;;=^274398
 ;;^UTILITY(U,$J,358.3,16244,0)
 ;;=838.06^^119^1019^71
 ;;^UTILITY(U,$J,358.3,16244,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16244,1,3,0)
 ;;=3^838.06
 ;;^UTILITY(U,$J,358.3,16244,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; interphalangeal(joint) foot
 ;;^UTILITY(U,$J,358.3,16244,2)
 ;;=^274399
 ;;^UTILITY(U,$J,358.3,16245,0)
 ;;=838.09^^119^1019^72
 ;;^UTILITY(U,$J,358.3,16245,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16245,1,3,0)
 ;;=3^838.09
 ;;^UTILITY(U,$J,358.3,16245,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; other, toe(s)
 ;;^UTILITY(U,$J,358.3,16245,2)
 ;;=^274400
 ;;^UTILITY(U,$J,358.3,16246,0)
 ;;=12.5^1^119^1019^12.5^-DIABETES MELLITUS^1^1
 ;;^UTILITY(U,$J,358.3,16246,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16246,1,3,0)
 ;;=3
 ;;^UTILITY(U,$J,358.3,16246,1,5,0)
 ;;=5
 ;;^UTILITY(U,$J,358.3,16247,0)
 ;;=64.5^1^119^1019^64.5^-Dislocation^1^1
 ;;^UTILITY(U,$J,358.3,16247,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16247,1,3,0)
 ;;=3
 ;;^UTILITY(U,$J,358.3,16247,1,5,0)
 ;;=5
 ;;^UTILITY(U,$J,358.3,16248,0)
 ;;=459.10^^119^1019^10
 ;;^UTILITY(U,$J,358.3,16248,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16248,1,3,0)
 ;;=3^459.10
 ;;^UTILITY(U,$J,358.3,16248,1,5,0)
 ;;=5^Post Phlebotic Syndrome
 ;;^UTILITY(U,$J,358.3,16248,2)
 ;;=Post Phlebotic Syndrome^328597
 ;;^UTILITY(U,$J,358.3,16249,0)
 ;;=719.7^^119^1019^64
 ;;^UTILITY(U,$J,358.3,16249,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16249,1,3,0)
 ;;=3^719.7
 ;;^UTILITY(U,$J,358.3,16249,1,5,0)
 ;;=5^Difficulty In Walking
 ;;^UTILITY(U,$J,358.3,16249,2)
 ;;=^329945
 ;;^UTILITY(U,$J,358.3,16250,0)
 ;;=453.40^^119^1019^1
 ;;^UTILITY(U,$J,358.3,16250,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16250,1,3,0)
 ;;=3^453.40
 ;;^UTILITY(U,$J,358.3,16250,1,5,0)
 ;;=5^Deep vein thrombosis lower extremity
 ;;^UTILITY(U,$J,358.3,16250,2)
 ;;=^338554
 ;;^UTILITY(U,$J,358.3,16251,0)
 ;;=692.9^^119^1020^1
 ;;^UTILITY(U,$J,358.3,16251,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16251,1,3,0)
 ;;=3^692.9
 ;;^UTILITY(U,$J,358.3,16251,1,5,0)
 ;;=5^Eczema
 ;;^UTILITY(U,$J,358.3,16251,2)
 ;;=^27800
 ;;^UTILITY(U,$J,358.3,16252,0)
 ;;=691.8^^119^1020^2
 ;;^UTILITY(U,$J,358.3,16252,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16252,1,3,0)
 ;;=3^691.8
 ;;^UTILITY(U,$J,358.3,16252,1,5,0)
 ;;=5^Eczema, allergic
 ;;^UTILITY(U,$J,358.3,16252,2)
 ;;=^87342
 ;;^UTILITY(U,$J,358.3,16253,0)
 ;;=782.3^^119^1020^3
 ;;^UTILITY(U,$J,358.3,16253,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16253,1,3,0)
 ;;=3^782.3
 ;;^UTILITY(U,$J,358.3,16253,1,5,0)
 ;;=5^Edema (any site)
 ;;^UTILITY(U,$J,358.3,16253,2)
 ;;=^38340
 ;;^UTILITY(U,$J,358.3,16254,0)
 ;;=726.70^^119^1020^4
 ;;^UTILITY(U,$J,358.3,16254,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16254,1,3,0)
 ;;=3^726.70
 ;;^UTILITY(U,$J,358.3,16254,1,5,0)
 ;;=5^Enthesopathy of ankle & tarsus, unspecified
 ;;^UTILITY(U,$J,358.3,16254,2)
 ;;=^272548
 ;;^UTILITY(U,$J,358.3,16255,0)
 ;;=736.72^^119^1020^5
 ;;^UTILITY(U,$J,358.3,16255,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16255,1,3,0)
 ;;=3^736.72
 ;;^UTILITY(U,$J,358.3,16255,1,5,0)
 ;;=5^Equinus deformity of foot, acquired
 ;;^UTILITY(U,$J,358.3,16255,2)
 ;;=^272744
 ;;^UTILITY(U,$J,358.3,16256,0)
 ;;=726.91^^119^1020^6
 ;;^UTILITY(U,$J,358.3,16256,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16256,1,3,0)
 ;;=3^726.91
 ;;^UTILITY(U,$J,358.3,16256,1,5,0)
 ;;=5^Exostosis of unspecified site
 ;;^UTILITY(U,$J,358.3,16256,2)
 ;;=^43688
 ;;^UTILITY(U,$J,358.3,16257,0)
 ;;=728.71^^119^1021^1
 ;;^UTILITY(U,$J,358.3,16257,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16257,1,3,0)
 ;;=3^728.71
 ;;^UTILITY(U,$J,358.3,16257,1,5,0)
 ;;=5^Fascite, Plantar
 ;;^UTILITY(U,$J,358.3,16257,2)
 ;;=^272598
 ;;^UTILITY(U,$J,358.3,16258,0)
 ;;=729.1^^119^1021^2
 ;;^UTILITY(U,$J,358.3,16258,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16258,1,3,0)
 ;;=3^729.1
 ;;^UTILITY(U,$J,358.3,16258,1,5,0)
 ;;=5^Fibromyalgia
 ;;^UTILITY(U,$J,358.3,16258,2)
 ;;=^80160
 ;;^UTILITY(U,$J,358.3,16259,0)
 ;;=709.8^^119^1021^3
 ;;^UTILITY(U,$J,358.3,16259,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16259,1,3,0)
 ;;=3^709.8
 ;;^UTILITY(U,$J,358.3,16259,1,5,0)
 ;;=5^Fissured skin
 ;;^UTILITY(U,$J,358.3,16259,2)
 ;;=^88026
 ;;^UTILITY(U,$J,358.3,16260,0)
 ;;=V53.7^^119^1021^4
 ;;^UTILITY(U,$J,358.3,16260,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16260,1,3,0)
 ;;=3^V53.7
 ;;^UTILITY(U,$J,358.3,16260,1,5,0)
 ;;=5^Fitting of Orthotic
 ;;^UTILITY(U,$J,358.3,16260,2)
 ;;=^295510
 ;;^UTILITY(U,$J,358.3,16261,0)
 ;;=709.4^^119^1021^5
 ;;^UTILITY(U,$J,358.3,16261,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16261,1,3,0)
 ;;=3^709.4
 ;;^UTILITY(U,$J,358.3,16261,1,5,0)
 ;;=5^Foreign body granuloma of skin and subcutaneous tissue (old) 
 ;;^UTILITY(U,$J,358.3,16261,2)
 ;;=^271943
 ;;^UTILITY(U,$J,358.3,16262,0)
 ;;=917.6^^119^1021^6
 ;;^UTILITY(U,$J,358.3,16262,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16262,1,3,0)
 ;;=3^917.6
 ;;^UTILITY(U,$J,358.3,16262,1,5,0)
 ;;=5^Foreign body, superficial injury (splinter) of foot & toe(s)without major open wound & without mention of infection  
 ;;^UTILITY(U,$J,358.3,16262,2)
 ;;=^275347
 ;;^UTILITY(U,$J,358.3,16263,0)
 ;;=892.1^^119^1021^7
 ;;^UTILITY(U,$J,358.3,16263,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16263,1,3,0)
 ;;=3^892.1
 ;;^UTILITY(U,$J,358.3,16263,1,5,0)
 ;;=5^Foreign body, open wound of foot except toe(s); complicated
 ;;^UTILITY(U,$J,358.3,16263,2)
 ;;=^275092
 ;;^UTILITY(U,$J,358.3,16264,0)
 ;;=893.1^^119^1021^8
 ;;^UTILITY(U,$J,358.3,16264,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16264,1,3,0)
 ;;=3^893.1
 ;;^UTILITY(U,$J,358.3,16264,1,5,0)
 ;;=5^Foreign body, open wound of toe; complicated
 ;;^UTILITY(U,$J,358.3,16264,2)
 ;;=^275096
 ;;^UTILITY(U,$J,358.3,16265,0)
 ;;=916.6^^119^1021^9
 ;;^UTILITY(U,$J,358.3,16265,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16265,1,3,0)
 ;;=3^916.6
 ;;^UTILITY(U,$J,358.3,16265,1,5,0)
 ;;=5^Foreign body, superficial injury (splinter) of hip, thigh, leg, and ankle w/o major open wound & w/o mention of infection
 ;;^UTILITY(U,$J,358.3,16265,2)
 ;;=^275336
 ;;^UTILITY(U,$J,358.3,16266,0)
 ;;=891.1^^119^1021^10
 ;;^UTILITY(U,$J,358.3,16266,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16266,1,3,0)
 ;;=3^891.1
 ;;^UTILITY(U,$J,358.3,16266,1,5,0)
 ;;=5^Foreign body, open wound of knee, leg(except thigh), & ankle; complicated
 ;;^UTILITY(U,$J,358.3,16266,2)
 ;;=^275088
