IBDEI011 ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,723,1,3,0)
 ;;=3^Group Audiometric Testing
 ;;^UTILITY(U,$J,358.3,724,0)
 ;;=92585^^11^52^3^^^^1
 ;;^UTILITY(U,$J,358.3,724,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,724,1,2,0)
 ;;=2^92585
 ;;^UTILITY(U,$J,358.3,724,1,3,0)
 ;;=3^Auditor Evoke Potent, Compre
 ;;^UTILITY(U,$J,358.3,725,0)
 ;;=92586^^11^52^4^^^^1
 ;;^UTILITY(U,$J,358.3,725,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,725,1,2,0)
 ;;=2^92586
 ;;^UTILITY(U,$J,358.3,725,1,3,0)
 ;;=3^Auditor Evoke Potent, Limit
 ;;^UTILITY(U,$J,358.3,726,0)
 ;;=92620^^11^52^6^^^^1
 ;;^UTILITY(U,$J,358.3,726,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,726,1,2,0)
 ;;=2^92620
 ;;^UTILITY(U,$J,358.3,726,1,3,0)
 ;;=3^Auditory Function, 60 Min
 ;;^UTILITY(U,$J,358.3,727,0)
 ;;=92621^^11^52^5^^^^1
 ;;^UTILITY(U,$J,358.3,727,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,727,1,2,0)
 ;;=2^92621
 ;;^UTILITY(U,$J,358.3,727,1,3,0)
 ;;=3^Auditory Function, + 15 Min
 ;;^UTILITY(U,$J,358.3,728,0)
 ;;=92557^^11^52^7^^^^1
 ;;^UTILITY(U,$J,358.3,728,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,728,1,2,0)
 ;;=2^92557
 ;;^UTILITY(U,$J,358.3,728,1,3,0)
 ;;=3^Comprehensive Audiometric Exam
 ;;^UTILITY(U,$J,358.3,729,0)
 ;;=92582^^11^52^8^^^^1
 ;;^UTILITY(U,$J,358.3,729,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,729,1,2,0)
 ;;=2^92582
 ;;^UTILITY(U,$J,358.3,729,1,3,0)
 ;;=3^Conditioning Play Audiometry
 ;;^UTILITY(U,$J,358.3,730,0)
 ;;=92571^^11^52^9^^^^1
 ;;^UTILITY(U,$J,358.3,730,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,730,1,2,0)
 ;;=2^92571
 ;;^UTILITY(U,$J,358.3,730,1,3,0)
 ;;=3^Disorted Speech Test
 ;;^UTILITY(U,$J,358.3,731,0)
 ;;=92596^^11^52^10^^^^1
 ;;^UTILITY(U,$J,358.3,731,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,731,1,2,0)
 ;;=2^92596
 ;;^UTILITY(U,$J,358.3,731,1,3,0)
 ;;=3^Ear Protector Attenuation
 ;;^UTILITY(U,$J,358.3,732,0)
 ;;=92584^^11^52^11^^^^1
 ;;^UTILITY(U,$J,358.3,732,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,732,1,2,0)
 ;;=2^92584
 ;;^UTILITY(U,$J,358.3,732,1,3,0)
 ;;=3^Electrocochleography
 ;;^UTILITY(U,$J,358.3,733,0)
 ;;=92562^^11^52^20^^^^1
 ;;^UTILITY(U,$J,358.3,733,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,733,1,2,0)
 ;;=2^92562
 ;;^UTILITY(U,$J,358.3,733,1,3,0)
 ;;=3^Loudness Balance Test
 ;;^UTILITY(U,$J,358.3,734,0)
 ;;=92588^^11^52^22^^^^1
 ;;^UTILITY(U,$J,358.3,734,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,734,1,2,0)
 ;;=2^92588
 ;;^UTILITY(U,$J,358.3,734,1,3,0)
 ;;=3^Otoacoustic Emissions,Diagnostic
 ;;^UTILITY(U,$J,358.3,735,0)
 ;;=92587^^11^52^23^^^^1
 ;;^UTILITY(U,$J,358.3,735,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,735,1,2,0)
 ;;=2^92587
 ;;^UTILITY(U,$J,358.3,735,1,3,0)
 ;;=3^Otoacoustic Emissions,Limited
 ;;^UTILITY(U,$J,358.3,736,0)
 ;;=92552^^11^52^24^^^^1
 ;;^UTILITY(U,$J,358.3,736,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,736,1,2,0)
 ;;=2^92552
 ;;^UTILITY(U,$J,358.3,736,1,3,0)
 ;;=3^Pure Tone Audiometry, Air
 ;;^UTILITY(U,$J,358.3,737,0)
 ;;=92553^^11^52^25^^^^1
 ;;^UTILITY(U,$J,358.3,737,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,737,1,2,0)
 ;;=2^92553
 ;;^UTILITY(U,$J,358.3,737,1,3,0)
 ;;=3^Pure Tone Audiometry, Air & Bone
 ;;^UTILITY(U,$J,358.3,738,0)
 ;;=92570^^11^52^1^^^^1
 ;;^UTILITY(U,$J,358.3,738,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,738,1,2,0)
 ;;=2^92570
 ;;^UTILITY(U,$J,358.3,738,1,3,0)
 ;;=3^Acoustic Immittance Testing
 ;;^UTILITY(U,$J,358.3,739,0)
 ;;=92558^^11^52^14^^^^1
 ;;^UTILITY(U,$J,358.3,739,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,739,1,2,0)
 ;;=2^92558
 ;;^UTILITY(U,$J,358.3,739,1,3,0)
 ;;=3^Evoked Otoacoustic Emmissions,Scrn,Auto
 ;;^UTILITY(U,$J,358.3,740,0)
 ;;=92611^^11^52^21^^^^1
 ;;^UTILITY(U,$J,358.3,740,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,740,1,2,0)
 ;;=2^92611
 ;;^UTILITY(U,$J,358.3,740,1,3,0)
 ;;=3^Motion Fluoroscopic Eval Swallowing
 ;;^UTILITY(U,$J,358.3,741,0)
 ;;=92612^^11^52^16^^^^1
 ;;^UTILITY(U,$J,358.3,741,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,741,1,2,0)
 ;;=2^92612
 ;;^UTILITY(U,$J,358.3,741,1,3,0)
 ;;=3^Flexible Fiberoptic Eval Swallowing
 ;;^UTILITY(U,$J,358.3,742,0)
 ;;=92626^^11^52^12^^^^1
 ;;^UTILITY(U,$J,358.3,742,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,742,1,2,0)
 ;;=2^92626
 ;;^UTILITY(U,$J,358.3,742,1,3,0)
 ;;=3^Eval Aud Rehab Status
 ;;^UTILITY(U,$J,358.3,743,0)
 ;;=92627^^11^52^13^^^^1
 ;;^UTILITY(U,$J,358.3,743,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,743,1,2,0)
 ;;=2^92627
 ;;^UTILITY(U,$J,358.3,743,1,3,0)
 ;;=3^Eval Aud Status Rehab,ea addl
 ;;^UTILITY(U,$J,358.3,744,0)
 ;;=92613^^11^52^15^^^^1
 ;;^UTILITY(U,$J,358.3,744,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,744,1,2,0)
 ;;=2^92613
 ;;^UTILITY(U,$J,358.3,744,1,3,0)
 ;;=3^Flex Fib Eval Swallow,Interp/Rpt Only
 ;;^UTILITY(U,$J,358.3,745,0)
 ;;=92614^^11^52^18^^^^1
 ;;^UTILITY(U,$J,358.3,745,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,745,1,2,0)
 ;;=2^92614
 ;;^UTILITY(U,$J,358.3,745,1,3,0)
 ;;=3^Laryngoscopic Sensory Test,Video
 ;;^UTILITY(U,$J,358.3,746,0)
 ;;=92615^^11^52^19^^^^1
 ;;^UTILITY(U,$J,358.3,746,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,746,1,2,0)
 ;;=2^92615
 ;;^UTILITY(U,$J,358.3,746,1,3,0)
 ;;=3^Laryngoscopic Sensory Tst,Interp&Rpt Only
 ;;^UTILITY(U,$J,358.3,747,0)
 ;;=92551^^11^53^3^^^^1
 ;;^UTILITY(U,$J,358.3,747,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,747,1,2,0)
 ;;=2^92551
 ;;^UTILITY(U,$J,358.3,747,1,3,0)
 ;;=3^Pure Tone Hearing Test, Air
 ;;^UTILITY(U,$J,358.3,748,0)
 ;;=V5008^^11^53^2^^^^1
 ;;^UTILITY(U,$J,358.3,748,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,748,1,2,0)
 ;;=2^V5008
 ;;^UTILITY(U,$J,358.3,748,1,3,0)
 ;;=3^Hearing Screening
 ;;^UTILITY(U,$J,358.3,749,0)
 ;;=92550^^11^53^4^^^^1
 ;;^UTILITY(U,$J,358.3,749,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,749,1,2,0)
 ;;=2^92550
 ;;^UTILITY(U,$J,358.3,749,1,3,0)
 ;;=3^Tympanometry & Reflex Threshold
 ;;^UTILITY(U,$J,358.3,750,0)
 ;;=V5010^^11^53^1^^^^1
 ;;^UTILITY(U,$J,358.3,750,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,750,1,2,0)
 ;;=2^V5010
 ;;^UTILITY(U,$J,358.3,750,1,3,0)
 ;;=3^Assessment for Hearing Aid
 ;;^UTILITY(U,$J,358.3,751,0)
 ;;=92700^^11^54^3^^^^1
 ;;^UTILITY(U,$J,358.3,751,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,751,1,2,0)
 ;;=2^92700
 ;;^UTILITY(U,$J,358.3,751,1,3,0)
 ;;=3^Video-Otoscopy, Diagnostic
 ;;^UTILITY(U,$J,358.3,752,0)
 ;;=92601^^11^55^2^^^^1
 ;;^UTILITY(U,$J,358.3,752,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,752,1,2,0)
 ;;=2^92601
 ;;^UTILITY(U,$J,358.3,752,1,3,0)
 ;;=3^Diagnostic Analysis Of Cochlear Implant<7Y
 ;;^UTILITY(U,$J,358.3,753,0)
 ;;=92602^^11^55^3^^^^1
 ;;^UTILITY(U,$J,358.3,753,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,753,1,2,0)
 ;;=2^92602
 ;;^UTILITY(U,$J,358.3,753,1,3,0)
 ;;=3^Reprogram Cochlear Implt < 7
 ;;^UTILITY(U,$J,358.3,754,0)
 ;;=92603^^11^55^4^^^^1
 ;;^UTILITY(U,$J,358.3,754,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,754,1,2,0)
 ;;=2^92603
 ;;^UTILITY(U,$J,358.3,754,1,3,0)
 ;;=3^Diagnostic Analysis Of Cochlear Implant 7+Y
 ;;^UTILITY(U,$J,358.3,755,0)
 ;;=92604^^11^55^5^^^^1
 ;;^UTILITY(U,$J,358.3,755,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,755,1,2,0)
 ;;=2^92604
 ;;^UTILITY(U,$J,358.3,755,1,3,0)
 ;;=3^Subsequent Re-Programming 7+Y
 ;;^UTILITY(U,$J,358.3,756,0)
 ;;=92700^^11^56^1^^^^1
 ;;^UTILITY(U,$J,358.3,756,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,756,1,2,0)
 ;;=2^92700
 ;;^UTILITY(U,$J,358.3,756,1,3,0)
 ;;=3^Support Group
 ;;^UTILITY(U,$J,358.3,757,0)
 ;;=97112^^11^56^2^^^^1
 ;;^UTILITY(U,$J,358.3,757,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,757,1,2,0)
 ;;=2^97112
 ;;^UTILITY(U,$J,358.3,757,1,3,0)
 ;;=3^Vestibuar Rehab, Each 15 Min
 ;;^UTILITY(U,$J,358.3,758,0)
 ;;=92508^^11^56^3^^^^1
 ;;^UTILITY(U,$J,358.3,758,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,758,1,2,0)
 ;;=2^92508
 ;;^UTILITY(U,$J,358.3,758,1,3,0)
 ;;=3^Group Treatment
 ;;^UTILITY(U,$J,358.3,759,0)
 ;;=95992^^11^56^5^^^^1
 ;;^UTILITY(U,$J,358.3,759,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,759,1,2,0)
 ;;=2^95992
 ;;^UTILITY(U,$J,358.3,759,1,3,0)
 ;;=3^Canalith Repositioning
 ;;^UTILITY(U,$J,358.3,760,0)
 ;;=V5275^^11^57^3^^^^1
 ;;^UTILITY(U,$J,358.3,760,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,760,1,2,0)
 ;;=2^V5275
 ;;^UTILITY(U,$J,358.3,760,1,3,0)
 ;;=3^Ear Impression, Each
 ;;^UTILITY(U,$J,358.3,761,0)
 ;;=92590^^11^57^6^^^^1
 ;;^UTILITY(U,$J,358.3,761,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,761,1,2,0)
 ;;=2^92590
 ;;^UTILITY(U,$J,358.3,761,1,3,0)
 ;;=3^Ha Assessment, Monaural
 ;;^UTILITY(U,$J,358.3,762,0)
 ;;=92591^^11^57^5^^^^1
 ;;^UTILITY(U,$J,358.3,762,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,762,1,2,0)
 ;;=2^92591
 ;;^UTILITY(U,$J,358.3,762,1,3,0)
 ;;=3^Ha Assessment, Binaural
 ;;^UTILITY(U,$J,358.3,763,0)
 ;;=92594^^11^57^10^^^^1
 ;;^UTILITY(U,$J,358.3,763,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,763,1,2,0)
 ;;=2^92594
 ;;^UTILITY(U,$J,358.3,763,1,3,0)
 ;;=3^Ha Programming, Monaural
 ;;^UTILITY(U,$J,358.3,764,0)
 ;;=92595^^11^57^9^^^^1
 ;;^UTILITY(U,$J,358.3,764,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,764,1,2,0)
 ;;=2^92595
 ;;^UTILITY(U,$J,358.3,764,1,3,0)
 ;;=3^Ha Programming Test, Binaural
 ;;^UTILITY(U,$J,358.3,765,0)
 ;;=92592^^11^57^8^^^^1
 ;;^UTILITY(U,$J,358.3,765,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,765,1,2,0)
 ;;=2^92592
 ;;^UTILITY(U,$J,358.3,765,1,3,0)
 ;;=3^Ha Check, Monaural
 ;;^UTILITY(U,$J,358.3,766,0)
 ;;=92593^^11^57^7^^^^1
 ;;^UTILITY(U,$J,358.3,766,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,766,1,2,0)
 ;;=2^92593
 ;;^UTILITY(U,$J,358.3,766,1,3,0)
 ;;=3^Ha Check, Binaural
 ;;^UTILITY(U,$J,358.3,767,0)
 ;;=V5014^^11^57^11^^^^1
 ;;^UTILITY(U,$J,358.3,767,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,767,1,2,0)
 ;;=2^V5014
 ;;^UTILITY(U,$J,358.3,767,1,3,0)
 ;;=3^Ha Repair/Modification
 ;;^UTILITY(U,$J,358.3,768,0)
 ;;=V5020^^11^57^12^^^^1
 ;;^UTILITY(U,$J,358.3,768,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,768,1,2,0)
 ;;=2^V5020
