IBDEI04I ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,5669,2)
 ;;=^123813
 ;;^UTILITY(U,$J,358.3,5670,0)
 ;;=443.9^^55^466^22
 ;;^UTILITY(U,$J,358.3,5670,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5670,1,3,0)
 ;;=3^443.9
 ;;^UTILITY(U,$J,358.3,5670,1,4,0)
 ;;=4^PERIPH VASCULAR DIS NOS
 ;;^UTILITY(U,$J,358.3,5670,2)
 ;;=^184182
 ;;^UTILITY(U,$J,358.3,5671,0)
 ;;=272.0^^55^466^23
 ;;^UTILITY(U,$J,358.3,5671,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5671,1,3,0)
 ;;=3^272.0
 ;;^UTILITY(U,$J,358.3,5671,1,4,0)
 ;;=4^PURE HYPERCHOLESTEROLEM
 ;;^UTILITY(U,$J,358.3,5671,2)
 ;;=^59973
 ;;^UTILITY(U,$J,358.3,5672,0)
 ;;=272.1^^55^466^24
 ;;^UTILITY(U,$J,358.3,5672,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5672,1,3,0)
 ;;=3^272.1
 ;;^UTILITY(U,$J,358.3,5672,1,4,0)
 ;;=4^PURE HYPERGLYCERIDEMIA
 ;;^UTILITY(U,$J,358.3,5672,2)
 ;;=^101303
 ;;^UTILITY(U,$J,358.3,5673,0)
 ;;=588.0^^55^466^25
 ;;^UTILITY(U,$J,358.3,5673,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5673,1,3,0)
 ;;=3^588.0
 ;;^UTILITY(U,$J,358.3,5673,1,4,0)
 ;;=4^RENAL OSTEODYSTROPHY
 ;;^UTILITY(U,$J,358.3,5673,2)
 ;;=^104747
 ;;^UTILITY(U,$J,358.3,5674,0)
 ;;=589.0^^55^466^26
 ;;^UTILITY(U,$J,358.3,5674,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5674,1,3,0)
 ;;=3^589.0
 ;;^UTILITY(U,$J,358.3,5674,1,4,0)
 ;;=4^UNILATERAL SMALL KIDNEY
 ;;^UTILITY(U,$J,358.3,5674,2)
 ;;=^270363
 ;;^UTILITY(U,$J,358.3,5675,0)
 ;;=593.2^^55^467^1
 ;;^UTILITY(U,$J,358.3,5675,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5675,1,3,0)
 ;;=3^593.2
 ;;^UTILITY(U,$J,358.3,5675,1,4,0)
 ;;=4^CYST OF KIDNEY, ACQUIRED
 ;;^UTILITY(U,$J,358.3,5675,2)
 ;;=^270380
 ;;^UTILITY(U,$J,358.3,5676,0)
 ;;=753.10^^55^467^2
 ;;^UTILITY(U,$J,358.3,5676,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5676,1,3,0)
 ;;=3^753.10
 ;;^UTILITY(U,$J,358.3,5676,1,4,0)
 ;;=4^CYSTIC KIDNEY DISEASE, UNSPEC
 ;;^UTILITY(U,$J,358.3,5676,2)
 ;;=^104720
 ;;^UTILITY(U,$J,358.3,5677,0)
 ;;=753.17^^55^467^3
 ;;^UTILITY(U,$J,358.3,5677,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5677,1,3,0)
 ;;=3^753.17
 ;;^UTILITY(U,$J,358.3,5677,1,4,0)
 ;;=4^MEDULLARY SPONGE KIDNEY
 ;;^UTILITY(U,$J,358.3,5677,2)
 ;;=^67302
 ;;^UTILITY(U,$J,358.3,5678,0)
 ;;=753.13^^55^467^4
 ;;^UTILITY(U,$J,358.3,5678,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5678,1,3,0)
 ;;=3^753.13
 ;;^UTILITY(U,$J,358.3,5678,1,4,0)
 ;;=4^POLYCYSTIC KIDNEY, AUTOSOM DOM
 ;;^UTILITY(U,$J,358.3,5678,2)
 ;;=^186049
 ;;^UTILITY(U,$J,358.3,5679,0)
 ;;=250.40^^55^468^10
 ;;^UTILITY(U,$J,358.3,5679,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5679,1,3,0)
 ;;=3^250.40
 ;;^UTILITY(U,$J,358.3,5679,1,4,0)
 ;;=4^DM II w/ RENAL UNSPEC
 ;;^UTILITY(U,$J,358.3,5679,2)
 ;;=^331795^583.81
 ;;^UTILITY(U,$J,358.3,5680,0)
 ;;=250.41^^55^468^4
 ;;^UTILITY(U,$J,358.3,5680,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5680,1,3,0)
 ;;=3^250.41
 ;;^UTILITY(U,$J,358.3,5680,1,4,0)
 ;;=4^DM I w/ RENAL UNSPEC
 ;;^UTILITY(U,$J,358.3,5680,2)
 ;;=^331796^583.81
 ;;^UTILITY(U,$J,358.3,5681,0)
 ;;=250.42^^55^468^9
 ;;^UTILITY(U,$J,358.3,5681,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5681,1,3,0)
 ;;=3^250.42
 ;;^UTILITY(U,$J,358.3,5681,1,4,0)
 ;;=4^DM II w/ RENAL UNCTRLD
 ;;^UTILITY(U,$J,358.3,5681,2)
 ;;=^331797^583.81
 ;;^UTILITY(U,$J,358.3,5682,0)
 ;;=250.43^^55^468^3
 ;;^UTILITY(U,$J,358.3,5682,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5682,1,3,0)
 ;;=3^250.43
 ;;^UTILITY(U,$J,358.3,5682,1,4,0)
 ;;=4^DM I w/ RENAL UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5682,2)
 ;;=^331798^583.81
 ;;^UTILITY(U,$J,358.3,5683,0)
 ;;=250.60^^55^468^8
 ;;^UTILITY(U,$J,358.3,5683,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5683,1,3,0)
 ;;=3^250.60
 ;;^UTILITY(U,$J,358.3,5683,1,4,0)
 ;;=4^DM II w/ NEURO UNSPEC
 ;;^UTILITY(U,$J,358.3,5683,2)
 ;;=^331803^357.2
 ;;^UTILITY(U,$J,358.3,5684,0)
 ;;=250.61^^55^468^2
 ;;^UTILITY(U,$J,358.3,5684,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5684,1,3,0)
 ;;=3^250.61
 ;;^UTILITY(U,$J,358.3,5684,1,4,0)
 ;;=4^DM I w/ NEURO UNSPEC
 ;;^UTILITY(U,$J,358.3,5684,2)
 ;;=^331804^357.2
 ;;^UTILITY(U,$J,358.3,5685,0)
 ;;=250.62^^55^468^7
 ;;^UTILITY(U,$J,358.3,5685,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5685,1,3,0)
 ;;=3^250.62
 ;;^UTILITY(U,$J,358.3,5685,1,4,0)
 ;;=4^DM II w/ NEURO UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5685,2)
 ;;=^331805^357.2
 ;;^UTILITY(U,$J,358.3,5686,0)
 ;;=250.63^^55^468^1
 ;;^UTILITY(U,$J,358.3,5686,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5686,1,3,0)
 ;;=3^250.63
 ;;^UTILITY(U,$J,358.3,5686,1,4,0)
 ;;=4^DM I w/ NEURO UNCTRLD
 ;;^UTILITY(U,$J,358.3,5686,2)
 ;;=^331806^357.2
 ;;^UTILITY(U,$J,358.3,5687,0)
 ;;=250.00^^55^468^12
 ;;^UTILITY(U,$J,358.3,5687,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5687,1,3,0)
 ;;=3^250.00
 ;;^UTILITY(U,$J,358.3,5687,1,4,0)
 ;;=4^DM II w/o COMP UNSPEC
 ;;^UTILITY(U,$J,358.3,5687,2)
 ;;=^331779
 ;;^UTILITY(U,$J,358.3,5688,0)
 ;;=250.01^^55^468^6
 ;;^UTILITY(U,$J,358.3,5688,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5688,1,3,0)
 ;;=3^250.01
 ;;^UTILITY(U,$J,358.3,5688,1,4,0)
 ;;=4^DM I w/o COMP UNSPEC
 ;;^UTILITY(U,$J,358.3,5688,2)
 ;;=^331780
 ;;^UTILITY(U,$J,358.3,5689,0)
 ;;=250.02^^55^468^11
 ;;^UTILITY(U,$J,358.3,5689,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5689,1,3,0)
 ;;=3^250.02
 ;;^UTILITY(U,$J,358.3,5689,1,4,0)
 ;;=4^DM II w/o COMP UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5689,2)
 ;;=^331781
 ;;^UTILITY(U,$J,358.3,5690,0)
 ;;=250.03^^55^468^5
 ;;^UTILITY(U,$J,358.3,5690,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5690,1,3,0)
 ;;=3^250.03
 ;;^UTILITY(U,$J,358.3,5690,1,4,0)
 ;;=4^DM I w/o COMP UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5690,2)
 ;;=^331782
 ;;^UTILITY(U,$J,358.3,5691,0)
 ;;=583.81^^55^468^13
 ;;^UTILITY(U,$J,358.3,5691,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5691,1,3,0)
 ;;=3^583.81
 ;;^UTILITY(U,$J,358.3,5691,1,4,0)
 ;;=4^NEPHRITIS/NEPHROPATHY IN OTH DIS
 ;;^UTILITY(U,$J,358.3,5691,2)
 ;;=^82243
 ;;^UTILITY(U,$J,358.3,5692,0)
 ;;=357.2^^55^468^15
 ;;^UTILITY(U,$J,358.3,5692,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5692,1,3,0)
 ;;=3^357.2
 ;;^UTILITY(U,$J,358.3,5692,1,4,0)
 ;;=4^POLYNEUROPATHY IN DM
 ;;^UTILITY(U,$J,358.3,5692,2)
 ;;=^96321
 ;;^UTILITY(U,$J,358.3,5693,0)
 ;;=581.81^^55^468^14
 ;;^UTILITY(U,$J,358.3,5693,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5693,1,3,0)
 ;;=3^581.81
 ;;^UTILITY(U,$J,358.3,5693,1,4,0)
 ;;=4^NEPHROTIC SYN IN OTH DIS
 ;;^UTILITY(U,$J,358.3,5693,2)
 ;;=^270346
 ;;^UTILITY(U,$J,358.3,5694,0)
 ;;=276.2^^55^469^1
 ;;^UTILITY(U,$J,358.3,5694,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5694,1,3,0)
 ;;=3^276.2
 ;;^UTILITY(U,$J,358.3,5694,1,4,0)
 ;;=4^ACIDOSIS
 ;;^UTILITY(U,$J,358.3,5694,2)
 ;;=^2070
 ;;^UTILITY(U,$J,358.3,5695,0)
 ;;=276.3^^55^469^2
 ;;^UTILITY(U,$J,358.3,5695,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5695,1,3,0)
 ;;=3^276.3
 ;;^UTILITY(U,$J,358.3,5695,1,4,0)
 ;;=4^ALKALOSIS
 ;;^UTILITY(U,$J,358.3,5695,2)
 ;;=^4889
 ;;^UTILITY(U,$J,358.3,5696,0)
 ;;=276.51^^55^469^3
 ;;^UTILITY(U,$J,358.3,5696,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5696,1,3,0)
 ;;=3^276.51
 ;;^UTILITY(U,$J,358.3,5696,1,4,0)
 ;;=4^DEHYDRATION
 ;;^UTILITY(U,$J,358.3,5696,2)
 ;;=^332743
 ;;^UTILITY(U,$J,358.3,5697,0)
 ;;=253.5^^55^469^4
 ;;^UTILITY(U,$J,358.3,5697,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5697,1,3,0)
 ;;=3^253.5
 ;;^UTILITY(U,$J,358.3,5697,1,4,0)
 ;;=4^DIABETES INSIPIDUS
 ;;^UTILITY(U,$J,358.3,5697,2)
 ;;=^33572
 ;;^UTILITY(U,$J,358.3,5698,0)
 ;;=276.9^^55^469^5
 ;;^UTILITY(U,$J,358.3,5698,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5698,1,3,0)
 ;;=3^276.9
 ;;^UTILITY(U,$J,358.3,5698,1,4,0)
 ;;=4^ELECTROLYTE DISORDER NOS
 ;;^UTILITY(U,$J,358.3,5698,2)
 ;;=^267949
 ;;^UTILITY(U,$J,358.3,5699,0)
 ;;=276.69^^55^469^6
 ;;^UTILITY(U,$J,358.3,5699,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5699,1,3,0)
 ;;=3^276.69
 ;;^UTILITY(U,$J,358.3,5699,1,4,0)
 ;;=4^FLUID OVERLOAD NEC
 ;;^UTILITY(U,$J,358.3,5699,2)
 ;;=^339607
 ;;^UTILITY(U,$J,358.3,5700,0)
 ;;=275.42^^55^469^7
 ;;^UTILITY(U,$J,358.3,5700,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5700,1,3,0)
 ;;=3^275.42
 ;;^UTILITY(U,$J,358.3,5700,1,4,0)
 ;;=4^HYPERCALCEMIA
 ;;^UTILITY(U,$J,358.3,5700,2)
 ;;=^59932
 ;;^UTILITY(U,$J,358.3,5701,0)
 ;;=276.0^^55^469^8
 ;;^UTILITY(U,$J,358.3,5701,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5701,1,3,0)
 ;;=3^276.0
 ;;^UTILITY(U,$J,358.3,5701,1,4,0)
 ;;=4^HYPERNATREMIA
 ;;^UTILITY(U,$J,358.3,5701,2)
 ;;=^60144
 ;;^UTILITY(U,$J,358.3,5702,0)
 ;;=276.7^^55^469^9
 ;;^UTILITY(U,$J,358.3,5702,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5702,1,3,0)
 ;;=3^276.7
 ;;^UTILITY(U,$J,358.3,5702,1,4,0)
 ;;=4^HYPERPOTASSEMIA
 ;;^UTILITY(U,$J,358.3,5702,2)
 ;;=^60042
 ;;^UTILITY(U,$J,358.3,5703,0)
 ;;=275.41^^55^469^10
 ;;^UTILITY(U,$J,358.3,5703,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5703,1,3,0)
 ;;=3^275.41
 ;;^UTILITY(U,$J,358.3,5703,1,4,0)
 ;;=4^HYPOCALCEMIA
 ;;^UTILITY(U,$J,358.3,5703,2)
 ;;=^60542
 ;;^UTILITY(U,$J,358.3,5704,0)
 ;;=588.89^^55^469^11
 ;;^UTILITY(U,$J,358.3,5704,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5704,1,3,0)
 ;;=3^588.89
 ;;^UTILITY(U,$J,358.3,5704,1,4,0)
 ;;=4^HYPOKALEMIC NEPHROPATHY
 ;;^UTILITY(U,$J,358.3,5704,2)
 ;;=^88036
 ;;^UTILITY(U,$J,358.3,5705,0)
 ;;=276.1^^55^469^12
 ;;^UTILITY(U,$J,358.3,5705,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5705,1,3,0)
 ;;=3^276.1
 ;;^UTILITY(U,$J,358.3,5705,1,4,0)
 ;;=4^HYPONATREMIA
 ;;^UTILITY(U,$J,358.3,5705,2)
 ;;=^60722
 ;;^UTILITY(U,$J,358.3,5706,0)
 ;;=275.3^^55^469^13
 ;;^UTILITY(U,$J,358.3,5706,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5706,1,3,0)
 ;;=3^275.3
 ;;^UTILITY(U,$J,358.3,5706,1,4,0)
 ;;=4^HYPOPHOSPHATEMIA
 ;;^UTILITY(U,$J,358.3,5706,2)
 ;;=^93796
 ;;^UTILITY(U,$J,358.3,5707,0)
 ;;=276.8^^55^469^14
 ;;^UTILITY(U,$J,358.3,5707,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5707,1,3,0)
 ;;=3^276.8
 ;;^UTILITY(U,$J,358.3,5707,1,4,0)
 ;;=4^HYPOPOTASSEMIA
 ;;^UTILITY(U,$J,358.3,5707,2)
 ;;=^60611
 ;;^UTILITY(U,$J,358.3,5708,0)
 ;;=276.52^^55^469^15
