IBDEI0AB ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,13688,1,4,0)
 ;;=4^249.60
 ;;^UTILITY(U,$J,358.3,13688,1,5,0)
 ;;=5^Secondary DM w/ Neuro Complication
 ;;^UTILITY(U,$J,358.3,13688,2)
 ;;=^336734
 ;;^UTILITY(U,$J,358.3,13689,0)
 ;;=793.2^^107^863^1
 ;;^UTILITY(U,$J,358.3,13689,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13689,1,4,0)
 ;;=4^793.2
 ;;^UTILITY(U,$J,358.3,13689,1,5,0)
 ;;=5^Abnormal Chest X-Ray, Other
 ;;^UTILITY(U,$J,358.3,13689,2)
 ;;=^273419
 ;;^UTILITY(U,$J,358.3,13690,0)
 ;;=277.6^^107^863^2
 ;;^UTILITY(U,$J,358.3,13690,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13690,1,4,0)
 ;;=4^277.6
 ;;^UTILITY(U,$J,358.3,13690,1,5,0)
 ;;=5^Alpha-1 Antitrypsin Deficiency
 ;;^UTILITY(U,$J,358.3,13690,2)
 ;;=^87463
 ;;^UTILITY(U,$J,358.3,13691,0)
 ;;=493.92^^107^863^3
 ;;^UTILITY(U,$J,358.3,13691,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13691,1,4,0)
 ;;=4^493.92
 ;;^UTILITY(U,$J,358.3,13691,1,5,0)
 ;;=5^Asthma, Acute Exacerbation
 ;;^UTILITY(U,$J,358.3,13691,2)
 ;;=^322001
 ;;^UTILITY(U,$J,358.3,13692,0)
 ;;=493.20^^107^863^10
 ;;^UTILITY(U,$J,358.3,13692,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13692,1,4,0)
 ;;=4^493.20
 ;;^UTILITY(U,$J,358.3,13692,1,5,0)
 ;;=5^COPD With Asthma
 ;;^UTILITY(U,$J,358.3,13692,2)
 ;;=COPD with Asthma^269964
 ;;^UTILITY(U,$J,358.3,13693,0)
 ;;=493.91^^107^863^4
 ;;^UTILITY(U,$J,358.3,13693,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13693,1,4,0)
 ;;=4^493.91
 ;;^UTILITY(U,$J,358.3,13693,1,5,0)
 ;;=5^Asthma, With Status Asthmat
 ;;^UTILITY(U,$J,358.3,13693,2)
 ;;=^269967
 ;;^UTILITY(U,$J,358.3,13694,0)
 ;;=491.21^^107^863^9
 ;;^UTILITY(U,$J,358.3,13694,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13694,1,4,0)
 ;;=4^491.21
 ;;^UTILITY(U,$J,358.3,13694,1,5,0)
 ;;=5^COPD Exacerbation
 ;;^UTILITY(U,$J,358.3,13694,2)
 ;;=COPD Exacerbation^269954
 ;;^UTILITY(U,$J,358.3,13695,0)
 ;;=494.0^^107^863^6
 ;;^UTILITY(U,$J,358.3,13695,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13695,1,4,0)
 ;;=4^494.0
 ;;^UTILITY(U,$J,358.3,13695,1,5,0)
 ;;=5^Bronchiectasis, Chronic
 ;;^UTILITY(U,$J,358.3,13695,2)
 ;;=^321990
 ;;^UTILITY(U,$J,358.3,13696,0)
 ;;=494.1^^107^863^5
 ;;^UTILITY(U,$J,358.3,13696,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13696,1,4,0)
 ;;=4^494.1
 ;;^UTILITY(U,$J,358.3,13696,1,5,0)
 ;;=5^Bronchiectasis With Exacerb
 ;;^UTILITY(U,$J,358.3,13696,2)
 ;;=^321991
 ;;^UTILITY(U,$J,358.3,13697,0)
 ;;=496.^^107^863^11
 ;;^UTILITY(U,$J,358.3,13697,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13697,1,4,0)
 ;;=4^496.
 ;;^UTILITY(U,$J,358.3,13697,1,5,0)
 ;;=5^COPD, General
 ;;^UTILITY(U,$J,358.3,13697,2)
 ;;=COPD, General^24355
 ;;^UTILITY(U,$J,358.3,13698,0)
 ;;=491.20^^107^863^7
 ;;^UTILITY(U,$J,358.3,13698,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13698,1,4,0)
 ;;=4^491.20
 ;;^UTILITY(U,$J,358.3,13698,1,5,0)
 ;;=5^Chronic Asthmatic Bronchitis
 ;;^UTILITY(U,$J,358.3,13698,2)
 ;;=Chronic Asthmatic Bronchitis^269953
 ;;^UTILITY(U,$J,358.3,13699,0)
 ;;=491.9^^107^863^8
 ;;^UTILITY(U,$J,358.3,13699,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13699,1,4,0)
 ;;=4^491.9
 ;;^UTILITY(U,$J,358.3,13699,1,5,0)
 ;;=5^Chronic Bronchitis
 ;;^UTILITY(U,$J,358.3,13699,2)
 ;;=^24359
 ;;^UTILITY(U,$J,358.3,13700,0)
 ;;=786.2^^107^863^12
 ;;^UTILITY(U,$J,358.3,13700,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13700,1,4,0)
 ;;=4^786.2
 ;;^UTILITY(U,$J,358.3,13700,1,5,0)
 ;;=5^Cough
 ;;^UTILITY(U,$J,358.3,13700,2)
 ;;=Cough^28905
 ;;^UTILITY(U,$J,358.3,13701,0)
 ;;=786.09^^107^863^13
 ;;^UTILITY(U,$J,358.3,13701,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13701,1,4,0)
 ;;=4^786.09
 ;;^UTILITY(U,$J,358.3,13701,1,5,0)
 ;;=5^Dyspnea
 ;;^UTILITY(U,$J,358.3,13701,2)
 ;;=Dyspnea^87547
 ;;^UTILITY(U,$J,358.3,13702,0)
 ;;=492.8^^107^863^14
 ;;^UTILITY(U,$J,358.3,13702,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13702,1,4,0)
 ;;=4^492.8
 ;;^UTILITY(U,$J,358.3,13702,1,5,0)
 ;;=5^Emphysema
 ;;^UTILITY(U,$J,358.3,13702,2)
 ;;=Emphysema^87569
 ;;^UTILITY(U,$J,358.3,13703,0)
 ;;=487.1^^107^863^16
 ;;^UTILITY(U,$J,358.3,13703,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13703,1,4,0)
 ;;=4^487.1
 ;;^UTILITY(U,$J,358.3,13703,1,5,0)
 ;;=5^Influenza With Other Resp Manifest
 ;;^UTILITY(U,$J,358.3,13703,2)
 ;;=^63125
 ;;^UTILITY(U,$J,358.3,13704,0)
 ;;=487.0^^107^863^15
 ;;^UTILITY(U,$J,358.3,13704,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13704,1,4,0)
 ;;=4^487.0
 ;;^UTILITY(U,$J,358.3,13704,1,5,0)
 ;;=5^Influenza W Pneumonia
 ;;^UTILITY(U,$J,358.3,13704,2)
 ;;=^269942
 ;;^UTILITY(U,$J,358.3,13705,0)
 ;;=515.^^107^863^17
 ;;^UTILITY(U,$J,358.3,13705,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13705,1,4,0)
 ;;=4^515.
 ;;^UTILITY(U,$J,358.3,13705,1,5,0)
 ;;=5^Interstitial Lung Disease
 ;;^UTILITY(U,$J,358.3,13705,2)
 ;;=^101072
 ;;^UTILITY(U,$J,358.3,13706,0)
 ;;=786.52^^107^863^19
 ;;^UTILITY(U,$J,358.3,13706,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13706,1,4,0)
 ;;=4^786.52
 ;;^UTILITY(U,$J,358.3,13706,1,5,0)
 ;;=5^Painful Resp, Pleurodynia
 ;;^UTILITY(U,$J,358.3,13706,2)
 ;;=^89126
 ;;^UTILITY(U,$J,358.3,13707,0)
 ;;=511.9^^107^863^21
 ;;^UTILITY(U,$J,358.3,13707,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13707,1,4,0)
 ;;=4^511.9
 ;;^UTILITY(U,$J,358.3,13707,1,5,0)
 ;;=5^Pleural Effusion, Unsp Type
 ;;^UTILITY(U,$J,358.3,13707,2)
 ;;=^123973
 ;;^UTILITY(U,$J,358.3,13708,0)
 ;;=511.0^^107^863^23
 ;;^UTILITY(U,$J,358.3,13708,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13708,1,4,0)
 ;;=4^511.0
 ;;^UTILITY(U,$J,358.3,13708,1,5,0)
 ;;=5^Pleurisy
 ;;^UTILITY(U,$J,358.3,13708,2)
 ;;=Pleurisy^95432
 ;;^UTILITY(U,$J,358.3,13709,0)
 ;;=486.^^107^863^24
 ;;^UTILITY(U,$J,358.3,13709,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13709,1,4,0)
 ;;=4^486.
 ;;^UTILITY(U,$J,358.3,13709,1,5,0)
 ;;=5^Pneumonia, Unsp Organism
 ;;^UTILITY(U,$J,358.3,13709,2)
 ;;=^95632
 ;;^UTILITY(U,$J,358.3,13710,0)
 ;;=135.^^107^863^27
 ;;^UTILITY(U,$J,358.3,13710,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13710,1,4,0)
 ;;=4^135.
 ;;^UTILITY(U,$J,358.3,13710,1,5,0)
 ;;=5^Sarcoidosis
 ;;^UTILITY(U,$J,358.3,13710,2)
 ;;=Sarcoidosis^107916^517.8
 ;;^UTILITY(U,$J,358.3,13711,0)
 ;;=786.05^^107^863^28
 ;;^UTILITY(U,$J,358.3,13711,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13711,1,4,0)
 ;;=4^786.05
 ;;^UTILITY(U,$J,358.3,13711,1,5,0)
 ;;=5^Shortness Of Breath
 ;;^UTILITY(U,$J,358.3,13711,2)
 ;;=Shortness of Breath^37632
 ;;^UTILITY(U,$J,358.3,13712,0)
 ;;=780.57^^107^863^29
 ;;^UTILITY(U,$J,358.3,13712,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13712,1,4,0)
 ;;=4^780.57
 ;;^UTILITY(U,$J,358.3,13712,1,5,0)
 ;;=5^Sleep Apnea
 ;;^UTILITY(U,$J,358.3,13712,2)
 ;;=Sleep Apnea^293933
 ;;^UTILITY(U,$J,358.3,13713,0)
 ;;=786.1^^107^863^31
 ;;^UTILITY(U,$J,358.3,13713,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13713,1,4,0)
 ;;=4^786.1
 ;;^UTILITY(U,$J,358.3,13713,1,5,0)
 ;;=5^Stridor
 ;;^UTILITY(U,$J,358.3,13713,2)
 ;;=Stridor^114767
 ;;^UTILITY(U,$J,358.3,13714,0)
 ;;=011.90^^107^863^33
 ;;^UTILITY(U,$J,358.3,13714,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13714,1,4,0)
 ;;=4^011.90
 ;;^UTILITY(U,$J,358.3,13714,1,5,0)
 ;;=5^Tb, Pulmonary, Nos
 ;;^UTILITY(U,$J,358.3,13714,2)
 ;;=TB, Pulmonary^122756
 ;;^UTILITY(U,$J,358.3,13715,0)
 ;;=786.06^^107^863^32
 ;;^UTILITY(U,$J,358.3,13715,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13715,1,4,0)
 ;;=4^786.06
 ;;^UTILITY(U,$J,358.3,13715,1,5,0)
 ;;=5^Tachypnea
 ;;^UTILITY(U,$J,358.3,13715,2)
 ;;=Tachypnea^321213
 ;;^UTILITY(U,$J,358.3,13716,0)
 ;;=305.1^^107^863^34
 ;;^UTILITY(U,$J,358.3,13716,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13716,1,4,0)
 ;;=4^305.1
 ;;^UTILITY(U,$J,358.3,13716,1,5,0)
 ;;=5^Tobacco Use
 ;;^UTILITY(U,$J,358.3,13716,2)
 ;;=Tobacco Use^119899
 ;;^UTILITY(U,$J,358.3,13717,0)
 ;;=786.07^^107^863^35
 ;;^UTILITY(U,$J,358.3,13717,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13717,1,4,0)
 ;;=4^786.07
 ;;^UTILITY(U,$J,358.3,13717,1,5,0)
 ;;=5^Wheezing
 ;;^UTILITY(U,$J,358.3,13717,2)
 ;;=Wheezing^127848
 ;;^UTILITY(U,$J,358.3,13718,0)
 ;;=511.81^^107^863^22
 ;;^UTILITY(U,$J,358.3,13718,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13718,1,4,0)
 ;;=4^511.81
 ;;^UTILITY(U,$J,358.3,13718,1,5,0)
 ;;=5^Pleural Effusion,Malignant
 ;;^UTILITY(U,$J,358.3,13718,2)
 ;;=^336603
 ;;^UTILITY(U,$J,358.3,13719,0)
 ;;=511.89^^107^863^20
 ;;^UTILITY(U,$J,358.3,13719,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13719,1,4,0)
 ;;=4^511.89
 ;;^UTILITY(U,$J,358.3,13719,1,5,0)
 ;;=5^Pleural Effusion, Other Spec
 ;;^UTILITY(U,$J,358.3,13719,2)
 ;;=^336604
 ;;^UTILITY(U,$J,358.3,13720,0)
 ;;=793.11^^107^863^30
 ;;^UTILITY(U,$J,358.3,13720,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13720,1,4,0)
 ;;=4^793.11
 ;;^UTILITY(U,$J,358.3,13720,1,5,0)
 ;;=5^Solitary Pulmonary Nodule
 ;;^UTILITY(U,$J,358.3,13720,2)
 ;;=^340570
 ;;^UTILITY(U,$J,358.3,13721,0)
 ;;=793.19^^107^863^18
 ;;^UTILITY(U,$J,358.3,13721,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13721,1,4,0)
 ;;=4^793.19
 ;;^UTILITY(U,$J,358.3,13721,1,5,0)
 ;;=5^Oth Nonspec X-Ray Findings Lung
 ;;^UTILITY(U,$J,358.3,13721,2)
 ;;=^340571
 ;;^UTILITY(U,$J,358.3,13722,0)
 ;;=795.51^^107^863^26
 ;;^UTILITY(U,$J,358.3,13722,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13722,1,4,0)
 ;;=4^795.51
 ;;^UTILITY(U,$J,358.3,13722,1,5,0)
 ;;=5^Pos PPD w/o Active TB
 ;;^UTILITY(U,$J,358.3,13722,2)
 ;;=^340572
 ;;^UTILITY(U,$J,358.3,13723,0)
 ;;=795.52^^107^863^25
 ;;^UTILITY(U,$J,358.3,13723,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13723,1,4,0)
 ;;=4^795.52
 ;;^UTILITY(U,$J,358.3,13723,1,5,0)
 ;;=5^Pos GFT w/o Active TB
 ;;^UTILITY(U,$J,358.3,13723,2)
 ;;=^340573
 ;;^UTILITY(U,$J,358.3,13724,0)
 ;;=528.8^^107^864^24
 ;;^UTILITY(U,$J,358.3,13724,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,13724,1,4,0)
 ;;=4^528.8
 ;;^UTILITY(U,$J,358.3,13724,1,5,0)
 ;;=5^Oral Aphthae
 ;;^UTILITY(U,$J,358.3,13724,2)
 ;;=^86127
 ;;^UTILITY(U,$J,358.3,13725,0)
 ;;=376.01^^107^864^25
 ;;^UTILITY(U,$J,358.3,13725,1,0)
 ;;=^358.31IA^5^2
