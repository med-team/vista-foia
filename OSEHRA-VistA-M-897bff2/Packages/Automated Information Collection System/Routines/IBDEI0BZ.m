IBDEI0BZ ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,15945,2)
 ;;=^275425
 ;;^UTILITY(U,$J,358.3,15946,0)
 ;;=924.10^^116^993^27
 ;;^UTILITY(U,$J,358.3,15946,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15946,1,3,0)
 ;;=3^924.10
 ;;^UTILITY(U,$J,358.3,15946,1,5,0)
 ;;=5^Contusion of lower leg
 ;;^UTILITY(U,$J,358.3,15946,2)
 ;;=^275419
 ;;^UTILITY(U,$J,358.3,15947,0)
 ;;=924.3^^116^993^28
 ;;^UTILITY(U,$J,358.3,15947,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15947,1,3,0)
 ;;=3^924.3
 ;;^UTILITY(U,$J,358.3,15947,1,5,0)
 ;;=5^Contusion of toe
 ;;^UTILITY(U,$J,358.3,15947,2)
 ;;=^275427
 ;;^UTILITY(U,$J,358.3,15948,0)
 ;;=700.^^116^993^29
 ;;^UTILITY(U,$J,358.3,15948,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15948,1,3,0)
 ;;=3^700.
 ;;^UTILITY(U,$J,358.3,15948,1,5,0)
 ;;=5^Corns and callosities
 ;;^UTILITY(U,$J,358.3,15948,2)
 ;;=^18351
 ;;^UTILITY(U,$J,358.3,15949,0)
 ;;=706.2^^116^993^31
 ;;^UTILITY(U,$J,358.3,15949,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15949,1,3,0)
 ;;=3^706.2
 ;;^UTILITY(U,$J,358.3,15949,1,5,0)
 ;;=5^Cyst of sebaceous
 ;;^UTILITY(U,$J,358.3,15949,2)
 ;;=^41304
 ;;^UTILITY(U,$J,358.3,15950,0)
 ;;=733.20^^116^993^30
 ;;^UTILITY(U,$J,358.3,15950,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15950,1,3,0)
 ;;=3^733.20
 ;;^UTILITY(U,$J,358.3,15950,1,5,0)
 ;;=5^Cyst of bone
 ;;^UTILITY(U,$J,358.3,15950,2)
 ;;=^30080
 ;;^UTILITY(U,$J,358.3,15951,0)
 ;;=729.99^^116^993^20
 ;;^UTILITY(U,$J,358.3,15951,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15951,1,3,0)
 ;;=3^729.99
 ;;^UTILITY(U,$J,358.3,15951,1,5,0)
 ;;=5^Compartmental syndrome, nontraumatic
 ;;^UTILITY(U,$J,358.3,15951,2)
 ;;=^336656
 ;;^UTILITY(U,$J,358.3,15952,0)
 ;;=453.89^^116^993^15
 ;;^UTILITY(U,$J,358.3,15952,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15952,1,3,0)
 ;;=3^453.89
 ;;^UTILITY(U,$J,358.3,15952,1,5,0)
 ;;=5^Claudication, venous
 ;;^UTILITY(U,$J,358.3,15952,2)
 ;;=^338259
 ;;^UTILITY(U,$J,358.3,15953,0)
 ;;=735.8^^116^994^2
 ;;^UTILITY(U,$J,358.3,15953,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15953,1,3,0)
 ;;=3^735.8
 ;;^UTILITY(U,$J,358.3,15953,1,5,0)
 ;;=5^Deformity, toe- acquired
 ;;^UTILITY(U,$J,358.3,15953,2)
 ;;=^272714
 ;;^UTILITY(U,$J,358.3,15954,0)
 ;;=736.70^^116^994^3
 ;;^UTILITY(U,$J,358.3,15954,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15954,1,3,0)
 ;;=3^736.70
 ;;^UTILITY(U,$J,358.3,15954,1,5,0)
 ;;=5^Deformity, ankle and foot- acquired
 ;;^UTILITY(U,$J,358.3,15954,2)
 ;;=^123805
 ;;^UTILITY(U,$J,358.3,15955,0)
 ;;=755.66^^116^994^4
 ;;^UTILITY(U,$J,358.3,15955,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15955,1,3,0)
 ;;=3^755.66
 ;;^UTILITY(U,$J,358.3,15955,1,5,0)
 ;;=5^Deformity, toe- congenital
 ;;^UTILITY(U,$J,358.3,15955,2)
 ;;=^273059
 ;;^UTILITY(U,$J,358.3,15956,0)
 ;;=754.70^^116^994^5
 ;;^UTILITY(U,$J,358.3,15956,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15956,1,3,0)
 ;;=3^754.70
 ;;^UTILITY(U,$J,358.3,15956,1,5,0)
 ;;=5^Deformity, foot- congenital
 ;;^UTILITY(U,$J,358.3,15956,2)
 ;;=^25440
 ;;^UTILITY(U,$J,358.3,15957,0)
 ;;=755.69^^116^994^6
 ;;^UTILITY(U,$J,358.3,15957,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15957,1,3,0)
 ;;=3^755.69
 ;;^UTILITY(U,$J,358.3,15957,1,5,0)
 ;;=5^Deformity, ankle- congenital
 ;;^UTILITY(U,$J,358.3,15957,2)
 ;;=^273054
 ;;^UTILITY(U,$J,358.3,15958,0)
 ;;=692.9^^116^994^7
 ;;^UTILITY(U,$J,358.3,15958,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15958,1,3,0)
 ;;=3^692.9
 ;;^UTILITY(U,$J,358.3,15958,1,5,0)
 ;;=5^Dermatitis (contact/eczema/venenata)
 ;;^UTILITY(U,$J,358.3,15958,2)
 ;;=^27800
 ;;^UTILITY(U,$J,358.3,15959,0)
 ;;=459.81^^116^994^8
 ;;^UTILITY(U,$J,358.3,15959,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15959,1,3,0)
 ;;=3^459.81
 ;;^UTILITY(U,$J,358.3,15959,1,5,0)
 ;;=5^Dermatitis, Stasis (w/o varicose veins) 
 ;;^UTILITY(U,$J,358.3,15959,2)
 ;;=^125826
 ;;^UTILITY(U,$J,358.3,15960,0)
 ;;=454.1^^116^994^9
 ;;^UTILITY(U,$J,358.3,15960,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15960,1,3,0)
 ;;=3^454.1
 ;;^UTILITY(U,$J,358.3,15960,1,5,0)
 ;;=5^Dermatitis, Status due to varicose veins 
 ;;^UTILITY(U,$J,358.3,15960,2)
 ;;=^125435
 ;;^UTILITY(U,$J,358.3,15961,0)
 ;;=454.2^^116^994^11
 ;;^UTILITY(U,$J,358.3,15961,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15961,1,3,0)
 ;;=3^454.2
 ;;^UTILITY(U,$J,358.3,15961,1,5,0)
 ;;=5^Dermatitis, Stasis with ulcer/ulcerated
 ;;^UTILITY(U,$J,358.3,15961,2)
 ;;=^269821
 ;;^UTILITY(U,$J,358.3,15962,0)
 ;;=110.4^^116^994^12
 ;;^UTILITY(U,$J,358.3,15962,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15962,1,3,0)
 ;;=3^110.4
 ;;^UTILITY(U,$J,358.3,15962,1,5,0)
 ;;=5^Dermatophytosis of foot
 ;;^UTILITY(U,$J,358.3,15962,2)
 ;;=^33168
 ;;^UTILITY(U,$J,358.3,15963,0)
 ;;=250.00^^116^994^13
 ;;^UTILITY(U,$J,358.3,15963,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15963,1,3,0)
 ;;=3^250.00
 ;;^UTILITY(U,$J,358.3,15963,1,5,0)
 ;;=5^DM II w/o complication 
 ;;^UTILITY(U,$J,358.3,15963,2)
 ;;=^33605
 ;;^UTILITY(U,$J,358.3,15964,0)
 ;;=250.01^^116^994^39
 ;;^UTILITY(U,$J,358.3,15964,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15964,1,3,0)
 ;;=3^250.01
 ;;^UTILITY(U,$J,358.3,15964,1,5,0)
 ;;=5^DM I w/o complication 
 ;;^UTILITY(U,$J,358.3,15964,2)
 ;;=^33586
 ;;^UTILITY(U,$J,358.3,15965,0)
 ;;=838.00^^116^994^65
 ;;^UTILITY(U,$J,358.3,15965,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15965,1,3,0)
 ;;=3^838.00
 ;;^UTILITY(U,$J,358.3,15965,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; foot, unspecified
 ;;^UTILITY(U,$J,358.3,15965,2)
 ;;=^274391
 ;;^UTILITY(U,$J,358.3,15966,0)
 ;;=838.01^^116^994^66
 ;;^UTILITY(U,$J,358.3,15966,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15966,1,3,0)
 ;;=3^838.01
 ;;^UTILITY(U,$J,358.3,15966,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; tarsal(bone), joint unspecified 
 ;;^UTILITY(U,$J,358.3,15966,2)
 ;;=^274394
 ;;^UTILITY(U,$J,358.3,15967,0)
 ;;=838.02^^116^994^67
 ;;^UTILITY(U,$J,358.3,15967,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15967,1,3,0)
 ;;=3^838.02
 ;;^UTILITY(U,$J,358.3,15967,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; midtarsal (joint)
 ;;^UTILITY(U,$J,358.3,15967,2)
 ;;=^274395
 ;;^UTILITY(U,$J,358.3,15968,0)
 ;;=838.03^^116^994^68
 ;;^UTILITY(U,$J,358.3,15968,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15968,1,3,0)
 ;;=3^838.03
 ;;^UTILITY(U,$J,358.3,15968,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; tarsometatarsal (joint)
 ;;^UTILITY(U,$J,358.3,15968,2)
 ;;=^274396
 ;;^UTILITY(U,$J,358.3,15969,0)
 ;;=838.04^^116^994^69
 ;;^UTILITY(U,$J,358.3,15969,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15969,1,3,0)
 ;;=3^838.04
 ;;^UTILITY(U,$J,358.3,15969,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; metatarsal(bone), joint unspecified
 ;;^UTILITY(U,$J,358.3,15969,2)
 ;;=^274397
 ;;^UTILITY(U,$J,358.3,15970,0)
 ;;=838.05^^116^994^70
 ;;^UTILITY(U,$J,358.3,15970,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15970,1,3,0)
 ;;=3^838.05
 ;;^UTILITY(U,$J,358.3,15970,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; metatarsophalangeal(joint)
 ;;^UTILITY(U,$J,358.3,15970,2)
 ;;=^274398
 ;;^UTILITY(U,$J,358.3,15971,0)
 ;;=838.06^^116^994^71
 ;;^UTILITY(U,$J,358.3,15971,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15971,1,3,0)
 ;;=3^838.06
 ;;^UTILITY(U,$J,358.3,15971,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; interphalangeal(joint) foot
 ;;^UTILITY(U,$J,358.3,15971,2)
 ;;=^274399
 ;;^UTILITY(U,$J,358.3,15972,0)
 ;;=838.09^^116^994^72
 ;;^UTILITY(U,$J,358.3,15972,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15972,1,3,0)
 ;;=3^838.09
 ;;^UTILITY(U,$J,358.3,15972,1,5,0)
 ;;=5^Dislocation of foot, closed dislocation; other, toe(s)
 ;;^UTILITY(U,$J,358.3,15972,2)
 ;;=^274400
 ;;^UTILITY(U,$J,358.3,15973,0)
 ;;=12.5^1^116^994^12.5^-DIABETES MELLITUS^1^1
 ;;^UTILITY(U,$J,358.3,15973,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15973,1,3,0)
 ;;=3
 ;;^UTILITY(U,$J,358.3,15973,1,5,0)
 ;;=5
 ;;^UTILITY(U,$J,358.3,15974,0)
 ;;=64.5^1^116^994^64.5^-Dislocation^1^1
 ;;^UTILITY(U,$J,358.3,15974,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15974,1,3,0)
 ;;=3
 ;;^UTILITY(U,$J,358.3,15974,1,5,0)
 ;;=5
 ;;^UTILITY(U,$J,358.3,15975,0)
 ;;=459.10^^116^994^10
 ;;^UTILITY(U,$J,358.3,15975,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15975,1,3,0)
 ;;=3^459.10
 ;;^UTILITY(U,$J,358.3,15975,1,5,0)
 ;;=5^Post Phlebotic Syndrome
 ;;^UTILITY(U,$J,358.3,15975,2)
 ;;=Post Phlebotic Syndrome^328597
 ;;^UTILITY(U,$J,358.3,15976,0)
 ;;=719.7^^116^994^64
 ;;^UTILITY(U,$J,358.3,15976,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15976,1,3,0)
 ;;=3^719.7
 ;;^UTILITY(U,$J,358.3,15976,1,5,0)
 ;;=5^Difficulty In Walking
 ;;^UTILITY(U,$J,358.3,15976,2)
 ;;=^329945
 ;;^UTILITY(U,$J,358.3,15977,0)
 ;;=453.40^^116^994^1
 ;;^UTILITY(U,$J,358.3,15977,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15977,1,3,0)
 ;;=3^453.40
 ;;^UTILITY(U,$J,358.3,15977,1,5,0)
 ;;=5^Deep vein thrombosis lower extremity
 ;;^UTILITY(U,$J,358.3,15977,2)
 ;;=^338554
 ;;^UTILITY(U,$J,358.3,15978,0)
 ;;=692.9^^116^995^1
 ;;^UTILITY(U,$J,358.3,15978,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15978,1,3,0)
 ;;=3^692.9
 ;;^UTILITY(U,$J,358.3,15978,1,5,0)
 ;;=5^Eczema
 ;;^UTILITY(U,$J,358.3,15978,2)
 ;;=^27800
 ;;^UTILITY(U,$J,358.3,15979,0)
 ;;=691.8^^116^995^2
 ;;^UTILITY(U,$J,358.3,15979,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15979,1,3,0)
 ;;=3^691.8
 ;;^UTILITY(U,$J,358.3,15979,1,5,0)
 ;;=5^Eczema, allergic
 ;;^UTILITY(U,$J,358.3,15979,2)
 ;;=^87342
 ;;^UTILITY(U,$J,358.3,15980,0)
 ;;=782.3^^116^995^3
 ;;^UTILITY(U,$J,358.3,15980,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15980,1,3,0)
 ;;=3^782.3
 ;;^UTILITY(U,$J,358.3,15980,1,5,0)
 ;;=5^Edema (any site)
 ;;^UTILITY(U,$J,358.3,15980,2)
 ;;=^38340
 ;;^UTILITY(U,$J,358.3,15981,0)
 ;;=726.70^^116^995^4
 ;;^UTILITY(U,$J,358.3,15981,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,15981,1,3,0)
 ;;=3^726.70
 ;;^UTILITY(U,$J,358.3,15981,1,5,0)
 ;;=5^Enthesopathy of ankle & tarsus, unspecified
