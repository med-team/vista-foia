IBDEI04F ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,5551,1,3,0)
 ;;=3^353.5
 ;;^UTILITY(U,$J,358.3,5551,1,4,0)
 ;;=4^NEURALGIC AMYOTROPHY
 ;;^UTILITY(U,$J,358.3,5551,2)
 ;;=^21928
 ;;^UTILITY(U,$J,358.3,5552,0)
 ;;=353.6^^53^453^28
 ;;^UTILITY(U,$J,358.3,5552,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5552,1,3,0)
 ;;=3^353.6
 ;;^UTILITY(U,$J,358.3,5552,1,4,0)
 ;;=4^PHANTOM LIMB (SYNDROME)
 ;;^UTILITY(U,$J,358.3,5552,2)
 ;;=^92758
 ;;^UTILITY(U,$J,358.3,5553,0)
 ;;=353.8^^53^453^24
 ;;^UTILITY(U,$J,358.3,5553,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5553,1,3,0)
 ;;=3^353.8
 ;;^UTILITY(U,$J,358.3,5553,1,4,0)
 ;;=4^OTH NERVE ROOT/PLEXUS DIS NEC
 ;;^UTILITY(U,$J,358.3,5553,2)
 ;;=^268502
 ;;^UTILITY(U,$J,358.3,5554,0)
 ;;=353.9^^53^453^37
 ;;^UTILITY(U,$J,358.3,5554,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5554,1,3,0)
 ;;=3^353.9
 ;;^UTILITY(U,$J,358.3,5554,1,4,0)
 ;;=4^UNSPEC NERVE ROOT/PLEXUS DIS NOS
 ;;^UTILITY(U,$J,358.3,5554,2)
 ;;=^268503
 ;;^UTILITY(U,$J,358.3,5555,0)
 ;;=356.0^^53^453^9
 ;;^UTILITY(U,$J,358.3,5555,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5555,1,3,0)
 ;;=3^356.0
 ;;^UTILITY(U,$J,358.3,5555,1,4,0)
 ;;=4^HERED PERIPH NEUROPATHY
 ;;^UTILITY(U,$J,358.3,5555,2)
 ;;=^268520
 ;;^UTILITY(U,$J,358.3,5556,0)
 ;;=356.1^^53^453^27
 ;;^UTILITY(U,$J,358.3,5556,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5556,1,3,0)
 ;;=3^356.1
 ;;^UTILITY(U,$J,358.3,5556,1,4,0)
 ;;=4^PERONEAL MUSCLE ATROPHY
 ;;^UTILITY(U,$J,358.3,5556,2)
 ;;=^22244
 ;;^UTILITY(U,$J,358.3,5557,0)
 ;;=356.2^^53^453^10
 ;;^UTILITY(U,$J,358.3,5557,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5557,1,3,0)
 ;;=3^356.2
 ;;^UTILITY(U,$J,358.3,5557,1,4,0)
 ;;=4^HERED SENSORY NEUROPATHY
 ;;^UTILITY(U,$J,358.3,5557,2)
 ;;=^56583
 ;;^UTILITY(U,$J,358.3,5558,0)
 ;;=356.3^^53^453^32
 ;;^UTILITY(U,$J,358.3,5558,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5558,1,3,0)
 ;;=3^356.3
 ;;^UTILITY(U,$J,358.3,5558,1,4,0)
 ;;=4^REFSUM'S DISEASE
 ;;^UTILITY(U,$J,358.3,5558,2)
 ;;=^104358
 ;;^UTILITY(U,$J,358.3,5559,0)
 ;;=356.4^^53^453^11
 ;;^UTILITY(U,$J,358.3,5559,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5559,1,3,0)
 ;;=3^356.4
 ;;^UTILITY(U,$J,358.3,5559,1,4,0)
 ;;=4^IDIO PROG POLYNEUROPATHY
 ;;^UTILITY(U,$J,358.3,5559,2)
 ;;=^268523
 ;;^UTILITY(U,$J,358.3,5560,0)
 ;;=356.8^^53^453^25
 ;;^UTILITY(U,$J,358.3,5560,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5560,1,3,0)
 ;;=3^356.8
 ;;^UTILITY(U,$J,358.3,5560,1,4,0)
 ;;=4^OTH SPEC IDIOPATHIC PER NEURO
 ;;^UTILITY(U,$J,358.3,5560,2)
 ;;=^268525
 ;;^UTILITY(U,$J,358.3,5561,0)
 ;;=356.9^^53^453^36
 ;;^UTILITY(U,$J,358.3,5561,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5561,1,3,0)
 ;;=3^356.9
 ;;^UTILITY(U,$J,358.3,5561,1,4,0)
 ;;=4^UNSPEC IDIOPATHIC PER NEURO
 ;;^UTILITY(U,$J,358.3,5561,2)
 ;;=^123931
 ;;^UTILITY(U,$J,358.3,5562,0)
 ;;=785.4^^53^454^1
 ;;^UTILITY(U,$J,358.3,5562,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5562,1,3,0)
 ;;=3^785.4
 ;;^UTILITY(U,$J,358.3,5562,1,4,0)
 ;;=4^GANGRENE
 ;;^UTILITY(U,$J,358.3,5562,2)
 ;;=^49194
 ;;^UTILITY(U,$J,358.3,5563,0)
 ;;=443.81^^53^454^2
 ;;^UTILITY(U,$J,358.3,5563,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5563,1,3,0)
 ;;=3^443.81
 ;;^UTILITY(U,$J,358.3,5563,1,4,0)
 ;;=4^ANGIOPATHY IN OTHER DIS
 ;;^UTILITY(U,$J,358.3,5563,2)
 ;;=^92164
 ;;^UTILITY(U,$J,358.3,5564,0)
 ;;=707.10^^53^455^1
 ;;^UTILITY(U,$J,358.3,5564,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5564,1,3,0)
 ;;=3^707.10
 ;;^UTILITY(U,$J,358.3,5564,1,4,0)
 ;;=4^UNSPEC ULCER LOWER LIMB
 ;;^UTILITY(U,$J,358.3,5564,2)
 ;;=^322142
 ;;^UTILITY(U,$J,358.3,5565,0)
 ;;=707.11^^53^455^2
 ;;^UTILITY(U,$J,358.3,5565,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5565,1,3,0)
 ;;=3^707.11
 ;;^UTILITY(U,$J,358.3,5565,1,4,0)
 ;;=4^ULCER OF THIGH
 ;;^UTILITY(U,$J,358.3,5565,2)
 ;;=^322143
 ;;^UTILITY(U,$J,358.3,5566,0)
 ;;=707.12^^53^455^3
 ;;^UTILITY(U,$J,358.3,5566,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5566,1,3,0)
 ;;=3^707.12
 ;;^UTILITY(U,$J,358.3,5566,1,4,0)
 ;;=4^ULCER OF CALF
 ;;^UTILITY(U,$J,358.3,5566,2)
 ;;=^322144
 ;;^UTILITY(U,$J,358.3,5567,0)
 ;;=707.13^^53^455^4
 ;;^UTILITY(U,$J,358.3,5567,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5567,1,3,0)
 ;;=3^707.13
 ;;^UTILITY(U,$J,358.3,5567,1,4,0)
 ;;=4^ULCER OF ANKLE
 ;;^UTILITY(U,$J,358.3,5567,2)
 ;;=^322145
 ;;^UTILITY(U,$J,358.3,5568,0)
 ;;=707.14^^53^455^5
 ;;^UTILITY(U,$J,358.3,5568,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5568,1,3,0)
 ;;=3^707.14
 ;;^UTILITY(U,$J,358.3,5568,1,4,0)
 ;;=4^ULCER OF HEEL AND MIDFOOT
 ;;^UTILITY(U,$J,358.3,5568,2)
 ;;=^322146
 ;;^UTILITY(U,$J,358.3,5569,0)
 ;;=707.15^^53^455^6
 ;;^UTILITY(U,$J,358.3,5569,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5569,1,3,0)
 ;;=3^707.15
 ;;^UTILITY(U,$J,358.3,5569,1,4,0)
 ;;=4^ULCER OF OTH PART OF FOOT
 ;;^UTILITY(U,$J,358.3,5569,2)
 ;;=^322148
 ;;^UTILITY(U,$J,358.3,5570,0)
 ;;=707.19^^53^455^7
 ;;^UTILITY(U,$J,358.3,5570,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5570,1,3,0)
 ;;=3^707.19
 ;;^UTILITY(U,$J,358.3,5570,1,4,0)
 ;;=4^ULCER OF OTH PART LOWER LIMB
 ;;^UTILITY(U,$J,358.3,5570,2)
 ;;=^322150
 ;;^UTILITY(U,$J,358.3,5571,0)
 ;;=707.8^^53^455^8
 ;;^UTILITY(U,$J,358.3,5571,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5571,1,3,0)
 ;;=3^707.8
 ;;^UTILITY(U,$J,358.3,5571,1,4,0)
 ;;=4^CHRONIC SKIN ULCER NEC
 ;;^UTILITY(U,$J,358.3,5571,2)
 ;;=^271935
 ;;^UTILITY(U,$J,358.3,5572,0)
 ;;=707.9^^53^455^9
 ;;^UTILITY(U,$J,358.3,5572,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5572,1,3,0)
 ;;=3^707.9
 ;;^UTILITY(U,$J,358.3,5572,1,4,0)
 ;;=4^CHRONIC SKIN ULCER NOS
 ;;^UTILITY(U,$J,358.3,5572,2)
 ;;=^24439
 ;;^UTILITY(U,$J,358.3,5573,0)
 ;;=731.8^^53^455^10
 ;;^UTILITY(U,$J,358.3,5573,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5573,1,3,0)
 ;;=3^731.8
 ;;^UTILITY(U,$J,358.3,5573,1,4,0)
 ;;=4^BONE INVOLV IN OTH DIS
 ;;^UTILITY(U,$J,358.3,5573,2)
 ;;=^272681
 ;;^UTILITY(U,$J,358.3,5574,0)
 ;;=V18.0^^53^456^1
 ;;^UTILITY(U,$J,358.3,5574,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5574,1,3,0)
 ;;=3^V18.0
 ;;^UTILITY(U,$J,358.3,5574,1,4,0)
 ;;=4^FAM HX-DIABETES MELLITUS
 ;;^UTILITY(U,$J,358.3,5574,2)
 ;;=^295311
 ;;^UTILITY(U,$J,358.3,5575,0)
 ;;=V58.67^^53^456^2
 ;;^UTILITY(U,$J,358.3,5575,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5575,1,3,0)
 ;;=3^V58.67
 ;;^UTILITY(U,$J,358.3,5575,1,4,0)
 ;;=4^LONG-TERM USE OF INSULIN
 ;;^UTILITY(U,$J,358.3,5575,2)
 ;;=^331585
 ;;^UTILITY(U,$J,358.3,5576,0)
 ;;=V68.1^^53^456^3
 ;;^UTILITY(U,$J,358.3,5576,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5576,1,3,0)
 ;;=3^V68.1
 ;;^UTILITY(U,$J,358.3,5576,1,4,0)
 ;;=4^ISSUE REPEAT PRESCRIPT
 ;;^UTILITY(U,$J,358.3,5576,2)
 ;;=^295585
 ;;^UTILITY(U,$J,358.3,5577,0)
 ;;=V67.59^^53^456^4
 ;;^UTILITY(U,$J,358.3,5577,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5577,1,3,0)
 ;;=3^V67.59
 ;;^UTILITY(U,$J,358.3,5577,1,4,0)
 ;;=4^FOLLOW-UP EXAM NEC
 ;;^UTILITY(U,$J,358.3,5577,2)
 ;;=^295581
 ;;^UTILITY(U,$J,358.3,5578,0)
 ;;=249.00^^53^457^17
 ;;^UTILITY(U,$J,358.3,5578,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5578,1,3,0)
 ;;=3^249.00
 ;;^UTILITY(U,$J,358.3,5578,1,4,0)
 ;;=4^SEC DM W/O COMP NOT UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5578,2)
 ;;=^336728
 ;;^UTILITY(U,$J,358.3,5579,0)
 ;;=249.01^^53^457^18
 ;;^UTILITY(U,$J,358.3,5579,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5579,1,3,0)
 ;;=3^249.01
 ;;^UTILITY(U,$J,358.3,5579,1,4,0)
 ;;=4^SEC DM WO COMP UNCONTRLD
 ;;^UTILITY(U,$J,358.3,5579,2)
 ;;=^336527
 ;;^UTILITY(U,$J,358.3,5580,0)
 ;;=249.10^^53^457^5
 ;;^UTILITY(U,$J,358.3,5580,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5580,1,3,0)
 ;;=3^249.10
 ;;^UTILITY(U,$J,358.3,5580,1,4,0)
 ;;=4^SEC DM KETO NOT UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5580,2)
 ;;=^336729
 ;;^UTILITY(U,$J,358.3,5581,0)
 ;;=249.11^^53^457^6
 ;;^UTILITY(U,$J,358.3,5581,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5581,1,3,0)
 ;;=3^249.11
 ;;^UTILITY(U,$J,358.3,5581,1,4,0)
 ;;=4^SEC DM KETOACD UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5581,2)
 ;;=^336528
 ;;^UTILITY(U,$J,358.3,5582,0)
 ;;=249.20^^53^457^3
 ;;^UTILITY(U,$J,358.3,5582,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5582,1,3,0)
 ;;=3^249.20
 ;;^UTILITY(U,$J,358.3,5582,1,4,0)
 ;;=4^SEC DM HPROS NOT UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5582,2)
 ;;=^336730
 ;;^UTILITY(U,$J,358.3,5583,0)
 ;;=249.21^^53^457^4
 ;;^UTILITY(U,$J,358.3,5583,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5583,1,3,0)
 ;;=3^249.21
 ;;^UTILITY(U,$J,358.3,5583,1,4,0)
 ;;=4^SEC DM HPROSMLR UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5583,2)
 ;;=^336529
 ;;^UTILITY(U,$J,358.3,5584,0)
 ;;=249.30^^53^457^11
 ;;^UTILITY(U,$J,358.3,5584,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5584,1,3,0)
 ;;=3^249.30
 ;;^UTILITY(U,$J,358.3,5584,1,4,0)
 ;;=4^SEC DM OTH COMA NOT UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5584,2)
 ;;=^336731
 ;;^UTILITY(U,$J,358.3,5585,0)
 ;;=249.31^^53^457^12
 ;;^UTILITY(U,$J,358.3,5585,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5585,1,3,0)
 ;;=3^249.31
 ;;^UTILITY(U,$J,358.3,5585,1,4,0)
 ;;=4^SEC DM OTH COMA UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5585,2)
 ;;=^336530
 ;;^UTILITY(U,$J,358.3,5586,0)
 ;;=249.40^^53^457^15
 ;;^UTILITY(U,$J,358.3,5586,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5586,1,3,0)
 ;;=3^249.40
 ;;^UTILITY(U,$J,358.3,5586,1,4,0)
 ;;=4^SEC DM RENAL NOT UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5586,2)
 ;;=^336732
 ;;^UTILITY(U,$J,358.3,5587,0)
 ;;=249.41^^53^457^16
 ;;^UTILITY(U,$J,358.3,5587,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5587,1,3,0)
 ;;=3^249.41
 ;;^UTILITY(U,$J,358.3,5587,1,4,0)
 ;;=4^SEC DM RENAL UNCONTRLD
 ;;^UTILITY(U,$J,358.3,5587,2)
 ;;=^336531
 ;;^UTILITY(U,$J,358.3,5588,0)
 ;;=249.50^^53^457^9
 ;;^UTILITY(U,$J,358.3,5588,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5588,1,3,0)
 ;;=3^249.50
 ;;^UTILITY(U,$J,358.3,5588,1,4,0)
 ;;=4^SEC DM OPHTH NOT UNCNTRLD
 ;;^UTILITY(U,$J,358.3,5588,2)
 ;;=^336733
 ;;^UTILITY(U,$J,358.3,5589,0)
 ;;=249.51^^53^457^10
 ;;^UTILITY(U,$J,358.3,5589,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5589,1,3,0)
 ;;=3^249.51
