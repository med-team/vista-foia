IBDEI06R ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,8845,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8845,1,4,0)
 ;;=4^283.9
 ;;^UTILITY(U,$J,358.3,8845,1,5,0)
 ;;=5^Hemolytic Anemia , Acquired
 ;;^UTILITY(U,$J,358.3,8845,2)
 ;;=^7071
 ;;^UTILITY(U,$J,358.3,8846,0)
 ;;=283.0^^74^633^92
 ;;^UTILITY(U,$J,358.3,8846,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8846,1,4,0)
 ;;=4^283.0
 ;;^UTILITY(U,$J,358.3,8846,1,5,0)
 ;;=5^Hemolytic Anemia, Autoimmune
 ;;^UTILITY(U,$J,358.3,8846,2)
 ;;=^7079
 ;;^UTILITY(U,$J,358.3,8847,0)
 ;;=282.9^^74^633^90
 ;;^UTILITY(U,$J,358.3,8847,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8847,1,4,0)
 ;;=4^282.9
 ;;^UTILITY(U,$J,358.3,8847,1,5,0)
 ;;=5^Hemolytic Anem, Heredit
 ;;^UTILITY(U,$J,358.3,8847,2)
 ;;=^56578
 ;;^UTILITY(U,$J,358.3,8848,0)
 ;;=283.19^^74^633^93
 ;;^UTILITY(U,$J,358.3,8848,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8848,1,4,0)
 ;;=4^283.19
 ;;^UTILITY(U,$J,358.3,8848,1,5,0)
 ;;=5^Hemolytic Anemia, Microang
 ;;^UTILITY(U,$J,358.3,8848,2)
 ;;=^293664
 ;;^UTILITY(U,$J,358.3,8849,0)
 ;;=280.9^^74^633^97
 ;;^UTILITY(U,$J,358.3,8849,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8849,1,4,0)
 ;;=4^280.9
 ;;^UTILITY(U,$J,358.3,8849,1,5,0)
 ;;=5^Iron Defic Anemia(Unspecified)
 ;;^UTILITY(U,$J,358.3,8849,2)
 ;;=^276946
 ;;^UTILITY(U,$J,358.3,8850,0)
 ;;=285.1^^74^633^95
 ;;^UTILITY(U,$J,358.3,8850,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8850,1,4,0)
 ;;=4^285.1
 ;;^UTILITY(U,$J,358.3,8850,1,5,0)
 ;;=5^Iron Defic Anemia Due To Acute Blood Loss
 ;;^UTILITY(U,$J,358.3,8850,2)
 ;;=^267986
 ;;^UTILITY(U,$J,358.3,8851,0)
 ;;=280.0^^74^633^96
 ;;^UTILITY(U,$J,358.3,8851,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8851,1,4,0)
 ;;=4^280.0
 ;;^UTILITY(U,$J,358.3,8851,1,5,0)
 ;;=5^Iron Defic Anemia Due To Chronic Blood Loss
 ;;^UTILITY(U,$J,358.3,8851,2)
 ;;=^267971
 ;;^UTILITY(U,$J,358.3,8852,0)
 ;;=281.9^^74^633^117
 ;;^UTILITY(U,$J,358.3,8852,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8852,1,4,0)
 ;;=4^281.9
 ;;^UTILITY(U,$J,358.3,8852,1,5,0)
 ;;=5^Nutritional Anemia
 ;;^UTILITY(U,$J,358.3,8852,2)
 ;;=^123801
 ;;^UTILITY(U,$J,358.3,8853,0)
 ;;=281.0^^74^633^140
 ;;^UTILITY(U,$J,358.3,8853,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8853,1,4,0)
 ;;=4^281.0
 ;;^UTILITY(U,$J,358.3,8853,1,5,0)
 ;;=5^Vit B12 Deficiency (Pernicious Anemia)
 ;;^UTILITY(U,$J,358.3,8853,2)
 ;;=^7161
 ;;^UTILITY(U,$J,358.3,8854,0)
 ;;=282.60^^74^633^124
 ;;^UTILITY(U,$J,358.3,8854,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8854,1,4,0)
 ;;=4^282.60
 ;;^UTILITY(U,$J,358.3,8854,1,5,0)
 ;;=5^Sickle-Cell Anemia
 ;;^UTILITY(U,$J,358.3,8854,2)
 ;;=^7188
 ;;^UTILITY(U,$J,358.3,8855,0)
 ;;=282.62^^74^633^125
 ;;^UTILITY(U,$J,358.3,8855,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8855,1,4,0)
 ;;=4^282.62
 ;;^UTILITY(U,$J,358.3,8855,1,5,0)
 ;;=5^Sickle-Cell With Crisis
 ;;^UTILITY(U,$J,358.3,8855,2)
 ;;=^267982
 ;;^UTILITY(U,$J,358.3,8856,0)
 ;;=281.1^^74^633^141
 ;;^UTILITY(U,$J,358.3,8856,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8856,1,4,0)
 ;;=4^281.1
 ;;^UTILITY(U,$J,358.3,8856,1,5,0)
 ;;=5^Vit B12 Deficiency(Dietary)
 ;;^UTILITY(U,$J,358.3,8856,2)
 ;;=^267974
 ;;^UTILITY(U,$J,358.3,8857,0)
 ;;=286.7^^74^633^64
 ;;^UTILITY(U,$J,358.3,8857,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8857,1,4,0)
 ;;=4^286.7
 ;;^UTILITY(U,$J,358.3,8857,1,5,0)
 ;;=5^Coagulation Defect(Any),Acquired
 ;;^UTILITY(U,$J,358.3,8857,2)
 ;;=^2235
 ;;^UTILITY(U,$J,358.3,8858,0)
 ;;=289.9^^74^633^137
 ;;^UTILITY(U,$J,358.3,8858,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8858,1,4,0)
 ;;=4^289.9
 ;;^UTILITY(U,$J,358.3,8858,1,5,0)
 ;;=5^Thrombocytosis, Essential
 ;;^UTILITY(U,$J,358.3,8858,2)
 ;;=^55344
 ;;^UTILITY(U,$J,358.3,8859,0)
 ;;=451.9^^74^633^138
 ;;^UTILITY(U,$J,358.3,8859,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8859,1,4,0)
 ;;=4^451.9
 ;;^UTILITY(U,$J,358.3,8859,1,5,0)
 ;;=5^Thrombophlebitis 
 ;;^UTILITY(U,$J,358.3,8859,2)
 ;;=^93357
 ;;^UTILITY(U,$J,358.3,8860,0)
 ;;=446.6^^74^633^139
 ;;^UTILITY(U,$J,358.3,8860,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8860,1,4,0)
 ;;=4^446.6
 ;;^UTILITY(U,$J,358.3,8860,1,5,0)
 ;;=5^Thrombotic Thrombocytopenic Purpura(TTP)
 ;;^UTILITY(U,$J,358.3,8860,2)
 ;;=^119061
 ;;^UTILITY(U,$J,358.3,8861,0)
 ;;=286.4^^74^633^143
 ;;^UTILITY(U,$J,358.3,8861,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8861,1,4,0)
 ;;=4^286.4
 ;;^UTILITY(U,$J,358.3,8861,1,5,0)
 ;;=5^Von Willebrand'S Disease
 ;;^UTILITY(U,$J,358.3,8861,2)
 ;;=^127267
 ;;^UTILITY(U,$J,358.3,8862,0)
 ;;=204.00^^74^633^17
 ;;^UTILITY(U,$J,358.3,8862,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8862,1,4,0)
 ;;=4^204.00
 ;;^UTILITY(U,$J,358.3,8862,1,5,0)
 ;;=5^All, W/O Remission
 ;;^UTILITY(U,$J,358.3,8862,2)
 ;;=^267521
 ;;^UTILITY(U,$J,358.3,8863,0)
 ;;=204.01^^74^633^16
 ;;^UTILITY(U,$J,358.3,8863,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8863,1,4,0)
 ;;=4^204.01
 ;;^UTILITY(U,$J,358.3,8863,1,5,0)
 ;;=5^All, In Remission
 ;;^UTILITY(U,$J,358.3,8863,2)
 ;;=^267522
 ;;^UTILITY(U,$J,358.3,8864,0)
 ;;=204.10^^74^633^61
 ;;^UTILITY(U,$J,358.3,8864,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8864,1,4,0)
 ;;=4^204.10
 ;;^UTILITY(U,$J,358.3,8864,1,5,0)
 ;;=5^Cll, W/O Remission
 ;;^UTILITY(U,$J,358.3,8864,2)
 ;;=^267523
 ;;^UTILITY(U,$J,358.3,8865,0)
 ;;=204.11^^74^633^60
 ;;^UTILITY(U,$J,358.3,8865,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8865,1,4,0)
 ;;=4^204.11
 ;;^UTILITY(U,$J,358.3,8865,1,5,0)
 ;;=5^Cll, In Remission
 ;;^UTILITY(U,$J,358.3,8865,2)
 ;;=^267524
 ;;^UTILITY(U,$J,358.3,8866,0)
 ;;=202.41^^74^633^87
 ;;^UTILITY(U,$J,358.3,8866,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8866,1,4,0)
 ;;=4^202.41
 ;;^UTILITY(U,$J,358.3,8866,1,5,0)
 ;;=5^Hairy Cell Leukemia,Unspec Site
 ;;^UTILITY(U,$J,358.3,8866,2)
 ;;=^267472
 ;;^UTILITY(U,$J,358.3,8867,0)
 ;;=201.90^^74^633^94
 ;;^UTILITY(U,$J,358.3,8867,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8867,1,4,0)
 ;;=4^201.90
 ;;^UTILITY(U,$J,358.3,8867,1,5,0)
 ;;=5^Hodgkin'S Lymphoma, Unpsec Type & Site
 ;;^UTILITY(U,$J,358.3,8867,2)
 ;;=^267430
 ;;^UTILITY(U,$J,358.3,8868,0)
 ;;=785.6^^74^633^101
 ;;^UTILITY(U,$J,358.3,8868,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8868,1,4,0)
 ;;=4^785.6
 ;;^UTILITY(U,$J,358.3,8868,1,5,0)
 ;;=5^Lymphadenopathy
 ;;^UTILITY(U,$J,358.3,8868,2)
 ;;=^72368
 ;;^UTILITY(U,$J,358.3,8869,0)
 ;;=200.20^^74^633^102
 ;;^UTILITY(U,$J,358.3,8869,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8869,1,4,0)
 ;;=4^200.20
 ;;^UTILITY(U,$J,358.3,8869,1,5,0)
 ;;=5^Lymphoma, Burkitt'S, Unspecified Sites
 ;;^UTILITY(U,$J,358.3,8869,2)
 ;;=^17529
 ;;^UTILITY(U,$J,358.3,8870,0)
 ;;=202.00^^74^633^104
 ;;^UTILITY(U,$J,358.3,8870,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8870,1,4,0)
 ;;=4^202.00
 ;;^UTILITY(U,$J,358.3,8870,1,5,0)
 ;;=5^Lymphoma, Low-Grade, Unspec Site
 ;;^UTILITY(U,$J,358.3,8870,2)
 ;;=^72606
 ;;^UTILITY(U,$J,358.3,8871,0)
 ;;=200.10^^74^633^103
 ;;^UTILITY(U,$J,358.3,8871,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8871,1,4,0)
 ;;=4^200.10
 ;;^UTILITY(U,$J,358.3,8871,1,5,0)
 ;;=5^Lymphoma, Int Or High Grade, Unspec Site
 ;;^UTILITY(U,$J,358.3,8871,2)
 ;;=^175886
 ;;^UTILITY(U,$J,358.3,8872,0)
 ;;=273.3^^74^633^105
 ;;^UTILITY(U,$J,358.3,8872,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8872,1,4,0)
 ;;=4^273.3
 ;;^UTILITY(U,$J,358.3,8872,1,5,0)
 ;;=5^Macroglobulinemia
 ;;^UTILITY(U,$J,358.3,8872,2)
 ;;=^73013
 ;;^UTILITY(U,$J,358.3,8873,0)
 ;;=203.00^^74^633^115
 ;;^UTILITY(U,$J,358.3,8873,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8873,1,4,0)
 ;;=4^203.00
 ;;^UTILITY(U,$J,358.3,8873,1,5,0)
 ;;=5^Multiple Myeloma W/O Rem
 ;;^UTILITY(U,$J,358.3,8873,2)
 ;;=^267514
 ;;^UTILITY(U,$J,358.3,8874,0)
 ;;=203.01^^74^633^114
 ;;^UTILITY(U,$J,358.3,8874,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8874,1,4,0)
 ;;=4^203.01
 ;;^UTILITY(U,$J,358.3,8874,1,5,0)
 ;;=5^Multiple Myeloma In Remission
 ;;^UTILITY(U,$J,358.3,8874,2)
 ;;=^267515
 ;;^UTILITY(U,$J,358.3,8875,0)
 ;;=238.6^^74^633^118
 ;;^UTILITY(U,$J,358.3,8875,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8875,1,4,0)
 ;;=4^238.6
 ;;^UTILITY(U,$J,358.3,8875,1,5,0)
 ;;=5^Plasmacytoma Nos
 ;;^UTILITY(U,$J,358.3,8875,2)
 ;;=^81973
 ;;^UTILITY(U,$J,358.3,8876,0)
 ;;=205.00^^74^633^19
 ;;^UTILITY(U,$J,358.3,8876,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8876,1,4,0)
 ;;=4^205.00
 ;;^UTILITY(U,$J,358.3,8876,1,5,0)
 ;;=5^Aml, W/O Remission
 ;;^UTILITY(U,$J,358.3,8876,2)
 ;;=^267531
 ;;^UTILITY(U,$J,358.3,8877,0)
 ;;=205.01^^74^633^18
 ;;^UTILITY(U,$J,358.3,8877,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8877,1,4,0)
 ;;=4^205.01
 ;;^UTILITY(U,$J,358.3,8877,1,5,0)
 ;;=5^Aml, In Remission
 ;;^UTILITY(U,$J,358.3,8877,2)
 ;;=^267532
 ;;^UTILITY(U,$J,358.3,8878,0)
 ;;=205.10^^74^633^63
 ;;^UTILITY(U,$J,358.3,8878,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8878,1,4,0)
 ;;=4^205.10
 ;;^UTILITY(U,$J,358.3,8878,1,5,0)
 ;;=5^Cml, W/O Remission
 ;;^UTILITY(U,$J,358.3,8878,2)
 ;;=^267533
 ;;^UTILITY(U,$J,358.3,8879,0)
 ;;=205.11^^74^633^62
 ;;^UTILITY(U,$J,358.3,8879,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8879,1,4,0)
 ;;=4^205.11
 ;;^UTILITY(U,$J,358.3,8879,1,5,0)
 ;;=5^Cml, In Remission
 ;;^UTILITY(U,$J,358.3,8879,2)
 ;;=^267534
 ;;^UTILITY(U,$J,358.3,8880,0)
 ;;=289.0^^74^633^65
 ;;^UTILITY(U,$J,358.3,8880,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8880,1,4,0)
 ;;=4^289.0
 ;;^UTILITY(U,$J,358.3,8880,1,5,0)
 ;;=5^Erthryocytosis, Secondary
 ;;^UTILITY(U,$J,358.3,8880,2)
 ;;=^186856
 ;;^UTILITY(U,$J,358.3,8881,0)
 ;;=238.4^^74^633^119
 ;;^UTILITY(U,$J,358.3,8881,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8881,1,4,0)
 ;;=4^238.4
 ;;^UTILITY(U,$J,358.3,8881,1,5,0)
 ;;=5^Polycytheria Rubra Vera
 ;;^UTILITY(U,$J,358.3,8881,2)
 ;;=^96105
 ;;^UTILITY(U,$J,358.3,8882,0)
 ;;=V58.61^^74^633^144
 ;;^UTILITY(U,$J,358.3,8882,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,8882,1,4,0)
 ;;=4^V58.61
 ;;^UTILITY(U,$J,358.3,8882,1,5,0)
 ;;=5^Warfarin/Coumadin Use
