IBDEI04M ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,5824,2)
 ;;=^270339
 ;;^UTILITY(U,$J,358.3,5825,0)
 ;;=580.9^^55^477^7
 ;;^UTILITY(U,$J,358.3,5825,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5825,1,3,0)
 ;;=3^580.9
 ;;^UTILITY(U,$J,358.3,5825,1,4,0)
 ;;=4^NEPHRITIS,AC NOS
 ;;^UTILITY(U,$J,358.3,5825,2)
 ;;=^270342
 ;;^UTILITY(U,$J,358.3,5826,0)
 ;;=582.0^^55^477^14
 ;;^UTILITY(U,$J,358.3,5826,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5826,1,3,0)
 ;;=3^582.0
 ;;^UTILITY(U,$J,358.3,5826,1,4,0)
 ;;=4^NEPHRITIS,CHR PROLIFERATIVE
 ;;^UTILITY(U,$J,358.3,5826,2)
 ;;=^270347
 ;;^UTILITY(U,$J,358.3,5827,0)
 ;;=582.1^^55^477^12
 ;;^UTILITY(U,$J,358.3,5827,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5827,1,3,0)
 ;;=3^582.1
 ;;^UTILITY(U,$J,358.3,5827,1,4,0)
 ;;=4^NEPHRITIS,CHR MEMBRANOUS
 ;;^UTILITY(U,$J,358.3,5827,2)
 ;;=^24388
 ;;^UTILITY(U,$J,358.3,5828,0)
 ;;=582.2^^55^477^11
 ;;^UTILITY(U,$J,358.3,5828,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5828,1,3,0)
 ;;=3^582.2
 ;;^UTILITY(U,$J,358.3,5828,1,4,0)
 ;;=4^NEPHRITIS,CHR MEMBRANOPROLIF
 ;;^UTILITY(U,$J,358.3,5828,2)
 ;;=^24386
 ;;^UTILITY(U,$J,358.3,5829,0)
 ;;=582.4^^55^477^15
 ;;^UTILITY(U,$J,358.3,5829,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5829,1,3,0)
 ;;=3^582.4
 ;;^UTILITY(U,$J,358.3,5829,1,4,0)
 ;;=4^NEPHRITIS,CHR RAPID PROGR
 ;;^UTILITY(U,$J,358.3,5829,2)
 ;;=^270348
 ;;^UTILITY(U,$J,358.3,5830,0)
 ;;=582.81^^55^477^10
 ;;^UTILITY(U,$J,358.3,5830,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5830,1,3,0)
 ;;=3^582.81
 ;;^UTILITY(U,$J,358.3,5830,1,4,0)
 ;;=4^NEPHRITIS,CHR IN OTH DIS
 ;;^UTILITY(U,$J,358.3,5830,2)
 ;;=^270350
 ;;^UTILITY(U,$J,358.3,5831,0)
 ;;=582.9^^55^477^13
 ;;^UTILITY(U,$J,358.3,5831,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5831,1,3,0)
 ;;=3^582.9
 ;;^UTILITY(U,$J,358.3,5831,1,4,0)
 ;;=4^NEPHRITIS,CHR NOS
 ;;^UTILITY(U,$J,358.3,5831,2)
 ;;=^270351
 ;;^UTILITY(U,$J,358.3,5832,0)
 ;;=583.81^^55^477^5
 ;;^UTILITY(U,$J,358.3,5832,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5832,1,3,0)
 ;;=3^583.81
 ;;^UTILITY(U,$J,358.3,5832,1,4,0)
 ;;=4^NEPHRITIS IN OTH DIS
 ;;^UTILITY(U,$J,358.3,5832,2)
 ;;=^82243
 ;;^UTILITY(U,$J,358.3,5833,0)
 ;;=583.81^^55^477^16
 ;;^UTILITY(U,$J,358.3,5833,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5833,1,3,0)
 ;;=3^583.81
 ;;^UTILITY(U,$J,358.3,5833,1,4,0)
 ;;=4^SECONDARY GLOMERONEPHRITIS
 ;;^UTILITY(U,$J,358.3,5833,2)
 ;;=^82243
 ;;^UTILITY(U,$J,358.3,5834,0)
 ;;=583.9^^55^477^6
 ;;^UTILITY(U,$J,358.3,5834,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5834,1,3,0)
 ;;=3^583.9
 ;;^UTILITY(U,$J,358.3,5834,1,4,0)
 ;;=4^NEPHRITIS NOS
 ;;^UTILITY(U,$J,358.3,5834,2)
 ;;=^83446
 ;;^UTILITY(U,$J,358.3,5835,0)
 ;;=710.0^^55^477^17
 ;;^UTILITY(U,$J,358.3,5835,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5835,1,3,0)
 ;;=3^710.0
 ;;^UTILITY(U,$J,358.3,5835,1,4,0)
 ;;=4^SYSTEMIC LUPUS
 ;;^UTILITY(U,$J,358.3,5835,2)
 ;;=^72159^582.81
 ;;^UTILITY(U,$J,358.3,5836,0)
 ;;=582.89^^55^478^2
 ;;^UTILITY(U,$J,358.3,5836,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5836,1,3,0)
 ;;=3^582.89
 ;;^UTILITY(U,$J,358.3,5836,1,4,0)
 ;;=4^NEPHRITIS INTERSTITIAL,CHR
 ;;^UTILITY(U,$J,358.3,5836,2)
 ;;=^270349
 ;;^UTILITY(U,$J,358.3,5837,0)
 ;;=580.89^^55^478^1
 ;;^UTILITY(U,$J,358.3,5837,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5837,1,3,0)
 ;;=3^580.89
 ;;^UTILITY(U,$J,358.3,5837,1,4,0)
 ;;=4^NEPHRITIS INTERSTITIAL,AC
 ;;^UTILITY(U,$J,358.3,5837,2)
 ;;=^270340
 ;;^UTILITY(U,$J,358.3,5838,0)
 ;;=583.89^^55^478^3
 ;;^UTILITY(U,$J,358.3,5838,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5838,1,3,0)
 ;;=3^583.89
 ;;^UTILITY(U,$J,358.3,5838,1,4,0)
 ;;=4^NEPHROPATHY,ANALGESIC
 ;;^UTILITY(U,$J,358.3,5838,2)
 ;;=^83443
 ;;^UTILITY(U,$J,358.3,5839,0)
 ;;=042.^^55^479^1
 ;;^UTILITY(U,$J,358.3,5839,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5839,1,3,0)
 ;;=3^042.
 ;;^UTILITY(U,$J,358.3,5839,1,4,0)
 ;;=4^AIDS
 ;;^UTILITY(U,$J,358.3,5839,2)
 ;;=^266500
 ;;^UTILITY(U,$J,358.3,5840,0)
 ;;=581.1^^55^479^2
 ;;^UTILITY(U,$J,358.3,5840,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5840,1,3,0)
 ;;=3^581.1
 ;;^UTILITY(U,$J,358.3,5840,1,4,0)
 ;;=4^NEPHRITIS EPIMEMBRANOUS (FSGS)
 ;;^UTILITY(U,$J,358.3,5840,2)
 ;;=^270344
 ;;^UTILITY(U,$J,358.3,5841,0)
 ;;=581.2^^55^479^3
 ;;^UTILITY(U,$J,358.3,5841,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5841,1,3,0)
 ;;=3^581.2
 ;;^UTILITY(U,$J,358.3,5841,1,4,0)
 ;;=4^NEPHROSIS MEMBRANOPROL
 ;;^UTILITY(U,$J,358.3,5841,2)
 ;;=^270345
 ;;^UTILITY(U,$J,358.3,5842,0)
 ;;=581.3^^55^479^4
 ;;^UTILITY(U,$J,358.3,5842,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5842,1,3,0)
 ;;=3^581.3
 ;;^UTILITY(U,$J,358.3,5842,1,4,0)
 ;;=4^NEPHROSIS,MINIMAL CHANGE
 ;;^UTILITY(U,$J,358.3,5842,2)
 ;;=^82320
 ;;^UTILITY(U,$J,358.3,5843,0)
 ;;=581.9^^55^479^5
 ;;^UTILITY(U,$J,358.3,5843,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5843,1,3,0)
 ;;=3^581.9
 ;;^UTILITY(U,$J,358.3,5843,1,4,0)
 ;;=4^NEPHROTIC SYNDROME NOS
 ;;^UTILITY(U,$J,358.3,5843,2)
 ;;=^82357
 ;;^UTILITY(U,$J,358.3,5844,0)
 ;;=446.0^^55^479^6
 ;;^UTILITY(U,$J,358.3,5844,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5844,1,3,0)
 ;;=3^446.0
 ;;^UTILITY(U,$J,358.3,5844,1,4,0)
 ;;=4^POLYARTERITIS
 ;;^UTILITY(U,$J,358.3,5844,2)
 ;;=^91874
 ;;^UTILITY(U,$J,358.3,5845,0)
 ;;=581.81^^55^479^7
 ;;^UTILITY(U,$J,358.3,5845,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5845,1,3,0)
 ;;=3^581.81
 ;;^UTILITY(U,$J,358.3,5845,1,4,0)
 ;;=4^SECONDARY NEPHROTIC SYNDROME
 ;;^UTILITY(U,$J,358.3,5845,2)
 ;;=^270346
 ;;^UTILITY(U,$J,358.3,5846,0)
 ;;=710.0^^55^479^8
 ;;^UTILITY(U,$J,358.3,5846,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5846,1,3,0)
 ;;=3^710.0
 ;;^UTILITY(U,$J,358.3,5846,1,4,0)
 ;;=4^SYSTEMIC LUPUS
 ;;^UTILITY(U,$J,358.3,5846,2)
 ;;=^72159
 ;;^UTILITY(U,$J,358.3,5847,0)
 ;;=596.0^^55^480^1
 ;;^UTILITY(U,$J,358.3,5847,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5847,1,3,0)
 ;;=3^596.0
 ;;^UTILITY(U,$J,358.3,5847,1,4,0)
 ;;=4^BLADDER NECK OBSTRUCTION
 ;;^UTILITY(U,$J,358.3,5847,2)
 ;;=^15144
 ;;^UTILITY(U,$J,358.3,5848,0)
 ;;=592.0^^55^480^2
 ;;^UTILITY(U,$J,358.3,5848,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5848,1,3,0)
 ;;=3^592.0
 ;;^UTILITY(U,$J,358.3,5848,1,4,0)
 ;;=4^CALCULUS OF KIDNEY
 ;;^UTILITY(U,$J,358.3,5848,2)
 ;;=^67056
 ;;^UTILITY(U,$J,358.3,5849,0)
 ;;=592.1^^55^480^3
 ;;^UTILITY(U,$J,358.3,5849,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5849,1,3,0)
 ;;=3^592.1
 ;;^UTILITY(U,$J,358.3,5849,1,4,0)
 ;;=4^CALCULUS OF URETER
 ;;^UTILITY(U,$J,358.3,5849,2)
 ;;=^124125
 ;;^UTILITY(U,$J,358.3,5850,0)
 ;;=591.^^55^480^4
 ;;^UTILITY(U,$J,358.3,5850,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5850,1,3,0)
 ;;=3^591.
 ;;^UTILITY(U,$J,358.3,5850,1,4,0)
 ;;=4^HYDRONEPHROSIS
 ;;^UTILITY(U,$J,358.3,5850,2)
 ;;=^59672
 ;;^UTILITY(U,$J,358.3,5851,0)
 ;;=344.61^^55^480^5
 ;;^UTILITY(U,$J,358.3,5851,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5851,1,3,0)
 ;;=3^344.61
 ;;^UTILITY(U,$J,358.3,5851,1,4,0)
 ;;=4^NEUROGENIC BLADDER
 ;;^UTILITY(U,$J,358.3,5851,2)
 ;;=^20561
 ;;^UTILITY(U,$J,358.3,5852,0)
 ;;=753.20^^55^480^6
 ;;^UTILITY(U,$J,358.3,5852,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5852,1,3,0)
 ;;=3^753.20
 ;;^UTILITY(U,$J,358.3,5852,1,4,0)
 ;;=4^UNSP OBS DEF/RENAL PEL&URET
 ;;^UTILITY(U,$J,358.3,5852,2)
 ;;=^272985
 ;;^UTILITY(U,$J,358.3,5853,0)
 ;;=599.60^^55^480^8
 ;;^UTILITY(U,$J,358.3,5853,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5853,1,3,0)
 ;;=3^599.60
 ;;^UTILITY(U,$J,358.3,5853,1,4,0)
 ;;=4^URINARY OBSTRUCTION NOS
 ;;^UTILITY(U,$J,358.3,5853,2)
 ;;=^332849
 ;;^UTILITY(U,$J,358.3,5854,0)
 ;;=599.69^^55^480^7
 ;;^UTILITY(U,$J,358.3,5854,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5854,1,3,0)
 ;;=3^599.69
 ;;^UTILITY(U,$J,358.3,5854,1,4,0)
 ;;=4^URINARY OBSTRUCTION NEC
 ;;^UTILITY(U,$J,358.3,5854,2)
 ;;=^332813
 ;;^UTILITY(U,$J,358.3,5855,0)
 ;;=150.9^^55^481^6
 ;;^UTILITY(U,$J,358.3,5855,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5855,1,3,0)
 ;;=3^150.9
 ;;^UTILITY(U,$J,358.3,5855,1,4,0)
 ;;=4^ESOPHAGEAL CANCER
 ;;^UTILITY(U,$J,358.3,5855,2)
 ;;=^267055
 ;;^UTILITY(U,$J,358.3,5856,0)
 ;;=154.0^^55^481^5
 ;;^UTILITY(U,$J,358.3,5856,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5856,1,3,0)
 ;;=3^154.0
 ;;^UTILITY(U,$J,358.3,5856,1,4,0)
 ;;=4^COLON CANCER
 ;;^UTILITY(U,$J,358.3,5856,2)
 ;;=^267089
 ;;^UTILITY(U,$J,358.3,5857,0)
 ;;=155.0^^55^481^10
 ;;^UTILITY(U,$J,358.3,5857,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5857,1,3,0)
 ;;=3^155.0
 ;;^UTILITY(U,$J,358.3,5857,1,4,0)
 ;;=4^LIVER CANCER
 ;;^UTILITY(U,$J,358.3,5857,2)
 ;;=^73526
 ;;^UTILITY(U,$J,358.3,5858,0)
 ;;=157.9^^55^481^17
 ;;^UTILITY(U,$J,358.3,5858,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5858,1,3,0)
 ;;=3^157.9
 ;;^UTILITY(U,$J,358.3,5858,1,4,0)
 ;;=4^PANCREATIC CANCER
 ;;^UTILITY(U,$J,358.3,5858,2)
 ;;=^267103
 ;;^UTILITY(U,$J,358.3,5859,0)
 ;;=162.9^^55^481^11
 ;;^UTILITY(U,$J,358.3,5859,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5859,1,3,0)
 ;;=3^162.9
 ;;^UTILITY(U,$J,358.3,5859,1,4,0)
 ;;=4^LUNG CANCER
 ;;^UTILITY(U,$J,358.3,5859,2)
 ;;=^73521
 ;;^UTILITY(U,$J,358.3,5860,0)
 ;;=170.9^^55^481^2
 ;;^UTILITY(U,$J,358.3,5860,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5860,1,3,0)
 ;;=3^170.9
 ;;^UTILITY(U,$J,358.3,5860,1,4,0)
 ;;=4^BONE CANCER
 ;;^UTILITY(U,$J,358.3,5860,2)
 ;;=^267155
 ;;^UTILITY(U,$J,358.3,5861,0)
 ;;=172.9^^55^481^14
 ;;^UTILITY(U,$J,358.3,5861,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5861,1,3,0)
 ;;=3^172.9
 ;;^UTILITY(U,$J,358.3,5861,1,4,0)
 ;;=4^MELANOMA
 ;;^UTILITY(U,$J,358.3,5861,2)
 ;;=^75462
 ;;^UTILITY(U,$J,358.3,5862,0)
 ;;=173.81^^55^481^1
 ;;^UTILITY(U,$J,358.3,5862,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5862,1,3,0)
 ;;=3^173.81
 ;;^UTILITY(U,$J,358.3,5862,1,4,0)
 ;;=4^BCCA SKIN,SITE NEC
 ;;^UTILITY(U,$J,358.3,5862,2)
 ;;=^340488
 ;;^UTILITY(U,$J,358.3,5863,0)
 ;;=173.82^^55^481^19
 ;;^UTILITY(U,$J,358.3,5863,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,5863,1,3,0)
 ;;=3^173.82
