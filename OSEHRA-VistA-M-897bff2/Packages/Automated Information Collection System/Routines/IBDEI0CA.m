IBDEI0CA ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,16337,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16337,1,3,0)
 ;;=3^757.5
 ;;^UTILITY(U,$J,358.3,16337,1,5,0)
 ;;=5^Onchodystrophy/Onycholysis; Congenital
 ;;^UTILITY(U,$J,358.3,16337,2)
 ;;=^273090
 ;;^UTILITY(U,$J,358.3,16338,0)
 ;;=703.8^^119^1029^3
 ;;^UTILITY(U,$J,358.3,16338,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16338,1,3,0)
 ;;=3^703.8
 ;;^UTILITY(U,$J,358.3,16338,1,5,0)
 ;;=5^Onchodystrophy/Onycholysis; Acquired
 ;;^UTILITY(U,$J,358.3,16338,2)
 ;;=^271926
 ;;^UTILITY(U,$J,358.3,16339,0)
 ;;=110.1^^119^1029^4
 ;;^UTILITY(U,$J,358.3,16339,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16339,1,3,0)
 ;;=3^110.1
 ;;^UTILITY(U,$J,358.3,16339,1,5,0)
 ;;=5^Onychomycosis
 ;;^UTILITY(U,$J,358.3,16339,2)
 ;;=^33173
 ;;^UTILITY(U,$J,358.3,16340,0)
 ;;=715.37^^119^1029^6
 ;;^UTILITY(U,$J,358.3,16340,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16340,1,3,0)
 ;;=3^715.37
 ;;^UTILITY(U,$J,358.3,16340,1,5,0)
 ;;=5^Osteoarthrosis, localized, not specified whether primary or secondary; ankle/foot/toe
 ;;^UTILITY(U,$J,358.3,16340,2)
 ;;=^272156
 ;;^UTILITY(U,$J,358.3,16341,0)
 ;;=715.90^^119^1029^7
 ;;^UTILITY(U,$J,358.3,16341,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16341,1,3,0)
 ;;=3^715.90
 ;;^UTILITY(U,$J,358.3,16341,1,5,0)
 ;;=5^Osteoarthrosis, unspecified whether generalized or localized
 ;;^UTILITY(U,$J,358.3,16341,2)
 ;;=^272161
 ;;^UTILITY(U,$J,358.3,16342,0)
 ;;=730.07^^119^1029^8
 ;;^UTILITY(U,$J,358.3,16342,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16342,1,3,0)
 ;;=3^730.07
 ;;^UTILITY(U,$J,358.3,16342,1,5,0)
 ;;=5^Osteomyelitis, Acute; ankle/foot/toe
 ;;^UTILITY(U,$J,358.3,16342,2)
 ;;=^272619
 ;;^UTILITY(U,$J,358.3,16343,0)
 ;;=730.17^^119^1029^9
 ;;^UTILITY(U,$J,358.3,16343,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16343,1,3,0)
 ;;=3^730.17
 ;;^UTILITY(U,$J,358.3,16343,1,5,0)
 ;;=5^Osteomyelitis, Chronic; ankle/foot/toe
 ;;^UTILITY(U,$J,358.3,16343,2)
 ;;=^272628
 ;;^UTILITY(U,$J,358.3,16344,0)
 ;;=730.27^^119^1029^10
 ;;^UTILITY(U,$J,358.3,16344,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16344,1,3,0)
 ;;=3^730.27
 ;;^UTILITY(U,$J,358.3,16344,1,5,0)
 ;;=5^Osteomyelitis, Unspecified; ankle/foot/toe
 ;;^UTILITY(U,$J,358.3,16344,2)
 ;;=^272637
 ;;^UTILITY(U,$J,358.3,16345,0)
 ;;=732.5^^119^1029^11
 ;;^UTILITY(U,$J,358.3,16345,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16345,1,3,0)
 ;;=3^732.5
 ;;^UTILITY(U,$J,358.3,16345,1,5,0)
 ;;=5^Osteochondrosis/Apophysitis, Juvenile; foot
 ;;^UTILITY(U,$J,358.3,16345,2)
 ;;=^272687
 ;;^UTILITY(U,$J,358.3,16346,0)
 ;;=V54.89^^119^1029^5
 ;;^UTILITY(U,$J,358.3,16346,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16346,1,3,0)
 ;;=3^V54.89
 ;;^UTILITY(U,$J,358.3,16346,1,5,0)
 ;;=5^Removal of External Fix Device
 ;;^UTILITY(U,$J,358.3,16346,2)
 ;;=Removal of External Fix Device^328685
 ;;^UTILITY(U,$J,358.3,16347,0)
 ;;=681.11^^119^1030^24
 ;;^UTILITY(U,$J,358.3,16347,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16347,1,3,0)
 ;;=3^681.11
 ;;^UTILITY(U,$J,358.3,16347,1,5,0)
 ;;=5^Paronychia/Onychia
 ;;^UTILITY(U,$J,358.3,16347,2)
 ;;=Paronychia/Onychia^271886
 ;;^UTILITY(U,$J,358.3,16348,0)
 ;;=730.07^^119^1030^25
 ;;^UTILITY(U,$J,358.3,16348,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16348,1,3,0)
 ;;=3^730.07
 ;;^UTILITY(U,$J,358.3,16348,1,5,0)
 ;;=5^Periosteotis
 ;;^UTILITY(U,$J,358.3,16348,2)
 ;;=Periostiitis^272619
 ;;^UTILITY(U,$J,358.3,16349,0)
 ;;=736.73^^119^1030^26
 ;;^UTILITY(U,$J,358.3,16349,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16349,1,3,0)
 ;;=3^736.73
 ;;^UTILITY(U,$J,358.3,16349,1,5,0)
 ;;=5^Pes Cavus
 ;;^UTILITY(U,$J,358.3,16349,2)
 ;;=Pes Cavus^272745
 ;;^UTILITY(U,$J,358.3,16350,0)
 ;;=754.61^^119^1030^27
 ;;^UTILITY(U,$J,358.3,16350,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16350,1,3,0)
 ;;=3^754.61
 ;;^UTILITY(U,$J,358.3,16350,1,5,0)
 ;;=5^Pes Planovalgus (Congenital)
 ;;^UTILITY(U,$J,358.3,16350,2)
 ;;=^273010
 ;;^UTILITY(U,$J,358.3,16351,0)
 ;;=734.^^119^1030^28
 ;;^UTILITY(U,$J,358.3,16351,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16351,1,3,0)
 ;;=3^734.
 ;;^UTILITY(U,$J,358.3,16351,1,5,0)
 ;;=5^Pes Planus
 ;;^UTILITY(U,$J,358.3,16351,2)
 ;;=Pes Planus^46756
 ;;^UTILITY(U,$J,358.3,16352,0)
 ;;=451.11^^119^1030^29
 ;;^UTILITY(U,$J,358.3,16352,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16352,1,3,0)
 ;;=3^451.11
 ;;^UTILITY(U,$J,358.3,16352,1,5,0)
 ;;=5^Phlebitis, Deep
 ;;^UTILITY(U,$J,358.3,16352,2)
 ;;=^269811
 ;;^UTILITY(U,$J,358.3,16353,0)
 ;;=451.0^^119^1030^30
 ;;^UTILITY(U,$J,358.3,16353,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16353,1,3,0)
 ;;=3^451.0
 ;;^UTILITY(U,$J,358.3,16353,1,5,0)
 ;;=5^Phlebitis, Superficial
 ;;^UTILITY(U,$J,358.3,16353,2)
 ;;=^269809
 ;;^UTILITY(U,$J,358.3,16354,0)
 ;;=728.71^^119^1030^31
 ;;^UTILITY(U,$J,358.3,16354,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16354,1,3,0)
 ;;=3^728.71
 ;;^UTILITY(U,$J,358.3,16354,1,5,0)
 ;;=5^Plantar Fibromatosis/Fasciitis
 ;;^UTILITY(U,$J,358.3,16354,2)
 ;;=^272598
 ;;^UTILITY(U,$J,358.3,16355,0)
 ;;=757.39^^119^1030^32
 ;;^UTILITY(U,$J,358.3,16355,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16355,1,3,0)
 ;;=3^757.39
 ;;^UTILITY(U,$J,358.3,16355,1,5,0)
 ;;=5^Porokeratosis
 ;;^UTILITY(U,$J,358.3,16355,2)
 ;;=^87938
 ;;^UTILITY(U,$J,358.3,16356,0)
 ;;=696.1^^119^1030^33
 ;;^UTILITY(U,$J,358.3,16356,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16356,1,3,0)
 ;;=3^696.1
 ;;^UTILITY(U,$J,358.3,16356,1,5,0)
 ;;=5^Psoriaisis
 ;;^UTILITY(U,$J,358.3,16356,2)
 ;;=^87816
 ;;^UTILITY(U,$J,358.3,16357,0)
 ;;=443.9^^119^1030^34
 ;;^UTILITY(U,$J,358.3,16357,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16357,1,3,0)
 ;;=3^443.9
 ;;^UTILITY(U,$J,358.3,16357,1,5,0)
 ;;=5^Pvd
 ;;^UTILITY(U,$J,358.3,16357,2)
 ;;=^184182
 ;;^UTILITY(U,$J,358.3,16358,0)
 ;;=686.1^^119^1030^35
 ;;^UTILITY(U,$J,358.3,16358,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16358,1,3,0)
 ;;=3^686.1
 ;;^UTILITY(U,$J,358.3,16358,1,5,0)
 ;;=5^Pyogenic Granuloma
 ;;^UTILITY(U,$J,358.3,16358,2)
 ;;=^101548
 ;;^UTILITY(U,$J,358.3,16359,0)
 ;;=36^1^119^1030^36^-Post Op Codes^1^1
 ;;^UTILITY(U,$J,358.3,16359,1,0)
 ;;=^358.31IA^0^0
 ;;^UTILITY(U,$J,358.3,16360,0)
 ;;=V54.89^^119^1030^38
 ;;^UTILITY(U,$J,358.3,16360,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16360,1,3,0)
 ;;=3^V54.89
 ;;^UTILITY(U,$J,358.3,16360,1,5,0)
 ;;=5^Other Aftercare
 ;;^UTILITY(U,$J,358.3,16360,2)
 ;;=^328685
 ;;^UTILITY(U,$J,358.3,16361,0)
 ;;=V67.09^^119^1030^39
 ;;^UTILITY(U,$J,358.3,16361,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16361,1,3,0)
 ;;=3^V67.09
 ;;^UTILITY(U,$J,358.3,16361,1,5,0)
 ;;=5^F/U Visit After Surgery
 ;;^UTILITY(U,$J,358.3,16361,2)
 ;;=^322080
 ;;^UTILITY(U,$J,358.3,16362,0)
 ;;=V54.01^^119^1030^37
 ;;^UTILITY(U,$J,358.3,16362,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16362,1,3,0)
 ;;=3^V54.01
 ;;^UTILITY(U,$J,358.3,16362,1,5,0)
 ;;=5^Removal Int Fixation Dev
 ;;^UTILITY(U,$J,358.3,16362,2)
 ;;=^329975
 ;;^UTILITY(U,$J,358.3,16363,0)
 ;;=337.22^^119^1031^2
 ;;^UTILITY(U,$J,358.3,16363,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16363,1,3,0)
 ;;=3^337.22
 ;;^UTILITY(U,$J,358.3,16363,1,5,0)
 ;;=5^Reflex Sympathetic Dystrophy LE
 ;;^UTILITY(U,$J,358.3,16363,2)
 ;;=^295725
 ;;^UTILITY(U,$J,358.3,16364,0)
 ;;=845.09^^119^1031^3
 ;;^UTILITY(U,$J,358.3,16364,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16364,1,3,0)
 ;;=3^845.09
 ;;^UTILITY(U,$J,358.3,16364,1,5,0)
 ;;=5^Rupture of Achilles Tendon
 ;;^UTILITY(U,$J,358.3,16364,2)
 ;;=Rupture of Achilles Tendon^274511
 ;;^UTILITY(U,$J,358.3,16365,0)
 ;;=845.00^^119^1031^4
 ;;^UTILITY(U,$J,358.3,16365,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16365,1,3,0)
 ;;=3^845.00
 ;;^UTILITY(U,$J,358.3,16365,1,5,0)
 ;;=5^Rupture of Ankle Tendon
 ;;^UTILITY(U,$J,358.3,16365,2)
 ;;=Rupture of Ankle Tendon^274507
 ;;^UTILITY(U,$J,358.3,16366,0)
 ;;=.5^1^119^1032^.5^Traumatic- See Sprains^^1
 ;;^UTILITY(U,$J,358.3,16366,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16366,1,3,0)
 ;;=3
 ;;^UTILITY(U,$J,358.3,16366,1,5,0)
 ;;=5
 ;;^UTILITY(U,$J,358.3,16367,0)
 ;;=709.2^^119^1032^2
 ;;^UTILITY(U,$J,358.3,16367,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16367,1,3,0)
 ;;=3^709.2
 ;;^UTILITY(U,$J,358.3,16367,1,5,0)
 ;;=5^Scarring
 ;;^UTILITY(U,$J,358.3,16367,2)
 ;;=^108131
 ;;^UTILITY(U,$J,358.3,16368,0)
 ;;=733.99^^119^1032^3
 ;;^UTILITY(U,$J,358.3,16368,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16368,1,3,0)
 ;;=3^733.99
 ;;^UTILITY(U,$J,358.3,16368,1,5,0)
 ;;=5^Sesamoiditis
 ;;^UTILITY(U,$J,358.3,16368,2)
 ;;=Sesamoiditisi^87503
 ;;^UTILITY(U,$J,358.3,16369,0)
 ;;=844.9^^119^1032^5
 ;;^UTILITY(U,$J,358.3,16369,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16369,1,3,0)
 ;;=3^844.9
 ;;^UTILITY(U,$J,358.3,16369,1,5,0)
 ;;=5^Sprain of Knee/Loower Leg
 ;;^UTILITY(U,$J,358.3,16369,2)
 ;;=Sprain of Knee/Leg NOS^274504
 ;;^UTILITY(U,$J,358.3,16370,0)
 ;;=845.00^^119^1032^6
 ;;^UTILITY(U,$J,358.3,16370,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16370,1,3,0)
 ;;=3^845.00
 ;;^UTILITY(U,$J,358.3,16370,1,5,0)
 ;;=5^Sprain of Ankle, NOS
 ;;^UTILITY(U,$J,358.3,16370,2)
 ;;=^274507
 ;;^UTILITY(U,$J,358.3,16371,0)
 ;;=845.09^^119^1032^7
 ;;^UTILITY(U,$J,358.3,16371,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16371,1,3,0)
 ;;=3^845.09
 ;;^UTILITY(U,$J,358.3,16371,1,5,0)
 ;;=5^Sprain of Achilles Tendon (Rupture)
 ;;^UTILITY(U,$J,358.3,16371,2)
 ;;=^274511
 ;;^UTILITY(U,$J,358.3,16372,0)
 ;;=845.10^^119^1032^8
 ;;^UTILITY(U,$J,358.3,16372,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16372,1,3,0)
 ;;=3^845.10
 ;;^UTILITY(U,$J,358.3,16372,1,5,0)
 ;;=5^Sprain of Foot, NOS
 ;;^UTILITY(U,$J,358.3,16372,2)
 ;;=Sprain of Foot^274513
 ;;^UTILITY(U,$J,358.3,16373,0)
 ;;=845.11^^119^1032^9
 ;;^UTILITY(U,$J,358.3,16373,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,16373,1,3,0)
 ;;=3^845.11
 ;;^UTILITY(U,$J,358.3,16373,1,5,0)
 ;;=5^Sprain of Tarsometatarsal Joint
