IBDEI0DZ ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,18661,1,2,0)
 ;;=2^296.80
 ;;^UTILITY(U,$J,358.3,18661,1,5,0)
 ;;=5^Bipolar Disorder,NOS
 ;;^UTILITY(U,$J,358.3,18661,2)
 ;;=^331892
 ;;^UTILITY(U,$J,358.3,18662,0)
 ;;=296.89^^139^1192^10
 ;;^UTILITY(U,$J,358.3,18662,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18662,1,2,0)
 ;;=2^296.89
 ;;^UTILITY(U,$J,358.3,18662,1,5,0)
 ;;=5^Bipolar II Disorder
 ;;^UTILITY(U,$J,358.3,18662,2)
 ;;=^331893
 ;;^UTILITY(U,$J,358.3,18663,0)
 ;;=297.0^^139^1193^3
 ;;^UTILITY(U,$J,358.3,18663,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18663,1,2,0)
 ;;=2^297.0
 ;;^UTILITY(U,$J,358.3,18663,1,5,0)
 ;;=5^Paranoid State, Simple
 ;;^UTILITY(U,$J,358.3,18663,2)
 ;;=^268149
 ;;^UTILITY(U,$J,358.3,18664,0)
 ;;=298.9^^139^1193^4
 ;;^UTILITY(U,$J,358.3,18664,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18664,1,2,0)
 ;;=2^298.9
 ;;^UTILITY(U,$J,358.3,18664,1,5,0)
 ;;=5^Psychosis, NOS
 ;;^UTILITY(U,$J,358.3,18664,2)
 ;;=^259059
 ;;^UTILITY(U,$J,358.3,18665,0)
 ;;=298.8^^139^1193^5
 ;;^UTILITY(U,$J,358.3,18665,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18665,1,2,0)
 ;;=2^298.8
 ;;^UTILITY(U,$J,358.3,18665,1,5,0)
 ;;=5^Psychosis, Reactive
 ;;^UTILITY(U,$J,358.3,18665,2)
 ;;=^87326
 ;;^UTILITY(U,$J,358.3,18666,0)
 ;;=297.9^^139^1193^2
 ;;^UTILITY(U,$J,358.3,18666,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18666,1,2,0)
 ;;=2^297.9
 ;;^UTILITY(U,$J,358.3,18666,1,5,0)
 ;;=5^Paranoia
 ;;^UTILITY(U,$J,358.3,18666,2)
 ;;=^123970
 ;;^UTILITY(U,$J,358.3,18667,0)
 ;;=297.1^^139^1193^1
 ;;^UTILITY(U,$J,358.3,18667,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18667,1,2,0)
 ;;=2^297.1
 ;;^UTILITY(U,$J,358.3,18667,1,5,0)
 ;;=5^Delusional Disorder
 ;;^UTILITY(U,$J,358.3,18667,2)
 ;;=^331896
 ;;^UTILITY(U,$J,358.3,18668,0)
 ;;=301.7^^139^1194^1
 ;;^UTILITY(U,$J,358.3,18668,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18668,1,2,0)
 ;;=2^301.7
 ;;^UTILITY(U,$J,358.3,18668,1,5,0)
 ;;=5^Antisocial Personality Dis
 ;;^UTILITY(U,$J,358.3,18668,2)
 ;;=Antisocial Personality Dis^9066
 ;;^UTILITY(U,$J,358.3,18669,0)
 ;;=301.82^^139^1194^2
 ;;^UTILITY(U,$J,358.3,18669,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18669,1,2,0)
 ;;=2^301.82
 ;;^UTILITY(U,$J,358.3,18669,1,5,0)
 ;;=5^Avoidant Personality Disorder
 ;;^UTILITY(U,$J,358.3,18669,2)
 ;;=Avoidant Personality Disorder^265347
 ;;^UTILITY(U,$J,358.3,18670,0)
 ;;=301.83^^139^1194^3
 ;;^UTILITY(U,$J,358.3,18670,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18670,1,2,0)
 ;;=2^301.83
 ;;^UTILITY(U,$J,358.3,18670,1,5,0)
 ;;=5^Borderline Personality Disorder
 ;;^UTILITY(U,$J,358.3,18670,2)
 ;;=Borderline Personality Disorder^16372
 ;;^UTILITY(U,$J,358.3,18671,0)
 ;;=301.6^^139^1194^8
 ;;^UTILITY(U,$J,358.3,18671,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18671,1,2,0)
 ;;=2^301.6
 ;;^UTILITY(U,$J,358.3,18671,1,5,0)
 ;;=5^Dependent Personality Disorder
 ;;^UTILITY(U,$J,358.3,18671,2)
 ;;=Dependent Personality Disorder^32860
 ;;^UTILITY(U,$J,358.3,18672,0)
 ;;=301.50^^139^1194^9
 ;;^UTILITY(U,$J,358.3,18672,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18672,1,2,0)
 ;;=2^301.50
 ;;^UTILITY(U,$J,358.3,18672,1,5,0)
 ;;=5^Histrionic Personality Disorder
 ;;^UTILITY(U,$J,358.3,18672,2)
 ;;=Histrionic Personality Disorder^57763
 ;;^UTILITY(U,$J,358.3,18673,0)
 ;;=301.81^^139^1194^11
 ;;^UTILITY(U,$J,358.3,18673,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18673,1,2,0)
 ;;=2^301.81
 ;;^UTILITY(U,$J,358.3,18673,1,5,0)
 ;;=5^Narcissistic Personality Disorder
 ;;^UTILITY(U,$J,358.3,18673,2)
 ;;=Narcissistic Personality Disorder^265353
 ;;^UTILITY(U,$J,358.3,18674,0)
 ;;=301.0^^139^1194^12
 ;;^UTILITY(U,$J,358.3,18674,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18674,1,2,0)
 ;;=2^301.0
 ;;^UTILITY(U,$J,358.3,18674,1,5,0)
 ;;=5^Paranoid Personality Disorder
 ;;^UTILITY(U,$J,358.3,18674,2)
 ;;=Paranoid Personality Disorder^89982
 ;;^UTILITY(U,$J,358.3,18675,0)
 ;;=301.9^^139^1194^16
 ;;^UTILITY(U,$J,358.3,18675,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18675,1,2,0)
 ;;=2^301.9
 ;;^UTILITY(U,$J,358.3,18675,1,5,0)
 ;;=5^Unspecified Personality Disorder
 ;;^UTILITY(U,$J,358.3,18675,2)
 ;;=Unspecified Personality Disorder^92451
 ;;^UTILITY(U,$J,358.3,18676,0)
 ;;=301.20^^139^1194^14
 ;;^UTILITY(U,$J,358.3,18676,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18676,1,2,0)
 ;;=2^301.20
 ;;^UTILITY(U,$J,358.3,18676,1,5,0)
 ;;=5^Schizoid Personality Disorder
 ;;^UTILITY(U,$J,358.3,18676,2)
 ;;=^108271
 ;;^UTILITY(U,$J,358.3,18677,0)
 ;;=301.22^^139^1194^15
 ;;^UTILITY(U,$J,358.3,18677,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18677,1,2,0)
 ;;=2^301.22
 ;;^UTILITY(U,$J,358.3,18677,1,5,0)
 ;;=5^Schizotypal Personality Disorder
 ;;^UTILITY(U,$J,358.3,18677,2)
 ;;=Schizotypal Personality Disorder^108367
 ;;^UTILITY(U,$J,358.3,18678,0)
 ;;=301.4^^139^1194^6
 ;;^UTILITY(U,$J,358.3,18678,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18678,1,2,0)
 ;;=2^301.4
 ;;^UTILITY(U,$J,358.3,18678,1,5,0)
 ;;=5^Compulsive Personality Disorder
 ;;^UTILITY(U,$J,358.3,18678,2)
 ;;=Compulsive Personality Disorder^27122
 ;;^UTILITY(U,$J,358.3,18679,0)
 ;;=301.84^^139^1194^13
 ;;^UTILITY(U,$J,358.3,18679,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18679,1,2,0)
 ;;=2^301.84
 ;;^UTILITY(U,$J,358.3,18679,1,5,0)
 ;;=5^Passive-Aggressive Personality d/o
 ;;^UTILITY(U,$J,358.3,18679,2)
 ;;=Passive-Aggressive Personality Dis^90602
 ;;^UTILITY(U,$J,358.3,18680,0)
 ;;=301.11^^139^1194^5
 ;;^UTILITY(U,$J,358.3,18680,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18680,1,2,0)
 ;;=2^301.11
 ;;^UTILITY(U,$J,358.3,18680,1,5,0)
 ;;=5^Chr Hypomaniac Personality d/o
 ;;^UTILITY(U,$J,358.3,18680,2)
 ;;=^268171
 ;;^UTILITY(U,$J,358.3,18681,0)
 ;;=301.12^^139^1194^4
 ;;^UTILITY(U,$J,358.3,18681,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18681,1,2,0)
 ;;=2^301.12
 ;;^UTILITY(U,$J,358.3,18681,1,5,0)
 ;;=5^Chr Depressive Personality d/o
 ;;^UTILITY(U,$J,358.3,18681,2)
 ;;=^268173
 ;;^UTILITY(U,$J,358.3,18682,0)
 ;;=301.13^^139^1194^7
 ;;^UTILITY(U,$J,358.3,18682,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18682,1,2,0)
 ;;=2^301.13
 ;;^UTILITY(U,$J,358.3,18682,1,5,0)
 ;;=5^Cyclothymic Disorder
 ;;^UTILITY(U,$J,358.3,18682,2)
 ;;=^30028
 ;;^UTILITY(U,$J,358.3,18683,0)
 ;;=301.21^^139^1194^10
 ;;^UTILITY(U,$J,358.3,18683,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18683,1,2,0)
 ;;=2^301.21
 ;;^UTILITY(U,$J,358.3,18683,1,5,0)
 ;;=5^Introverted Personality
 ;;^UTILITY(U,$J,358.3,18683,2)
 ;;=^268174
 ;;^UTILITY(U,$J,358.3,18684,0)
 ;;=302.2^^139^1195^7
 ;;^UTILITY(U,$J,358.3,18684,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18684,1,2,0)
 ;;=2^302.2
 ;;^UTILITY(U,$J,358.3,18684,1,5,0)
 ;;=5^Pedophilia
 ;;^UTILITY(U,$J,358.3,18684,2)
 ;;=^91008
 ;;^UTILITY(U,$J,358.3,18685,0)
 ;;=302.4^^139^1195^2
 ;;^UTILITY(U,$J,358.3,18685,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18685,1,2,0)
 ;;=2^302.4
 ;;^UTILITY(U,$J,358.3,18685,1,5,0)
 ;;=5^Exhibitionism
 ;;^UTILITY(U,$J,358.3,18685,2)
 ;;=^43610
 ;;^UTILITY(U,$J,358.3,18686,0)
 ;;=302.72^^139^1195^5
 ;;^UTILITY(U,$J,358.3,18686,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18686,1,2,0)
 ;;=2^302.72
 ;;^UTILITY(U,$J,358.3,18686,1,5,0)
 ;;=5^Inhibited Sex Excitement
 ;;^UTILITY(U,$J,358.3,18686,2)
 ;;=^100632
 ;;^UTILITY(U,$J,358.3,18687,0)
 ;;=302.73^^139^1195^3
 ;;^UTILITY(U,$J,358.3,18687,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18687,1,2,0)
 ;;=2^302.73
 ;;^UTILITY(U,$J,358.3,18687,1,5,0)
 ;;=5^Female Orgasmic Disorder
 ;;^UTILITY(U,$J,358.3,18687,2)
 ;;=^100628
 ;;^UTILITY(U,$J,358.3,18688,0)
 ;;=302.74^^139^1195^6
 ;;^UTILITY(U,$J,358.3,18688,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18688,1,2,0)
 ;;=2^302.74
 ;;^UTILITY(U,$J,358.3,18688,1,5,0)
 ;;=5^Male Orgasmic Disorder
 ;;^UTILITY(U,$J,358.3,18688,2)
 ;;=^100630
 ;;^UTILITY(U,$J,358.3,18689,0)
 ;;=302.75^^139^1195^8
 ;;^UTILITY(U,$J,358.3,18689,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18689,1,2,0)
 ;;=2^302.75
 ;;^UTILITY(U,$J,358.3,18689,1,5,0)
 ;;=5^Premature Ejaculation
 ;;^UTILITY(U,$J,358.3,18689,2)
 ;;=^100637
 ;;^UTILITY(U,$J,358.3,18690,0)
 ;;=302.85^^139^1195^4
 ;;^UTILITY(U,$J,358.3,18690,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18690,1,2,0)
 ;;=2^302.85
 ;;^UTILITY(U,$J,358.3,18690,1,5,0)
 ;;=5^Gender Ident Disorder
 ;;^UTILITY(U,$J,358.3,18690,2)
 ;;=^268180
 ;;^UTILITY(U,$J,358.3,18691,0)
 ;;=302.0^^139^1195^1
 ;;^UTILITY(U,$J,358.3,18691,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18691,1,2,0)
 ;;=2^302.0
 ;;^UTILITY(U,$J,358.3,18691,1,5,0)
 ;;=5^Ego-Dystonic Sexual Orient
 ;;^UTILITY(U,$J,358.3,18691,2)
 ;;=^331922
 ;;^UTILITY(U,$J,358.3,18692,0)
 ;;=302.1^^139^1195^14
 ;;^UTILITY(U,$J,358.3,18692,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18692,1,2,0)
 ;;=2^302.1
 ;;^UTILITY(U,$J,358.3,18692,1,5,0)
 ;;=5^Zoophilia
 ;;^UTILITY(U,$J,358.3,18692,2)
 ;;=^265356
 ;;^UTILITY(U,$J,358.3,18693,0)
 ;;=302.3^^139^1195^13
 ;;^UTILITY(U,$J,358.3,18693,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18693,1,2,0)
 ;;=2^302.3
 ;;^UTILITY(U,$J,358.3,18693,1,5,0)
 ;;=5^Transvestic Fetishism
 ;;^UTILITY(U,$J,358.3,18693,2)
 ;;=^331923
 ;;^UTILITY(U,$J,358.3,18694,0)
 ;;=302.50^^139^1195^12
 ;;^UTILITY(U,$J,358.3,18694,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18694,1,2,0)
 ;;=2^302.50
 ;;^UTILITY(U,$J,358.3,18694,1,5,0)
 ;;=5^Trans-sexualism w/Unspec Sexual Hx
 ;;^UTILITY(U,$J,358.3,18694,2)
 ;;=^120949
 ;;^UTILITY(U,$J,358.3,18695,0)
 ;;=302.51^^139^1195^9
 ;;^UTILITY(U,$J,358.3,18695,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18695,1,2,0)
 ;;=2^302.51
 ;;^UTILITY(U,$J,358.3,18695,1,5,0)
 ;;=5^Trans-sexualism w/Asexual Hx
 ;;^UTILITY(U,$J,358.3,18695,2)
 ;;=^268175
 ;;^UTILITY(U,$J,358.3,18696,0)
 ;;=302.52^^139^1195^11
 ;;^UTILITY(U,$J,358.3,18696,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,18696,1,2,0)
 ;;=2^302.52
 ;;^UTILITY(U,$J,358.3,18696,1,5,0)
 ;;=5^Trans-sexualism w/Homosexual Hx
 ;;^UTILITY(U,$J,358.3,18696,2)
 ;;=^268176
