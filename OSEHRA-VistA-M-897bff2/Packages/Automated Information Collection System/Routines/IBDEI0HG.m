IBDEI0HG ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,23535,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23535,1,4,0)
 ;;=4^Resp Abnormality
 ;;^UTILITY(U,$J,358.3,23535,1,5,0)
 ;;=5^786.00
 ;;^UTILITY(U,$J,358.3,23535,2)
 ;;=^105164
 ;;^UTILITY(U,$J,358.3,23536,0)
 ;;=786.01^^188^1602^35
 ;;^UTILITY(U,$J,358.3,23536,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23536,1,4,0)
 ;;=4^Hyperventilation
 ;;^UTILITY(U,$J,358.3,23536,1,5,0)
 ;;=5^786.01
 ;;^UTILITY(U,$J,358.3,23536,2)
 ;;=^60480
 ;;^UTILITY(U,$J,358.3,23537,0)
 ;;=786.02^^188^1602^47
 ;;^UTILITY(U,$J,358.3,23537,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23537,1,4,0)
 ;;=4^Orthopnea
 ;;^UTILITY(U,$J,358.3,23537,1,5,0)
 ;;=5^786.02
 ;;^UTILITY(U,$J,358.3,23537,2)
 ;;=^186737
 ;;^UTILITY(U,$J,358.3,23538,0)
 ;;=786.03^^188^1602^9
 ;;^UTILITY(U,$J,358.3,23538,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23538,1,4,0)
 ;;=4^Apnea
 ;;^UTILITY(U,$J,358.3,23538,1,5,0)
 ;;=5^786.03
 ;;^UTILITY(U,$J,358.3,23538,2)
 ;;=^9569
 ;;^UTILITY(U,$J,358.3,23539,0)
 ;;=786.05^^188^1602^55
 ;;^UTILITY(U,$J,358.3,23539,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23539,1,4,0)
 ;;=4^Shortness Of Breath
 ;;^UTILITY(U,$J,358.3,23539,1,5,0)
 ;;=5^786.05
 ;;^UTILITY(U,$J,358.3,23539,2)
 ;;=^37632
 ;;^UTILITY(U,$J,358.3,23540,0)
 ;;=786.06^^188^1602^61
 ;;^UTILITY(U,$J,358.3,23540,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23540,1,4,0)
 ;;=4^Tachypnea
 ;;^UTILITY(U,$J,358.3,23540,1,5,0)
 ;;=5^786.06
 ;;^UTILITY(U,$J,358.3,23540,2)
 ;;=^321213
 ;;^UTILITY(U,$J,358.3,23541,0)
 ;;=786.07^^188^1602^66
 ;;^UTILITY(U,$J,358.3,23541,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23541,1,4,0)
 ;;=4^Wheezing
 ;;^UTILITY(U,$J,358.3,23541,1,5,0)
 ;;=5^786.07
 ;;^UTILITY(U,$J,358.3,23541,2)
 ;;=^127848
 ;;^UTILITY(U,$J,358.3,23542,0)
 ;;=786.09^^188^1602^54
 ;;^UTILITY(U,$J,358.3,23542,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23542,1,4,0)
 ;;=4^Resp Distress
 ;;^UTILITY(U,$J,358.3,23542,1,5,0)
 ;;=5^786.09
 ;;^UTILITY(U,$J,358.3,23542,2)
 ;;=^87547
 ;;^UTILITY(U,$J,358.3,23543,0)
 ;;=786.1^^188^1602^58
 ;;^UTILITY(U,$J,358.3,23543,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23543,1,4,0)
 ;;=4^Stridor
 ;;^UTILITY(U,$J,358.3,23543,1,5,0)
 ;;=5^786.1
 ;;^UTILITY(U,$J,358.3,23543,2)
 ;;=^114767
 ;;^UTILITY(U,$J,358.3,23544,0)
 ;;=786.2^^188^1602^16
 ;;^UTILITY(U,$J,358.3,23544,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23544,1,4,0)
 ;;=4^Cough
 ;;^UTILITY(U,$J,358.3,23544,1,5,0)
 ;;=5^786.2
 ;;^UTILITY(U,$J,358.3,23544,2)
 ;;=^28905
 ;;^UTILITY(U,$J,358.3,23545,0)
 ;;=786.4^^188^1602^3
 ;;^UTILITY(U,$J,358.3,23545,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23545,1,4,0)
 ;;=4^Abnormal Sputum
 ;;^UTILITY(U,$J,358.3,23545,1,5,0)
 ;;=5^786.4
 ;;^UTILITY(U,$J,358.3,23545,2)
 ;;=^273377
 ;;^UTILITY(U,$J,358.3,23546,0)
 ;;=786.50^^188^1602^11
 ;;^UTILITY(U,$J,358.3,23546,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23546,1,4,0)
 ;;=4^Chest Pain
 ;;^UTILITY(U,$J,358.3,23546,1,5,0)
 ;;=5^786.50
 ;;^UTILITY(U,$J,358.3,23546,2)
 ;;=^22485
 ;;^UTILITY(U,$J,358.3,23547,0)
 ;;=786.51^^188^1602^51
 ;;^UTILITY(U,$J,358.3,23547,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23547,1,4,0)
 ;;=4^Precordial Pain
 ;;^UTILITY(U,$J,358.3,23547,1,5,0)
 ;;=5^786.51
 ;;^UTILITY(U,$J,358.3,23547,2)
 ;;=^276877
 ;;^UTILITY(U,$J,358.3,23548,0)
 ;;=786.52^^188^1602^49
 ;;^UTILITY(U,$J,358.3,23548,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23548,1,4,0)
 ;;=4^Painful Respiration
 ;;^UTILITY(U,$J,358.3,23548,1,5,0)
 ;;=5^786.52
 ;;^UTILITY(U,$J,358.3,23548,2)
 ;;=^89126
 ;;^UTILITY(U,$J,358.3,23549,0)
 ;;=786.59^^188^1602^52
 ;;^UTILITY(U,$J,358.3,23549,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23549,1,4,0)
 ;;=4^Pressure Chest
 ;;^UTILITY(U,$J,358.3,23549,1,5,0)
 ;;=5^786.59
 ;;^UTILITY(U,$J,358.3,23549,2)
 ;;=^87384
 ;;^UTILITY(U,$J,358.3,23550,0)
 ;;=786.6^^188^1602^59
 ;;^UTILITY(U,$J,358.3,23550,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23550,1,4,0)
 ;;=4^Swelling/Mass Chest
 ;;^UTILITY(U,$J,358.3,23550,1,5,0)
 ;;=5^786.6
 ;;^UTILITY(U,$J,358.3,23550,2)
 ;;=^273380
 ;;^UTILITY(U,$J,358.3,23551,0)
 ;;=786.7^^188^1602^2
 ;;^UTILITY(U,$J,358.3,23551,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23551,1,4,0)
 ;;=4^Abnormal Chest Sounds
 ;;^UTILITY(U,$J,358.3,23551,1,5,0)
 ;;=5^786.7
 ;;^UTILITY(U,$J,358.3,23551,2)
 ;;=^273381
 ;;^UTILITY(U,$J,358.3,23552,0)
 ;;=786.8^^188^1602^32
 ;;^UTILITY(U,$J,358.3,23552,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23552,1,4,0)
 ;;=4^Hiccough
 ;;^UTILITY(U,$J,358.3,23552,1,5,0)
 ;;=5^786.8
 ;;^UTILITY(U,$J,358.3,23552,2)
 ;;=^57197
 ;;^UTILITY(U,$J,358.3,23553,0)
 ;;=787.1^^188^1602^30
 ;;^UTILITY(U,$J,358.3,23553,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23553,1,4,0)
 ;;=4^Heartburn
 ;;^UTILITY(U,$J,358.3,23553,1,5,0)
 ;;=5^787.1
 ;;^UTILITY(U,$J,358.3,23553,2)
 ;;=^54996
 ;;^UTILITY(U,$J,358.3,23554,0)
 ;;=787.3^^188^1602^10
 ;;^UTILITY(U,$J,358.3,23554,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23554,1,4,0)
 ;;=4^Bloating
 ;;^UTILITY(U,$J,358.3,23554,1,5,0)
 ;;=5^787.3
 ;;^UTILITY(U,$J,358.3,23554,2)
 ;;=^46766
 ;;^UTILITY(U,$J,358.3,23555,0)
 ;;=787.4^^188^1602^33
 ;;^UTILITY(U,$J,358.3,23555,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23555,1,4,0)
 ;;=4^Hyperperistalsis
 ;;^UTILITY(U,$J,358.3,23555,1,5,0)
 ;;=5^787.4
 ;;^UTILITY(U,$J,358.3,23555,2)
 ;;=^273385
 ;;^UTILITY(U,$J,358.3,23556,0)
 ;;=787.5^^188^1602^1
 ;;^UTILITY(U,$J,358.3,23556,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23556,1,4,0)
 ;;=4^Abnormal Bowel Sounds
 ;;^UTILITY(U,$J,358.3,23556,1,5,0)
 ;;=5^787.5
 ;;^UTILITY(U,$J,358.3,23556,2)
 ;;=^273386
 ;;^UTILITY(U,$J,358.3,23557,0)
 ;;=787.91^^188^1602^20
 ;;^UTILITY(U,$J,358.3,23557,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23557,1,4,0)
 ;;=4^Diarrhea
 ;;^UTILITY(U,$J,358.3,23557,1,5,0)
 ;;=5^787.91
 ;;^UTILITY(U,$J,358.3,23557,2)
 ;;=^33921
 ;;^UTILITY(U,$J,358.3,23558,0)
 ;;=787.02^^188^1602^45
 ;;^UTILITY(U,$J,358.3,23558,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23558,1,4,0)
 ;;=4^Nausea Alone
 ;;^UTILITY(U,$J,358.3,23558,1,5,0)
 ;;=5^787.02
 ;;^UTILITY(U,$J,358.3,23558,2)
 ;;=^81639
 ;;^UTILITY(U,$J,358.3,23559,0)
 ;;=787.01^^188^1602^46
 ;;^UTILITY(U,$J,358.3,23559,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23559,1,4,0)
 ;;=4^Nausea W/ Vomiting
 ;;^UTILITY(U,$J,358.3,23559,1,5,0)
 ;;=5^787.01
 ;;^UTILITY(U,$J,358.3,23559,2)
 ;;=^81644
 ;;^UTILITY(U,$J,358.3,23560,0)
 ;;=787.03^^188^1602^65
 ;;^UTILITY(U,$J,358.3,23560,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23560,1,4,0)
 ;;=4^Vomiting Alone
 ;;^UTILITY(U,$J,358.3,23560,1,5,0)
 ;;=5^787.03
 ;;^UTILITY(U,$J,358.3,23560,2)
 ;;=^127237
 ;;^UTILITY(U,$J,358.3,23561,0)
 ;;=435.9^^188^1602^62
 ;;^UTILITY(U,$J,358.3,23561,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23561,1,4,0)
 ;;=4^TIA
 ;;^UTILITY(U,$J,358.3,23561,1,5,0)
 ;;=5^435.9
 ;;^UTILITY(U,$J,358.3,23561,2)
 ;;=^21635
 ;;^UTILITY(U,$J,358.3,23562,0)
 ;;=285.9^^188^1602^5
 ;;^UTILITY(U,$J,358.3,23562,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23562,1,4,0)
 ;;=4^Anemia Nos
 ;;^UTILITY(U,$J,358.3,23562,1,5,0)
 ;;=5^285.9
 ;;^UTILITY(U,$J,358.3,23562,2)
 ;;=^7007
 ;;^UTILITY(U,$J,358.3,23563,0)
 ;;=285.1^^188^1602^6
 ;;^UTILITY(U,$J,358.3,23563,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23563,1,4,0)
 ;;=4^Anemia,Acute Posthemorr
 ;;^UTILITY(U,$J,358.3,23563,1,5,0)
 ;;=5^285.1
 ;;^UTILITY(U,$J,358.3,23563,2)
 ;;=^267986
 ;;^UTILITY(U,$J,358.3,23564,0)
 ;;=280.0^^188^1602^7
 ;;^UTILITY(U,$J,358.3,23564,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23564,1,4,0)
 ;;=4^Anemia,Chr Blood Loss
 ;;^UTILITY(U,$J,358.3,23564,1,5,0)
 ;;=5^280.0
 ;;^UTILITY(U,$J,358.3,23564,2)
 ;;=^267971
 ;;^UTILITY(U,$J,358.3,23565,0)
 ;;=280.9^^188^1602^8
 ;;^UTILITY(U,$J,358.3,23565,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23565,1,4,0)
 ;;=4^Anemia,Iron Defic
 ;;^UTILITY(U,$J,358.3,23565,1,5,0)
 ;;=5^280.9
 ;;^UTILITY(U,$J,358.3,23565,2)
 ;;=^276946
 ;;^UTILITY(U,$J,358.3,23566,0)
 ;;=331.0^^188^1602^4
 ;;^UTILITY(U,$J,358.3,23566,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23566,1,4,0)
 ;;=4^Alzheimer's Disease
 ;;^UTILITY(U,$J,358.3,23566,1,5,0)
 ;;=5^331.0
 ;;^UTILITY(U,$J,358.3,23566,2)
 ;;=^5679
 ;;^UTILITY(U,$J,358.3,23567,0)
 ;;=250.01^^188^1602^18
 ;;^UTILITY(U,$J,358.3,23567,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23567,1,4,0)
 ;;=4^Diabetes Type 1
 ;;^UTILITY(U,$J,358.3,23567,1,5,0)
 ;;=5^250.01
 ;;^UTILITY(U,$J,358.3,23567,2)
 ;;=^33586
 ;;^UTILITY(U,$J,358.3,23568,0)
 ;;=250.00^^188^1602^19
 ;;^UTILITY(U,$J,358.3,23568,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23568,1,4,0)
 ;;=4^Diabetes Type 2
 ;;^UTILITY(U,$J,358.3,23568,1,5,0)
 ;;=5^250.00
 ;;^UTILITY(U,$J,358.3,23568,2)
 ;;=^33605
 ;;^UTILITY(U,$J,358.3,23569,0)
 ;;=577.0^^188^1602^50
 ;;^UTILITY(U,$J,358.3,23569,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23569,1,4,0)
 ;;=4^Pancreatitis,Acute
 ;;^UTILITY(U,$J,358.3,23569,1,5,0)
 ;;=5^577.0
 ;;^UTILITY(U,$J,358.3,23569,2)
 ;;=^2643
 ;;^UTILITY(U,$J,358.3,23570,0)
 ;;=785.4^^188^1602^27
 ;;^UTILITY(U,$J,358.3,23570,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23570,1,4,0)
 ;;=4^Gangrene
 ;;^UTILITY(U,$J,358.3,23570,1,5,0)
 ;;=5^785.4
 ;;^UTILITY(U,$J,358.3,23570,2)
 ;;=^49194
 ;;^UTILITY(U,$J,358.3,23571,0)
 ;;=780.99^^188^1602^12
 ;;^UTILITY(U,$J,358.3,23571,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23571,1,4,0)
 ;;=4^Chills
 ;;^UTILITY(U,$J,358.3,23571,1,5,0)
 ;;=5^780.99
 ;;^UTILITY(U,$J,358.3,23571,2)
 ;;=Chills^1
 ;;^UTILITY(U,$J,358.3,23572,0)
 ;;=991.3^^188^1602^24
 ;;^UTILITY(U,$J,358.3,23572,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23572,1,4,0)
 ;;=4^Frostbite Nec/Nos
 ;;^UTILITY(U,$J,358.3,23572,1,5,0)
 ;;=5^991.3
 ;;^UTILITY(U,$J,358.3,23572,2)
 ;;=^48340
 ;;^UTILITY(U,$J,358.3,23573,0)
 ;;=991.2^^188^1602^25
 ;;^UTILITY(U,$J,358.3,23573,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,23573,1,4,0)
 ;;=4^Frostbite Of Foot
 ;;^UTILITY(U,$J,358.3,23573,1,5,0)
 ;;=5^991.2
