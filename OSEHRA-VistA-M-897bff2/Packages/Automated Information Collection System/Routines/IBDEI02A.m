IBDEI02A ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,2519,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2519,1,1,0)
 ;;=1^284.19
 ;;^UTILITY(U,$J,358.3,2519,1,8,0)
 ;;=8^Oth Pancytopenia
 ;;^UTILITY(U,$J,358.3,2519,2)
 ;;=^340501
 ;;^UTILITY(U,$J,358.3,2520,0)
 ;;=286.9^^29^208^3
 ;;^UTILITY(U,$J,358.3,2520,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2520,1,1,0)
 ;;=1^286.9
 ;;^UTILITY(U,$J,358.3,2520,1,8,0)
 ;;=8^Defective Coagulation,Unspec
 ;;^UTILITY(U,$J,358.3,2520,2)
 ;;=^87267
 ;;^UTILITY(U,$J,358.3,2521,0)
 ;;=446.6^^29^208^10
 ;;^UTILITY(U,$J,358.3,2521,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2521,1,1,0)
 ;;=1^446.6
 ;;^UTILITY(U,$J,358.3,2521,1,8,0)
 ;;=8^Thrombotic Thrombocytopenic Purpura (TTP)
 ;;^UTILITY(U,$J,358.3,2521,2)
 ;;=^119061
 ;;^UTILITY(U,$J,358.3,2522,0)
 ;;=451.9^^29^208^9
 ;;^UTILITY(U,$J,358.3,2522,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2522,1,1,0)
 ;;=1^451.9
 ;;^UTILITY(U,$J,358.3,2522,1,8,0)
 ;;=8^Thrombophlebitis
 ;;^UTILITY(U,$J,358.3,2522,2)
 ;;=^93357
 ;;^UTILITY(U,$J,358.3,2523,0)
 ;;=287.5^^29^208^7
 ;;^UTILITY(U,$J,358.3,2523,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2523,1,1,0)
 ;;=1^287.5
 ;;^UTILITY(U,$J,358.3,2523,1,8,0)
 ;;=8^Thrombocytopenia Nos
 ;;^UTILITY(U,$J,358.3,2523,2)
 ;;=^118988
 ;;^UTILITY(U,$J,358.3,2524,0)
 ;;=287.1^^29^208^6
 ;;^UTILITY(U,$J,358.3,2524,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2524,1,1,0)
 ;;=1^287.1
 ;;^UTILITY(U,$J,358.3,2524,1,8,0)
 ;;=8^Qualitative Platelet Defects
 ;;^UTILITY(U,$J,358.3,2524,2)
 ;;=^101922
 ;;^UTILITY(U,$J,358.3,2525,0)
 ;;=287.30^^29^208^5
 ;;^UTILITY(U,$J,358.3,2525,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2525,1,1,0)
 ;;=1^287.30
 ;;^UTILITY(U,$J,358.3,2525,1,8,0)
 ;;=8^Primary Thrombocytopenia (ITP)
 ;;^UTILITY(U,$J,358.3,2525,2)
 ;;=^332841
 ;;^UTILITY(U,$J,358.3,2526,0)
 ;;=238.71^^29^208^8
 ;;^UTILITY(U,$J,358.3,2526,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2526,1,1,0)
 ;;=1^238.71
 ;;^UTILITY(U,$J,358.3,2526,1,8,0)
 ;;=8^Thrombocytosis,Essential
 ;;^UTILITY(U,$J,358.3,2526,2)
 ;;=^334027
 ;;^UTILITY(U,$J,358.3,2527,0)
 ;;=286.52^^29^208^1
 ;;^UTILITY(U,$J,358.3,2527,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2527,1,1,0)
 ;;=1^286.52
 ;;^UTILITY(U,$J,358.3,2527,1,8,0)
 ;;=8^Acquired Hemophilia
 ;;^UTILITY(U,$J,358.3,2527,2)
 ;;=^340502
 ;;^UTILITY(U,$J,358.3,2528,0)
 ;;=286.53^^29^208^2
 ;;^UTILITY(U,$J,358.3,2528,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2528,1,1,0)
 ;;=1^286.53
 ;;^UTILITY(U,$J,358.3,2528,1,8,0)
 ;;=8^Antiphospholipid w/ Hemor d/o
 ;;^UTILITY(U,$J,358.3,2528,2)
 ;;=^340503
 ;;^UTILITY(U,$J,358.3,2529,0)
 ;;=286.59^^29^208^4
 ;;^UTILITY(U,$J,358.3,2529,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2529,1,1,0)
 ;;=1^286.59
 ;;^UTILITY(U,$J,358.3,2529,1,8,0)
 ;;=8^Oth Hemor d/o d/t Anticoagulant
 ;;^UTILITY(U,$J,358.3,2529,2)
 ;;=^340504
 ;;^UTILITY(U,$J,358.3,2530,0)
 ;;=205.00^^29^209^1
 ;;^UTILITY(U,$J,358.3,2530,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2530,1,1,0)
 ;;=1^205.00
 ;;^UTILITY(U,$J,358.3,2530,1,8,0)
 ;;=8^A M L, W/O Remission
 ;;^UTILITY(U,$J,358.3,2530,2)
 ;;=^267531
 ;;^UTILITY(U,$J,358.3,2531,0)
 ;;=205.01^^29^209^2
 ;;^UTILITY(U,$J,358.3,2531,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2531,1,1,0)
 ;;=1^205.01
 ;;^UTILITY(U,$J,358.3,2531,1,8,0)
 ;;=8^A M L,In Remission
 ;;^UTILITY(U,$J,358.3,2531,2)
 ;;=^267532
 ;;^UTILITY(U,$J,358.3,2532,0)
 ;;=205.10^^29^209^4
 ;;^UTILITY(U,$J,358.3,2532,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2532,1,1,0)
 ;;=1^205.10
 ;;^UTILITY(U,$J,358.3,2532,1,8,0)
 ;;=8^C M L,W/O Remission
 ;;^UTILITY(U,$J,358.3,2532,2)
 ;;=^267533
 ;;^UTILITY(U,$J,358.3,2533,0)
 ;;=205.11^^29^209^3
 ;;^UTILITY(U,$J,358.3,2533,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2533,1,1,0)
 ;;=1^205.11
 ;;^UTILITY(U,$J,358.3,2533,1,8,0)
 ;;=8^C M L,In Remission
 ;;^UTILITY(U,$J,358.3,2533,2)
 ;;=^267534
 ;;^UTILITY(U,$J,358.3,2534,0)
 ;;=289.0^^29^209^7
 ;;^UTILITY(U,$J,358.3,2534,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2534,1,1,0)
 ;;=1^289.0
 ;;^UTILITY(U,$J,358.3,2534,1,8,0)
 ;;=8^Secondary Polycythemia
 ;;^UTILITY(U,$J,358.3,2534,2)
 ;;=^186856
 ;;^UTILITY(U,$J,358.3,2535,0)
 ;;=238.4^^29^209^6
 ;;^UTILITY(U,$J,358.3,2535,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2535,1,1,0)
 ;;=1^238.4
 ;;^UTILITY(U,$J,358.3,2535,1,8,0)
 ;;=8^Polycythemia Rubra Vera
 ;;^UTILITY(U,$J,358.3,2535,2)
 ;;=^96105
 ;;^UTILITY(U,$J,358.3,2536,0)
 ;;=288.9^^29^209^9
 ;;^UTILITY(U,$J,358.3,2536,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2536,1,1,0)
 ;;=1^288.9
 ;;^UTILITY(U,$J,358.3,2536,1,8,0)
 ;;=8^Unspecified Dis Of W B C
 ;;^UTILITY(U,$J,358.3,2536,2)
 ;;=^267993
 ;;^UTILITY(U,$J,358.3,2537,0)
 ;;=289.81^^29^209^8
 ;;^UTILITY(U,$J,358.3,2537,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2537,1,1,0)
 ;;=1^289.81
 ;;^UTILITY(U,$J,358.3,2537,1,8,0)
 ;;=8^Unspecified Blood Diseases
 ;;^UTILITY(U,$J,358.3,2537,2)
 ;;=^329886
 ;;^UTILITY(U,$J,358.3,2538,0)
 ;;=238.75^^29^209^5
 ;;^UTILITY(U,$J,358.3,2538,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2538,1,1,0)
 ;;=1^238.75
 ;;^UTILITY(U,$J,358.3,2538,1,8,0)
 ;;=8^Myelodysplastic Syndrome
 ;;^UTILITY(U,$J,358.3,2538,2)
 ;;=^334031
 ;;^UTILITY(U,$J,358.3,2539,0)
 ;;=204.00^^29^210^2
 ;;^UTILITY(U,$J,358.3,2539,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2539,1,1,0)
 ;;=1^204.00
 ;;^UTILITY(U,$J,358.3,2539,1,8,0)
 ;;=8^A L L,W/O Remission
 ;;^UTILITY(U,$J,358.3,2539,2)
 ;;=^267521
 ;;^UTILITY(U,$J,358.3,2540,0)
 ;;=204.01^^29^210^1
 ;;^UTILITY(U,$J,358.3,2540,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2540,1,1,0)
 ;;=1^204.01
 ;;^UTILITY(U,$J,358.3,2540,1,8,0)
 ;;=8^A L L, In Remission
 ;;^UTILITY(U,$J,358.3,2540,2)
 ;;=^267522
 ;;^UTILITY(U,$J,358.3,2541,0)
 ;;=204.10^^29^210^4
 ;;^UTILITY(U,$J,358.3,2541,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2541,1,1,0)
 ;;=1^204.10
 ;;^UTILITY(U,$J,358.3,2541,1,8,0)
 ;;=8^C L L,W/O Remission
 ;;^UTILITY(U,$J,358.3,2541,2)
 ;;=^267523
 ;;^UTILITY(U,$J,358.3,2542,0)
 ;;=204.11^^29^210^3
 ;;^UTILITY(U,$J,358.3,2542,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2542,1,1,0)
 ;;=1^204.11
 ;;^UTILITY(U,$J,358.3,2542,1,8,0)
 ;;=8^C L L,In Remission
 ;;^UTILITY(U,$J,358.3,2542,2)
 ;;=^267524
 ;;^UTILITY(U,$J,358.3,2543,0)
 ;;=201.90^^29^210^6
 ;;^UTILITY(U,$J,358.3,2543,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2543,1,1,0)
 ;;=1^201.90
 ;;^UTILITY(U,$J,358.3,2543,1,8,0)
 ;;=8^Hodgkins Lymphoma,Unspec Type
 ;;^UTILITY(U,$J,358.3,2543,2)
 ;;=^267430
 ;;^UTILITY(U,$J,358.3,2544,0)
 ;;=202.00^^29^210^7
 ;;^UTILITY(U,$J,358.3,2544,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2544,1,1,0)
 ;;=1^202.00
 ;;^UTILITY(U,$J,358.3,2544,1,8,0)
 ;;=8^Lymphoma, Low-Grade (Nodular)
 ;;^UTILITY(U,$J,358.3,2544,2)
 ;;=^72606
 ;;^UTILITY(U,$J,358.3,2545,0)
 ;;=200.20^^29^210^8
 ;;^UTILITY(U,$J,358.3,2545,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2545,1,1,0)
 ;;=1^200.20
 ;;^UTILITY(U,$J,358.3,2545,1,8,0)
 ;;=8^Lymphoma,Burkitts
 ;;^UTILITY(U,$J,358.3,2545,2)
 ;;=^17529
 ;;^UTILITY(U,$J,358.3,2546,0)
 ;;=202.10^^29^210^16
 ;;^UTILITY(U,$J,358.3,2546,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2546,1,1,0)
 ;;=1^202.10
 ;;^UTILITY(U,$J,358.3,2546,1,8,0)
 ;;=8^Mycosis Fungoides
 ;;^UTILITY(U,$J,358.3,2546,2)
 ;;=^80360
 ;;^UTILITY(U,$J,358.3,2547,0)
 ;;=273.3^^29^210^12
 ;;^UTILITY(U,$J,358.3,2547,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2547,1,1,0)
 ;;=1^273.3
 ;;^UTILITY(U,$J,358.3,2547,1,8,0)
 ;;=8^Macroglobulinemia
 ;;^UTILITY(U,$J,358.3,2547,2)
 ;;=^73013
 ;;^UTILITY(U,$J,358.3,2548,0)
 ;;=203.00^^29^210^15
 ;;^UTILITY(U,$J,358.3,2548,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2548,1,1,0)
 ;;=1^203.00
 ;;^UTILITY(U,$J,358.3,2548,1,8,0)
 ;;=8^Multiple Myeloma,W/O Remission
 ;;^UTILITY(U,$J,358.3,2548,2)
 ;;=^267514
 ;;^UTILITY(U,$J,358.3,2549,0)
 ;;=203.01^^29^210^14
 ;;^UTILITY(U,$J,358.3,2549,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2549,1,1,0)
 ;;=1^203.01
 ;;^UTILITY(U,$J,358.3,2549,1,8,0)
 ;;=8^Multiple Myeloma,In Remission
 ;;^UTILITY(U,$J,358.3,2549,2)
 ;;=^267515
 ;;^UTILITY(U,$J,358.3,2550,0)
 ;;=202.40^^29^210^5
 ;;^UTILITY(U,$J,358.3,2550,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2550,1,1,0)
 ;;=1^202.40
 ;;^UTILITY(U,$J,358.3,2550,1,8,0)
 ;;=8^Hairy-Cell Leukemia
 ;;^UTILITY(U,$J,358.3,2550,2)
 ;;=^69587
 ;;^UTILITY(U,$J,358.3,2551,0)
 ;;=200.10^^29^210^10
 ;;^UTILITY(U,$J,358.3,2551,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2551,1,1,0)
 ;;=1^200.10
 ;;^UTILITY(U,$J,358.3,2551,1,8,0)
 ;;=8^Lymphoma,Int/High (Diffuse)
 ;;^UTILITY(U,$J,358.3,2551,2)
 ;;=^175886
 ;;^UTILITY(U,$J,358.3,2552,0)
 ;;=273.1^^29^210^13
 ;;^UTILITY(U,$J,358.3,2552,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2552,1,1,0)
 ;;=1^273.1
 ;;^UTILITY(U,$J,358.3,2552,1,8,0)
 ;;=8^Monoclon Paraproteinemia
 ;;^UTILITY(U,$J,358.3,2552,2)
 ;;=^78888
 ;;^UTILITY(U,$J,358.3,2553,0)
 ;;=238.79^^29^210^11
 ;;^UTILITY(U,$J,358.3,2553,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2553,1,1,0)
 ;;=1^238.79
 ;;^UTILITY(U,$J,358.3,2553,1,8,0)
 ;;=8^Lymphoproliferat Dis
 ;;^UTILITY(U,$J,358.3,2553,2)
 ;;=^334033
 ;;^UTILITY(U,$J,358.3,2554,0)
 ;;=202.90^^29^210^9
 ;;^UTILITY(U,$J,358.3,2554,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2554,1,1,0)
 ;;=1^202.90
 ;;^UTILITY(U,$J,358.3,2554,1,8,0)
 ;;=8^Lymphoma,Diffuse,Unspec Site
 ;;^UTILITY(U,$J,358.3,2554,2)
 ;;=^267504
 ;;^UTILITY(U,$J,358.3,2555,0)
 ;;=156.2^^29^211^1
 ;;^UTILITY(U,$J,358.3,2555,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2555,1,1,0)
 ;;=1^156.2
 ;;^UTILITY(U,$J,358.3,2555,1,8,0)
 ;;=8^Ampulla Of Vater
 ;;^UTILITY(U,$J,358.3,2555,2)
 ;;=^267100
 ;;^UTILITY(U,$J,358.3,2556,0)
 ;;=154.3^^29^211^2
 ;;^UTILITY(U,$J,358.3,2556,1,0)
 ;;=^358.31IA^8^2
 ;;^UTILITY(U,$J,358.3,2556,1,1,0)
 ;;=1^154.3
 ;;^UTILITY(U,$J,358.3,2556,1,8,0)
 ;;=8^Anus
 ;;^UTILITY(U,$J,358.3,2556,2)
 ;;=^267092
 ;;^UTILITY(U,$J,358.3,2557,0)
 ;;=156.1^^29^211^3
 ;;^UTILITY(U,$J,358.3,2557,1,0)
 ;;=^358.31IA^8^2
