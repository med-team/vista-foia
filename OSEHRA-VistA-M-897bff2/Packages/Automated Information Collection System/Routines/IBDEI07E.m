IBDEI07E ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,9721,0)
 ;;=446.5^^77^661^21
 ;;^UTILITY(U,$J,358.3,9721,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9721,1,3,0)
 ;;=3^Temporal Arteritis
 ;;^UTILITY(U,$J,358.3,9721,1,4,0)
 ;;=4^446.5
 ;;^UTILITY(U,$J,358.3,9721,2)
 ;;=Temporal Arteritis^117658
 ;;^UTILITY(U,$J,358.3,9722,0)
 ;;=401.9^^77^661^12
 ;;^UTILITY(U,$J,358.3,9722,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9722,1,3,0)
 ;;=3^Hypertension
 ;;^UTILITY(U,$J,358.3,9722,1,4,0)
 ;;=4^401.9
 ;;^UTILITY(U,$J,358.3,9722,2)
 ;;=Hypertension^186630
 ;;^UTILITY(U,$J,358.3,9723,0)
 ;;=V72.0^^77^661^9
 ;;^UTILITY(U,$J,358.3,9723,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9723,1,3,0)
 ;;=3^Eye Exam
 ;;^UTILITY(U,$J,358.3,9723,1,4,0)
 ;;=4^V72.0
 ;;^UTILITY(U,$J,358.3,9723,2)
 ;;=Eye Exam^43432
 ;;^UTILITY(U,$J,358.3,9724,0)
 ;;=V41.0^^77^661^19
 ;;^UTILITY(U,$J,358.3,9724,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9724,1,3,0)
 ;;=3^Problems with Sight
 ;;^UTILITY(U,$J,358.3,9724,1,4,0)
 ;;=4^V41.0
 ;;^UTILITY(U,$J,358.3,9724,2)
 ;;=^295427
 ;;^UTILITY(U,$J,358.3,9725,0)
 ;;=998.59^^77^661^18
 ;;^UTILITY(U,$J,358.3,9725,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9725,1,3,0)
 ;;=3^Post Op Infection
 ;;^UTILITY(U,$J,358.3,9725,1,4,0)
 ;;=4^998.59
 ;;^UTILITY(U,$J,358.3,9725,2)
 ;;=Post Op Infection^97081
 ;;^UTILITY(U,$J,358.3,9726,0)
 ;;=365.11^^77^662^15
 ;;^UTILITY(U,$J,358.3,9726,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9726,1,3,0)
 ;;=3^Open Angle Glaucoma
 ;;^UTILITY(U,$J,358.3,9726,1,4,0)
 ;;=4^365.11
 ;;^UTILITY(U,$J,358.3,9726,2)
 ;;=Open Angle Glaucoma^51203
 ;;^UTILITY(U,$J,358.3,9727,0)
 ;;=365.12^^77^662^10
 ;;^UTILITY(U,$J,358.3,9727,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9727,1,3,0)
 ;;=3^Low Tension Glaucoma
 ;;^UTILITY(U,$J,358.3,9727,1,4,0)
 ;;=4^365.12
 ;;^UTILITY(U,$J,358.3,9727,2)
 ;;=Low Tension Glaucoma^265223
 ;;^UTILITY(U,$J,358.3,9728,0)
 ;;=365.63^^77^662^13
 ;;^UTILITY(U,$J,358.3,9728,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9728,1,3,0)
 ;;=3^Neovascular Glaucoma
 ;;^UTILITY(U,$J,358.3,9728,1,4,0)
 ;;=4^365.63
 ;;^UTILITY(U,$J,358.3,9728,2)
 ;;=Neovascular Glaucoma^268778
 ;;^UTILITY(U,$J,358.3,9729,0)
 ;;=365.10^^77^662^17
 ;;^UTILITY(U,$J,358.3,9729,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9729,1,3,0)
 ;;=3^Open Angle, Glaucoma Unspec
 ;;^UTILITY(U,$J,358.3,9729,1,4,0)
 ;;=4^365.10
 ;;^UTILITY(U,$J,358.3,9729,2)
 ;;=^51206
 ;;^UTILITY(U,$J,358.3,9730,0)
 ;;=365.13^^77^662^21
 ;;^UTILITY(U,$J,358.3,9730,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9730,1,3,0)
 ;;=3^Pigmentary Glaucoma
 ;;^UTILITY(U,$J,358.3,9730,1,4,0)
 ;;=4^365.13
 ;;^UTILITY(U,$J,358.3,9730,2)
 ;;=Pigmentary Glaucoma^51211
 ;;^UTILITY(U,$J,358.3,9731,0)
 ;;=365.20^^77^662^23
 ;;^UTILITY(U,$J,358.3,9731,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9731,1,3,0)
 ;;=3^Prim Angle Closure Glaucoma
 ;;^UTILITY(U,$J,358.3,9731,1,4,0)
 ;;=4^365.20
 ;;^UTILITY(U,$J,358.3,9731,2)
 ;;=^51195
 ;;^UTILITY(U,$J,358.3,9732,0)
 ;;=365.52^^77^662^24
 ;;^UTILITY(U,$J,358.3,9732,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9732,1,3,0)
 ;;=3^Pseudoexfoliation Glaucoma
 ;;^UTILITY(U,$J,358.3,9732,1,4,0)
 ;;=4^365.52
 ;;^UTILITY(U,$J,358.3,9732,2)
 ;;=Pseudoexfoliation Glaucoma^268771
 ;;^UTILITY(U,$J,358.3,9733,0)
 ;;=365.15^^77^662^27
 ;;^UTILITY(U,$J,358.3,9733,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9733,1,3,0)
 ;;=3^Residual Open Angle Glaucoma
 ;;^UTILITY(U,$J,358.3,9733,1,4,0)
 ;;=4^365.15
 ;;^UTILITY(U,$J,358.3,9733,2)
 ;;=Residual Open Angle Glaucoma^268751
 ;;^UTILITY(U,$J,358.3,9734,0)
 ;;=365.31^^77^662^31
 ;;^UTILITY(U,$J,358.3,9734,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9734,1,3,0)
 ;;=3^Steroid Induced Glaucoma
 ;;^UTILITY(U,$J,358.3,9734,1,4,0)
 ;;=4^365.31
 ;;^UTILITY(U,$J,358.3,9734,2)
 ;;=Steroid Induced Glaucoma^268761
 ;;^UTILITY(U,$J,358.3,9735,0)
 ;;=365.61^^77^662^8
 ;;^UTILITY(U,$J,358.3,9735,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9735,1,3,0)
 ;;=3^Glaucoma W/Pupillary Block
 ;;^UTILITY(U,$J,358.3,9735,1,4,0)
 ;;=4^365.61
 ;;^UTILITY(U,$J,358.3,9735,2)
 ;;=Glaucoma W/Pupillary Block^268776
 ;;^UTILITY(U,$J,358.3,9736,0)
 ;;=365.23^^77^662^4
 ;;^UTILITY(U,$J,358.3,9736,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9736,1,3,0)
 ;;=3^Chronic Angle Clos Glaucoma
 ;;^UTILITY(U,$J,358.3,9736,1,4,0)
 ;;=4^365.23
 ;;^UTILITY(U,$J,358.3,9736,2)
 ;;=^268756
 ;;^UTILITY(U,$J,358.3,9737,0)
 ;;=363.71^^77^662^29
 ;;^UTILITY(U,$J,358.3,9737,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9737,1,3,0)
 ;;=3^Serous Choroidal Detachment
 ;;^UTILITY(U,$J,358.3,9737,1,4,0)
 ;;=4^363.71
 ;;^UTILITY(U,$J,358.3,9737,2)
 ;;=Choroidal Detachment^268699
 ;;^UTILITY(U,$J,358.3,9738,0)
 ;;=365.51^^77^662^19
 ;;^UTILITY(U,$J,358.3,9738,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9738,1,3,0)
 ;;=3^Phacolytic Glaucoma
 ;;^UTILITY(U,$J,358.3,9738,1,4,0)
 ;;=4^365.51
 ;;^UTILITY(U,$J,358.3,9738,2)
 ;;=Phacolytic Glaucoma^265226
 ;;^UTILITY(U,$J,358.3,9739,0)
 ;;=365.01^^77^662^16
 ;;^UTILITY(U,$J,358.3,9739,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9739,1,3,0)
 ;;=3^Open Angle Glaucoma Suspect
 ;;^UTILITY(U,$J,358.3,9739,1,4,0)
 ;;=4^365.01
 ;;^UTILITY(U,$J,358.3,9739,2)
 ;;=Open Angle Glaucoma Suspect^268747
 ;;^UTILITY(U,$J,358.3,9740,0)
 ;;=365.04^^77^662^14
 ;;^UTILITY(U,$J,358.3,9740,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9740,1,3,0)
 ;;=3^Ocular Hypertension
 ;;^UTILITY(U,$J,358.3,9740,1,4,0)
 ;;=4^365.04
 ;;^UTILITY(U,$J,358.3,9740,2)
 ;;=Ocular Hypertension^85124
 ;;^UTILITY(U,$J,358.3,9741,0)
 ;;=365.03^^77^662^32
 ;;^UTILITY(U,$J,358.3,9741,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9741,1,3,0)
 ;;=3^Steroid Responder
 ;;^UTILITY(U,$J,358.3,9741,1,4,0)
 ;;=4^365.03
 ;;^UTILITY(U,$J,358.3,9741,2)
 ;;=^268749
 ;;^UTILITY(U,$J,358.3,9742,0)
 ;;=366.11^^77^662^25
 ;;^UTILITY(U,$J,358.3,9742,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9742,1,3,0)
 ;;=3^Pseudoexfoliation w/o Glaucoma
 ;;^UTILITY(U,$J,358.3,9742,1,4,0)
 ;;=4^366.11
 ;;^UTILITY(U,$J,358.3,9742,2)
 ;;=^265538
 ;;^UTILITY(U,$J,358.3,9743,0)
 ;;=365.02^^77^662^1
 ;;^UTILITY(U,$J,358.3,9743,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9743,1,3,0)
 ;;=3^Anatomic Narrow Angle
 ;;^UTILITY(U,$J,358.3,9743,1,4,0)
 ;;=4^365.02
 ;;^UTILITY(U,$J,358.3,9743,2)
 ;;=Anatomic Narrow Angle^268748
 ;;^UTILITY(U,$J,358.3,9744,0)
 ;;=364.53^^77^662^20
 ;;^UTILITY(U,$J,358.3,9744,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9744,1,3,0)
 ;;=3^Pigment Dispersion w/o Glauc
 ;;^UTILITY(U,$J,358.3,9744,1,4,0)
 ;;=4^364.53
 ;;^UTILITY(U,$J,358.3,9744,2)
 ;;=^268720
 ;;^UTILITY(U,$J,358.3,9745,0)
 ;;=364.42^^77^662^28
 ;;^UTILITY(U,$J,358.3,9745,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9745,1,3,0)
 ;;=3^Rubeosis Iridis w/o Glaucoma
 ;;^UTILITY(U,$J,358.3,9745,1,4,0)
 ;;=4^364.42
 ;;^UTILITY(U,$J,358.3,9745,2)
 ;;=Rubeosis Iridis w/o Glaucoma^268716
 ;;^UTILITY(U,$J,358.3,9746,0)
 ;;=364.77^^77^662^2
 ;;^UTILITY(U,$J,358.3,9746,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9746,1,3,0)
 ;;=3^Angle Recession w/o Glauc
 ;;^UTILITY(U,$J,358.3,9746,1,4,0)
 ;;=4^364.77
 ;;^UTILITY(U,$J,358.3,9746,2)
 ;;=Angle Recession w/o Glauc^268743
 ;;^UTILITY(U,$J,358.3,9747,0)
 ;;=368.40^^77^662^35
 ;;^UTILITY(U,$J,358.3,9747,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9747,1,3,0)
 ;;=3^Visual Field Defect
 ;;^UTILITY(U,$J,358.3,9747,1,4,0)
 ;;=4^368.40
 ;;^UTILITY(U,$J,358.3,9747,2)
 ;;=Visual Field Defect^126859
 ;;^UTILITY(U,$J,358.3,9748,0)
 ;;=363.70^^77^662^3
 ;;^UTILITY(U,$J,358.3,9748,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9748,1,3,0)
 ;;=3^Choroidal Detachment NOS
 ;;^UTILITY(U,$J,358.3,9748,1,4,0)
 ;;=4^363.70
 ;;^UTILITY(U,$J,358.3,9748,2)
 ;;=^276841
 ;;^UTILITY(U,$J,358.3,9749,0)
 ;;=365.24^^77^662^26
 ;;^UTILITY(U,$J,358.3,9749,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9749,1,3,0)
 ;;=3^Residual Angle-Closure Glaucoma
 ;;^UTILITY(U,$J,358.3,9749,1,4,0)
 ;;=4^365.24
 ;;^UTILITY(U,$J,358.3,9749,2)
 ;;=^268758
 ;;^UTILITY(U,$J,358.3,9750,0)
 ;;=365.65^^77^662^33
 ;;^UTILITY(U,$J,358.3,9750,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9750,1,3,0)
 ;;=3^Traumatic Glaucoma
 ;;^UTILITY(U,$J,358.3,9750,1,4,0)
 ;;=4^365.65
 ;;^UTILITY(U,$J,358.3,9750,2)
 ;;=^268780
 ;;^UTILITY(U,$J,358.3,9751,0)
 ;;=365.89^^77^662^34
 ;;^UTILITY(U,$J,358.3,9751,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9751,1,3,0)
 ;;=3^Uveitic Glaucoma
 ;;^UTILITY(U,$J,358.3,9751,1,4,0)
 ;;=4^365.89
 ;;^UTILITY(U,$J,358.3,9751,2)
 ;;=^88069
 ;;^UTILITY(U,$J,358.3,9752,0)
 ;;=365.05^^77^662^18
 ;;^UTILITY(U,$J,358.3,9752,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9752,1,3,0)
 ;;=3^Opn Ang w/ brdrlne fnd-Hi Risk
 ;;^UTILITY(U,$J,358.3,9752,1,4,0)
 ;;=4^365.05
 ;;^UTILITY(U,$J,358.3,9752,2)
 ;;=^340511
 ;;^UTILITY(U,$J,358.3,9753,0)
 ;;=365.06^^77^662^22
 ;;^UTILITY(U,$J,358.3,9753,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9753,1,3,0)
 ;;=3^Prim Ang Clos w/o Glauc Dmg
 ;;^UTILITY(U,$J,358.3,9753,1,4,0)
 ;;=4^365.06
 ;;^UTILITY(U,$J,358.3,9753,2)
 ;;=^340512
 ;;^UTILITY(U,$J,358.3,9754,0)
 ;;=365.70^^77^662^7
 ;;^UTILITY(U,$J,358.3,9754,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9754,1,3,0)
 ;;=3^Glaucoma Stage NOS
 ;;^UTILITY(U,$J,358.3,9754,1,4,0)
 ;;=4^365.70
 ;;^UTILITY(U,$J,358.3,9754,2)
 ;;=^340609
 ;;^UTILITY(U,$J,358.3,9755,0)
 ;;=365.71^^77^662^11
 ;;^UTILITY(U,$J,358.3,9755,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9755,1,3,0)
 ;;=3^Mild Stage Glaucoma
 ;;^UTILITY(U,$J,358.3,9755,1,4,0)
 ;;=4^365.71
 ;;^UTILITY(U,$J,358.3,9755,2)
 ;;=^340513
 ;;^UTILITY(U,$J,358.3,9756,0)
 ;;=365.72^^77^662^12
 ;;^UTILITY(U,$J,358.3,9756,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,9756,1,3,0)
 ;;=3^Moderate Stage Glaucoma
 ;;^UTILITY(U,$J,358.3,9756,1,4,0)
 ;;=4^365.72
 ;;^UTILITY(U,$J,358.3,9756,2)
 ;;=^340514
 ;;^UTILITY(U,$J,358.3,9757,0)
 ;;=365.73^^77^662^30
 ;;^UTILITY(U,$J,358.3,9757,1,0)
 ;;=^358.31IA^4^2
