IBDEI03S ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,4642,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4642,1,4,0)
 ;;=4^CA of Fundus of Stomach
 ;;^UTILITY(U,$J,358.3,4642,1,5,0)
 ;;=5^151.3
 ;;^UTILITY(U,$J,358.3,4642,2)
 ;;=CA of Fundus of Stomach^267066
 ;;^UTILITY(U,$J,358.3,4643,0)
 ;;=151.6^^44^321^16
 ;;^UTILITY(U,$J,358.3,4643,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4643,1,4,0)
 ;;=4^CA of Great Curve of Stomach
 ;;^UTILITY(U,$J,358.3,4643,1,5,0)
 ;;=5^151.6
 ;;^UTILITY(U,$J,358.3,4643,2)
 ;;=^267069
 ;;^UTILITY(U,$J,358.3,4644,0)
 ;;=151.5^^44^321^36
 ;;^UTILITY(U,$J,358.3,4644,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4644,1,4,0)
 ;;=4^Ca,Stomach Less Curv
 ;;^UTILITY(U,$J,358.3,4644,1,5,0)
 ;;=5^151.5
 ;;^UTILITY(U,$J,358.3,4644,2)
 ;;=CA of Lesser Curve of Stomach^267068
 ;;^UTILITY(U,$J,358.3,4645,0)
 ;;=151.1^^44^321^37
 ;;^UTILITY(U,$J,358.3,4645,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4645,1,4,0)
 ;;=4^Cancer of Pylorus
 ;;^UTILITY(U,$J,358.3,4645,1,5,0)
 ;;=5^151.1
 ;;^UTILITY(U,$J,358.3,4645,2)
 ;;=Cancer of Pylorus^267064
 ;;^UTILITY(U,$J,358.3,4646,0)
 ;;=151.8^^44^321^33
 ;;^UTILITY(U,$J,358.3,4646,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4646,1,4,0)
 ;;=4^CA, Stomach, Other
 ;;^UTILITY(U,$J,358.3,4646,1,5,0)
 ;;=5^151.8
 ;;^UTILITY(U,$J,358.3,4646,2)
 ;;=CA, Stomach, Other^267070
 ;;^UTILITY(U,$J,358.3,4647,0)
 ;;=150.9^^44^321^14
 ;;^UTILITY(U,$J,358.3,4647,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4647,1,4,0)
 ;;=4^CA of Esophagus
 ;;^UTILITY(U,$J,358.3,4647,1,5,0)
 ;;=5^150.9
 ;;^UTILITY(U,$J,358.3,4647,2)
 ;;=CA of Esophagus^267055
 ;;^UTILITY(U,$J,358.3,4648,0)
 ;;=150.2^^44^321^3
 ;;^UTILITY(U,$J,358.3,4648,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4648,1,4,0)
 ;;=4^CA of Abdominal Esophagus
 ;;^UTILITY(U,$J,358.3,4648,1,5,0)
 ;;=5^150.2
 ;;^UTILITY(U,$J,358.3,4648,2)
 ;;=CA of Abdominal Esophagus^267058
 ;;^UTILITY(U,$J,358.3,4649,0)
 ;;=150.0^^44^321^7
 ;;^UTILITY(U,$J,358.3,4649,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4649,1,4,0)
 ;;=4^CA of Cervical Esophagus
 ;;^UTILITY(U,$J,358.3,4649,1,5,0)
 ;;=5^150.0
 ;;^UTILITY(U,$J,358.3,4649,2)
 ;;=CA of Cervical Esophagus^267056
 ;;^UTILITY(U,$J,358.3,4650,0)
 ;;=150.5^^44^321^19
 ;;^UTILITY(U,$J,358.3,4650,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4650,1,4,0)
 ;;=4^CA of Lower Esophagus
 ;;^UTILITY(U,$J,358.3,4650,1,5,0)
 ;;=5^150.5
 ;;^UTILITY(U,$J,358.3,4650,2)
 ;;=CA of Lower Esophagus^267061
 ;;^UTILITY(U,$J,358.3,4651,0)
 ;;=150.4^^44^321^20
 ;;^UTILITY(U,$J,358.3,4651,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4651,1,4,0)
 ;;=4^CA of Middle Esoph
 ;;^UTILITY(U,$J,358.3,4651,1,5,0)
 ;;=5^150.4
 ;;^UTILITY(U,$J,358.3,4651,2)
 ;;=CA of Middle Esoph^267060
 ;;^UTILITY(U,$J,358.3,4652,0)
 ;;=150.3^^44^321^34
 ;;^UTILITY(U,$J,358.3,4652,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4652,1,4,0)
 ;;=4^CA, Upper Esophagus
 ;;^UTILITY(U,$J,358.3,4652,1,5,0)
 ;;=5^150.3
 ;;^UTILITY(U,$J,358.3,4652,2)
 ;;=CA, Upper Esophagus^267059
 ;;^UTILITY(U,$J,358.3,4653,0)
 ;;=150.1^^44^321^31
 ;;^UTILITY(U,$J,358.3,4653,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4653,1,4,0)
 ;;=4^CA of Thoracic Esophagus
 ;;^UTILITY(U,$J,358.3,4653,1,5,0)
 ;;=5^150.1
 ;;^UTILITY(U,$J,358.3,4653,2)
 ;;=CA of Thoracic Esophagus^267057
 ;;^UTILITY(U,$J,358.3,4654,0)
 ;;=157.9^^44^321^21
 ;;^UTILITY(U,$J,358.3,4654,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4654,1,4,0)
 ;;=4^CA of Pancreas
 ;;^UTILITY(U,$J,358.3,4654,1,5,0)
 ;;=5^157.9
 ;;^UTILITY(U,$J,358.3,4654,2)
 ;;=CA of Pancreas^267103
 ;;^UTILITY(U,$J,358.3,4655,0)
 ;;=157.1^^44^321^23
 ;;^UTILITY(U,$J,358.3,4655,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4655,1,4,0)
 ;;=4^CA of Pancreatic Body
 ;;^UTILITY(U,$J,358.3,4655,1,5,0)
 ;;=5^157.1
 ;;^UTILITY(U,$J,358.3,4655,2)
 ;;=^267105
 ;;^UTILITY(U,$J,358.3,4656,0)
 ;;=157.3^^44^321^24
 ;;^UTILITY(U,$J,358.3,4656,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4656,1,4,0)
 ;;=4^CA of Pancreatic Duct
 ;;^UTILITY(U,$J,358.3,4656,1,5,0)
 ;;=5^157.3
 ;;^UTILITY(U,$J,358.3,4656,2)
 ;;=CA of Pancreatic Duct^267107
 ;;^UTILITY(U,$J,358.3,4657,0)
 ;;=157.0^^44^321^25
 ;;^UTILITY(U,$J,358.3,4657,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4657,1,4,0)
 ;;=4^CA of Pancreatic Head
 ;;^UTILITY(U,$J,358.3,4657,1,5,0)
 ;;=5^157.0
 ;;^UTILITY(U,$J,358.3,4657,2)
 ;;=CA of Pancreatic Head^267104
 ;;^UTILITY(U,$J,358.3,4658,0)
 ;;=157.4^^44^321^18
 ;;^UTILITY(U,$J,358.3,4658,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4658,1,4,0)
 ;;=4^CA of Islet of Langerhans
 ;;^UTILITY(U,$J,358.3,4658,1,5,0)
 ;;=5^157.4
 ;;^UTILITY(U,$J,358.3,4658,2)
 ;;=CA of Islet of Langerhans^267108
 ;;^UTILITY(U,$J,358.3,4659,0)
 ;;=157.8^^44^321^22
 ;;^UTILITY(U,$J,358.3,4659,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4659,1,4,0)
 ;;=4^CA of Pancreas, Oth
 ;;^UTILITY(U,$J,358.3,4659,1,5,0)
 ;;=5^157.8
 ;;^UTILITY(U,$J,358.3,4659,2)
 ;;=CA of Pancreas, Oth^267109
 ;;^UTILITY(U,$J,358.3,4660,0)
 ;;=157.2^^44^321^26
 ;;^UTILITY(U,$J,358.3,4660,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4660,1,4,0)
 ;;=4^CA of Pancreatic Tail
 ;;^UTILITY(U,$J,358.3,4660,1,5,0)
 ;;=5^157.2
 ;;^UTILITY(U,$J,358.3,4660,2)
 ;;=CA of Pancreatic Tail^267106
 ;;^UTILITY(U,$J,358.3,4661,0)
 ;;=789.1^^44^321^57
 ;;^UTILITY(U,$J,358.3,4661,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4661,1,4,0)
 ;;=4^Hepatomegaly
 ;;^UTILITY(U,$J,358.3,4661,1,5,0)
 ;;=5^789.1
 ;;^UTILITY(U,$J,358.3,4661,2)
 ;;=^56494
 ;;^UTILITY(U,$J,358.3,4662,0)
 ;;=211.3^^44^321^77
 ;;^UTILITY(U,$J,358.3,4662,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4662,1,4,0)
 ;;=4^Polyp,Colon
 ;;^UTILITY(U,$J,358.3,4662,1,5,0)
 ;;=5^211.3
 ;;^UTILITY(U,$J,358.3,4662,2)
 ;;=^13295
 ;;^UTILITY(U,$J,358.3,4663,0)
 ;;=792.1^^44^321^74
 ;;^UTILITY(U,$J,358.3,4663,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4663,1,4,0)
 ;;=4^Occult Blood In Stool
 ;;^UTILITY(U,$J,358.3,4663,1,5,0)
 ;;=5^792.1
 ;;^UTILITY(U,$J,358.3,4663,2)
 ;;=^273412
 ;;^UTILITY(U,$J,358.3,4664,0)
 ;;=564.00^^44^321^43
 ;;^UTILITY(U,$J,358.3,4664,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4664,1,4,0)
 ;;=4^Constipation
 ;;^UTILITY(U,$J,358.3,4664,1,5,0)
 ;;=5^564.00
 ;;^UTILITY(U,$J,358.3,4664,2)
 ;;=^323537
 ;;^UTILITY(U,$J,358.3,4665,0)
 ;;=560.39^^44^321^64
 ;;^UTILITY(U,$J,358.3,4665,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4665,1,4,0)
 ;;=4^Impaction,Fecal
 ;;^UTILITY(U,$J,358.3,4665,1,5,0)
 ;;=5^560.39
 ;;^UTILITY(U,$J,358.3,4665,2)
 ;;=^87650
 ;;^UTILITY(U,$J,358.3,4666,0)
 ;;=455.3^^44^321^54
 ;;^UTILITY(U,$J,358.3,4666,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4666,1,4,0)
 ;;=4^Hemorrhoid,External
 ;;^UTILITY(U,$J,358.3,4666,1,5,0)
 ;;=5^455.3
 ;;^UTILITY(U,$J,358.3,4666,2)
 ;;=^269827
 ;;^UTILITY(U,$J,358.3,4667,0)
 ;;=571.5^^44^321^41
 ;;^UTILITY(U,$J,358.3,4667,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4667,1,4,0)
 ;;=4^Cirrhosis,Nonalcoholic
 ;;^UTILITY(U,$J,358.3,4667,1,5,0)
 ;;=5^571.5
 ;;^UTILITY(U,$J,358.3,4667,2)
 ;;=^24731
 ;;^UTILITY(U,$J,358.3,4668,0)
 ;;=553.21^^44^321^59
 ;;^UTILITY(U,$J,358.3,4668,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4668,1,4,0)
 ;;=4^Hernia,Incisional
 ;;^UTILITY(U,$J,358.3,4668,1,5,0)
 ;;=5^553.21
 ;;^UTILITY(U,$J,358.3,4668,2)
 ;;=^270246
 ;;^UTILITY(U,$J,358.3,4669,0)
 ;;=530.20^^44^321^78
 ;;^UTILITY(U,$J,358.3,4669,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4669,1,4,0)
 ;;=4^Ulcer of Esophagus
 ;;^UTILITY(U,$J,358.3,4669,1,5,0)
 ;;=5^530.20
 ;;^UTILITY(U,$J,358.3,4669,2)
 ;;=^329929
 ;;^UTILITY(U,$J,358.3,4670,0)
 ;;=555.0^^44^321^45
 ;;^UTILITY(U,$J,358.3,4670,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4670,1,4,0)
 ;;=4^Crohn's/Enteritis Sm Intes
 ;;^UTILITY(U,$J,358.3,4670,1,5,0)
 ;;=5^555.0
 ;;^UTILITY(U,$J,358.3,4670,2)
 ;;=^270247
 ;;^UTILITY(U,$J,358.3,4671,0)
 ;;=555.1^^44^321^44
 ;;^UTILITY(U,$J,358.3,4671,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4671,1,4,0)
 ;;=4^Crohn's/Enteritis Lg Intes
 ;;^UTILITY(U,$J,358.3,4671,1,5,0)
 ;;=5^555.1
 ;;^UTILITY(U,$J,358.3,4671,2)
 ;;=^270248
 ;;^UTILITY(U,$J,358.3,4672,0)
 ;;=555.2^^44^321^47
 ;;^UTILITY(U,$J,358.3,4672,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4672,1,4,0)
 ;;=4^Crohn/Enteri Sm/Lg Intes
 ;;^UTILITY(U,$J,358.3,4672,1,5,0)
 ;;=5^555.2
 ;;^UTILITY(U,$J,358.3,4672,2)
 ;;=^270249
 ;;^UTILITY(U,$J,358.3,4673,0)
 ;;=531.90^^44^321^53
 ;;^UTILITY(U,$J,358.3,4673,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4673,1,4,0)
 ;;=4^Gastric Ulcer
 ;;^UTILITY(U,$J,358.3,4673,1,5,0)
 ;;=5^531.90
 ;;^UTILITY(U,$J,358.3,4673,2)
 ;;=^51128
 ;;^UTILITY(U,$J,358.3,4674,0)
 ;;=536.41^^44^321^66
 ;;^UTILITY(U,$J,358.3,4674,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4674,1,4,0)
 ;;=4^Infection of Gastrostomy
 ;;^UTILITY(U,$J,358.3,4674,1,5,0)
 ;;=5^536.41
 ;;^UTILITY(U,$J,358.3,4674,2)
 ;;=^321189
 ;;^UTILITY(U,$J,358.3,4675,0)
 ;;=536.42^^44^321^70
 ;;^UTILITY(U,$J,358.3,4675,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4675,1,4,0)
 ;;=4^Mech Compl of Gastrostomy
 ;;^UTILITY(U,$J,358.3,4675,1,5,0)
 ;;=5^536.42
 ;;^UTILITY(U,$J,358.3,4675,2)
 ;;=^321190
 ;;^UTILITY(U,$J,358.3,4676,0)
 ;;=536.49^^44^321^76
 ;;^UTILITY(U,$J,358.3,4676,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4676,1,4,0)
 ;;=4^Oth Gastrostomy Compl
 ;;^UTILITY(U,$J,358.3,4676,1,5,0)
 ;;=5^536.49
 ;;^UTILITY(U,$J,358.3,4676,2)
 ;;=^321191
 ;;^UTILITY(U,$J,358.3,4677,0)
 ;;=569.61^^44^321^65
 ;;^UTILITY(U,$J,358.3,4677,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4677,1,4,0)
 ;;=4^Infect Colos/Enteros
 ;;^UTILITY(U,$J,358.3,4677,1,5,0)
 ;;=5^569.61
 ;;^UTILITY(U,$J,358.3,4677,2)
 ;;=^303300
 ;;^UTILITY(U,$J,358.3,4678,0)
 ;;=569.62^^44^321^69
 ;;^UTILITY(U,$J,358.3,4678,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,4678,1,4,0)
 ;;=4^Mech Compl Colost/Enterost
 ;;^UTILITY(U,$J,358.3,4678,1,5,0)
 ;;=5^569.62
 ;;^UTILITY(U,$J,358.3,4678,2)
 ;;=^321192
 ;;^UTILITY(U,$J,358.3,4679,0)
 ;;=569.69^^44^321^75
