IBDEI0FQ ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,21126,2)
 ;;=^328568
 ;;^UTILITY(U,$J,358.3,21127,0)
 ;;=307.89^^159^1385^13
 ;;^UTILITY(U,$J,358.3,21127,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21127,1,3,0)
 ;;=3^PSYCHOGENIC PAIN NEC
 ;;^UTILITY(U,$J,358.3,21127,1,4,0)
 ;;=4^307.89
 ;;^UTILITY(U,$J,358.3,21127,2)
 ;;=^331947
 ;;^UTILITY(U,$J,358.3,21128,0)
 ;;=958.3^^159^1385^12
 ;;^UTILITY(U,$J,358.3,21128,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21128,1,3,0)
 ;;=3^POSTTRAUM WND INFEC NEC
 ;;^UTILITY(U,$J,358.3,21128,1,4,0)
 ;;=4^958.3
 ;;^UTILITY(U,$J,358.3,21128,2)
 ;;=^97141
 ;;^UTILITY(U,$J,358.3,21129,0)
 ;;=V82.89^^159^1385^14
 ;;^UTILITY(U,$J,358.3,21129,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21129,1,3,0)
 ;;=3^SPC SCRN OTH SPEC CONDITIONS
 ;;^UTILITY(U,$J,358.3,21129,1,4,0)
 ;;=4^V82.89
 ;;^UTILITY(U,$J,358.3,21129,2)
 ;;=^322099
 ;;^UTILITY(U,$J,358.3,21130,0)
 ;;=524.60^^159^1385^16
 ;;^UTILITY(U,$J,358.3,21130,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21130,1,3,0)
 ;;=3^TMJ
 ;;^UTILITY(U,$J,358.3,21130,1,4,0)
 ;;=4^524.60
 ;;^UTILITY(U,$J,358.3,21130,2)
 ;;=^117722
 ;;^UTILITY(U,$J,358.3,21131,0)
 ;;=788.30^^159^1385^17
 ;;^UTILITY(U,$J,358.3,21131,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21131,1,3,0)
 ;;=3^URINARY INCONTIN
 ;;^UTILITY(U,$J,358.3,21131,1,4,0)
 ;;=4^788.30
 ;;^UTILITY(U,$J,358.3,21131,2)
 ;;=^124400
 ;;^UTILITY(U,$J,358.3,21132,0)
 ;;=783.0^^159^1385^1
 ;;^UTILITY(U,$J,358.3,21132,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21132,1,3,0)
 ;;=3^ANOREXIA
 ;;^UTILITY(U,$J,358.3,21132,1,4,0)
 ;;=4^783.0
 ;;^UTILITY(U,$J,358.3,21132,2)
 ;;=^7939
 ;;^UTILITY(U,$J,358.3,21133,0)
 ;;=780.79^^159^1385^11
 ;;^UTILITY(U,$J,358.3,21133,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21133,1,3,0)
 ;;=3^OTHER MALAISE AND FATIGUE
 ;;^UTILITY(U,$J,358.3,21133,1,4,0)
 ;;=4^780.79
 ;;^UTILITY(U,$J,358.3,21133,2)
 ;;=^73344
 ;;^UTILITY(U,$J,358.3,21134,0)
 ;;=V15.52^^160^1386^10
 ;;^UTILITY(U,$J,358.3,21134,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21134,1,3,0)
 ;;=3^HX TRAUMATC BRAIN INJURY
 ;;^UTILITY(U,$J,358.3,21134,1,4,0)
 ;;=4^V15.52
 ;;^UTILITY(U,$J,358.3,21134,2)
 ;;=^338495
 ;;^UTILITY(U,$J,358.3,21135,0)
 ;;=V80.01^^160^1386^11
 ;;^UTILITY(U,$J,358.3,21135,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21135,1,3,0)
 ;;=3^SCREEN FOR TBI
 ;;^UTILITY(U,$J,358.3,21135,1,4,0)
 ;;=4^V80.01
 ;;^UTILITY(U,$J,358.3,21135,2)
 ;;=^338517
 ;;^UTILITY(U,$J,358.3,21136,0)
 ;;=850.0^^160^1386^7
 ;;^UTILITY(U,$J,358.3,21136,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21136,1,3,0)
 ;;=3^CONCUSSION W/O LOSS OF CONSCIOUSNESS
 ;;^UTILITY(U,$J,358.3,21136,1,4,0)
 ;;=4^850.0
 ;;^UTILITY(U,$J,358.3,21136,2)
 ;;=^274540
 ;;^UTILITY(U,$J,358.3,21137,0)
 ;;=850.11^^160^1386^3
 ;;^UTILITY(U,$J,358.3,21137,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21137,1,3,0)
 ;;=3^CONCUS-BRIEF LOC <31 MN
 ;;^UTILITY(U,$J,358.3,21137,1,4,0)
 ;;=4^850.11
 ;;^UTILITY(U,$J,358.3,21137,2)
 ;;=^329958
 ;;^UTILITY(U,$J,358.3,21138,0)
 ;;=850.12^^160^1386^2
 ;;^UTILITY(U,$J,358.3,21138,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21138,1,3,0)
 ;;=3^CONCUS-BRIEF LOC 31-59 MN
 ;;^UTILITY(U,$J,358.3,21138,1,4,0)
 ;;=4^850.12
 ;;^UTILITY(U,$J,358.3,21138,2)
 ;;=^329959
 ;;^UTILITY(U,$J,358.3,21139,0)
 ;;=850.2^^160^1386^8
 ;;^UTILITY(U,$J,358.3,21139,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21139,1,3,0)
 ;;=3^CONCUSSION-MODERATE LOC
 ;;^UTILITY(U,$J,358.3,21139,1,4,0)
 ;;=4^850.2
 ;;^UTILITY(U,$J,358.3,21139,2)
 ;;=^274542
 ;;^UTILITY(U,$J,358.3,21140,0)
 ;;=850.3^^160^1386^9
 ;;^UTILITY(U,$J,358.3,21140,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21140,1,3,0)
 ;;=3^CONCUSSION-PROLONG LOC
 ;;^UTILITY(U,$J,358.3,21140,1,4,0)
 ;;=4^850.3
 ;;^UTILITY(U,$J,358.3,21140,2)
 ;;=^274543
 ;;^UTILITY(U,$J,358.3,21141,0)
 ;;=850.4^^160^1386^4
 ;;^UTILITY(U,$J,358.3,21141,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21141,1,3,0)
 ;;=3^CONCUS-PROLONG LOC W/O RTN TO PRE-EXIST
 ;;^UTILITY(U,$J,358.3,21141,1,4,0)
 ;;=4^850.4
 ;;^UTILITY(U,$J,358.3,21141,2)
 ;;=^274544
 ;;^UTILITY(U,$J,358.3,21142,0)
 ;;=850.5^^160^1386^6
 ;;^UTILITY(U,$J,358.3,21142,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21142,1,3,0)
 ;;=3^CONCUSSION W LOS TIME NOS
 ;;^UTILITY(U,$J,358.3,21142,1,4,0)
 ;;=4^850.5
 ;;^UTILITY(U,$J,358.3,21142,2)
 ;;=^274545
 ;;^UTILITY(U,$J,358.3,21143,0)
 ;;=850.9^^160^1386^5
 ;;^UTILITY(U,$J,358.3,21143,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21143,1,3,0)
 ;;=3^CONCUSSION NOS
 ;;^UTILITY(U,$J,358.3,21143,1,4,0)
 ;;=4^850.9
 ;;^UTILITY(U,$J,358.3,21143,2)
 ;;=^186748
 ;;^UTILITY(U,$J,358.3,21144,0)
 ;;=854.00^^160^1386^1
 ;;^UTILITY(U,$J,358.3,21144,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21144,1,3,0)
 ;;=3^TBI NEC-NOS w/o Opn Wnd/LOC NOS
 ;;^UTILITY(U,$J,358.3,21144,1,4,0)
 ;;=4^854.00
 ;;^UTILITY(U,$J,358.3,21144,2)
 ;;=^64996
 ;;^UTILITY(U,$J,358.3,21145,0)
 ;;=854.01^^160^1386^16
 ;;^UTILITY(U,$J,358.3,21145,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21145,1,3,0)
 ;;=3^TBI NEC-NOS w/o Opn Wnd/LOC - None
 ;;^UTILITY(U,$J,358.3,21145,1,4,0)
 ;;=4^854.01
 ;;^UTILITY(U,$J,358.3,21145,2)
 ;;=^274712
 ;;^UTILITY(U,$J,358.3,21146,0)
 ;;=854.02^^160^1386^13
 ;;^UTILITY(U,$J,358.3,21146,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21146,1,3,0)
 ;;=3^TBI NEC-NOS W/O OPN WND/LOC BRIEF
 ;;^UTILITY(U,$J,358.3,21146,1,4,0)
 ;;=4^854.02
 ;;^UTILITY(U,$J,358.3,21146,2)
 ;;=^274713
 ;;^UTILITY(U,$J,358.3,21147,0)
 ;;=854.03^^160^1386^15
 ;;^UTILITY(U,$J,358.3,21147,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21147,1,3,0)
 ;;=3^TBI NEC-NOS W/O OPN WND/LOC MOD
 ;;^UTILITY(U,$J,358.3,21147,1,4,0)
 ;;=4^854.03
 ;;^UTILITY(U,$J,358.3,21147,2)
 ;;=^274714
 ;;^UTILITY(U,$J,358.3,21148,0)
 ;;=854.04^^160^1386^17
 ;;^UTILITY(U,$J,358.3,21148,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21148,1,3,0)
 ;;=3^TBI NEC-NOS W/O OPN WND/LOC PROLONG
 ;;^UTILITY(U,$J,358.3,21148,1,4,0)
 ;;=4^854.04
 ;;^UTILITY(U,$J,358.3,21148,2)
 ;;=^274715
 ;;^UTILITY(U,$J,358.3,21149,0)
 ;;=854.05^^160^1386^14
 ;;^UTILITY(U,$J,358.3,21149,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21149,1,3,0)
 ;;=3^TBI NEC-NOS W/O OPN WND/LOC DEEP
 ;;^UTILITY(U,$J,358.3,21149,1,4,0)
 ;;=4^854.05
 ;;^UTILITY(U,$J,358.3,21149,2)
 ;;=^274716
 ;;^UTILITY(U,$J,358.3,21150,0)
 ;;=854.06^^160^1386^18
 ;;^UTILITY(U,$J,358.3,21150,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21150,1,3,0)
 ;;=3^TBI NEC-NOS W/O OPN WND/LOC TIME NOS
 ;;^UTILITY(U,$J,358.3,21150,1,4,0)
 ;;=4^854.06
 ;;^UTILITY(U,$J,358.3,21150,2)
 ;;=^274717
 ;;^UTILITY(U,$J,358.3,21151,0)
 ;;=854.09^^160^1386^12
 ;;^UTILITY(U,$J,358.3,21151,1,0)
 ;;=^358.31IA^4^2
 ;;^UTILITY(U,$J,358.3,21151,1,3,0)
 ;;=3^TBI NEC-NOS W/O OPN WND W/ CONCUSSION
 ;;^UTILITY(U,$J,358.3,21151,1,4,0)
 ;;=4^854.09
 ;;^UTILITY(U,$J,358.3,21151,2)
 ;;=^274718
 ;;^UTILITY(U,$J,358.3,21152,0)
 ;;=99201^^161^1387^1
 ;;^UTILITY(U,$J,358.3,21152,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21152,1,1,0)
 ;;=1^Problem Focus
 ;;^UTILITY(U,$J,358.3,21152,1,2,0)
 ;;=2^99201
 ;;^UTILITY(U,$J,358.3,21153,0)
 ;;=99202^^161^1387^2
 ;;^UTILITY(U,$J,358.3,21153,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21153,1,1,0)
 ;;=1^Expanded Problem Focus
 ;;^UTILITY(U,$J,358.3,21153,1,2,0)
 ;;=2^99202
 ;;^UTILITY(U,$J,358.3,21154,0)
 ;;=99203^^161^1387^3
 ;;^UTILITY(U,$J,358.3,21154,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21154,1,1,0)
 ;;=1^Detailed
 ;;^UTILITY(U,$J,358.3,21154,1,2,0)
 ;;=2^99203
 ;;^UTILITY(U,$J,358.3,21155,0)
 ;;=99204^^161^1387^4
 ;;^UTILITY(U,$J,358.3,21155,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21155,1,1,0)
 ;;=1^Comprehensive, Moderate
 ;;^UTILITY(U,$J,358.3,21155,1,2,0)
 ;;=2^99204
 ;;^UTILITY(U,$J,358.3,21156,0)
 ;;=99205^^161^1387^5
 ;;^UTILITY(U,$J,358.3,21156,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21156,1,1,0)
 ;;=1^Comprehensive, High
 ;;^UTILITY(U,$J,358.3,21156,1,2,0)
 ;;=2^99205
 ;;^UTILITY(U,$J,358.3,21157,0)
 ;;=99211^^161^1388^1
 ;;^UTILITY(U,$J,358.3,21157,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21157,1,1,0)
 ;;=1^Brief (no MD seen)
 ;;^UTILITY(U,$J,358.3,21157,1,2,0)
 ;;=2^99211
 ;;^UTILITY(U,$J,358.3,21158,0)
 ;;=99212^^161^1388^2
 ;;^UTILITY(U,$J,358.3,21158,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21158,1,1,0)
 ;;=1^Problem Focused
 ;;^UTILITY(U,$J,358.3,21158,1,2,0)
 ;;=2^99212
 ;;^UTILITY(U,$J,358.3,21159,0)
 ;;=99213^^161^1388^3
 ;;^UTILITY(U,$J,358.3,21159,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21159,1,1,0)
 ;;=1^Expanded Problem Focus
 ;;^UTILITY(U,$J,358.3,21159,1,2,0)
 ;;=2^99213
 ;;^UTILITY(U,$J,358.3,21160,0)
 ;;=99214^^161^1388^4
 ;;^UTILITY(U,$J,358.3,21160,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21160,1,1,0)
 ;;=1^Detailed
 ;;^UTILITY(U,$J,358.3,21160,1,2,0)
 ;;=2^99214
 ;;^UTILITY(U,$J,358.3,21161,0)
 ;;=99215^^161^1388^5
 ;;^UTILITY(U,$J,358.3,21161,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21161,1,1,0)
 ;;=1^Comprehensive
 ;;^UTILITY(U,$J,358.3,21161,1,2,0)
 ;;=2^99215
 ;;^UTILITY(U,$J,358.3,21162,0)
 ;;=99241^^161^1389^1
 ;;^UTILITY(U,$J,358.3,21162,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21162,1,1,0)
 ;;=1^Problem Focused
 ;;^UTILITY(U,$J,358.3,21162,1,2,0)
 ;;=2^99241
 ;;^UTILITY(U,$J,358.3,21163,0)
 ;;=99242^^161^1389^2
 ;;^UTILITY(U,$J,358.3,21163,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21163,1,1,0)
 ;;=1^Expanded Problem Focus
 ;;^UTILITY(U,$J,358.3,21163,1,2,0)
 ;;=2^99242
 ;;^UTILITY(U,$J,358.3,21164,0)
 ;;=99243^^161^1389^3
 ;;^UTILITY(U,$J,358.3,21164,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21164,1,1,0)
 ;;=1^Detailed
 ;;^UTILITY(U,$J,358.3,21164,1,2,0)
 ;;=2^99243
 ;;^UTILITY(U,$J,358.3,21165,0)
 ;;=99244^^161^1389^4
 ;;^UTILITY(U,$J,358.3,21165,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,21165,1,1,0)
 ;;=1^Comprehensive, Moderate
 ;;^UTILITY(U,$J,358.3,21165,1,2,0)
 ;;=2^99244
 ;;^UTILITY(U,$J,358.3,21166,0)
 ;;=99245^^161^1389^5
 ;;^UTILITY(U,$J,358.3,21166,1,0)
 ;;=^358.31IA^2^2
