IBDEI0BV ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,15795,1,2,0)
 ;;=2^Open Tx of tarsometatarsal joint dislocation, with or w/o internal or external fixation
 ;;^UTILITY(U,$J,358.3,15795,1,3,0)
 ;;=3^28615
 ;;^UTILITY(U,$J,358.3,15796,0)
 ;;=28630^^114^980^58^^^^1
 ;;^UTILITY(U,$J,358.3,15796,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15796,1,2,0)
 ;;=2^Closed Tx of metatarsophalangeal joint dislocation; w/o anesthesia
 ;;^UTILITY(U,$J,358.3,15796,1,3,0)
 ;;=3^28630
 ;;^UTILITY(U,$J,358.3,15797,0)
 ;;=28635^^114^980^59^^^^1
 ;;^UTILITY(U,$J,358.3,15797,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15797,1,2,0)
 ;;=2^Closed Tx of metatarsophalangeal joint dislocation; requiring anesthesia
 ;;^UTILITY(U,$J,358.3,15797,1,3,0)
 ;;=3^28635
 ;;^UTILITY(U,$J,358.3,15798,0)
 ;;=28636^^114^980^60^^^^1
 ;;^UTILITY(U,$J,358.3,15798,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15798,1,2,0)
 ;;=2^Perc Fixation Metatarsophalangeal Joint Dislocation
 ;;^UTILITY(U,$J,358.3,15798,1,3,0)
 ;;=3^28636
 ;;^UTILITY(U,$J,358.3,15799,0)
 ;;=28645^^114^980^61^^^^1
 ;;^UTILITY(U,$J,358.3,15799,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15799,1,2,0)
 ;;=2^Open Tx of metatarsophalangeal joint dislocation, with or w/o internal or external fixation
 ;;^UTILITY(U,$J,358.3,15799,1,3,0)
 ;;=3^28645
 ;;^UTILITY(U,$J,358.3,15800,0)
 ;;=28660^^114^980^62^^^^1
 ;;^UTILITY(U,$J,358.3,15800,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15800,1,2,0)
 ;;=2^Closed Tx of interphalangeal joint dislocation; w/o anesthesia
 ;;^UTILITY(U,$J,358.3,15800,1,3,0)
 ;;=3^28660
 ;;^UTILITY(U,$J,358.3,15801,0)
 ;;=28665^^114^980^63^^^^1
 ;;^UTILITY(U,$J,358.3,15801,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15801,1,2,0)
 ;;=2^Closed Tx of interphalangeal joint dislocation; requiring anesthesia
 ;;^UTILITY(U,$J,358.3,15801,1,3,0)
 ;;=3^28665
 ;;^UTILITY(U,$J,358.3,15802,0)
 ;;=28666^^114^980^64^^^^1
 ;;^UTILITY(U,$J,358.3,15802,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15802,1,2,0)
 ;;=2^Perc Fixation Interphalangeal Joint Dislocation
 ;;^UTILITY(U,$J,358.3,15802,1,3,0)
 ;;=3^28666
 ;;^UTILITY(U,$J,358.3,15803,0)
 ;;=28675^^114^980^65^^^^1
 ;;^UTILITY(U,$J,358.3,15803,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15803,1,2,0)
 ;;=2^Open Tx of interphalangeal joint dislocation, with or without internal or external fixation
 ;;^UTILITY(U,$J,358.3,15803,1,3,0)
 ;;=3^28675
 ;;^UTILITY(U,$J,358.3,15804,0)
 ;;=27840^^114^980^66^^^^1
 ;;^UTILITY(U,$J,358.3,15804,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15804,1,2,0)
 ;;=2^Closed Tx of ankle dislocation; w/o anesthesia
 ;;^UTILITY(U,$J,358.3,15804,1,3,0)
 ;;=3^27840
 ;;^UTILITY(U,$J,358.3,15805,0)
 ;;=27842^^114^980^67^^^^1
 ;;^UTILITY(U,$J,358.3,15805,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15805,1,2,0)
 ;;=2^Closed Tx of ankle dislocation; requiring anesthesia, with or w/o percutaneous skeletal fixation
 ;;^UTILITY(U,$J,358.3,15805,1,3,0)
 ;;=3^27842
 ;;^UTILITY(U,$J,358.3,15806,0)
 ;;=27846^^114^980^68^^^^1
 ;;^UTILITY(U,$J,358.3,15806,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15806,1,2,0)
 ;;=2^Open Tx of ankle dislocation, with or w/o percutaneous skeletal fixation; w/o repair or internal fixation
 ;;^UTILITY(U,$J,358.3,15806,1,3,0)
 ;;=3^27846
 ;;^UTILITY(U,$J,358.3,15807,0)
 ;;=27848^^114^980^69^^^^1
 ;;^UTILITY(U,$J,358.3,15807,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15807,1,2,0)
 ;;=2^Open Tx of ankle dislocation, with or w/o precutaneous skeletalfixation; with repair or internal or external fixation
 ;;^UTILITY(U,$J,358.3,15807,1,3,0)
 ;;=3^27848
 ;;^UTILITY(U,$J,358.3,15808,0)
 ;;=28750^^114^981^1^^^^1
 ;;^UTILITY(U,$J,358.3,15808,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15808,1,2,0)
 ;;=2^Arthrodesis, great toe; metartarsophalangeal joint
 ;;^UTILITY(U,$J,358.3,15808,1,3,0)
 ;;=3^28750
 ;;^UTILITY(U,$J,358.3,15809,0)
 ;;=28755^^114^981^2^^^^1
 ;;^UTILITY(U,$J,358.3,15809,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15809,1,2,0)
 ;;=2^Arthrodesis, great toe; interphlangeal joint
 ;;^UTILITY(U,$J,358.3,15809,1,3,0)
 ;;=3^28755
 ;;^UTILITY(U,$J,358.3,15810,0)
 ;;=27870^^114^981^3^^^^1
 ;;^UTILITY(U,$J,358.3,15810,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15810,1,2,0)
 ;;=2^Arthrodesis, ankle, any method
 ;;^UTILITY(U,$J,358.3,15810,1,3,0)
 ;;=3^27870
 ;;^UTILITY(U,$J,358.3,15811,0)
 ;;=27871^^114^981^4^^^^1
 ;;^UTILITY(U,$J,358.3,15811,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15811,1,2,0)
 ;;=2^Arthrodesis, tibiofibular joint, proximal or distal
 ;;^UTILITY(U,$J,358.3,15811,1,3,0)
 ;;=3^27871
 ;;^UTILITY(U,$J,358.3,15812,0)
 ;;=29358^^114^982^2^^^^1
 ;;^UTILITY(U,$J,358.3,15812,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15812,1,2,0)
 ;;=2^Application Of Long Leg Cast Brace
 ;;^UTILITY(U,$J,358.3,15812,1,3,0)
 ;;=3^29358
 ;;^UTILITY(U,$J,358.3,15813,0)
 ;;=29405^^114^982^4^^^^1
 ;;^UTILITY(U,$J,358.3,15813,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15813,1,2,0)
 ;;=2^Application Of Short Leg Cast
 ;;^UTILITY(U,$J,358.3,15813,1,3,0)
 ;;=3^29405
 ;;^UTILITY(U,$J,358.3,15814,0)
 ;;=29425^^114^982^5^^^^1
 ;;^UTILITY(U,$J,358.3,15814,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15814,1,2,0)
 ;;=2^Application Of Short Leg Cast; Walking Or Ambulatory Type
 ;;^UTILITY(U,$J,358.3,15814,1,3,0)
 ;;=3^29425
 ;;^UTILITY(U,$J,358.3,15815,0)
 ;;=29440^^114^982^1^^^^1
 ;;^UTILITY(U,$J,358.3,15815,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15815,1,2,0)
 ;;=2^Adding Walker to Previous Cast
 ;;^UTILITY(U,$J,358.3,15815,1,3,0)
 ;;=3^29440
 ;;^UTILITY(U,$J,358.3,15816,0)
 ;;=29445^^114^982^3^^^^1
 ;;^UTILITY(U,$J,358.3,15816,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15816,1,2,0)
 ;;=2^Application Of Rigid Total Contact Leg Cast
 ;;^UTILITY(U,$J,358.3,15816,1,3,0)
 ;;=3^29445
 ;;^UTILITY(U,$J,358.3,15817,0)
 ;;=29515^^114^982^6^^^^1
 ;;^UTILITY(U,$J,358.3,15817,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15817,1,2,0)
 ;;=2^Application Of Short Leg Splint (Calf To Foot)
 ;;^UTILITY(U,$J,358.3,15817,1,3,0)
 ;;=3^29515
 ;;^UTILITY(U,$J,358.3,15818,0)
 ;;=29540^^114^982^10^^^^1
 ;;^UTILITY(U,$J,358.3,15818,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15818,1,2,0)
 ;;=2^Strapping ; Ankle
 ;;^UTILITY(U,$J,358.3,15818,1,3,0)
 ;;=3^29540
 ;;^UTILITY(U,$J,358.3,15819,0)
 ;;=29530^^114^982^11^^^^1
 ;;^UTILITY(U,$J,358.3,15819,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15819,1,2,0)
 ;;=2^Strapping; Knee
 ;;^UTILITY(U,$J,358.3,15819,1,3,0)
 ;;=3^29530
 ;;^UTILITY(U,$J,358.3,15820,0)
 ;;=29550^^114^982^12^^^^1
 ;;^UTILITY(U,$J,358.3,15820,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15820,1,2,0)
 ;;=2^Strapping; Toes
 ;;^UTILITY(U,$J,358.3,15820,1,3,0)
 ;;=3^29550
 ;;^UTILITY(U,$J,358.3,15821,0)
 ;;=29580^^114^982^13^^^^1
 ;;^UTILITY(U,$J,358.3,15821,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15821,1,2,0)
 ;;=2^Strapping; Unna Boot
 ;;^UTILITY(U,$J,358.3,15821,1,3,0)
 ;;=3^29580
 ;;^UTILITY(U,$J,358.3,15822,0)
 ;;=29581^^114^982^8^^^^1
 ;;^UTILITY(U,$J,358.3,15822,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15822,1,2,0)
 ;;=2^Multilay Venous Wound Comp System
 ;;^UTILITY(U,$J,358.3,15822,1,3,0)
 ;;=3^29581
 ;;^UTILITY(U,$J,358.3,15823,0)
 ;;=29582^^114^982^9^^^^1
 ;;^UTILITY(U,$J,358.3,15823,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15823,1,2,0)
 ;;=2^Multilay Wnd Comp Syst Thigh,Incl Ft
 ;;^UTILITY(U,$J,358.3,15823,1,3,0)
 ;;=3^29582
 ;;^UTILITY(U,$J,358.3,15824,0)
 ;;=29700^^114^983^1^^^^1
 ;;^UTILITY(U,$J,358.3,15824,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15824,1,2,0)
 ;;=2^Removal or bivalving; gauntlet, boot or body cast
 ;;^UTILITY(U,$J,358.3,15824,1,3,0)
 ;;=3^29700
 ;;^UTILITY(U,$J,358.3,15825,0)
 ;;=29730^^114^983^2^^^^1
 ;;^UTILITY(U,$J,358.3,15825,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15825,1,2,0)
 ;;=2^Windowing of cast
 ;;^UTILITY(U,$J,358.3,15825,1,3,0)
 ;;=3^29730
 ;;^UTILITY(U,$J,358.3,15826,0)
 ;;=L9900^^114^984^3^^^^1
 ;;^UTILITY(U,$J,358.3,15826,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15826,1,2,0)
 ;;=2^Orthotic & prosthetic supply, accessory, &/or service component of another HCPCS L code
 ;;^UTILITY(U,$J,358.3,15826,1,3,0)
 ;;=3^L9900
 ;;^UTILITY(U,$J,358.3,15827,0)
 ;;=A9150^^114^985^2^^^^1
 ;;^UTILITY(U,$J,358.3,15827,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15827,1,2,0)
 ;;=2^Nonprescription drug
 ;;^UTILITY(U,$J,358.3,15827,1,3,0)
 ;;=3^A9150
 ;;^UTILITY(U,$J,358.3,15828,0)
 ;;=L3332^^114^985^3^^^^1
 ;;^UTILITY(U,$J,358.3,15828,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15828,1,2,0)
 ;;=2^Lift, elevation, inside shoe, tapered, up to one-half inch
 ;;^UTILITY(U,$J,358.3,15828,1,3,0)
 ;;=3^L3332
 ;;^UTILITY(U,$J,358.3,15829,0)
 ;;=L3170^^114^985^4^^^^1
 ;;^UTILITY(U,$J,358.3,15829,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15829,1,2,0)
 ;;=2^Foot, plastic heel stabilizer
 ;;^UTILITY(U,$J,358.3,15829,1,3,0)
 ;;=3^L3170
 ;;^UTILITY(U,$J,358.3,15830,0)
 ;;=A4570^^114^985^6^^^^1
 ;;^UTILITY(U,$J,358.3,15830,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15830,1,2,0)
 ;;=2^Splint                      
 ;;^UTILITY(U,$J,358.3,15830,1,3,0)
 ;;=3^A4570
 ;;^UTILITY(U,$J,358.3,15831,0)
 ;;=L3260^^114^985^7^^^^1
 ;;^UTILITY(U,$J,358.3,15831,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15831,1,2,0)
 ;;=2^Ambulatory surgical boot, each 
 ;;^UTILITY(U,$J,358.3,15831,1,3,0)
 ;;=3^L3260
 ;;^UTILITY(U,$J,358.3,15832,0)
 ;;=A4500^^114^985^8^^^^1
 ;;^UTILITY(U,$J,358.3,15832,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15832,1,2,0)
 ;;=2^Surgical stocking below knee lenght, each
 ;;^UTILITY(U,$J,358.3,15832,1,3,0)
 ;;=3^A4500
 ;;^UTILITY(U,$J,358.3,15833,0)
 ;;=A5501^^114^985^9^^^^1
 ;;^UTILITY(U,$J,358.3,15833,1,0)
 ;;=^358.31IA^3^2
 ;;^UTILITY(U,$J,358.3,15833,1,2,0)
 ;;=2^Diabetic Shoes, Custom Fit
 ;;^UTILITY(U,$J,358.3,15833,1,3,0)
 ;;=3^A5501
 ;;^UTILITY(U,$J,358.3,15834,0)
 ;;=28190^^114^986^1^^^^1
 ;;^UTILITY(U,$J,358.3,15834,1,0)
 ;;=^358.31IA^3^2
