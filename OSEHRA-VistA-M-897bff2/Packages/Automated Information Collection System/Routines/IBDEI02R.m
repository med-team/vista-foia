IBDEI02R ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,3199,1,4,0)
 ;;=4^428.42
 ;;^UTILITY(U,$J,358.3,3199,1,5,0)
 ;;=5^Heart Failure,Systolic&Diastolic,Chronic
 ;;^UTILITY(U,$J,358.3,3199,2)
 ;;=^328501
 ;;^UTILITY(U,$J,358.3,3200,0)
 ;;=428.43^^40^248^53
 ;;^UTILITY(U,$J,358.3,3200,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3200,1,4,0)
 ;;=4^428.43
 ;;^UTILITY(U,$J,358.3,3200,1,5,0)
 ;;=5^Heart Failure,Systolic&Diastolic
 ;;^UTILITY(U,$J,358.3,3200,2)
 ;;=^328502
 ;;^UTILITY(U,$J,358.3,3201,0)
 ;;=396.3^^40^248^12
 ;;^UTILITY(U,$J,358.3,3201,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3201,1,4,0)
 ;;=4^396.3
 ;;^UTILITY(U,$J,358.3,3201,1,5,0)
 ;;=5^Aortic and Mitral Insufficiency
 ;;^UTILITY(U,$J,358.3,3201,2)
 ;;=Aortic and Mitral Insufficiency^269583
 ;;^UTILITY(U,$J,358.3,3202,0)
 ;;=429.9^^40^248^30
 ;;^UTILITY(U,$J,358.3,3202,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3202,1,4,0)
 ;;=4^429.9
 ;;^UTILITY(U,$J,358.3,3202,1,5,0)
 ;;=5^Diastolic Dysfunction
 ;;^UTILITY(U,$J,358.3,3202,2)
 ;;=^54741
 ;;^UTILITY(U,$J,358.3,3203,0)
 ;;=453.79^^40^248^29
 ;;^UTILITY(U,$J,358.3,3203,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3203,1,4,0)
 ;;=4^453.79
 ;;^UTILITY(U,$J,358.3,3203,1,5,0)
 ;;=5^Chr Venous Emblsm Oth Spec Veins
 ;;^UTILITY(U,$J,358.3,3203,2)
 ;;=^338251
 ;;^UTILITY(U,$J,358.3,3204,0)
 ;;=453.89^^40^248^1
 ;;^UTILITY(U,$J,358.3,3204,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3204,1,4,0)
 ;;=4^453.89
 ;;^UTILITY(U,$J,358.3,3204,1,5,0)
 ;;=5^AC Venous Emblsm Oth Spec Veins
 ;;^UTILITY(U,$J,358.3,3204,2)
 ;;=^338259
 ;;^UTILITY(U,$J,358.3,3205,0)
 ;;=454.0^^40^248^83
 ;;^UTILITY(U,$J,358.3,3205,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3205,1,4,0)
 ;;=4^454.0
 ;;^UTILITY(U,$J,358.3,3205,1,5,0)
 ;;=5^Varicose Veins
 ;;^UTILITY(U,$J,358.3,3205,2)
 ;;=^125410
 ;;^UTILITY(U,$J,358.3,3206,0)
 ;;=454.2^^40^248^84
 ;;^UTILITY(U,$J,358.3,3206,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3206,1,4,0)
 ;;=4^454.2
 ;;^UTILITY(U,$J,358.3,3206,1,5,0)
 ;;=5^Varicose Veins w/Ulcer&Inflam
 ;;^UTILITY(U,$J,358.3,3206,2)
 ;;=^269821
 ;;^UTILITY(U,$J,358.3,3207,0)
 ;;=426.10^^40^248^3
 ;;^UTILITY(U,$J,358.3,3207,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3207,1,4,0)
 ;;=4^426.10
 ;;^UTILITY(U,$J,358.3,3207,1,5,0)
 ;;=5^AV CONDUCTION ABNORMAL
 ;;^UTILITY(U,$J,358.3,3207,2)
 ;;=^11396
 ;;^UTILITY(U,$J,358.3,3208,0)
 ;;=396.8^^40^248^2
 ;;^UTILITY(U,$J,358.3,3208,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3208,1,4,0)
 ;;=4^396.8
 ;;^UTILITY(U,$J,358.3,3208,1,5,0)
 ;;=5^AORTIC/MITRAL STENOSIS/REGURG
 ;;^UTILITY(U,$J,358.3,3208,2)
 ;;=^269584
 ;;^UTILITY(U,$J,358.3,3209,0)
 ;;=414.00^^40^248^18
 ;;^UTILITY(U,$J,358.3,3209,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3209,1,4,0)
 ;;=4^414.00
 ;;^UTILITY(U,$J,358.3,3209,1,5,0)
 ;;=5^CAD
 ;;^UTILITY(U,$J,358.3,3209,2)
 ;;=^28512
 ;;^UTILITY(U,$J,358.3,3210,0)
 ;;=425.4^^40^248^20
 ;;^UTILITY(U,$J,358.3,3210,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3210,1,4,0)
 ;;=4^425.4
 ;;^UTILITY(U,$J,358.3,3210,1,5,0)
 ;;=5^CARDIOMYOPATHY
 ;;^UTILITY(U,$J,358.3,3210,2)
 ;;=^87808
 ;;^UTILITY(U,$J,358.3,3211,0)
 ;;=410.90^^40^248^60
 ;;^UTILITY(U,$J,358.3,3211,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3211,1,4,0)
 ;;=4^410.90
 ;;^UTILITY(U,$J,358.3,3211,1,5,0)
 ;;=5^MYOCARDIAL INFARCTION
 ;;^UTILITY(U,$J,358.3,3211,2)
 ;;=^269673
 ;;^UTILITY(U,$J,358.3,3212,0)
 ;;=427.2^^40^248^65
 ;;^UTILITY(U,$J,358.3,3212,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3212,1,4,0)
 ;;=4^427.2
 ;;^UTILITY(U,$J,358.3,3212,1,5,0)
 ;;=5^PAROX ATR TACHYCARDIA
 ;;^UTILITY(U,$J,358.3,3212,2)
 ;;=^117073
 ;;^UTILITY(U,$J,358.3,3213,0)
 ;;=271.3^^40^249^11
 ;;^UTILITY(U,$J,358.3,3213,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3213,1,4,0)
 ;;=4^271.3
 ;;^UTILITY(U,$J,358.3,3213,1,5,0)
 ;;=5^Glucose Intolerance
 ;;^UTILITY(U,$J,358.3,3213,2)
 ;;=^64790
 ;;^UTILITY(U,$J,358.3,3214,0)
 ;;=611.1^^40^249^16
 ;;^UTILITY(U,$J,358.3,3214,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3214,1,4,0)
 ;;=4^611.1
 ;;^UTILITY(U,$J,358.3,3214,1,5,0)
 ;;=5^Gynecomastia
 ;;^UTILITY(U,$J,358.3,3214,2)
 ;;=^60454
 ;;^UTILITY(U,$J,358.3,3215,0)
 ;;=704.1^^40^249^17
 ;;^UTILITY(U,$J,358.3,3215,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3215,1,4,0)
 ;;=4^704.1
 ;;^UTILITY(U,$J,358.3,3215,1,5,0)
 ;;=5^Hirsutism
 ;;^UTILITY(U,$J,358.3,3215,2)
 ;;=^57407
 ;;^UTILITY(U,$J,358.3,3216,0)
 ;;=251.2^^40^249^28
 ;;^UTILITY(U,$J,358.3,3216,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3216,1,4,0)
 ;;=4^251.2
 ;;^UTILITY(U,$J,358.3,3216,1,5,0)
 ;;=5^Hypoglycemia Nos
 ;;^UTILITY(U,$J,358.3,3216,2)
 ;;=^60580
 ;;^UTILITY(U,$J,358.3,3217,0)
 ;;=257.2^^40^249^29
 ;;^UTILITY(U,$J,358.3,3217,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3217,1,4,0)
 ;;=4^257.2
 ;;^UTILITY(U,$J,358.3,3217,1,5,0)
 ;;=5^Hypogonadism,Male
 ;;^UTILITY(U,$J,358.3,3217,2)
 ;;=^88213
 ;;^UTILITY(U,$J,358.3,3218,0)
 ;;=253.2^^40^249^32
 ;;^UTILITY(U,$J,358.3,3218,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3218,1,4,0)
 ;;=4^253.2
 ;;^UTILITY(U,$J,358.3,3218,1,5,0)
 ;;=5^Hypopituitarism
 ;;^UTILITY(U,$J,358.3,3218,2)
 ;;=^60686
 ;;^UTILITY(U,$J,358.3,3219,0)
 ;;=733.00^^40^249^41
 ;;^UTILITY(U,$J,358.3,3219,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3219,1,4,0)
 ;;=4^733.00
 ;;^UTILITY(U,$J,358.3,3219,1,5,0)
 ;;=5^Osteoporosis Nos
 ;;^UTILITY(U,$J,358.3,3219,2)
 ;;=^87159
 ;;^UTILITY(U,$J,358.3,3220,0)
 ;;=278.00^^40^249^38
 ;;^UTILITY(U,$J,358.3,3220,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3220,1,4,0)
 ;;=4^278.00
 ;;^UTILITY(U,$J,358.3,3220,1,5,0)
 ;;=5^Obesity
 ;;^UTILITY(U,$J,358.3,3220,2)
 ;;=^84823
 ;;^UTILITY(U,$J,358.3,3221,0)
 ;;=278.01^^40^249^37
 ;;^UTILITY(U,$J,358.3,3221,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3221,1,4,0)
 ;;=4^278.01
 ;;^UTILITY(U,$J,358.3,3221,1,5,0)
 ;;=5^Morbid Obesity
 ;;^UTILITY(U,$J,358.3,3221,2)
 ;;=^84844
 ;;^UTILITY(U,$J,358.3,3222,0)
 ;;=250.80^^40^249^9
 ;;^UTILITY(U,$J,358.3,3222,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3222,1,4,0)
 ;;=4^250.80
 ;;^UTILITY(U,$J,358.3,3222,1,5,0)
 ;;=5^DM Type II with LE Ulcer
 ;;^UTILITY(U,$J,358.3,3222,2)
 ;;=DM Type II with LE Ulcer^267846^707.10
 ;;^UTILITY(U,$J,358.3,3223,0)
 ;;=250.00^^40^249^4
 ;;^UTILITY(U,$J,358.3,3223,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3223,1,4,0)
 ;;=4^250.00
 ;;^UTILITY(U,$J,358.3,3223,1,5,0)
 ;;=5^DM Type II Dm W/O Complications
 ;;^UTILITY(U,$J,358.3,3223,2)
 ;;=^33605
 ;;^UTILITY(U,$J,358.3,3224,0)
 ;;=250.40^^40^249^5
 ;;^UTILITY(U,$J,358.3,3224,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3224,1,4,0)
 ;;=4^250.40
 ;;^UTILITY(U,$J,358.3,3224,1,5,0)
 ;;=5^DM Type II Dm with Nephropathy
 ;;^UTILITY(U,$J,358.3,3224,2)
 ;;=^267837^583.81
 ;;^UTILITY(U,$J,358.3,3225,0)
 ;;=250.50^^40^249^8
 ;;^UTILITY(U,$J,358.3,3225,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3225,1,4,0)
 ;;=4^250.50
 ;;^UTILITY(U,$J,358.3,3225,1,5,0)
 ;;=5^DM Type II w/ PDR
 ;;^UTILITY(U,$J,358.3,3225,2)
 ;;=DM Type II w/ PDR^267839^362.02
 ;;^UTILITY(U,$J,358.3,3226,0)
 ;;=250.60^^40^249^6
 ;;^UTILITY(U,$J,358.3,3226,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3226,1,4,0)
 ;;=4^250.60
 ;;^UTILITY(U,$J,358.3,3226,1,5,0)
 ;;=5^DM Type II Dm with Neuropathy
 ;;^UTILITY(U,$J,358.3,3226,2)
 ;;=^267841^357.2
 ;;^UTILITY(U,$J,358.3,3227,0)
 ;;=250.70^^40^249^7
 ;;^UTILITY(U,$J,358.3,3227,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3227,1,4,0)
 ;;=4^250.70
 ;;^UTILITY(U,$J,358.3,3227,1,5,0)
 ;;=5^DM Type II Dm with Peripheral Vasc Dis
 ;;^UTILITY(U,$J,358.3,3227,2)
 ;;=^267843^443.81
 ;;^UTILITY(U,$J,358.3,3228,0)
 ;;=250.01^^40^249^3
 ;;^UTILITY(U,$J,358.3,3228,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3228,1,4,0)
 ;;=4^250.01
 ;;^UTILITY(U,$J,358.3,3228,1,5,0)
 ;;=5^DM Type I DM W/O Complications
 ;;^UTILITY(U,$J,358.3,3228,2)
 ;;=^33586
 ;;^UTILITY(U,$J,358.3,3229,0)
 ;;=272.0^^40^249^21
 ;;^UTILITY(U,$J,358.3,3229,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3229,1,4,0)
 ;;=4^272.0
 ;;^UTILITY(U,$J,358.3,3229,1,5,0)
 ;;=5^Hypercholesterolemia, Pure
 ;;^UTILITY(U,$J,358.3,3229,2)
 ;;=Hypercholesterolemia, Pure^59973
 ;;^UTILITY(U,$J,358.3,3230,0)
 ;;=272.1^^40^249^26
 ;;^UTILITY(U,$J,358.3,3230,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3230,1,4,0)
 ;;=4^272.1
 ;;^UTILITY(U,$J,358.3,3230,1,5,0)
 ;;=5^Hypertriglyceridemia, Pure
 ;;^UTILITY(U,$J,358.3,3230,2)
 ;;=Hypertriglyceridemia, Pure^101303
 ;;^UTILITY(U,$J,358.3,3231,0)
 ;;=272.2^^40^249^23
 ;;^UTILITY(U,$J,358.3,3231,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3231,1,4,0)
 ;;=4^272.2
 ;;^UTILITY(U,$J,358.3,3231,1,5,0)
 ;;=5^Hyperlipidemia, Mixed
 ;;^UTILITY(U,$J,358.3,3231,2)
 ;;=^78424
 ;;^UTILITY(U,$J,358.3,3232,0)
 ;;=275.42^^40^249^20
 ;;^UTILITY(U,$J,358.3,3232,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3232,1,4,0)
 ;;=4^275.42
 ;;^UTILITY(U,$J,358.3,3232,1,5,0)
 ;;=5^Hypercalcemia
 ;;^UTILITY(U,$J,358.3,3232,2)
 ;;=^59932
 ;;^UTILITY(U,$J,358.3,3233,0)
 ;;=275.41^^40^249^27
 ;;^UTILITY(U,$J,358.3,3233,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3233,1,4,0)
 ;;=4^275.41
 ;;^UTILITY(U,$J,358.3,3233,1,5,0)
 ;;=5^Hypocalcemia
 ;;^UTILITY(U,$J,358.3,3233,2)
 ;;=^60542
 ;;^UTILITY(U,$J,358.3,3234,0)
 ;;=276.7^^40^249^22
 ;;^UTILITY(U,$J,358.3,3234,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3234,1,4,0)
 ;;=4^276.7
 ;;^UTILITY(U,$J,358.3,3234,1,5,0)
 ;;=5^Hyperkalemia
 ;;^UTILITY(U,$J,358.3,3234,2)
 ;;=^60042
 ;;^UTILITY(U,$J,358.3,3235,0)
 ;;=275.2^^40^249^18
 ;;^UTILITY(U,$J,358.3,3235,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3235,1,4,0)
 ;;=4^275.2
 ;;^UTILITY(U,$J,358.3,3235,1,5,0)
 ;;=5^Hyper Or Hypomagnesemia
 ;;^UTILITY(U,$J,358.3,3235,2)
 ;;=^35626
 ;;^UTILITY(U,$J,358.3,3236,0)
 ;;=276.0^^40^249^24
 ;;^UTILITY(U,$J,358.3,3236,1,0)
 ;;=^358.31IA^5^2
 ;;^UTILITY(U,$J,358.3,3236,1,4,0)
 ;;=4^276.0
 ;;^UTILITY(U,$J,358.3,3236,1,5,0)
 ;;=5^Hypernatremia
 ;;^UTILITY(U,$J,358.3,3236,2)
 ;;=^60144
 ;;^UTILITY(U,$J,358.3,3237,0)
 ;;=276.1^^40^249^30
