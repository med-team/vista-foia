IBDEI08L ; ; 20-FEB-2013
 ;;3.0;IB ENCOUNTER FORM IMP/EXP;;FEB 20, 2013
 Q:'DIFQR(358.3)  F I=1:2 S X=$T(Q+I) Q:X=""  S Y=$E($T(Q+I+1),4,999),X=$E(X,4,999) S:$A(Y)=126 I=I+1,Y=$E(Y,2,999)_$E($T(Q+I+1),5,99) S:$A(Y)=61 Y=$E(Y,2,999) X NO E  S @X=Y
Q Q
 ;;^UTILITY(U,$J,358.3,11344,1,2,0)
 ;;=2^INTRACEREBRAL HEMORRHAGE
 ;;^UTILITY(U,$J,358.3,11344,2)
 ;;=^64977
 ;;^UTILITY(U,$J,358.3,11345,0)
 ;;=340.^^94^762^16
 ;;^UTILITY(U,$J,358.3,11345,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11345,1,1,0)
 ;;=1^340.
 ;;^UTILITY(U,$J,358.3,11345,1,2,0)
 ;;=2^MULTIPLE SCLEROSIS
 ;;^UTILITY(U,$J,358.3,11345,2)
 ;;=^79761
 ;;^UTILITY(U,$J,358.3,11346,0)
 ;;=445.89^^94^762^18
 ;;^UTILITY(U,$J,358.3,11346,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11346,1,1,0)
 ;;=1^445.89
 ;;^UTILITY(U,$J,358.3,11346,1,2,0)
 ;;=2^STROKE,EMBOLIC
 ;;^UTILITY(U,$J,358.3,11346,2)
 ;;=^328517
 ;;^UTILITY(U,$J,358.3,11347,0)
 ;;=434.90^^94^762^19
 ;;^UTILITY(U,$J,358.3,11347,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11347,1,1,0)
 ;;=1^434.90
 ;;^UTILITY(U,$J,358.3,11347,1,2,0)
 ;;=2^STROKE,THROMBOTIC
 ;;^UTILITY(U,$J,358.3,11347,2)
 ;;=^295808
 ;;^UTILITY(U,$J,358.3,11348,0)
 ;;=430.^^94^762^20
 ;;^UTILITY(U,$J,358.3,11348,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11348,1,1,0)
 ;;=1^430.
 ;;^UTILITY(U,$J,358.3,11348,1,2,0)
 ;;=2^SUBARACHNOID HEMORRHAGE
 ;;^UTILITY(U,$J,358.3,11348,2)
 ;;=^114989
 ;;^UTILITY(U,$J,358.3,11349,0)
 ;;=349.9^^94^762^17
 ;;^UTILITY(U,$J,358.3,11349,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11349,1,1,0)
 ;;=1^349.9
 ;;^UTILITY(U,$J,358.3,11349,1,2,0)
 ;;=2^OTHER NERVOUS SYSTEM COND
 ;;^UTILITY(U,$J,358.3,11349,2)
 ;;=^123890
 ;;^UTILITY(U,$J,358.3,11350,0)
 ;;=294.8^^94^762^15
 ;;^UTILITY(U,$J,358.3,11350,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11350,1,1,0)
 ;;=1^294.8
 ;;^UTILITY(U,$J,358.3,11350,1,2,0)
 ;;=2^MENTAL DISOR NEC OTH DIS
 ;;^UTILITY(U,$J,358.3,11350,2)
 ;;=^331843
 ;;^UTILITY(U,$J,358.3,11351,0)
 ;;=434.01^^94^762^5
 ;;^UTILITY(U,$J,358.3,11351,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11351,1,1,0)
 ;;=1^434.01
 ;;^UTILITY(U,$J,358.3,11351,1,2,0)
 ;;=2^CRB THROMB W/ CRB INF
 ;;^UTILITY(U,$J,358.3,11351,2)
 ;;=^295736
 ;;^UTILITY(U,$J,358.3,11352,0)
 ;;=434.11^^94^762^4
 ;;^UTILITY(U,$J,358.3,11352,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11352,1,1,0)
 ;;=1^434.11
 ;;^UTILITY(U,$J,358.3,11352,1,2,0)
 ;;=2^CRB EMB W/ CRB INF
 ;;^UTILITY(U,$J,358.3,11352,2)
 ;;=^295737
 ;;^UTILITY(U,$J,358.3,11353,0)
 ;;=294.20^^94^762^6
 ;;^UTILITY(U,$J,358.3,11353,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11353,1,1,0)
 ;;=1^294.20
 ;;^UTILITY(U,$J,358.3,11353,1,2,0)
 ;;=2^DEMENTIA
 ;;^UTILITY(U,$J,358.3,11353,2)
 ;;=^340607
 ;;^UTILITY(U,$J,358.3,11354,0)
 ;;=410.90^^94^763^1
 ;;^UTILITY(U,$J,358.3,11354,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11354,1,1,0)
 ;;=1^410.90
 ;;^UTILITY(U,$J,358.3,11354,1,2,0)
 ;;=2^ACUTE MYOCARDIAL INFARCTION
 ;;^UTILITY(U,$J,358.3,11354,2)
 ;;=^269673
 ;;^UTILITY(U,$J,358.3,11355,0)
 ;;=500.^^94^763^2
 ;;^UTILITY(U,$J,358.3,11355,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11355,1,1,0)
 ;;=1^500.
 ;;^UTILITY(U,$J,358.3,11355,1,2,0)
 ;;=2^ANTHRACOSIS
 ;;^UTILITY(U,$J,358.3,11355,2)
 ;;=^8060
 ;;^UTILITY(U,$J,358.3,11356,0)
 ;;=501.^^94^763^3
 ;;^UTILITY(U,$J,358.3,11356,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11356,1,1,0)
 ;;=1^501.
 ;;^UTILITY(U,$J,358.3,11356,1,2,0)
 ;;=2^ASBESTOSIS
 ;;^UTILITY(U,$J,358.3,11356,2)
 ;;=^10704
 ;;^UTILITY(U,$J,358.3,11357,0)
 ;;=507.0^^94^763^4
 ;;^UTILITY(U,$J,358.3,11357,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11357,1,1,0)
 ;;=1^507.0
 ;;^UTILITY(U,$J,358.3,11357,1,2,0)
 ;;=2^ASPIRATION PNEUMONIA
 ;;^UTILITY(U,$J,358.3,11357,2)
 ;;=^95581
 ;;^UTILITY(U,$J,358.3,11358,0)
 ;;=493.90^^94^763^5
 ;;^UTILITY(U,$J,358.3,11358,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11358,1,1,0)
 ;;=1^493.90
 ;;^UTILITY(U,$J,358.3,11358,1,2,0)
 ;;=2^ASTHMA NOS
 ;;^UTILITY(U,$J,358.3,11358,2)
 ;;=^330091
 ;;^UTILITY(U,$J,358.3,11359,0)
 ;;=425.9^^94^763^6
 ;;^UTILITY(U,$J,358.3,11359,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11359,1,1,0)
 ;;=1^425.9
 ;;^UTILITY(U,$J,358.3,11359,1,2,0)
 ;;=2^CARDIOMYOPATHY
 ;;^UTILITY(U,$J,358.3,11359,2)
 ;;=^265123
 ;;^UTILITY(U,$J,358.3,11360,0)
 ;;=491.9^^94^763^7
 ;;^UTILITY(U,$J,358.3,11360,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11360,1,1,0)
 ;;=1^491.9
 ;;^UTILITY(U,$J,358.3,11360,1,2,0)
 ;;=2^CHRONIC BRONCHITIS NOS
 ;;^UTILITY(U,$J,358.3,11360,2)
 ;;=^24359
 ;;^UTILITY(U,$J,358.3,11361,0)
 ;;=428.9^^94^763^9
 ;;^UTILITY(U,$J,358.3,11361,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11361,1,1,0)
 ;;=1^428.9
 ;;^UTILITY(U,$J,358.3,11361,1,2,0)
 ;;=2^CONGESTIVE HEART FAILURE
 ;;^UTILITY(U,$J,358.3,11361,2)
 ;;=^54754
 ;;^UTILITY(U,$J,358.3,11362,0)
 ;;=492.8^^94^763^10
 ;;^UTILITY(U,$J,358.3,11362,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11362,1,1,0)
 ;;=1^492.8
 ;;^UTILITY(U,$J,358.3,11362,1,2,0)
 ;;=2^EMPHYSEMA NEC
 ;;^UTILITY(U,$J,358.3,11362,2)
 ;;=^87569
 ;;^UTILITY(U,$J,358.3,11363,0)
 ;;=421.9^^94^763^11
 ;;^UTILITY(U,$J,358.3,11363,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11363,1,1,0)
 ;;=1^421.9
 ;;^UTILITY(U,$J,358.3,11363,1,2,0)
 ;;=2^ENDOCARDITIS
 ;;^UTILITY(U,$J,358.3,11363,2)
 ;;=^269701
 ;;^UTILITY(U,$J,358.3,11364,0)
 ;;=422.90^^94^763^12
 ;;^UTILITY(U,$J,358.3,11364,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11364,1,1,0)
 ;;=1^422.90
 ;;^UTILITY(U,$J,358.3,11364,1,2,0)
 ;;=2^MYOCARDITIS
 ;;^UTILITY(U,$J,358.3,11364,2)
 ;;=^269702
 ;;^UTILITY(U,$J,358.3,11365,0)
 ;;=420.90^^94^763^14
 ;;^UTILITY(U,$J,358.3,11365,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11365,1,1,0)
 ;;=1^420.90
 ;;^UTILITY(U,$J,358.3,11365,1,2,0)
 ;;=2^PERICARDITIS
 ;;^UTILITY(U,$J,358.3,11365,2)
 ;;=^269692
 ;;^UTILITY(U,$J,358.3,11366,0)
 ;;=505.^^94^763^15
 ;;^UTILITY(U,$J,358.3,11366,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11366,1,1,0)
 ;;=1^505.
 ;;^UTILITY(U,$J,358.3,11366,1,2,0)
 ;;=2^PNEUMOCONIOSIS NOS
 ;;^UTILITY(U,$J,358.3,11366,2)
 ;;=^95539
 ;;^UTILITY(U,$J,358.3,11367,0)
 ;;=481.^^94^763^17
 ;;^UTILITY(U,$J,358.3,11367,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11367,1,1,0)
 ;;=1^481.
 ;;^UTILITY(U,$J,358.3,11367,1,2,0)
 ;;=2^PNEUMONIA,PNEUMOCOCCAL
 ;;^UTILITY(U,$J,358.3,11367,2)
 ;;=^95612
 ;;^UTILITY(U,$J,358.3,11368,0)
 ;;=480.0^^94^763^18
 ;;^UTILITY(U,$J,358.3,11368,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11368,1,1,0)
 ;;=1^480.0
 ;;^UTILITY(U,$J,358.3,11368,1,2,0)
 ;;=2^PNEUMONIA,VIRAL
 ;;^UTILITY(U,$J,358.3,11368,2)
 ;;=^269928
 ;;^UTILITY(U,$J,358.3,11369,0)
 ;;=486.^^94^763^16
 ;;^UTILITY(U,$J,358.3,11369,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11369,1,1,0)
 ;;=1^486.
 ;;^UTILITY(U,$J,358.3,11369,1,2,0)
 ;;=2^PNEUMONIA,ORGANISM NOS
 ;;^UTILITY(U,$J,358.3,11369,2)
 ;;=^95632
 ;;^UTILITY(U,$J,358.3,11370,0)
 ;;=506.9^^94^763^19
 ;;^UTILITY(U,$J,358.3,11370,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11370,1,1,0)
 ;;=1^506.9
 ;;^UTILITY(U,$J,358.3,11370,1,2,0)
 ;;=2^PNEUMONITIS D/T FUMES/VAPORS
 ;;^UTILITY(U,$J,358.3,11370,2)
 ;;=^123983
 ;;^UTILITY(U,$J,358.3,11371,0)
 ;;=416.0^^94^763^20
 ;;^UTILITY(U,$J,358.3,11371,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11371,1,1,0)
 ;;=1^416.0
 ;;^UTILITY(U,$J,358.3,11371,1,2,0)
 ;;=2^PRIM PULM HYPERTENSION
 ;;^UTILITY(U,$J,358.3,11371,2)
 ;;=^265310
 ;;^UTILITY(U,$J,358.3,11372,0)
 ;;=415.19^^94^763^21
 ;;^UTILITY(U,$J,358.3,11372,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11372,1,1,0)
 ;;=1^415.19
 ;;^UTILITY(U,$J,358.3,11372,1,2,0)
 ;;=2^PULM EMBOLISM & INFARCTION
 ;;^UTILITY(U,$J,358.3,11372,2)
 ;;=^303285
 ;;^UTILITY(U,$J,358.3,11373,0)
 ;;=515.^^94^763^22
 ;;^UTILITY(U,$J,358.3,11373,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11373,1,1,0)
 ;;=1^515.
 ;;^UTILITY(U,$J,358.3,11373,1,2,0)
 ;;=2^PULMONARY FIBROSIS
 ;;^UTILITY(U,$J,358.3,11373,2)
 ;;=^101072
 ;;^UTILITY(U,$J,358.3,11374,0)
 ;;=502.^^94^763^23
 ;;^UTILITY(U,$J,358.3,11374,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11374,1,1,0)
 ;;=1^502.
 ;;^UTILITY(U,$J,358.3,11374,1,2,0)
 ;;=2^SILICOSIS
 ;;^UTILITY(U,$J,358.3,11374,2)
 ;;=^110600
 ;;^UTILITY(U,$J,358.3,11375,0)
 ;;=519.9^^94^763^13
 ;;^UTILITY(U,$J,358.3,11375,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11375,1,1,0)
 ;;=1^519.9
 ;;^UTILITY(U,$J,358.3,11375,1,2,0)
 ;;=2^OTHER RESP CONDITIONS
 ;;^UTILITY(U,$J,358.3,11375,2)
 ;;=^105137
 ;;^UTILITY(U,$J,358.3,11376,0)
 ;;=428.0^^94^763^8
 ;;^UTILITY(U,$J,358.3,11376,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11376,1,1,0)
 ;;=1^428.0
 ;;^UTILITY(U,$J,358.3,11376,1,2,0)
 ;;=2^CONGEST HEART FAIL UNSPECIFIED
 ;;^UTILITY(U,$J,358.3,11376,2)
 ;;=^54758
 ;;^UTILITY(U,$J,358.3,11377,0)
 ;;=540.9^^94^764^1
 ;;^UTILITY(U,$J,358.3,11377,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11377,1,1,0)
 ;;=1^540.9
 ;;^UTILITY(U,$J,358.3,11377,1,2,0)
 ;;=2^APPENDICITIS
 ;;^UTILITY(U,$J,358.3,11377,2)
 ;;=^270195
 ;;^UTILITY(U,$J,358.3,11378,0)
 ;;=571.1^^94^764^2
 ;;^UTILITY(U,$J,358.3,11378,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11378,1,1,0)
 ;;=1^571.1
 ;;^UTILITY(U,$J,358.3,11378,1,2,0)
 ;;=2^ALCOHOLIC HEPATITIS
 ;;^UTILITY(U,$J,358.3,11378,2)
 ;;=^2597
 ;;^UTILITY(U,$J,358.3,11379,0)
 ;;=571.6^^94^764^3
 ;;^UTILITY(U,$J,358.3,11379,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11379,1,1,0)
 ;;=1^571.6
 ;;^UTILITY(U,$J,358.3,11379,1,2,0)
 ;;=2^BILIARY CIRRHOSIS
 ;;^UTILITY(U,$J,358.3,11379,2)
 ;;=^71525
 ;;^UTILITY(U,$J,358.3,11380,0)
 ;;=560.9^^94^764^4
 ;;^UTILITY(U,$J,358.3,11380,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11380,1,1,0)
 ;;=1^560.9
 ;;^UTILITY(U,$J,358.3,11380,1,2,0)
 ;;=2^BOWEL OBSTRUCTION
 ;;^UTILITY(U,$J,358.3,11380,2)
 ;;=^64849
 ;;^UTILITY(U,$J,358.3,11381,0)
 ;;=576.1^^94^764^5
 ;;^UTILITY(U,$J,358.3,11381,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11381,1,1,0)
 ;;=1^576.1
 ;;^UTILITY(U,$J,358.3,11381,1,2,0)
 ;;=2^CHOLANGITIS
 ;;^UTILITY(U,$J,358.3,11381,2)
 ;;=^23291
 ;;^UTILITY(U,$J,358.3,11382,0)
 ;;=574.70^^94^764^6
 ;;^UTILITY(U,$J,358.3,11382,1,0)
 ;;=^358.31IA^2^2
 ;;^UTILITY(U,$J,358.3,11382,1,1,0)
 ;;=1^574.70
 ;;^UTILITY(U,$J,358.3,11382,1,2,0)
 ;;=2^CHOLECYSTITIS W/CAL GB & BD
